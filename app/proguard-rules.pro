# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in G:\IDE\adt-bundle-windows-x86_64-20140702\sdk/tools/proguard/proguard-service.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}
-optimizationpasses 5
-dontusemixedcaseclassnames
-allowaccessmodification
-dontskipnonpubliclibraryclasses
-dontpreverify
-dontoptimize
-ignorewarning

-dontpreverify
-repackageclasses ''
-allowaccessmodification
-optimizations !code/simplification/arithmetic
-keepattributes *Annotation*

-keep public class * extends android.app.Activity
-keep public class * extends android.app.Application
-keep public class * extends android.app.Service
-keep public class * extends android.content.BroadcastReceiver
-keep public class * extends android.content.ContentProvider
-keep public class * extends android.app.backup.BackupAgentHelper
-keep public class * extends android.preference.Preference
-keep public class com.android.vending.licensing.ILicensingService
-keep public class android.graphics.Canvas

-keep class bolts.**               { *; }
-keep class com.bolts.**               { *; }
-keep class org.apache.harmony.awt.**               { *; }
-keep class org.apache.harmony.mics.**               { *; }

-keep class com.sun.mail.**               { *; }
-keep class javax.mail.**               { *; }
-keep class javax.activation.**

-keep class com.google.ads.**               { *; }
-keep class com.google.android.gms.**               { *; }

-keep class net.nightwhistler.htmlspanner.**               { *; }

-keep class com.android.volley.**               { *; }

-keep class com.customviews.library.**               { *; }

-keepattributes Signature
-keep class com.facebook.** { *; }

-keepnames class * implements java.io.Serializable

-keepclassmembers class * implements java.io.Serializable {
    static final long serialVersionUID;
    private static final java.io.ObjectStreamField[] serialPersistentFields;
    !static !transient <fields>;
    private void writeObject(java.io.ObjectOutputStream);
    private void readObject(java.io.ObjectInputStream);
    java.lang.Object writeReplace();
    java.lang.Object readResolve();
}

-keepclassmembers class * {
	native <methods>;
}

-keepclassmembers class * {
    public <init>(android.content.Context, android.util.AttributeSet);
	public boolean performClick();
}

-keepclassmembers class * {
    public <init>(android.content.Context, android.util.AttributeSet, int);
}

-keep public class * extends android.view.View {
    public <init>(android.content.Context);
    public <init>(android.content.Context, android.util.AttributeSet);
    public <init>(android.content.Context, android.util.AttributeSet, int);
    public void set*(...);
}

-keepclassmembers class * extends andriod.app.Activity {
public void *On*Click(android.view.View);
public void *on*Click(android.view.View);
}


-keepclassmembers enum * {
    public static **[] values();
    public static ** valueOf(java.lang.String);
}

-keep class * implements android.os.Parcelable {
  public static final android.os.Parcelable$Creator *;
}
-keepclassmembers class **.R$* {
    public static <fields>;
}