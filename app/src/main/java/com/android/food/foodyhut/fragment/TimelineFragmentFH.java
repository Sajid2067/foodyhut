package com.android.food.foodyhut.fragment;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.food.foodyhut.R;
import com.android.food.foodyhut.activity.annotation.FragmentId;
import com.android.food.foodyhut.activity.listener.OnFragmentInteractionListener;
import com.android.food.foodyhut.adapter.recyclerview.BookTicketListAdapter;
import com.android.food.foodyhut.adapter.recyclerview.TimelineListAdapterFH;
import com.android.food.foodyhut.api.data.item.bookticket.BookTicket;
import com.android.food.foodyhut.api.data.item.googlesearchdetailshistory.GoogleSearchDetailList;
import com.android.food.foodyhut.api.data.item.googlesearchdetailshistory.GoogleSearchDetailListResponseOpeningHours;
import com.android.food.foodyhut.application.AppController;
import com.android.food.foodyhut.fragment.item.FilterData;
import com.android.food.foodyhut.toolbox.ObjectRequest;
import com.android.food.foodyhut.util.AppManager;
import com.android.food.foodyhut.view.widget.ProgressBar;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import static android.view.View.GONE;

public class TimelineFragmentFH extends  BaseFragment  implements DialogInterface.OnClickListener,View.OnClickListener,Response.Listener<GoogleSearchDetailListResponseOpeningHours>, Response.ErrorListener, RecyclerView.OnItemTouchListener {
	@Override
	public void onResponse(GoogleSearchDetailListResponseOpeningHours response) {
		tripDetailsHistoryProgressbar.setVisibility(GONE);
//       if(response.getData().getTrips().getList()!=null){
//		   Log.d(TAG, "DATA");
//
//		   googleSearchDetailList = response.getData().getTrips().getList();
//		   tripDetailsHistoryListAdapter.setItems(googleSearchDetailList);
//		   		if (googleSearchDetailList != null) {
//					tripDetailsHistoryView.setVisibility(VISIBLE);
//
//			this.tripLists = googleSearchDetailList;
//
//			totalTripListTextView.setText(googleSearchDetailList.size()+"");
//		} else {
//			if (appManager.isNetworkAvailable()) {
//				requestTripDetailsInfo();
//			} else {
//				noInternetAlertDialog.show();
//			}
//		}
//		   tripDetailsHistoryListAdapter.notifyDataSetChanged();
//		   if (tripDetailsHistoryRecyclerView.getVisibility() == View.GONE) {
//			   tripDetailsHistoryRecyclerView.setVisibility(View.VISIBLE);
//		   }
//		   tripDetailsHistoryProgressbar.setVisibility(View.GONE);
//	   }
	}
	@Override
	public void onPause() {
		super.onPause();
	}

	@Override
	public void onResume() {
		super.onResume();
	}

	@Override
	public void onErrorResponse(VolleyError error) {
		Log.e(TAG, "error");
		tripDetailsHistoryProgressbar.setVisibility(GONE);
	//	tripDetailsHistoryView.setVisibility(GONE);
		noInternetAlertDialog.show();

		String message = "";
		try {
			Gson gson = new GsonBuilder().create();
			GoogleSearchDetailListResponseOpeningHours response = gson.fromJson(new String(error.networkResponse.data, "UTF-8"), GoogleSearchDetailListResponseOpeningHours.class);
			int i = 0;
//			for (String m : response.getError().getMessages()) {
//				if (i > 0) {
//					message += "\n";
//				}
//				message += m;
//			}
		} catch (Exception e) {
			message = "Something went wrong.Please Check your Internet Connection";
			//Log.e(TAG, e.getMessage());
		}

	}

	@Override
	public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
		return false;
	}

	@Override
	public void onTouchEvent(RecyclerView rv, MotionEvent e) {

	}

	@Override
	public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

	}

	String label;

	@FragmentId
	static final String PREVIOUS_FRAGMENT_ID_TAG = "previousFragmentId";
	static final String PARCELABLE_FRAGMENT_ITEM_ID_TAG = "parcelableFragmentItem";
	private static final String TAG = TimelineFragmentFH.class.getSimpleName();

	private RecyclerView tripDetailsHistoryRecyclerView;
	private ProgressBar tripDetailsHistoryProgressbar;
	private TimelineListAdapterFH tripDetailsHistoryListAdapter;
	private List<GoogleSearchDetailList> googleSearchDetailList;
	protected View rootView;
	private ObjectRequest<GoogleSearchDetailListResponseOpeningHours> upcomingTripsMessageObjectRequest;
	private static final SimpleDateFormat monthFormat = new SimpleDateFormat("MMM", Locale.US);
	private static final SimpleDateFormat dayFormat = new SimpleDateFormat("EEE", Locale.US);
	private static final String dateFormat = "%s %s %s %d";

	private AlertDialog errorAlertDialog;
	private ObjectRequest<BookTicket> bookTicketObjectRequest;
	private BookTicketListAdapter bookTicketListAdapter;
	private List<GoogleSearchDetailList> tripLists;
//	private View operationSortButton;
//	private View departureSortButton;
 //  	private View tripDetailsHistoryView;
	Tracker mTracker;

//	private ImageView operationSortButtonImageView;
//	private ImageView departureSortButtonImageView;
	//private TextView operationSortButtonTextView;
	private TextView departureSortButtonTextView;
	private TextView totalTripListTextView;
	private GestureDetector mGestureDetector;
//	private SortTripDetailsHistoryButtonUIUpdate sortTripDetailsHistoryButtonUIUpdate;
	private Calendar calendar;
	private LinearLayoutManager linearLayoutManager;
	private AppController appController = AppController.getInstance();

	private AlertDialog noInternetAlertDialog;
	private AppManager appManager;
	private FilterData filterData;
	private TextView noResultTextView;
	private String api;
	private boolean listUpdated = false;

	public static TimelineFragmentFH newInstance(ArrayList<Integer> previousFragmentIds, Parcelable parcelable) {
		Log.d(TAG, "newInstance(@FragmentId int previousFragmentId, Parcelable parcelable)");
		Log.i(TAG, "previousFragmentIds - " + previousFragmentIds.toString());
		if (parcelable != null) {
			Log.i(TAG, "parcelable - " + parcelable.toString());
		}
		TimelineFragmentFH upcomingTripsFragment = new TimelineFragmentFH();
		Bundle bundle = new Bundle();
		bundle.putIntegerArrayList(PREVIOUS_FRAGMENT_ID_TAG, previousFragmentIds);
		bundle.putParcelable(PARCELABLE_FRAGMENT_ITEM_ID_TAG, parcelable);
		upcomingTripsFragment.setArguments(bundle);
		return upcomingTripsFragment;
	}
	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		appManager = new AppManager(getActivity());
		if(getArguments()!=null){
			api=getArguments().getString("api");
		}
		AppController application = (AppController) getActivity().getApplication();
		mTracker = application.getDefaultTracker();
		mTracker.setScreenName("Terms");
		// Send a screen view.
		mTracker.send(new HitBuilders.ScreenViewBuilder().build());
	}


	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

        if(appManager.isNetworkAvailable()) {
	      requestTripDetailsInfo();
        }else {
	     noInternetAlertDialog.show();
			tripDetailsHistoryProgressbar.setVisibility(GONE);
        }
	}


	public TimelineFragmentFH() {
	}


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		if (rootView == null) {
			rootView = inflater.inflate(R.layout.fragment_search_result_container_fh, container, false);
		}
		initializeViewComponents();
		initializeViewComponentListeners();
		createAlertDialog();
		return rootView;
	}



	protected void initializeViewComponents(){
		//sortTripDetailsHistoryButtonUIUpdate = new SortTripDetailsHistoryButtonUIUpdate(getActivity());
	//	operationSortButton = rootView.findViewById(R.id.operation_sort_button);
	//	departureSortButton = rootView.findViewById(R.id.departure_sort_button);
		noResultTextView = (TextView)rootView.findViewById(R.id.no_result_text_view);
	//	operationSortButtonImageView =(ImageView)rootView. findViewById(R.id.operation_sort_button_image_view);
	//	sortTripDetailsHistoryButtonUIUpdate.setOperationSortButtonImageView(operationSortButtonImageView);
	//	departureSortButtonImageView =(ImageView)rootView.  findViewById(R.id.departure_sort_button_image_view);
		//sortTripDetailsHistoryButtonUIUpdate.setDepartureSortButtonImageView(departureSortButtonImageView);
		tripDetailsHistoryRecyclerView = (RecyclerView) rootView.findViewById(R.id.book_ticket_recycler_view);

		totalTripListTextView = (TextView) rootView.findViewById(R.id.total_trip_list_text_view);
		tripDetailsHistoryProgressbar = (ProgressBar) rootView.findViewById(R.id.book_ticket_progress_bar);
		tripDetailsHistoryRecyclerView.setHasFixedSize(true);
		linearLayoutManager = new LinearLayoutManager(getContext());
		tripDetailsHistoryRecyclerView.setLayoutManager(linearLayoutManager);
		GoogleSearchDetailList th = new GoogleSearchDetailList();
	//	th.setBooking_date("test");

		googleSearchDetailList = new ArrayList<>();
		//googleSearchDetailList.add(th);
	//	googleSearchDetailList.add(th);

		tripDetailsHistoryProgressbar.setVisibility(GONE);

		tripDetailsHistoryListAdapter = new TimelineListAdapterFH(getActivity(), googleSearchDetailList,noResultTextView);
		tripDetailsHistoryRecyclerView.setAdapter(tripDetailsHistoryListAdapter);

		//googleSearchDetailList = response.getData().getTrips().getList();
		tripDetailsHistoryListAdapter.setItems(googleSearchDetailList);
//		if (googleSearchDetailList != null) {
//			//tripDetailsHistoryView.setVisibility(VISIBLE);
//
//			this.tripLists = googleSearchDetailList;
//
//			totalTripListTextView.setText(googleSearchDetailList.size()+"");
//		} else {
//			if (appManager.isNetworkAvailable()) {
//				requestTripDetailsInfo();
//			} else {
//				noInternetAlertDialog.show();
//			}
//		}
//		tripDetailsHistoryListAdapter.notifyDataSetChanged();
//		if (tripDetailsHistoryRecyclerView.getVisibility() == View.GONE) {
//			tripDetailsHistoryRecyclerView.setVisibility(View.VISIBLE);
//		}
//		tripDetailsHistoryProgressbar.setVisibility(View.GONE);
		//operationSortButtonTextView =(TextView)rootView. findViewById(R.id.operation_sort_button_text_view);
	//	sortTripDetailsHistoryButtonUIUpdate.setOperationSortButtonTextView(operationSortButtonTextView);
	//	departureSortButtonTextView = (TextView)rootView.findViewById(R.id.departure_sort_button_text_view);
		//sortTripDetailsHistoryButtonUIUpdate.setDepartureSortButtonTextView(departureSortButtonTextView);

	//	tripDetailsHistoryView = rootView.findViewById(R.id.book_ticket_view);

	}

	@Override
	void initializeEditTextComponents() {

	}

	@Override
	void initializeButtonComponents() {

	}

	@Override
	void initializeTextViewComponents() {

	}

	@Override
	void initializeOtherViewComponents() {

	}

	@Override
	void initializeOnclickListener() {

	}

	@Override
	void removeOnclickListener() {

	}


	private void initializeViewComponentListeners(){

		mGestureDetector = new GestureDetector(getActivity(), new GestureDetector.SimpleOnGestureListener() {

			@Override
			public boolean onSingleTapUp(MotionEvent e) {
				return true;
			}
		});

		mGestureDetector.setIsLongpressEnabled(true);

		tripDetailsHistoryRecyclerView.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
			@Override
			public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
				View child = tripDetailsHistoryRecyclerView.findChildViewUnder(e.getX(), e.getY());


				if (child != null && mGestureDetector.onTouchEvent(e)) {
					int position = tripDetailsHistoryRecyclerView.getChildAdapterPosition(child);
					//Trips tripsItem = tempTrips.get(position);
					Log.d(TAG, "SEARCH_RESULT_CONTAINER_FRAGMENT_ID_FH:" );

					previousFragmentIds.add(OnFragmentInteractionListener.SEARCH_RESULT_CONTAINER_FRAGMENT_ID_FH);

					onFragmentChangeCallListener.fragmentChange(OnFragmentInteractionListener.MAP_FRAGMENT_ID,filterData, previousFragmentIds);
					return true;

				}
				return false;
			}

			@Override
			public void onTouchEvent(RecyclerView rv, MotionEvent e) {

			}

			@Override
			public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

			}
		});

	}

	@Override
	public void onClick(View v) {

		switch (v.getId()) {
			case R.id.operation_sort_button:
				//sortTripDetailsHistoryButtonUIUpdate.sortCase(OPERATOR_SORT);
				//int operator = sortTripDetailsHistoryButtonUIUpdate.getOperator();
			//	Log.e(TAG, "operation:" + operator);
			//	tripDetailsHistoryListAdapter.getFilter().filter("operator:" + operator);
			//	break;
//			case R.id.departure_sort_button:
//				sortTripDetailsHistoryButtonUIUpdate.sortCase(DEPARTURE_SORT);
//				int departure = sortTripDetailsHistoryButtonUIUpdate.getDeparture();
//				Log.e(TAG, "departure:" + departure);
//				tripDetailsHistoryListAdapter.getFilter().filter("departure:" + departure);
//				break;
			}
	}

	@Override
	public void onClick(DialogInterface dialog, int which) {
		Log.d(TAG, "onClick(DialogInterface dialog, int which)");
		if (dialog.equals(noInternetAlertDialog)) {
			switch (which) {
				case Dialog.BUTTON_POSITIVE:
					Log.d(TAG, "noInternetAlertDialog Positive Button Pressed");
					if (appManager.isNetworkAvailable()) {
						noInternetAlertDialog.cancel();
						noInternetAlertDialog.dismiss();
						requestTripDetailsInfo();
					} else {
						noInternetAlertDialog.cancel();
						noInternetAlertDialog = getNoInternetAlertDialogBuilder().create();
						noInternetAlertDialog.show();
					}
					break;
				case Dialog.BUTTON_NEUTRAL:
					Log.d(TAG, "noInternetAlertDialog Neutral Button Pressed");
					noInternetAlertDialog.cancel();
					startActivityForResult(new Intent(Settings.ACTION_SETTINGS), 0);
					break;
				case Dialog.BUTTON_NEGATIVE:
					Log.d(TAG, "noInternetAlertDialog Negative Button Pressed");
					getActivity().finish();
					break;
			}
		} else if (dialog.equals(errorAlertDialog)) {
			switch (which) {
				case Dialog.BUTTON_POSITIVE:
					Log.d(TAG, "errorAlertDialog Positive Button Pressed");
					requestTripDetailsInfo();
					break;
				case Dialog.BUTTON_NEUTRAL:
					Log.d(TAG, "errorAlertDialog Neutral Button Pressed");
					break;
				case Dialog.BUTTON_NEGATIVE:
					Log.d(TAG, "errorAlertDialog Negative Button Pressed");
					errorAlertDialog.cancel();
					errorAlertDialog.dismiss();
					getActivity().finish();
					break;
			}
		}
	}

	public AlertDialog.Builder getNoInternetAlertDialogBuilder() {
		AlertDialog.Builder alertDialogBuilder;
		alertDialogBuilder = new AlertDialog.Builder(getActivity());
		alertDialogBuilder.setNeutralButton(R.string.action_settings, this);
		alertDialogBuilder.setPositiveButton(R.string.try_again, this);
		alertDialogBuilder.setNegativeButton(R.string.close_app, this);
		alertDialogBuilder.setTitle(R.string.error);
		alertDialogBuilder.setMessage(R.string.no_internet_connection);
		return alertDialogBuilder;
	}

	private void createAlertDialog() {
		Log.d(TAG, "createAlertDialog()");
		noInternetAlertDialog = getNoInternetAlertDialogBuilder().create();
		errorAlertDialog = getErrorAlertDialog().create();
	}

	private AlertDialog.Builder getErrorAlertDialog() {
		AlertDialog.Builder alertDialogBuilder;
		alertDialogBuilder = new AlertDialog.Builder(getActivity());
		alertDialogBuilder.setPositiveButton(R.string.try_again, this);
		alertDialogBuilder.setNegativeButton(R.string.close_app, this);
		alertDialogBuilder.setTitle(R.string.error);
		alertDialogBuilder.setMessage(R.string.server_connection_fail);
		return alertDialogBuilder;
	}

	private void requestTripDetailsInfo(){
//		String url =api ;
//		HashMap<String, String> params = new HashMap<>();
//		params.put(API.Parameter.ANDROID_APP_VERSION, appManager.getAppVersion());
//		params.put(API.Parameter.ANDROID_DEVICE_ID, appManager.getDeviceId());
//		GetUrlBuilder getUrlBuilder = new GetUrlBuilder(url, params);
//		upcomingTripsMessageObjectRequest = new ObjectRequest<>(API.Method.UPCOMING_TRIPS_DETAIL_API_METHOD,
//				getUrlBuilder.getQueryUrl(), null, this, this, GoogleSearchDetailListResponseOpeningHours.class);
//		appController.addToRequestQueue(upcomingTripsMessageObjectRequest);
	}


}
