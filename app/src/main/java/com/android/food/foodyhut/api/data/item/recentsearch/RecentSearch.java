package com.android.food.foodyhut.api.data.item.recentsearch;

import com.android.food.foodyhut.adapter.recyclerview.item.CuisineType;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by sajid on 11/2/2015.
 */
public class RecentSearch {

    @SerializedName("data")
    private List<CuisineType> cuisineTypes;

    public RecentSearch() {
    }

    public RecentSearch(List<CuisineType> cuisineTypes) {
        this.cuisineTypes = cuisineTypes;
    }

    public List<CuisineType> getCuisineTypes() {

        return cuisineTypes;
    }

    public void setCuisineTypes(List<CuisineType> cuisineTypes) {
        this.cuisineTypes = cuisineTypes;
    }

    @Override
    public String toString() {
        return "RecentSearch{" +
                "cuisineTypes=" + cuisineTypes +
                '}';
    }
}
