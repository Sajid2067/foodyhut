package com.android.food.foodyhut.api.data.item.user;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by sajid on 12/14/2015.
 */
public class Response {

    @SerializedName("userId")
    private String userId;
    @SerializedName("firstName")
    private String firstName;
    @SerializedName("lastName")
    private String lastName;
    @SerializedName("userEmail")
    private String userEmail;
    @SerializedName("userModbil")
    private String userMobil;
    @SerializedName("userStatus")
    private String userStatus;
    @SerializedName("userGender")
    private String userGender;

    public Response() {
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserMobil() {
        return userMobil;
    }

    public void setUserMobil(String userMobil) {
        this.userMobil = userMobil;
    }

    public String getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(String userStatus) {
        this.userStatus = userStatus;
    }

    public String getUserGender() {
        return userGender;
    }

    public void setUserGender(String userGender) {
        this.userGender = userGender;
    }
}
