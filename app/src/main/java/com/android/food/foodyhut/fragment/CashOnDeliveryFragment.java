package com.android.food.foodyhut.fragment;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.android.food.foodyhut.activity.annotation.FragmentId;
import com.android.food.foodyhut.application.AppController;
import com.android.food.foodyhut.database.data.item.CityDataItem;
import com.android.food.foodyhut.database.data.item.CodCoverageDataItem;
import com.android.food.foodyhut.database.data.source.CityDataSource;
import com.android.food.foodyhut.database.data.source.CodCoverageDataSource;
import com.android.food.foodyhut.fragment.item.CashOnDeliveryData;
import com.android.food.foodyhut.fragment.item.SeatLayoutData;
import com.android.food.foodyhut.fragment.item.SeatNumberData;
import com.android.food.foodyhut.util.AppManager;
import com.android.food.foodyhut.util.Constant;
import com.android.food.foodyhut.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sajid on 11/7/2015.
 */
public class CashOnDeliveryFragment extends BaseFragment {

    ArrayList<CodCoverageDataItem> codCoverageDataItems;
    ArrayAdapter<CodCoverageDataItem> areas;
    ArrayList<CityDataItem> cityDataItems;
    ArrayAdapter<CityDataItem> cities;
    AppManager appManager;
    private Spinner citySpinner;
    private Spinner areaSpinner;
    private EditText firstNameEditText;
    private EditText lastNameEditText;
    private EditText addressLineOneEditText;
    private EditText addressLineTwoEditText;
    private EditText landmarkEditText;
    private EditText postalCodeEditText;
    private EditText alternateContactNumberEditText;
    private Button continueBookingButton;
    private TextView totalPaymentTextView;
    private CashOnDeliveryData cashOnDeliveryData;
    private CodCoverageDataItem selectedCodCoverageDataItem;
    Tracker mTracker;
    public static CashOnDeliveryFragment newInstance(ArrayList<Integer> previousFragmentIds, Parcelable parcelable) {
        CashOnDeliveryFragment cashOnDeliveryFragment = new CashOnDeliveryFragment();
        Bundle bundle = new Bundle();
        bundle.putIntegerArrayList(PREVIOUS_FRAGMENT_ID_TAG, previousFragmentIds);
        bundle.putParcelable(PARCELABLE_FRAGMENT_ITEM_ID_TAG, parcelable);
        cashOnDeliveryFragment.setArguments(bundle);
        return cashOnDeliveryFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        appManager = new AppManager(getActivity());
        // Obtain the shared Tracker instance.
        AppController application = (AppController) getActivity().getApplication();
        mTracker = application.getDefaultTracker();
        mTracker.setScreenName("Cash on delivery");
        // Send a screen view.
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.fragment_cash_on_delivery, container, false);
        }

        if (parcelable instanceof CashOnDeliveryData) {
            cashOnDeliveryData = (CashOnDeliveryData) parcelable;
        }
        return super.onCreateView(inflater, container, savedInstanceState);
    }


    @Override
    void initializeEditTextComponents() {
        citySpinner = (Spinner) rootView.findViewById(R.id.city_spinner);
        final CityDataSource cityDataSource = new CityDataSource(getContext());
        areaSpinner = (Spinner) rootView.findViewById(R.id.area_spinner);

        final CodCoverageDataSource codCoverageDataSource = new CodCoverageDataSource(getContext());
        codCoverageDataSource.open();
        cityDataSource.open();
        CityDataItem cityDataItem = new CityDataItem();
        cityDataItem.setCityName("Dhaka");
        cityDataItems = cityDataSource.getAllCityItem(cityDataItem);
        cities = new ArrayAdapter<>(getContext(), android.R.layout.simple_dropdown_item_1line, cityDataItems.toArray(new CityDataItem[cityDataItems.size()]));
        citySpinner.setAdapter(cities);
        for (int i = 0; i < cityDataItems.size(); i++) {
            if (cityDataItems.get(i).getCityName().equals(cashOnDeliveryData.getPaymentDetailsData().getSeatLayoutData().getBookTicketData().getFromCity())) {
                citySpinner.setSelection(i);
                break;
            }
        }
        areas = new ArrayAdapter<>(getContext(), android.R.layout.simple_dropdown_item_1line, new CodCoverageDataItem[0]);
        areaSpinner.setAdapter(areas);
        citySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                CityDataItem cityDataItem = cityDataItems.get(position);
                codCoverageDataItems = codCoverageDataSource.getAllCodCoverageDataItemsByCityId(cityDataItem.getCityId());
                areas = new ArrayAdapter<>(getContext(), android.R.layout.simple_dropdown_item_1line, codCoverageDataItems.toArray(new CodCoverageDataItem[codCoverageDataItems.size()]));
                areaSpinner.setAdapter(areas);
                String payment;
                if (appManager.getDiscount()){
                    payment = getPaymentDetails(Float.parseFloat(getTicketFare()) + Float.parseFloat(calculateShohozFee()) - Constant.KEY_DISCOUNT);
                }else{
                    payment = getPaymentDetails(Float.parseFloat(getTicketFare()) + Float.parseFloat(calculateShohozFee()) - getDiscountAmount());
                }

                totalPaymentTextView.setText(payment);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        areaSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedCodCoverageDataItem = codCoverageDataItems.get(position);
                String payment;
                if (appManager.getDiscount()){
                    payment = getPaymentDetails(Float.parseFloat(getTicketFare()) + Float.parseFloat(calculateShohozFee()) + selectedCodCoverageDataItem.getCodFees() - Constant.KEY_DISCOUNT);
                }else{
                    payment = getPaymentDetails(Float.parseFloat(getTicketFare()) + Float.parseFloat(calculateShohozFee()) + selectedCodCoverageDataItem.getCodFees() - getDiscountAmount());
                }
                totalPaymentTextView.setText(payment);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                String payment;
                if (appManager.getDiscount()){
                    payment = "Total Amount Payable : ৳ " + (Float.toString(Float.parseFloat(getTicketFare()) + Float.parseFloat(calculateShohozFee()) - Constant.KEY_DISCOUNT));
                }else{
                    payment = "Total Amount Payable : ৳ " + (Float.toString(Float.parseFloat(getTicketFare()) + Float.parseFloat(calculateShohozFee()) - getDiscountAmount()));
                }

                totalPaymentTextView.setText(payment);
            }
        });

        firstNameEditText = (EditText) rootView.findViewById(R.id.first_name_edit_text);
        lastNameEditText = (EditText) rootView.findViewById(R.id.last_name_edit_text);
        String fullName = cashOnDeliveryData.getPaymentDetailsData().getFullName().get(0);
        String[] fullNameSplit = fullName.split(" ");
        for (int i = 0; i < fullNameSplit.length; i++) {
            if (i == 0) {
                firstNameEditText.setText(fullNameSplit[i]);
            } else if (fullNameSplit.length > 2 && i != (fullNameSplit.length - 1)) {
                String tempFirstName = firstNameEditText.getText().toString();
                tempFirstName += (" " + fullNameSplit[i]);
                firstNameEditText.setText(tempFirstName);
            } else {
                lastNameEditText.setText(fullNameSplit[i]);
            }
        }
        addressLineOneEditText = (EditText) rootView.findViewById(R.id.address_line_one_edit_text);
        addressLineTwoEditText = (EditText) rootView.findViewById(R.id.address_line_two_edit_text);
        landmarkEditText = (EditText) rootView.findViewById(R.id.landmark_edit_text);
        postalCodeEditText = (EditText) rootView.findViewById(R.id.postal_code_edit_text);
        alternateContactNumberEditText = (EditText) rootView.findViewById(R.id.alternate_contact_number_edit_text);

    }

    @NonNull
    private String getPaymentDetails(float f) {
        return String.format("৳ %.02f", f);
    }

    private String calculateShohozFee() {
        return String.format("%.02f", appManager.getShohozFee() * cashOnDeliveryData.getPaymentDetailsData().getSeatLayoutData().getSelectedSeat().size());
    }

    @Override
    void initializeButtonComponents() {
        continueBookingButton = (Button) rootView.findViewById(R.id.continue_booking_button);
    }

    @Override
    void initializeTextViewComponents() {
        String payment;
        totalPaymentTextView = (TextView) rootView.findViewById(R.id.total_payment_text_view);
        if(appManager.getDiscount()){
            payment = getPaymentDetails(Float.parseFloat(getTicketFare()) - Constant.KEY_DISCOUNT);
        }else{
            payment = getPaymentDetails(Float.parseFloat(getTicketFare()) - getDiscountAmount());
        }

        totalPaymentTextView.setText(payment);
    }

    private String getTicketFare() {
        SeatLayoutData seatLayoutData = cashOnDeliveryData.getPaymentDetailsData().getSeatLayoutData();
        List<Parcelable> selectedSeats = seatLayoutData.getSelectedSeat();
        String baseFareString = "0.0";
        for (int i = 0; i < selectedSeats.size(); i++) {
            SeatNumberData seatNumberData = (SeatNumberData) selectedSeats.get(i);
            baseFareString = Float.toString(Float.parseFloat(baseFareString) + seatNumberData.getSeatFare());
        }
        return baseFareString;
    }

    private float getDiscountAmount() {
        if (Constant.KEY_IS_COUPON_APPLIED) {
            return Constant.KEY_COUPON_DISCOUNT;
        } else {
            SeatLayoutData seatLayoutData = cashOnDeliveryData.getPaymentDetailsData().getSeatLayoutData();
            List<Parcelable> selectedSeats = seatLayoutData.getSelectedSeat();

            return Constant.KEY_DISCOUNT * selectedSeats.size();
        }
    }

    @Override
    void initializeOtherViewComponents() {

    }

    @Override
    void initializeOnclickListener() {
        continueBookingButton.setOnClickListener(this);
    }

    @Override
    void removeOnclickListener() {
        continueBookingButton.setOnClickListener(null);
    }

    @Override
    public void onClick(View view) {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        super.onClick(view);
        switch (view.getId()) {
            case R.id.continue_booking_button:
                if (isZeroLengthText(addressLineOneEditText) ||
                        isZeroLengthText(firstNameEditText) ||
                        isZeroLengthText(lastNameEditText) ||
                        isZeroLengthText(areaSpinner.getSelectedItem().toString()) ||
                        isZeroLengthText(citySpinner.getSelectedItem().toString())) {
                    makeAToast("Fill up the fields", Toast.LENGTH_SHORT);
                } else if (isZeroLengthText(citySpinner.getSelectedItem().toString())) {
                    makeAToast("Sorry Cash on Delivery isn't available in your city", Toast.LENGTH_SHORT);
                } else {
                    if (isZeroLengthText(alternateContactNumberEditText)) {
                        returnToPaymentDetails();
                    } else {
                        if (getMobileNumber(alternateContactNumberEditText).matches(Constant.VALID_MOBILE_REG_EXP)) {
                            returnToPaymentDetails();
                        } else {
                            makeAToast("Mobile number is not valid", Toast.LENGTH_SHORT);
                        }
                    }
                }
                break;
        }
    }

    private String getMobileNumber(EditText mobileNumberEditText) {
        return mobileNumberEditText.getText().toString().trim();
    }

    private void returnToPaymentDetails() {
        cashOnDeliveryData.setAddressLineOne(getText(addressLineOneEditText));
        cashOnDeliveryData.setAddressLineTwo(getText(addressLineTwoEditText));
        cashOnDeliveryData.setPaymentDetailsData(cashOnDeliveryData.getPaymentDetailsData());
        String alternateContactNumber = getText(alternateContactNumberEditText).length() != 0 ? getText(alternateContactNumberEditText) : "";
        cashOnDeliveryData.setAlternateContactNumber(alternateContactNumber);
        cashOnDeliveryData.setCityId(Integer.parseInt(((CityDataItem) citySpinner.getSelectedItem()).getCityId()));
        cashOnDeliveryData.setAreaId(((CodCoverageDataItem) areaSpinner.getSelectedItem()).getAreaId());
        cashOnDeliveryData.setCityName(((CityDataItem) citySpinner.getSelectedItem()).getCityName());
        cashOnDeliveryData.setAreaName(((CodCoverageDataItem) areaSpinner.getSelectedItem()).getAreaName());
        cashOnDeliveryData.setLandmark(getText(landmarkEditText));
        cashOnDeliveryData.setPostalCode(getText(postalCodeEditText));
        cashOnDeliveryData.setCashOnDeliveryFee(selectedCodCoverageDataItem.getCodFees());
        cashOnDeliveryData.setFirstName(getText(firstNameEditText));
        cashOnDeliveryData.setLastName(getText(lastNameEditText));
        super.onBackPressed();


    }

    @Override
    public void onBackPressed() {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(rootView.getWindowToken(), 0);
        @FragmentId
        int previousFragmentId = previousFragmentIds.get(previousFragmentIds.size() - 1);
        previousFragmentIds.remove(previousFragmentIds.size() - 1);
        onFragmentChangeCallListener.onBackPressed(cashOnDeliveryData.getPaymentDetailsData(), previousFragmentId, previousFragmentIds);
    }
}
