package com.android.food.foodyhut.api.data.item;

import com.google.gson.annotations.SerializedName;

/**
 * Created by sajid on 11/3/2015.
 */
public class BoardingPoint {

    @SerializedName("trip_point_id")
    private int tripPointId;
    @SerializedName("location_id")
    private int locationId;
    @SerializedName("location_name")
    private String locationName;
    @SerializedName("location_type")
    private int locationType;
    @SerializedName("location_date")
    private String locationDate;
    @SerializedName("location_time")
    private String locationTime;
    @SerializedName("location_description")
    private String locationDescription;

    public BoardingPoint() {
    }

    public BoardingPoint(int tripPointId, int locationId, String locationName, int locationType, String locationDate, String locationTime, String locationDescription) {

        this.tripPointId = tripPointId;
        this.locationId = locationId;
        this.locationName = locationName;
        this.locationType = locationType;
        this.locationDate = locationDate;
        this.locationTime = locationTime;
        this.locationDescription = locationDescription;
    }

    public int getTripPointId() {

        return tripPointId;
    }

    public void setTripPointId(int tripPointId) {
        this.tripPointId = tripPointId;
    }

    public int getLocationId() {
        return locationId;
    }

    public void setLocationId(int locationId) {
        this.locationId = locationId;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public int getLocationType() {
        return locationType;
    }

    public void setLocationType(int locationType) {
        this.locationType = locationType;
    }

    public String getLocationDate() {
        return locationDate;
    }

    public void setLocationDate(String locationDate) {
        this.locationDate = locationDate;
    }

    public String getLocationTime() {
        return locationTime;
    }

    public void setLocationTime(String locationTime) {
        this.locationTime = locationTime;
    }

    public String getLocationDescription() {
        return locationDescription;
    }

    public void setLocationDescription(String locationDescription) {
        this.locationDescription = locationDescription;
    }

    @Override
    public String toString() {
        return "BoardingPoint{" +
                "tripPointId=" + tripPointId +
                ", locationId=" + locationId +
                ", locationName='" + locationName + '\'' +
                ", locationType=" + locationType +
                ", locationDate='" + locationDate + '\'' +
                ", locationTime='" + locationTime + '\'' +
                ", locationDescription='" + locationDescription + '\'' +
                '}';
    }
}
