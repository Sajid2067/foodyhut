package com.android.food.foodyhut.view.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Movie;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.android.food.foodyhut.R;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

/**
 * Created by sajid on 11/2/2015.
 */
@SuppressWarnings("ResourceType")
public class ProgressBar extends ImageView {

    public static enum TYPE {
        FIT_CENTER, STREACH_TO_FIT, AS_IS
    }

    public ProgressBar(Context context, AttributeSet attrs,
                       int defStyle) {
        super(context, attrs, defStyle);
        initialize(attrs, context);
    }

    int[] attrsArray = new int[]{
            android.R.attr.id, // 0
            android.R.attr.background, // 1
            android.R.attr.layout_width, // 2
            android.R.attr.layout_height // 3
    };

    private void initialize(AttributeSet attrs, Context context) {

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.ProgressBar);
        int resourceId = a.getResourceId(R.styleable.ProgressBar_animatedResource, -1);
        TypedArray ta = context.obtainStyledAttributes(attrs, attrsArray);
        try {
            int layout_width = ta.getDimensionPixelSize(2, ViewGroup.LayoutParams.WRAP_CONTENT);
            int layout_height = ta.getDimensionPixelSize(3, ViewGroup.LayoutParams.WRAP_CONTENT);
            if (layout_height == ViewGroup.LayoutParams.WRAP_CONTENT & layout_width == ViewGroup.LayoutParams.WRAP_CONTENT) {
                setAnimatedGif(resourceId, TYPE.AS_IS);
            } else {
                setAnimatedGif(resourceId, TYPE.STREACH_TO_FIT);
            }
        } catch (Exception e) {
            setAnimatedGif(resourceId, TYPE.AS_IS);

        }
        a.recycle();
        ta.recycle();
    }

    public ProgressBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        initialize(attrs, context);
    }

    public ProgressBar(Context context) {
        super(context);
    }

    boolean animatedGifImage = false;
    private InputStream is = null;
    private Movie mMovie = null;
    private long mMovieStart = 0;
    private TYPE mType = TYPE.STREACH_TO_FIT;

    public void setAnimatedGif(int rawResourceId, TYPE streachType) {
        setImageBitmap(null);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        }
        mType = streachType;
        animatedGifImage = true;
        is = getContext().getResources().openRawResource(rawResourceId);
        try {
            mMovie = Movie.decodeStream(is);
        } catch (Exception e) {
            e.printStackTrace();
            byte[] array = streamToBytes(is);
            mMovie = Movie.decodeByteArray(array, 0, array.length);
        }
        p = new Paint();
    }


    @Override
    public void setImageResource(int resId) {
        animatedGifImage = false;
        super.setImageResource(resId);
    }

    @Override
    public void setImageURI(Uri uri) {
        animatedGifImage = false;
        super.setImageURI(uri);
    }

    @Override
    public void setImageDrawable(Drawable drawable) {
        animatedGifImage = false;
        super.setImageDrawable(drawable);
    }

    Paint p;
    private float mScaleH = 1f, mScaleW = 1f;
    private int mMeasuredMovieWidth;
    private int mMeasuredMovieHeight;
    private float mLeft;
    private float mTop;

    private static byte[] streamToBytes(InputStream is) {
        ByteArrayOutputStream os = new ByteArrayOutputStream(1024);
        byte[] buffer = new byte[1024];
        int len;
        try {
            while ((len = is.read(buffer)) >= 0) {
                os.write(buffer, 0, len);
            }
        } catch (java.io.IOException e) {
        }
        return os.toByteArray();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        if (mMovie != null) {
            int movieWidth = mMovie.width();
            int movieHeight = mMovie.height();
            /*
             * Calculate horizontal scaling
			 */
            int measureModeWidth = MeasureSpec.getMode(widthMeasureSpec);
            float scaleW = 1f, scaleH = 1f;
            if (measureModeWidth != MeasureSpec.UNSPECIFIED) {
                int maximumWidth = MeasureSpec.getSize(widthMeasureSpec);
                if (movieWidth > maximumWidth) {
                    scaleW = (float) movieWidth / (float) maximumWidth;
                } else {
                    scaleW = (float) maximumWidth / (float) movieWidth;
                }
            }

			/*
             * calculate vertical scaling
			 */
            int measureModeHeight = MeasureSpec.getMode(heightMeasureSpec);

            if (measureModeHeight != MeasureSpec.UNSPECIFIED) {
                int maximumHeight = MeasureSpec.getSize(heightMeasureSpec);
                if (movieHeight > maximumHeight) {
                    scaleH = (float) movieHeight / (float) maximumHeight;
                } else {
                    scaleH = (float) maximumHeight / (float) movieHeight;
                }
            }

			/*
             * calculate overall scale
			 */
            switch (mType) {
                case FIT_CENTER:
                    mScaleH = mScaleW = Math.min(scaleH, scaleW);
                    break;
                case AS_IS:
                    mScaleH = mScaleW = 1f;
                    break;
                case STREACH_TO_FIT:
                    mScaleH = scaleH;
                    mScaleW = scaleW;
                    break;
            }

            mMeasuredMovieWidth = (int) (movieWidth * mScaleW);
            mMeasuredMovieHeight = (int) (movieHeight * mScaleH);

            setMeasuredDimension(mMeasuredMovieWidth, mMeasuredMovieHeight);

        } else {
            setMeasuredDimension(getSuggestedMinimumWidth(),
                    getSuggestedMinimumHeight());
        }
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        super.onLayout(changed, l, t, r, b);
        mLeft = (getWidth() - mMeasuredMovieWidth) / 2f;
        mTop = (getHeight() - mMeasuredMovieHeight) / 2f;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (animatedGifImage) {
            long now = android.os.SystemClock.uptimeMillis();
            if (mMovieStart == 0) { // first time
                mMovieStart = now;
            }
            if (mMovie != null) {
                p.setAntiAlias(true);
                int dur = mMovie.duration();
                if (dur == 0) {
                    dur = 1000;
                }
                int relTime = (int) ((now - mMovieStart) % dur);
                mMovie.setTime(relTime);
                canvas.save(Canvas.MATRIX_SAVE_FLAG);
                canvas.scale(mScaleW, mScaleH);
                mMovie.draw(canvas, mLeft / mScaleW, mTop / mScaleH);
                canvas.restore();
                invalidate();
            }
        }

    }
}