package com.android.food.foodyhut.api.data.item.user;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class UserStatus {

    private String user_id;
    private String user_first_name;
    private String user_middle_name;
    private String user_last_name;
    private String user_email;
    private boolean isLogin;
    private String token;

    private Context context;
    private SharedPreferences mPrefs;
    public UserStatus(Context context) {

        this.context = context;
        mPrefs = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public SharedPreferences getPrefs() {
        return mPrefs;
    }

    public String getUser_first_name() {
        return mPrefs.getString("user_first_name", "");

    }

    public void setUser_first_name(String user_first_name) {
        mPrefs.edit().putString("user_first_name", user_first_name).commit();
    }

    public String getUser_middle_name() {
        return mPrefs.getString("user_middle_name", "");

    }

    public void setUser_middle_name(String user_middle_name) {
        mPrefs.edit().putString("user_middle_name", user_middle_name).commit();
    }

    public String getUser_last_name() {
        return mPrefs.getString("user_last_name", "");

    }

    public void setUser_last_name(String user_last_name) {
        mPrefs.edit().putString("user_last_name", user_last_name).commit();
    }

    public String getUser_email() {
        return user_email;
    }

    public void setUser_email(String user_email) {
        mPrefs.edit().putString("user_email", user_email).commit();
         }

    public String getUser_id() {
        return mPrefs.getString("user_id", "");
    }

    public void setUser_id( String user_id) {
        mPrefs.edit().putString("user_id", user_id).commit();
    }

//    public String getUser_full_name() {
//        return mPrefs.getString("user_full_name", "");
//
//    }
//
//    public void setUser_full_name(String user_full_name) {
//
//        mPrefs.edit().putString("user_full_name", user_full_name).commit();
//    }

    public String getUser_mobile() {
        return mPrefs.getString("user_mobile", "");

    }

    public void setUser_mobile(String user_mobile) {
      //  this.user_mobile = user_mobile;
        mPrefs.edit().putString("user_mobile", user_mobile).commit();
    }

    public boolean isLogin() {
        return mPrefs.getBoolean("isLogin", false);
    }

    public void setLogin(boolean isLogin) {

        mPrefs.edit().putBoolean("isLogin", isLogin).commit();
    }

    public String getToken() {
        return mPrefs.getString("token", "");
    }

    public void setToken(String token) {
        mPrefs.edit().putString("token", token).commit();
    }

    public UserStatus() {
    }



}
