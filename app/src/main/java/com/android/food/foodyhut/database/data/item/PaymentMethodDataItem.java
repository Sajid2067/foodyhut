package com.android.food.foodyhut.database.data.item;

/**
 * Created by tuman on 4/5/2015.
 */
public class PaymentMethodDataItem implements Item {
    private String paymentMethodName;
    private int paymentMethodHours;
    private boolean paymentMethodIsActive;

    public PaymentMethodDataItem() {
    }

    public PaymentMethodDataItem(String paymentMethodName, int paymentMethodHours, boolean paymentMethodIsActive) {
        this.paymentMethodName = paymentMethodName;
        this.paymentMethodHours = paymentMethodHours;
        this.paymentMethodIsActive = paymentMethodIsActive;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PaymentMethodDataItem)) return false;

        PaymentMethodDataItem that = (PaymentMethodDataItem) o;

        if (paymentMethodHours != that.paymentMethodHours) return false;
        if (paymentMethodIsActive != that.paymentMethodIsActive) return false;
        return paymentMethodName.equals(that.paymentMethodName);

    }

    @Override
    public int hashCode() {
        int result = paymentMethodName.hashCode();
        result = 31 * result + paymentMethodHours;
        result = 31 * result + (paymentMethodIsActive ? 1 : 0);
        return result;
    }

    public String getPaymentMethodName() {
        return paymentMethodName;
    }

    public void setPaymentMethodName(String paymentMethodName) {
        this.paymentMethodName = paymentMethodName;
    }

    public int getPaymentMethodHours() {
        return paymentMethodHours;
    }

    public void setPaymentMethodHours(int paymentMethodHours) {
        this.paymentMethodHours = paymentMethodHours;
    }

    public boolean isPaymentMethodIsActive() {
        return paymentMethodIsActive;
    }

    public void setPaymentMethodIsActive(boolean paymentMethodIsActive) {
        this.paymentMethodIsActive = paymentMethodIsActive;
    }

    @Override
    public String toString() {
        return "PaymentMethodDataItem{" +
                "paymentMethodName='" + paymentMethodName + '\'' +
                ", paymeHours=" + paymentMethodHours +
                ", paymentMethodIsActive=" + paymentMethodIsActive +
                '}';
    }
}
