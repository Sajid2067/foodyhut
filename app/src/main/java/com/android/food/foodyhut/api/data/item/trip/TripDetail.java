package com.android.food.foodyhut.api.data.item.trip;

import com.google.gson.annotations.SerializedName;

/**
 * Created by sajid on 11/3/2015.
 */
public class TripDetail {

    @SerializedName("trip_id")
    private int tripId;
    @SerializedName("trip_number")
    private String tripNumber;
    @SerializedName("bus_desc")
    private String busDescription;
    @SerializedName("route_id")
    private int routeId;
    @SerializedName("bus_id")
    private int busId;
    @SerializedName("company_id")
    private int companyId;
    @SerializedName("trip_origin_date")
    private String tripOriginDate;
    @SerializedName("trip_origin_time")
    private String tripOriginTime;
    @SerializedName("trip_status")
    private int tripStatus;
    @SerializedName("trip_description")
    private String tripDescription;
    @SerializedName("tnc_policy_id")
    private int tncPolicyId;
    @SerializedName("luggage_policy_id")
    private int luggagePolicyId;
    @SerializedName("cancellation_policy_id")
    private int cancellationPolicyId;
    @SerializedName("available_seats")
    private int availableSeats;
    @SerializedName("available_till_datetime")
    private String availableTillDatetime;
    @SerializedName("is_eid_day")
    private int isEidDay;
    @SerializedName("mobile_booking_enabled")
    private String mobileBookingEnabled;
    @SerializedName("trip_heading")
    private String tripHeading;

    public TripDetail() {
    }

    @Override
    public String toString() {
        return "TripDetail{" +
                "tripId=" + tripId +
                ", tripNumber='" + tripNumber + '\'' +
                ", busDescription='" + busDescription + '\'' +
                ", routeId=" + routeId +
                ", busId=" + busId +
                ", companyId=" + companyId +
                ", tripOriginDate='" + tripOriginDate + '\'' +
                ", tripOriginTime='" + tripOriginTime + '\'' +
                ", tripStatus=" + tripStatus +
                ", tripDescription='" + tripDescription + '\'' +
                ", tncPolicyId=" + tncPolicyId +
                ", luggagePolicyId=" + luggagePolicyId +
                ", cancellationPolicyId=" + cancellationPolicyId +
                ", availableSeats=" + availableSeats +
                ", availableTillDatetime='" + availableTillDatetime + '\'' +
                ", isEidDay=" + isEidDay +
                ", mobileBookingEnabled='" + mobileBookingEnabled + '\'' +
                ", tripHeading='" + tripHeading + '\'' +
                '}';
    }

    public int getTripId() {
        return tripId;
    }

    public void setTripId(int tripId) {
        this.tripId = tripId;
    }

    public String getTripNumber() {
        return tripNumber;
    }

    public void setTripNumber(String tripNumber) {
        this.tripNumber = tripNumber;
    }

    public String getBusDescription() {
        return busDescription;
    }

    public void setBusDescription(String busDescription) {
        this.busDescription = busDescription;
    }

    public int getRouteId() {
        return routeId;
    }

    public void setRouteId(int routeId) {
        this.routeId = routeId;
    }

    public int getBusId() {
        return busId;
    }

    public void setBusId(int busId) {
        this.busId = busId;
    }

    public int getCompanyId() {
        return companyId;
    }

    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }

    public String getTripOriginDate() {
        return tripOriginDate;
    }

    public void setTripOriginDate(String tripOriginDate) {
        this.tripOriginDate = tripOriginDate;
    }

    public String getTripOriginTime() {
        return tripOriginTime;
    }

    public void setTripOriginTime(String tripOriginTime) {
        this.tripOriginTime = tripOriginTime;
    }

    public int getTripStatus() {
        return tripStatus;
    }

    public void setTripStatus(int tripStatus) {
        this.tripStatus = tripStatus;
    }

    public String getTripDescription() {
        return tripDescription;
    }

    public void setTripDescription(String tripDescription) {
        this.tripDescription = tripDescription;
    }

    public int getTncPolicyId() {
        return tncPolicyId;
    }

    public void setTncPolicyId(int tncPolicyId) {
        this.tncPolicyId = tncPolicyId;
    }

    public int getLuggagePolicyId() {
        return luggagePolicyId;
    }

    public void setLuggagePolicyId(int luggagePolicyId) {
        this.luggagePolicyId = luggagePolicyId;
    }

    public int getCancellationPolicyId() {
        return cancellationPolicyId;
    }

    public void setCancellationPolicyId(int cancellationPolicyId) {
        this.cancellationPolicyId = cancellationPolicyId;
    }

    public int getAvailableSeats() {
        return availableSeats;
    }

    public void setAvailableSeats(int availableSeats) {
        this.availableSeats = availableSeats;
    }

    public String getAvailableTillDatetime() {
        return availableTillDatetime;
    }

    public void setAvailableTillDatetime(String availableTillDatetime) {
        this.availableTillDatetime = availableTillDatetime;
    }

    public int getIsEidDay() {
        return isEidDay;
    }

    public void setIsEidDay(int isEidDay) {
        this.isEidDay = isEidDay;
    }

    public String getMobileBookingEnabled() {
        return mobileBookingEnabled;
    }

    public void setMobileBookingEnabled(String mobileBookingEnabled) {
        this.mobileBookingEnabled = mobileBookingEnabled;
    }

    public String getTripHeading() {
        return tripHeading;
    }

    public void setTripHeading(String tripHeading) {
        this.tripHeading = tripHeading;
    }

    public TripDetail(int tripId, String tripNumber, String busDescription, int routeId, int busId, int companyId, String tripOriginDate, String tripOriginTime, int tripStatus, String tripDescription, int tncPolicyId, int luggagePolicyId, int cancellationPolicyId, int availableSeats, String availableTillDatetime, int isEidDay, String mobileBookingEnabled, String tripHeading) {
        this.tripId = tripId;
        this.tripNumber = tripNumber;
        this.busDescription = busDescription;
        this.routeId = routeId;
        this.busId = busId;
        this.companyId = companyId;
        this.tripOriginDate = tripOriginDate;
        this.tripOriginTime = tripOriginTime;
        this.tripStatus = tripStatus;
        this.tripDescription = tripDescription;
        this.tncPolicyId = tncPolicyId;
        this.luggagePolicyId = luggagePolicyId;
        this.cancellationPolicyId = cancellationPolicyId;
        this.availableSeats = availableSeats;
        this.availableTillDatetime = availableTillDatetime;
        this.isEidDay = isEidDay;
        this.mobileBookingEnabled = mobileBookingEnabled;
        this.tripHeading = tripHeading;
    }
}
