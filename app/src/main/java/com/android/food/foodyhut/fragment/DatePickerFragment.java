package com.android.food.foodyhut.fragment;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CalendarView;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.android.food.foodyhut.activity.listener.OnFragmentInteractionListener;
import com.android.food.foodyhut.application.AppController;
import com.android.food.foodyhut.fragment.item.BookTicketData;
import com.android.food.foodyhut.fragment.item.CalendarData;
import com.android.food.foodyhut.R;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Created by sajid on 10/21/2015.
 */
public class DatePickerFragment extends BaseFragment implements CalendarView.OnDateChangeListener {

    private static final String TAG = DatePickerFragment.class.getSimpleName();
    private ImageButton forwardButton;
    private CalendarView journeyDatePicker;
    private CalendarData calendarData;
    Tracker mTracker;
    private BookTicketData bookTicketData;

    private boolean oldDateSelected = false;
    private boolean todaysDateChanged = false;

    public static DatePickerFragment newInstance(Parcelable parcelable, ArrayList<Integer> previousFragmentIds) {
        Log.d(TAG, "newInstance(@FragmentId int previousFragmentId, Parcelable parcelable)");
        Log.i(TAG, "previousFragmentIds - " + previousFragmentIds.toString());
        if (parcelable != null) {
            Log.i(TAG, "parcelable - " + parcelable.toString());
        }
        DatePickerFragment seatLayoutFragment = new DatePickerFragment();
        Bundle bundle = new Bundle();
        bundle.putIntegerArrayList(PREVIOUS_FRAGMENT_ID_TAG, previousFragmentIds);
        bundle.putParcelable(PARCELABLE_FRAGMENT_ITEM_ID_TAG, parcelable);
        seatLayoutFragment.setArguments(bundle);
        return seatLayoutFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onCreate(@Nullable Bundle savedInstanceState)");
        super.onCreate(savedInstanceState);
        if (parcelable instanceof BookTicketData) {
            bookTicketData = (BookTicketData) parcelable;
        }
        // Obtain the shared Tracker instance.
        AppController application = (AppController) getActivity().getApplication();
        mTracker = application.getDefaultTracker();
        mTracker.setScreenName("DatePicker");
        // Send a screen view.
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)");
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.fragment_date_selection, container, false);
        }
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onActivityCreated(@Nullable Bundle savedInstanceState)");
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    void initializeEditTextComponents() {
        Log.d(TAG, "initializeEditTextComponents()");
    }

    @Override
    void initializeButtonComponents() {
        Log.d(TAG, "initializeButtonComponents()");
        forwardButton = (ImageButton) rootView.findViewById(R.id.forward_button);
    }

    @Override
    void initializeTextViewComponents() {
        Log.d(TAG, "initializeTextViewComponents()");
    }

    @Override
    void initializeOtherViewComponents() {
        Log.d(TAG, "initializeOtherViewComponents()");
        journeyDatePicker = (CalendarView) rootView.findViewById(R.id.journey_date_picker);
        if (bookTicketData != null && bookTicketData.getCalendarData() != null && !noDateSelected(bookTicketData.getCalendarData())) {
            CalendarData calendarData = bookTicketData.getCalendarData();
            Calendar calendar = new GregorianCalendar(calendarData.getYear(), calendarData.getMonth(), calendarData.getDay());
            journeyDatePicker.setDate(calendar.getTime().getTime());
        }
        journeyDatePicker.setShowWeekNumber(false);
    }

    private boolean noDateSelected(CalendarData calendarData) {
        return calendarData.getDay() == 0 && calendarData.getMonth() == 0 && calendarData.getYear() == 0;
    }

    @Override
    void initializeOnclickListener() {
        Log.d(TAG, "initializeOnclickListener()");
        journeyDatePicker.setOnDateChangeListener(this);
        forwardButton.setOnClickListener(this);
    }


    @Override
    void removeOnclickListener() {
        Log.d(TAG, "removeOnclickListener()");
        journeyDatePicker.setOnDateChangeListener(null);
        forwardButton.setOnClickListener(null);
    }

    @Override
    public void onClick(View view) {
        Log.d(TAG, "removeOnclickListener()");
        super.onClick(view);
        switch (view.getId()) {
            case R.id.forward_button:
                backToMainSearchFragment();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        Log.d(TAG, "onBackPressed()");
        backToMainSearchFragment();
    }

    @Override
    public void onSelectedDayChange(CalendarView view, int year, int monthOfYear, int dayOfMonth) {
        Log.d(TAG, "onSelectedDayChange(CalendarView view, int year, int monthOfYear, int dayOfMonth)");
        calendarData = new CalendarData(year, monthOfYear, dayOfMonth);
        Calendar calendar = new GregorianCalendar(year, monthOfYear, dayOfMonth, 0, 0, 0);
        Calendar todayCalendar = new GregorianCalendar();
        if (bookTicketData != null && bookTicketData.getCalendarData() != null && !noDateSelected(bookTicketData.getCalendarData())) {
            if (year == bookTicketData.getCalendarData().getYear() && monthOfYear == bookTicketData.getCalendarData().getMonth() && dayOfMonth == bookTicketData.getCalendarData().getDay()) {
                return;
            }
        }
        if (!((calendar.get(Calendar.YEAR) == todayCalendar.get(Calendar.YEAR)) && todayCalendar.get(Calendar.DAY_OF_YEAR) == calendar.get(Calendar.DAY_OF_YEAR))) {
            if ((calendar.get(Calendar.YEAR) < todayCalendar.get(Calendar.YEAR))) {
                todaysDateChanged = oldDateSelected = true;
                makeAToast("You can't Select an old date for your Trip.", Toast.LENGTH_SHORT);
            } else if (((calendar.get(Calendar.YEAR) == todayCalendar.get(Calendar.YEAR))) && todayCalendar.get(Calendar.DAY_OF_YEAR) > calendar.get(Calendar.DAY_OF_YEAR)) {
                todaysDateChanged = oldDateSelected = true;
                makeAToast("You can't Select an old date for your Trip.", Toast.LENGTH_SHORT);
            } else {
                setAndCallMainSearchFragment();
            }
        } else if (todaysDateChanged && oldDateSelected) {
            setAndCallMainSearchFragment();
        }
    }


    private void backToMainSearchFragment() {
        Log.d(TAG, "backToMainSearchFragment()");
        Calendar calendar = new GregorianCalendar();
        //calendar.setTimeInMillis(journeyDatePicker.getDate());
        calendarData = new CalendarData(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        setAndCallMainSearchFragment();
    }

    private void setAndCallMainSearchFragment() {
        Log.d(TAG, "setAndCallMainSearchFragment()");
        if (bookTicketData != null) {
            bookTicketData.setCalendarData(calendarData);
            previousFragmentIds.remove(previousFragmentIds.size() - 1);
            onFragmentChangeCallListener.fragmentChange(OnFragmentInteractionListener.LOCATION_FRAGMENT_FH, bookTicketData, previousFragmentIds);
        } else {
            previousFragmentIds.remove(previousFragmentIds.size() - 1);
            onFragmentChangeCallListener.fragmentChange(OnFragmentInteractionListener.LOCATION_FRAGMENT_FH, calendarData, previousFragmentIds);
        }
    }
}
