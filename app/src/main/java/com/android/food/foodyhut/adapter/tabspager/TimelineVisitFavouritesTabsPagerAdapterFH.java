package com.android.food.foodyhut.adapter.tabspager;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.android.food.foodyhut.adapter.recyclerview.TimelineListAdapterFH;
import com.android.food.foodyhut.api.data.item.user.UserStatus;
import com.android.food.foodyhut.fragment.FavouritesFragmentFH;
import com.android.food.foodyhut.fragment.TimelineFragmentFH;
import com.android.food.foodyhut.fragment.WillVisitFragmentFH;


public class TimelineVisitFavouritesTabsPagerAdapterFH extends FragmentPagerAdapter {

    FragmentManager fm;
    String userId;
    public TimelineVisitFavouritesTabsPagerAdapterFH(FragmentManager fm, String userId) {
        super(fm);
        this.fm=fm;
        this.userId=userId;
    }

    @Override
    public Fragment getItem(int position) {
        TimelineFragmentFH timelineFragmentFH=new TimelineFragmentFH();
        Bundle bundle = new Bundle();
        UserStatus userStatus =new UserStatus();
        switch (position){

            case 0:
              //  bundle.putString("api", API.TRIP_DETAILS_HISTORY_BASE_API_URL+"/"+userId+"/"+API.TRIP_PAST_API_TAG);
                timelineFragmentFH.setArguments(bundle);
                return timelineFragmentFH;

            case 1:
              //  bundle.putString("api", API.TRIP_DETAILS_HISTORY_BASE_API_URL+"/"+userId+"/"+API.TRIP_UPCOMING_API_TAG);
                WillVisitFragmentFH willVisitFragmentFH=new WillVisitFragmentFH();
                willVisitFragmentFH.setArguments(bundle);
                return willVisitFragmentFH;

            case 2:
             //   bundle.putString("api", API.TRIP_DETAILS_HISTORY_BASE_API_URL+"/"+userId+"/"+API.TRIP_CANCELLED_API_TAG);
                FavouritesFragmentFH favouritesFragmentFH=new FavouritesFragmentFH();
                favouritesFragmentFH.setArguments(bundle);
                return favouritesFragmentFH;


        }
        return timelineFragmentFH;
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {

        switch (position){
            case 0:
                return "Timeline";
            case 1:
                return "Will Visit";
            case 2:
                return "Favourites";

        }
        return "Timeline";
    }
}