package com.android.food.foodyhut.api.data.item.handshake.cib;

import com.google.gson.annotations.SerializedName;

/**
 * Created by sajid on 11/7/2015.
 */
public class CibData {

    @SerializedName("short_code")
    private String short_code;
    @SerializedName("name")
    private String name;
    @SerializedName("percentage")
    private float percentage;

    public CibData() {
    }

    public CibData(String short_code, String name, float percentage) {
        this.short_code = short_code;
        this.name = name;
        this.percentage = percentage;
    }

    public String getShort_code() {
        return short_code;
    }

    public void setShort_code(String short_code) {
        this.short_code = short_code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getPercentage() {
        return percentage;
    }

    public void setPercentage(float percentage) {
        this.percentage = percentage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CibData)) return false;

        CibData cibData = (CibData) o;

        if (Float.compare(cibData.getPercentage(), getPercentage()) != 0) return false;
        if (!getShort_code().equals(cibData.getShort_code())) return false;
        return getName().equals(cibData.getName());

    }

    @Override
    public int hashCode() {
        int result = getShort_code().hashCode();
        result = 31 * result + getName().hashCode();
        result = 31 * result + (getPercentage() != +0.0f ? Float.floatToIntBits(getPercentage()) : 0);
        return result;
    }

    @Override
    public String toString() {
        return "CibData{" +
                "short_code='" + short_code + '\'' +
                ", name='" + name + '\'' +
                ", percentage=" + percentage +
                '}';
    }
}
