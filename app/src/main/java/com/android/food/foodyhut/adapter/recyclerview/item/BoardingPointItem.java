package com.android.food.foodyhut.adapter.recyclerview.item;

import com.android.food.foodyhut.fragment.item.BoardingPointData;

/**
 * Created by sajid on 11/6/2015.
 */
public class BoardingPointItem implements RecyclerViewBaseItem {
    private int tripPointId;
    private int locationId;
    private String locationName;
    private int locationType;
    private String locationDate;
    private String locationTime;
    private String locationDescription;

    public BoardingPointItem() {
    }


    public BoardingPointItem(BoardingPointData boardingPointData) {
        this.tripPointId = boardingPointData.getTripPointId();
        this.locationId = boardingPointData.getLocationId();
        this.locationName = boardingPointData.getLocationName();
        this.locationType = boardingPointData.getLocationType();
        this.locationDate = boardingPointData.getLocationDate();
        this.locationTime = boardingPointData.getLocationTime();
        this.locationDescription = boardingPointData.getLocationDescription();
    }

    public BoardingPointItem(int tripPointId, int locationId, String locationName, int locationType, String locationDate, String locationTime, String locationDescription) {
        this.tripPointId = tripPointId;
        this.locationId = locationId;
        this.locationName = locationName;
        this.locationType = locationType;
        this.locationDate = locationDate;
        this.locationTime = locationTime;
        this.locationDescription = locationDescription;
    }

    public int getTripPointId() {
        return tripPointId;
    }

    public void setTripPointId(int tripPointId) {
        this.tripPointId = tripPointId;
    }

    public int getLocationId() {
        return locationId;
    }

    public void setLocationId(int locationId) {
        this.locationId = locationId;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public int getLocationType() {
        return locationType;
    }

    public void setLocationType(int locationType) {
        this.locationType = locationType;
    }

    public String getLocationDate() {
        return locationDate;
    }

    public void setLocationDate(String locationDate) {
        this.locationDate = locationDate;
    }

    public String getLocationTime() {
        return locationTime;
    }

    public void setLocationTime(String locationTime) {
        this.locationTime = locationTime;
    }

    public String getLocationDescription() {
        return locationDescription;
    }

    public void setLocationDescription(String locationDescription) {
        this.locationDescription = locationDescription;
    }

    @Override

    public String toString() {
        return "BoardingPointItem{" +
                "tripPointId=" + tripPointId +
                ", locationId=" + locationId +
                ", locationName='" + locationName + '\'' +
                ", locationType=" + locationType +
                ", locationDate='" + locationDate + '\'' +
                ", locationTime='" + locationTime + '\'' +
                ", locationDescription='" + locationDescription + '\'' +
                '}';
    }
}
