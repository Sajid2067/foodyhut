package com.android.food.foodyhut.fragment;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.android.food.foodyhut.activity.BaseActivity;
import com.android.food.foodyhut.activity.listener.OnFragmentInteractionListener;
import com.android.food.foodyhut.api.data.item.confirmticket.TicketConfirm;
import com.android.food.foodyhut.api.data.item.handshake.HandShake;
import com.android.food.foodyhut.api.data.item.user.UserStatus;
import com.android.food.foodyhut.application.AppController;
import com.android.food.foodyhut.fragment.item.BkashPaymentData;
import com.android.food.foodyhut.fragment.item.BookTicketData;
import com.android.food.foodyhut.fragment.item.CalendarData;
import com.android.food.foodyhut.fragment.item.CashOnDeliveryData;
import com.android.food.foodyhut.fragment.item.PaymentDetailsData;
import com.android.food.foodyhut.fragment.item.SSLPaymentData;
import com.android.food.foodyhut.fragment.item.SeatLayoutData;
import com.android.food.foodyhut.fragment.item.SeatNumberData;
import com.android.food.foodyhut.fragment.item.TicketSuccessItem;
import com.android.food.foodyhut.toolbox.ObjectRequest;
import com.android.food.foodyhut.toolbox.PostUrlBuilder;
import com.android.food.foodyhut.util.API;
import com.android.food.foodyhut.util.AppManager;
import com.android.food.foodyhut.util.Constant;
import com.android.food.foodyhut.R;
import com.android.food.foodyhut.util.FrequentFunction;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * Created by sajid on 12/3/2015.
 */
public class TicketDetailsFragment extends BaseFragment {

    private static final String TAG = TicketDetailsFragment.class.getSimpleName();
    Tracker mTracker;
    private TextView journeyDateTextView;
    private TextView busOperatorNameTextView;
    private TextView fullNameTextView;
    private TextView seatTextView;
    private TextView baseFareTextView;
    private TextView reportingTimeTextView;
    private TextView departureTimeTextView;
    private TextView boardingPointTextView;
    private TextView addressTextView;
    private TextView mobileNumberTextView;
    private CashOnDeliveryData cashOnDeliveryData;
    private BkashPaymentData bkashPaymentData;
    private SSLPaymentData sslPaymentData;
    private AppManager appManager;
    private TextView journeyRouteTextView;
    String returnJSON;
    private BookTicketData bookTicketData;
    private SeatLayoutData seatLayoutData;
    private PaymentDetailsData paymentDetailsData;
    private Calendar calendar;
    private Button confirmReservationButton;
    TicketSuccessItem ticketSuccessItem;
    int REQUEST_CODE_SSL = Constant.RESPONSE_CODE_SSL;
    static TicketDetailsFragment filterFragment;
    BaseActivity activity;

    public static TicketDetailsFragment newInstance(ArrayList<Integer> previousFragmentIds, Parcelable parcelable) {
        Log.d(TAG, "newInstance(@FragmentId int previousFragmentId)");
        filterFragment = new TicketDetailsFragment();
        Bundle bundle = new Bundle();
        bundle.putIntegerArrayList(PREVIOUS_FRAGMENT_ID_TAG, previousFragmentIds);
        bundle.putParcelable(PARCELABLE_FRAGMENT_ITEM_ID_TAG, parcelable);
        filterFragment.setArguments(bundle);
        return filterFragment;
    }



    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onCreate(@Nullable Bundle savedInstanceState)");
        super.onCreate(savedInstanceState);
        appManager = new AppManager(getActivity());

        if (parcelable instanceof CashOnDeliveryData) {
            cashOnDeliveryData = (CashOnDeliveryData) parcelable;
            paymentDetailsData = cashOnDeliveryData.getPaymentDetailsData();
            seatLayoutData = paymentDetailsData.getSeatLayoutData();
            bookTicketData = seatLayoutData.getBookTicketData();
        } else if (parcelable instanceof BkashPaymentData) {
            bkashPaymentData = (BkashPaymentData) parcelable;
            paymentDetailsData = bkashPaymentData.getPaymentDetailsData();
            seatLayoutData = paymentDetailsData.getSeatLayoutData();
            bookTicketData = seatLayoutData.getBookTicketData();

        } else if (parcelable instanceof SSLPaymentData) {
            sslPaymentData = (SSLPaymentData) parcelable;
            paymentDetailsData = sslPaymentData.getPaymentDetailsData();
            seatLayoutData = paymentDetailsData.getSeatLayoutData();
            bookTicketData = seatLayoutData.getBookTicketData();

        }
        if (bookTicketData != null && bookTicketData.getCalendarData() != null) {
            calendar = new GregorianCalendar(bookTicketData.getCalendarData().getYear(), bookTicketData.getCalendarData().getMonth(), bookTicketData.getCalendarData().getDay());
        }
        // Obtain the shared Tracker instance.
        AppController application = (AppController) getActivity().getApplication();
        mTracker = application.getDefaultTracker();
        mTracker.setScreenName("Ticket Details");
        // Send a screen view.
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

//        Toast.makeText(getActivity(), Float.toString(getSSLGateWayFee()), Toast.LENGTH_SHORT).show();

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)");

        if (rootView == null) {
            rootView = inflater.inflate(R.layout.fragment_ticket_details, container, false);
        }
        activity = new BaseActivity();
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    void initializeEditTextComponents() {

    }

    @Override
    void initializeButtonComponents() {
        confirmReservationButton = findViewById(R.id.confirm_reservation_button);
    }


    @Override
    void initializeTextViewComponents() {
        journeyRouteTextView = findViewById(R.id.journey_route_text_view);
        journeyRouteTextView.setText(getJourneyRoute());

        journeyDateTextView = findViewById(R.id.journey_date_text_view);
        String htmlDate = appManager.getHTMLFormatDate(bookTicketData.getCalendarData(), calendar);
        FrequentFunction.convertHtml(journeyDateTextView,htmlDate);
       // journeyDateTextView.setText(Html.fromHtml(htmlDate));

        busOperatorNameTextView = findViewById(R.id.bus_operator_name_text_view);

        busOperatorNameTextView.setText(paymentDetailsData.getSeatLayoutData().getOperatorName());

        fullNameTextView = findViewById(R.id.full_name_text_view);
        fullNameTextView.setText(paymentDetailsData.getFullName().get(0));

        seatTextView = findViewById(R.id.seat_text_view);
        seatTextView.setText(getSeats());

        baseFareTextView = findViewById(R.id.base_fare_text_view);
        String ticketFare = getPriceFormat(getTicketFare());
        String shohozFee = getPriceFormat(calculateShohozFee());
        String cashOnDeliveryFee = getPriceFormat(0.0f);
        String discountFee;
        if (appManager.getDiscount()) {
            discountFee = getPriceFormat(Constant.KEY_DISCOUNT);
        }else{

            discountFee = getPriceFormat(getDiscountAmount());
        }

        String sslGateWayFee="0";
        String totalPayment = null;
        if (cashOnDeliveryData != null) {
            cashOnDeliveryFee = Float.toString(cashOnDeliveryData.getCashOnDeliveryFee());
            totalPayment = getPriceFormat(calculateTotalFee(ticketFare, shohozFee, cashOnDeliveryFee, sslGateWayFee, discountFee));
        }
        String paymentGateWayFee = getPriceFormat(0.0f);

        if (bkashPaymentData != null) {
            paymentGateWayFee = getPriceFormat(bkashPaymentData.getbKashFee());
            totalPayment = getPriceFormat(calculateTotalFee(ticketFare, shohozFee, cashOnDeliveryFee, paymentGateWayFee, discountFee));
        }
        if (sslPaymentData != null) {
            sslGateWayFee = getPriceFormat(getSSLGateWayFee());
            totalPayment = getPriceFormat(calculateTotalFee(ticketFare, shohozFee, cashOnDeliveryFee, sslGateWayFee, discountFee));
//            Toast.makeText(getActivity(), "SSL Fee: " + sslGateWayFee + "\nTotal: " + totalPayment, Toast.LENGTH_SHORT).show();
//            Toast.makeText(getActivity(), appManager.getCardName(), Toast.LENGTH_SHORT).show();
        }
//        else{
//            totalPayment= getPriceFormat(calculateTotalFee(ticketFare, shohozFee, cashOnDeliveryFee, paymentGateWayFee, discountFee));
//        }

        totalPayment = "৳ " + totalPayment;
        baseFareTextView.setText(totalPayment);

        reportingTimeTextView = findViewById(R.id.reporting_time_text_view);
        reportingTimeTextView.setText(getReportingTime(seatLayoutData));

        departureTimeTextView = findViewById(R.id.departure_time_text_view);
        departureTimeTextView.setText(appManager.getTime(seatLayoutData.getSelectedBoardingPoint().getLocationTime()));

        boardingPointTextView = findViewById(R.id.boarding_point_text_view);
        boardingPointTextView.setText(seatLayoutData.getSelectedBoardingPoint().getLocationName());

        addressTextView = findViewById(R.id.address_text_view);
        addressTextView.setText(getAddress());

        mobileNumberTextView = findViewById(R.id.mobile_number_text_view);
        mobileNumberTextView.setText(getContactNumber());
    }

    private String getPriceFormat(float price) {
        return String.format("%.02f", price);
    }

    private String getReportingTime(SeatLayoutData seatLayoutData) {
        String subTime[] = seatLayoutData.getSelectedBoardingPoint().getLocationTime().split(":");
        CalendarData calendarData = seatLayoutData.getBookTicketData().getCalendarData();
        Calendar calendar = new GregorianCalendar(calendarData.getYear(), calendarData.getMonth(), calendarData.getDay(), Integer.parseInt(subTime[0]), Integer.parseInt(subTime[1]), Integer.parseInt(subTime[2]));
        calendar.add(Calendar.MINUTE, -20);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm a", Locale.ENGLISH);
        return simpleDateFormat.format(calendar.getTime());
    }


    @NonNull
    private String getJourneyRoute() {
        return bookTicketData.getFromCity() + " to " + bookTicketData.getToCity();
    }

    private float calculateTotalFee(String ticketFare, String shohozFee, String cashOnDeliveryFee, String paymentGateWayFee, String discountFee) {
        return Float.parseFloat(ticketFare) + Float.parseFloat(cashOnDeliveryFee) + Float.parseFloat(paymentGateWayFee) + Float.parseFloat(shohozFee) - Float.parseFloat(discountFee);
    }

    private float calculateShohozFee() {
        return (appManager.getShohozFee() * paymentDetailsData.getSeatLayoutData().getSelectedSeat().size());
    }

    private float getCashOnDeliveryFee() {
        return cashOnDeliveryData != null ? cashOnDeliveryData.getCashOnDeliveryFee() : 0.00f;

    }


    private float getPaymentGateWayFee() {
        return bkashPaymentData != null ? bkashPaymentData.getbKashFee() : 0.00f;
    }

    private float getSSLGateWayFee() {
        return appManager.getSSLFee();
    }


    private float getTicketFare() {
        SeatLayoutData seatLayoutData = paymentDetailsData.getSeatLayoutData();
        List<Parcelable> selectedSeats = seatLayoutData.getSelectedSeat();
        float baseFare = 0.0f;
        for (int i = 0; i < selectedSeats.size(); i++) {
            SeatNumberData seatNumberData = (SeatNumberData) selectedSeats.get(i);
            baseFare = baseFare + seatNumberData.getSeatFare();
        }
        return baseFare;
    }

    private float getDiscountAmount() {
        if (Constant.KEY_IS_COUPON_APPLIED) {
            return Constant.KEY_COUPON_DISCOUNT;
        } else {
            SeatLayoutData seatLayoutData = paymentDetailsData.getSeatLayoutData();
            List<Parcelable> selectedSeats = seatLayoutData.getSelectedSeat();

            return Constant.KEY_DISCOUNT * selectedSeats.size();
        }
    }

    private String getSeats() {
        String seats = "";
        for (int i = 0; i < seatLayoutData.getSelectedSeat().size(); i++) {
            SeatNumberData seatNumberData = (SeatNumberData) seatLayoutData.getSelectedSeat().get(i);
            seats += seatNumberData.getSeatNumber();
            if (i != seatLayoutData.getSelectedSeat().size() - 1) {
                seats += ", ";
            }
        }
        return seats;
    }

    @Override
    void initializeOtherViewComponents() {
        createProgressDialogView();
    }

    @Override
    void initializeOnclickListener() {
        confirmReservationButton.setOnClickListener(this);
    }

    @Override
    void removeOnclickListener() {
        confirmReservationButton.setOnClickListener(null);
    }

    private String getContactNumber() {
        String number = paymentDetailsData.getMobileNumber().trim();
        if (cashOnDeliveryData != null)
            number += cashOnDeliveryData.getAlternateContactNumber().trim().length() == 0 ? "." : ", " + cashOnDeliveryData.getAlternateContactNumber().trim() + ".";
        return number;
    }

    private String getAddress() {
        // String address = cashOnDeliveryData.getAddressLineOne().trim();
        //address += cashOnDeliveryData.getAddressLineTwo().trim().length() == 0 ? "." : " " + cashOnDeliveryData.getAddressLineTwo().trim() + ".";
        return paymentDetailsData.getSeatLayoutData().getSelectedBoardingPoint().getLocationDescription();
    }

    private void confirmReservation() {
        List<Parcelable> selectedSeats = seatLayoutData.getSelectedSeat();
        List<Integer> selectedSeatsId = getSelectedSeats(selectedSeats);
        HashMap<String, List<String>> arrayParameters = getArrayParameters(selectedSeatsId);

        HashMap<String, String> singleParameters = getSingleParameters(seatLayoutData);

        PostUrlBuilder postUrlBuilder = new PostUrlBuilder(API.BOOK_TRIP_API_URL, singleParameters, arrayParameters);
        progressBarMessageTextView.setText("Booking your Ticket...");
        progressBarDialog.show();
        Log.e(TAG, postUrlBuilder.getQueryUrl());
        ObjectRequest<TicketConfirm> ticketConfirmDataObjectRequest = new ObjectRequest<TicketConfirm>(API.Method.BOOK_TRIP_API_METHOD, postUrlBuilder.getQueryUrl(), null, new Response.Listener<TicketConfirm>() {
            @Override
            public void onResponse(TicketConfirm response) {
                Log.e(TAG, "POST CuisineTypeResponse>>>>" + response.toString());
                if (response.getData() != null) {
                    progressBarDialog.dismiss();
                    ticketSuccessItem = new TicketSuccessItem();
                    if (bkashPaymentData != null) {
                        ticketSuccessItem.setOrderTypeTag(TicketSuccessItem.TYPE_BKASH);
                    } else if (sslPaymentData != null) {
                        ticketSuccessItem.setOrderTypeTag(TicketSuccessItem.TYPE_CARD);
                    } else {
                        ticketSuccessItem.setOrderTypeTag(TicketSuccessItem.TYPE_CASH_ON_DELIVERY);
                        ticketSuccessItem.setCashOnDeliveryData(cashOnDeliveryData);

                    }
                    if (appManager.getDiscount()){
                        ticketSuccessItem.setTicketPrice(getTicketFare() + getCashOnDeliveryFee() + getPaymentGateWayFee() + getSSLGateWayFee() + calculateShohozFee() - Constant.KEY_DISCOUNT);
                    }else{
                        ticketSuccessItem.setTicketPrice(getTicketFare() + getCashOnDeliveryFee() + getPaymentGateWayFee() + getSSLGateWayFee() + calculateShohozFee() - getDiscountAmount());
                    }

                    ticketSuccessItem.setMessage(response.getData().getMessage());
                    ticketSuccessItem.setOrderId(Integer.parseInt(response.getData().getOrderId()));
                    ticketSuccessItem.setReservationReference(response.getData().getReservationReference());
                    ticketSuccessItem.setPaymentId(response.getData().getPaymentId());
                    ticketSuccessItem.setOrderValue(response.getData().getOrderValue());

                    //setting user info

                    ticketSuccessItem.setName(paymentDetailsData.getContactPersonName());
                    ticketSuccessItem.setMobileNumber(paymentDetailsData.getMobileNumber());

                    previousFragmentIds = new ArrayList<>();
                    if (ticketSuccessItem.getOrderTypeTag().equalsIgnoreCase(TicketSuccessItem.TYPE_CARD)) {
//                        mCallbacks.onSSLCallback(ticketSuccessItem.getOrderValue(), ticketSuccessItem.getOrderId());

//                        if (Constant.KEY_IS_DEVELOPMENT_BUILD){
//                            sslSDKCall(ticketSuccessItem,Constant.SSL_TESTBOX_STORE_KEY,Constant.SSL_TESTBOX_STORE_PASSWORD,"TESTBOX");
//                        }
//                        else{
//                            sslSDKCall(ticketSuccessItem,Constant.SSL_LIVE_STORE_KEY,Constant.SSL_LIVE_STORE_PASSWORD,"LIVE");
//                        }

                    } else {
                        previousFragmentIds.add(OnFragmentInteractionListener.LOCATION_FRAGMENT_FH);
                        onFragmentChangeCallListener.fragmentChange(OnFragmentInteractionListener.TICKET_BUY_SUCCESS_FRAGMENT_ID, ticketSuccessItem, previousFragmentIds);
                        //   Toast.makeText(getContext(), "Ticket was Reserved.Reservation Reference- " + response.getData().getReservationReference(), Toast.LENGTH_SHORT).show();
                    }

                } else if (response.getError() != null) {
                    progressBarDialog.dismiss();
                    if (Integer.parseInt(response.getError().getCode()) == 801) {
                        ArrayList<String> arrayList = new ArrayList<>();
                        arrayList.add(response.getError().getMessage());
                        updateAppMessage(arrayList);
                    }
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBarDialog.dismiss();
                try {
                    Gson gson = new GsonBuilder().create();
                    HandShake handShake = gson.fromJson(new String(error.networkResponse.data, "UTF-8"), HandShake.class);
                    if (handShake.getError() != null) {
                        if (handShake.getError().getCode() == 801) {
                            updateAppMessage(handShake.getError().getMessages());
                        }
                    }
                } catch (Exception e) {
                    Toast.makeText(getContext(), "Something went wrong. Ticket wasn't confirmed.", Toast.LENGTH_SHORT).show();
                }
            }
        }, TicketConfirm.class);
        AppController.getInstance().addToRequestQueue(ticketConfirmDataObjectRequest);
    }

    private void updateAppMessage(ArrayList<String> messages) {
        final String appPackageName = getActivity().getPackageName();
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setPositiveButton("Update", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                try {
                    startActivityForResult(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)), 1);
                } catch (android.content.ActivityNotFoundException e) {
                    startActivityForResult(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)), 1);
                }
            }
        });
        alertDialogBuilder.setNegativeButton("Close App", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                getActivity().finish();
            }
        });
        String alertMessage = "";
        for (String message : messages) {
            alertMessage += message;
            alertMessage += "\n";
        }
        alertDialogBuilder.setMessage(alertMessage);
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.create().show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);

//        Log.d(TAG, "onActivityResult(int requestCode, int resultCode, Intent data) " +
//                "requestCode = " + requestCode + " resultCode = " + resultCode);
//        if (REQUEST_CODE_SSL == requestCode) {
//            if (resultCode == getActivity().RESULT_OK) {
//                TransactionInfo returnInfo = (TransactionInfo) data.getExtras().getSerializable("transaction_info");
//
//                returnJSON = "{"+"\"status\":"+"\""+returnInfo.getStatus()+"\""+","+"\"sessionkey\":"+"\""+returnInfo.getSessionkey()+"\""
//                        +","+"\"tran_date\":"+"\""+returnInfo.getTranDate()+"\""+","+"\"tran_id\":"+"\""+returnInfo.getTranId()+"\""+","+"\"val_id\":"
//                        +"\""+returnInfo.getValId()+"\""+","+"\"amount\":"+"\""+returnInfo.getAmount()+"\""+","+"\"store_amount\":"+"\""+returnInfo.getStoreAmount()+"\""
//                        +","+"\"bank_tran_id\":"+"\""+returnInfo.getBankTranId()+"\""+","+"\"card_type\":"+"\""+returnInfo.getCardType()+"\""
//                        +","+"\"card_no\":"+"\""+returnInfo.getCardNo()+"\""+","+"\"card_issuer\":"+"\""+returnInfo.getCardIssuer()+"\""
//                        +","+"\"card_brand\":"+"\""+returnInfo.getCardBrand()+"\""+","+"\"card_issuer_country\":"+"\""+returnInfo.getCardIssuerCountry()+"\""
//                        +","+"\"card_issuer_country_code\":"+"\""+returnInfo.getCardIssuerCountryCode()+"\""+","+"\"currency_type\":"+"\""+returnInfo.getCurrencyType()+"\""
//                        +","+"\"currency_amount\":"+"\""+returnInfo.getCurrencyAmount()+"\""+","+"\"currency_rate\":"+"\""+returnInfo.getCurrencyRate()+"\""
//                        +","+"\"base_fair\":"+"\""+returnInfo.getBaseFair()+"\""+","+"\"value_a\":"+"\""+returnInfo.getValueA()+"\""+","+"\"value_b\":"+"\""+returnInfo.getValueB()+"\""
//                        +","+"\"value_c\":"+"\""+returnInfo.getValueC()+"\""+","+"\"value_d\":"+"\""+returnInfo.getValueD()+"\""+","+"\"risk_title\":"+"\""+returnInfo.getRiskTitle()+"\""
//                        +","+"\"risk_level\":"+"\""+returnInfo.getRiskLevel()+"\""+","+"\"APIConnect\":"+"\""+returnInfo.getAPIConnect()+"\""
//                        +","+"\"validated_on\":"+"\""+returnInfo.getValidatedOn()+"\""+","+"\"gw_version\":"+"\""+returnInfo.getGwVersion()+"\""+"}";
//
//                Log.e(TAG, returnJSON);
//                Log.d("Check Result", "Status : " + returnInfo.getStatus() + "\nSession Key : " + returnInfo.getSessionkey() + "\nAmount : " + returnInfo.getAmount() + "\nCard Number : " + returnInfo.getCardNo());
//
//                if (returnInfo.getStatus().equalsIgnoreCase("VALID")) {
//                    confirmPayment("VALID");
//                } else if (returnInfo.getStatus().equalsIgnoreCase("FAILED")) {
//                    confirmPayment("FAILED");
//                }
//
//
////                previousFragmentIds.add(OnFragmentInteractionListener.TICKET_DETAILS_FRAGMENT_ID);
////                onFragmentChangeCallListener.fragmentChange(OnFragmentInteractionListener.TICKET_BUY_SUCCESS_FRAGMENT_ID, ticketSuccessItem, previousFragmentIds);
////                                getActivity().finish();
//            } else if (resultCode == getActivity().RESULT_CANCELED) {
//                confirmPayment("FAILED");
//                Log.e("Check CuisineTypeResponse", "User Input CuisineTypeResponse");
//                if (data.getIntExtra("error_key", 0) == ErrorKeys.USER_INPUT_ERROR)
//                    Log.e("Check CuisineTypeResponse", "User Input CuisineTypeResponse");
//                else if (data.getIntExtra("error_key", 0) == ErrorKeys.CANCEL_TRANSACTION_ERROR)
//                    Log.e("Check CuisineTypeResponse", "Transaction Canceled");
//                else if (data.getIntExtra("error_key", 0) == ErrorKeys.DATA_PARSING_ERROR)
//                    Log.e("Check CuisineTypeResponse", "Data Parsing CuisineTypeResponse");
//                else if (data.getIntExtra("error_key", 0) == ErrorKeys.INTERNET_CONNECTION_ERROR)
//                    Log.e("Check CuisineTypeResponse", "Internet Not Connected");
//            }
//        }
//        getActivity().finish();
    }

    @NonNull
    private HashMap<String, String> getSingleParameters(SeatLayoutData seatLayoutData) {
        HashMap<String, String> singleParameters;
        singleParameters = new HashMap<>();

        singleParameters.put(API.Parameter.ANDROID_APP_VERSION, appManager.getAppVersion());
        singleParameters.put(API.Parameter.ANDROID_DEVICE_ID, appManager.getDeviceId());

        singleParameters.put("boarding_point_id", Integer.toString(seatLayoutData.getSelectedBoardingPoint().getTripPointId()));
        singleParameters.put("mobile_number", paymentDetailsData.getMobileNumber());
        singleParameters.put("contact_email", paymentDetailsData.getEmail());
        singleParameters.put("primary_contact_name", paymentDetailsData.getContactPersonName());
        singleParameters.put("age[]", "");
        UserStatus userStatus=new UserStatus(getActivity());
      //  Toast.makeText(getActivity(),userStatus.isLogin()+"",Toast.LENGTH_LONG).show();
        if(userStatus.isLogin()){
            singleParameters.put("user_id",userStatus.getUser_id());
        }
        if (bkashPaymentData != null) {
            singleParameters.put("payment_method", "2");
            singleParameters.put("is_cod", "0");

        } else if (cashOnDeliveryData != null) {
            singleParameters.put("payment_method", "1");
            singleParameters.put("is_cod", "1");
        } else if (sslPaymentData != null) {
            singleParameters.put("payment_method", "3");
            singleParameters.put("is_cod", "0");
            singleParameters.put("card_name", appManager.getCardName());
        }
        singleParameters.put("trip_id", seatLayoutData.getTripId());
        singleParameters.put("trip_route_id", seatLayoutData.getTripRouteId());

        if (cashOnDeliveryData != null) {
            singleParameters.put("address_1", cashOnDeliveryData.getAddressLineOne().trim());
            if (!isZeroLengthText(cashOnDeliveryData.getAddressLineTwo())) {
                singleParameters.put("address_2", cashOnDeliveryData.getAddressLineTwo().trim());
            }
            if (!isZeroLengthText(cashOnDeliveryData.getAlternateContactNumber())) {
                singleParameters.put("alternate_mobile", cashOnDeliveryData.getAlternateContactNumber().trim());
            }
            singleParameters.put("area", Integer.toString(cashOnDeliveryData.getAreaId()));
            singleParameters.put("city", Integer.toString(cashOnDeliveryData.getCityId()));
            singleParameters.put("first_name", cashOnDeliveryData.getFirstName().trim());

            singleParameters.put("last_name", cashOnDeliveryData.getLastName().trim());
            if (!isZeroLengthText(cashOnDeliveryData.getPostalCode())) {
                singleParameters.put("post_code", cashOnDeliveryData.getPostalCode().trim());
            }
            if (!isZeroLengthText(cashOnDeliveryData.getLandmark())) {
                singleParameters.put("land_mark", cashOnDeliveryData.getLandmark().trim());
            }
        }
        singleParameters.put(API.Parameter.COUPON_CODE, Constant.KEY_APPLIED_COUPON_CODE);
        Log.e(TAG, singleParameters.toString());
        return singleParameters;
    }


    private List<Integer> getSelectedSeats(List<Parcelable> selectedSeats) {
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < selectedSeats.size(); i++) {
            SeatNumberData seatNumberData = (SeatNumberData) selectedSeats.get(i);
            list.add(seatNumberData.getTicketId());
        }
        return list;
    }

    public HashMap<String, List<String>> getArrayParameters(List<Integer> selectedSeatsId) {
        HashMap<String, List<String>> arrayParameters = new HashMap<>();

//        List<String> ages = null;
//        arrayParameters.put("age",ages );

        List<String> passport = paymentDetailsData.getPassport();
        arrayParameters.put("passport_number",passport);

        List<String> contactNames = paymentDetailsData.getFullName();
        arrayParameters.put("contact_name", contactNames);

        List<String> genders = paymentDetailsData.getGender();
        arrayParameters.put("gender", genders);

        List<String> ticketIds = new ArrayList<>();
        for (int i = 0; i < selectedSeatsId.size(); i++) {
            ticketIds.add(Integer.toString(selectedSeatsId.get(i)));
        }
        arrayParameters.put("ticket_id", ticketIds);
        Log.e(TAG, arrayParameters.toString());
        return arrayParameters;
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        switch (view.getId()) {
            case R.id.confirm_reservation_button:
                confirmReservation();


                break;

        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void confirmPayment(final String sslResult) {
        progressBarMessageTextView.setText("Please wait...");
        progressBarDialog.show();
        String URL;
        if (sslResult.equalsIgnoreCase("VALID")) {
            URL = API.PAYMENT_SUCCESS_API_URL;
        } else {
            URL = API.PAYMENT_FAILURE_API_URL;
        }
        Log.i(TAG, URL);
        StringRequest confirmRequest = new StringRequest(API.Method.SSL_STATUS_API_METHOD, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progressBarDialog.dismiss();
                Log.e(TAG, ">>>>>>YES<<<<<<---:"+response);


                String responseMessage = null;
                try {

                    if(sslResult.equalsIgnoreCase("VALID")){
                        JSONObject jsonObject = new JSONObject(response);
                        JSONObject object = jsonObject.getJSONObject("data");
                        responseMessage = object.getString("message");
                        ticketSuccessItem.setMessage("<p>"+responseMessage+"</p>");
                        Toast.makeText(getActivity(), responseMessage, Toast.LENGTH_SHORT).show();
                        Log.i(TAG+"<<>>",jsonObject.toString());
                        previousFragmentIds.add(OnFragmentInteractionListener.LOCATION_FRAGMENT_FH);
                        onFragmentChangeCallListener.fragmentChange(OnFragmentInteractionListener.TICKET_BUY_SUCCESS_FRAGMENT_ID, ticketSuccessItem, previousFragmentIds);
                    }
                    else if (sslResult.equalsIgnoreCase("FAILED")){
                        JSONObject jsonObject = new JSONObject(response);
                        JSONObject object = jsonObject.getJSONObject("error");
                        JSONArray jsonArray = object.getJSONArray("messages");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            responseMessage = jsonArray.getString(i);
                            Toast.makeText(getActivity(), responseMessage, Toast.LENGTH_SHORT).show();
                        }
                        ticketSuccessItem.setMessage("<p>"+responseMessage+"</p>");

                        previousFragmentIds.add(OnFragmentInteractionListener.LOCATION_FRAGMENT_FH);
                        onFragmentChangeCallListener.fragmentChange(OnFragmentInteractionListener.TICKET_BUY_SUCCESS_FRAGMENT_ID, ticketSuccessItem, previousFragmentIds);
                    }



                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                progressBarDialog.dismiss();
            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put(API.Parameter.ANDROID_APP_VERSION, appManager.getAppVersion());
                params.put(API.Parameter.ANDROID_DEVICE_ID, appManager.getDeviceId());
                if (sslResult.equalsIgnoreCase("FAILED")) {
                    params.put("payment_id", ticketSuccessItem.getPaymentId());
                } else if (sslResult.equalsIgnoreCase("VALID")) {
                    params.put("response", returnJSON);
                }

                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<String, String>();
                String creds = String.format("%s:%s","guest","33615sua");
                String auth = "Basic " + Base64.encodeToString(creds.getBytes(), Base64.DEFAULT);
                params.put("Authorization", auth);
                return params;
            }
        };

        AppController.getInstance().addToRequestQueue(confirmRequest);
    }


}
