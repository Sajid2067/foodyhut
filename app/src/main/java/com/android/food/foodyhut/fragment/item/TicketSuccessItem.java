package com.android.food.foodyhut.fragment.item;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by sajid on 12/7/2015.
 */
public class TicketSuccessItem implements Parcelable {
    private int orderId;
    private String reservationReference;
    private String orderTypeTag;
    private String message;
    private float ticketPrice;
    private String orderValue;
    private String paymentId;
    private String name;
    private String mobileNumber;
    private CashOnDeliveryData cashOnDeliveryData;

    public static final String TYPE_CASH_ON_DELIVERY = "COD";
    public static final String TYPE_BKASH = "BKASH";
    public static final String TYPE_CARD = "CARD";

    public TicketSuccessItem() {
    }

    public TicketSuccessItem(int orderId, @TicketSuccessTag String orderTypeTag, String reservationReference, String message) {

        this.orderId = orderId;
        this.orderTypeTag = orderTypeTag;
        this.reservationReference = reservationReference;
        this.message = message;
    }


    protected TicketSuccessItem(Parcel in) {
        orderId = in.readInt();
        reservationReference = in.readString();
        message = in.readString();
        orderTypeTag = in.readString();
    }

    public static final Creator<TicketSuccessItem> CREATOR = new Creator<TicketSuccessItem>() {
        @Override
        public TicketSuccessItem createFromParcel(Parcel in) {
            return new TicketSuccessItem(in);
        }

        @Override
        public TicketSuccessItem[] newArray(int size) {
            return new TicketSuccessItem[size];
        }
    };

    public int getOrderId() {

        return orderId;
    }

    public String getOrderValue() {
        return orderValue;
    }

    public void setOrderValue(String orderValue) {
        this.orderValue = orderValue;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public String getOrderTypeTag() {
        return orderTypeTag;
    }

    public String getReservationReference() {
        return reservationReference;
    }

    public void setReservationReference(String reservationReference) {
        this.reservationReference = reservationReference;
    }

    public void setOrderTypeTag(@TicketSuccessTag String orderTypeTag) {
        this.orderTypeTag = orderTypeTag;
    }

    public String getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public float getTicketPrice() {
        return ticketPrice;
    }

    public void setTicketPrice(float ticketPrice) {
        this.ticketPrice = ticketPrice;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public CashOnDeliveryData getCashOnDeliveryData() {
        return cashOnDeliveryData;
    }

    public void setCashOnDeliveryData(CashOnDeliveryData cashOnDeliveryData) {
        this.cashOnDeliveryData = cashOnDeliveryData;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TicketSuccessItem)) return false;

        TicketSuccessItem that = (TicketSuccessItem) o;

        if (getOrderId() != that.getOrderId()) return false;
        if (Float.compare(that.getTicketPrice(), getTicketPrice()) != 0) return false;
        if (!getReservationReference().equals(that.getReservationReference())) return false;
        if (!getOrderTypeTag().equals(that.getOrderTypeTag())) return false;
        return getMessage().equals(that.getMessage());

    }

    @Override
    public int hashCode() {
        int result = getOrderId();
        result = 31 * result + getReservationReference().hashCode();
        result = 31 * result + getOrderTypeTag().hashCode();
        result = 31 * result + getMessage().hashCode();
        result = 31 * result + (getTicketPrice() != +0.0f ? Float.floatToIntBits(getTicketPrice()) : 0);
        return result;
    }

    @Override
    public String toString() {
        return "TicketSuccessItem{" +
                "orderId=" + orderId +
                ", reservationReference='" + reservationReference + '\'' +
                ", orderTypeTag='" + orderTypeTag + '\'' +
                ", message='" + message + '\'' +
                ", ticketPrice=" + ticketPrice +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(orderId);
        dest.writeString(reservationReference);
        dest.writeString(message);
        dest.writeString(orderTypeTag);
    }

    @StringDef({TYPE_BKASH, TYPE_CASH_ON_DELIVERY,TYPE_CARD})
    @Retention(RetentionPolicy.SOURCE)
    public @interface TicketSuccessTag {

    }
}
