package com.android.food.foodyhut.api.data.item.handshake;

import com.google.gson.annotations.SerializedName;

/**
 * Created by sajid on 11/7/2015.
 */
public class City {
    @SerializedName("city_id")
    private String cityId;
    @SerializedName("city_name")
    private String cityName;
    @SerializedName("city_sequence")
    private String citySequence;
    @SerializedName("is_top_city")
    private boolean isTopCity;

    public City() {
    }

    public City(String cityId, String cityName, String citySequence, boolean isTopCity) {

        this.cityId = cityId;
        this.cityName = cityName;
        this.citySequence = citySequence;
        this.isTopCity = isTopCity;
    }

    public String getCityId() {

        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getCitySequence() {
        return citySequence;
    }

    public void setCitySequence(String citySequence) {
        this.citySequence = citySequence;
    }

    public boolean isTopCity() {
        return isTopCity;
    }

    public void setIsTopCity(boolean isTopCity) {
        this.isTopCity = isTopCity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof City)) return false;

        City that = (City) o;

        return this.isTopCity() == that.isTopCity()
                && this.getCityId().equals(that.getCityId())
                && this.getCityName().equals(that.getCityName())
                && this.getCitySequence().equals(that.getCitySequence());

    }

    @Override
    public int hashCode() {
        int result = getCityId().hashCode();
        result = 31 * result + getCityName().hashCode();
        result = 31 * result + getCitySequence().hashCode();
        result = 31 * result + (isTopCity() ? 1 : 0);
        return result;
    }

    @Override
    public String toString() {
        return "City{" +
                "cityId='" + cityId + '\'' +
                ", cityName='" + cityName + '\'' +
                ", citySequence='" + citySequence + '\'' +
                ", isTopCity=" + isTopCity +
                '}';
    }
}
