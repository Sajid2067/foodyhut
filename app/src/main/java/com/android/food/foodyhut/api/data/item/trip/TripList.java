package com.android.food.foodyhut.api.data.item.trip;

import com.google.gson.annotations.SerializedName;
import com.android.food.foodyhut.api.data.item.BoardingPoint;
import com.android.food.foodyhut.api.data.item.DroppingPoint;
import com.android.food.foodyhut.api.data.item.bus.Bus;
import com.android.food.foodyhut.api.data.item.bus.Operator;

import java.util.List;

/**
 * Created by sajid on 11/3/2015.
 */
public class TripList {
    @SerializedName("tripId")
    private String tripId;

    @SerializedName("details")
    private TripDetail tripDetail;

    @SerializedName("tripRoute")
    private TripRoute tripRoute;

    @SerializedName("boardingPoints")
    private List<BoardingPoint> boardingPoints;

    @SerializedName("bus")
    private Bus bus;

    @SerializedName("seatsLayout")
    private String seatLayout;

    @SerializedName("tripFacilities")
    private List<TripFacility> tripFacilities;

    @SerializedName("operator")
    private Operator operator;

    @SerializedName("shohozQuota")
    private int shohozQuota;

    @SerializedName("isEidDay")
    private int isEidDay;

    @SerializedName("dropingPoints")
    private List<DroppingPoint> droppingPoints;

    @SerializedName("incrementing")
    private boolean incrementing;

    @SerializedName("timestamps")
    private boolean timestamps;

    @SerializedName("exists")
    private boolean exists;

    public TripList() {
    }

    public TripList(String tripId, TripDetail tripDetail, TripRoute tripRoute, List<BoardingPoint> boardingPoints, Bus bus, String seatLayout, List<TripFacility> tripFacilities, Operator operator, int shohozQuota, int isEidDay, List<DroppingPoint> droppingPoints, boolean incrementing, boolean timestamps, boolean exists) {
        this.tripId = tripId;
        this.tripDetail = tripDetail;
        this.tripRoute = tripRoute;
        this.boardingPoints = boardingPoints;
        this.bus = bus;
        this.seatLayout = seatLayout;
        this.tripFacilities = tripFacilities;
        this.operator = operator;
        this.shohozQuota = shohozQuota;
        this.isEidDay = isEidDay;
        this.droppingPoints = droppingPoints;
        this.incrementing = incrementing;
        this.timestamps = timestamps;
        this.exists = exists;
    }

    public String getTripId() {

        return tripId;
    }

    public void setTripId(String tripId) {
        this.tripId = tripId;
    }

    public TripDetail getTripDetail() {
        return tripDetail;
    }

    public void setTripDetail(TripDetail tripDetail) {
        this.tripDetail = tripDetail;
    }

    public TripRoute getTripRoute() {
        return tripRoute;
    }

    public void setTripRoute(TripRoute tripRoute) {
        this.tripRoute = tripRoute;
    }

    public List<BoardingPoint> getBoardingPoints() {
        return boardingPoints;
    }

    public void setBoardingPoints(List<BoardingPoint> boardingPoints) {
        this.boardingPoints = boardingPoints;
    }

    public Bus getBus() {
        return bus;
    }

    public void setBus(Bus bus) {
        this.bus = bus;
    }

    public String getSeatLayout() {
        return seatLayout;
    }

    public void setSeatLayout(String seatLayout) {
        this.seatLayout = seatLayout;
    }

    public List<TripFacility> getTripFacilities() {
        return tripFacilities;
    }

    public void setTripFacilities(List<TripFacility> tripFacilities) {
        this.tripFacilities = tripFacilities;
    }

    public Operator getOperator() {
        return operator;
    }

    public void setOperator(Operator operator) {
        this.operator = operator;
    }

    public int getShohozQuota() {
        return shohozQuota;
    }

    public void setShohozQuota(int shohozQuota) {
        this.shohozQuota = shohozQuota;
    }


    public int getIsEidDay() {
        return isEidDay;
    }

    public void setIsEidDay(int isEidDay) {
        this.isEidDay = isEidDay;
    }

    public List<DroppingPoint> getDroppingPoints() {
        return droppingPoints;
    }

    public void setDroppingPoints(List<DroppingPoint> droppingPoints) {
        this.droppingPoints = droppingPoints;
    }

    public boolean isIncrementing() {
        return incrementing;
    }

    public void setIncrementing(boolean incrementing) {
        this.incrementing = incrementing;
    }

    public boolean isTimestamps() {
        return timestamps;
    }

    public void setTimestamps(boolean timestamps) {
        this.timestamps = timestamps;
    }

    public boolean isExists() {
        return exists;
    }

    public void setExists(boolean exists) {
        this.exists = exists;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TripList)) return false;

        TripList tripList = (TripList) o;

        if (getShohozQuota() != tripList.getShohozQuota()) return false;
        if (getIsEidDay() != tripList.getIsEidDay()) return false;
        if (isIncrementing() != tripList.isIncrementing()) return false;
        if (isTimestamps() != tripList.isTimestamps()) return false;
        if (isExists() != tripList.isExists()) return false;
        if (!getTripId().equals(tripList.getTripId())) return false;
        if (!getTripDetail().equals(tripList.getTripDetail())) return false;
        if (!getTripRoute().equals(tripList.getTripRoute())) return false;
        if (!getBoardingPoints().equals(tripList.getBoardingPoints())) return false;
        if (!getBus().equals(tripList.getBus())) return false;
        if (!getSeatLayout().equals(tripList.getSeatLayout())) return false;
        if (!getTripFacilities().equals(tripList.getTripFacilities())) return false;
        if (!getOperator().equals(tripList.getOperator())) return false;
        return getDroppingPoints().equals(tripList.getDroppingPoints());

    }

    @Override
    public int hashCode() {
        int result = getTripId().hashCode();
        result = 31 * result + getTripDetail().hashCode();
        result = 31 * result + getTripRoute().hashCode();
        result = 31 * result + getBoardingPoints().hashCode();
        result = 31 * result + getBus().hashCode();
        result = 31 * result + getSeatLayout().hashCode();
        result = 31 * result + getTripFacilities().hashCode();
        result = 31 * result + getOperator().hashCode();
        result = 31 * result + getShohozQuota();
        result = 31 * result + getIsEidDay();
        result = 31 * result + getDroppingPoints().hashCode();
        result = 31 * result + (isIncrementing() ? 1 : 0);
        result = 31 * result + (isTimestamps() ? 1 : 0);
        result = 31 * result + (isExists() ? 1 : 0);
        return result;
    }

    @Override
    public String toString() {
        return "TripList{" +
                "tripId='" + tripId + '\'' +
                ", tripDetail=" + tripDetail +
                ", tripRoute=" + tripRoute +
                ", boardingPoints=" + boardingPoints +
                ", bus=" + bus +
                ", seatLayout='" + seatLayout + '\'' +
                ", tripFacilities=" + tripFacilities +
                ", operator=" + operator +
                ", shohozQuota=" + shohozQuota +
                ", isEidDay=" + isEidDay +
                ", droppingPoints=" + droppingPoints +
                ", incrementing=" + incrementing +
                ", timestamps=" + timestamps +
                ", exists=" + exists +
                '}';
    }
}
