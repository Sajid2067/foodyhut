package com.android.food.foodyhut.util;

/**
 * Created by sajid on 12/9/2015.
 */
public class Constant {
    public static final String VALID_MOBILE_REG_EXP = "^01(1|5|6|7|8|9)\\d{8}$";
    public static final String SENT_TOKEN_TO_SERVER = "sentTokenToServer";
    public static final String GCM_TOKEN = "GCM_TOKEN";
    public static final String REGISTRATION_COMPLETE = "registrationComplete";
    public static final String CONST_CASH_ON_DELIVERY_NAME = "Cash On Delivery";
    public static final String CONST_CREDIT_OR_DEBIT_CARD_NAME = "Credit Or Debit Card";
    public static final String CONST_INTERNET_BANKING_NAME = "Internet Banking";
    public static final String CONST_MOBILE_BANKING_NAME = "Mobile Banking";
    public static final String CONST_CIB_TYPE_CARDS = "cards";
    public static final String CONST_CIB_TYPE_INTERNET_BANKING = "internet_banking";
    //Below KEY is true for Development build and false for production build
    public static boolean KEY_IS_DEVELOPMENT_BUILD = false;
    public static float KEY_DISCOUNT = 0.00f;
    public static float KEY_DISCOUNT_AMOUNT = 0.00f;
    public static boolean KEY_IS_COUPON_APPLIED = false;
    public static float KEY_COUPON_DISCOUNT = 0.00f;
    public static String KEY_APPLIED_COUPON_CODE = "";
    public static int RESPONSE_CODE_SSL=101;
    public static String SSL_TESTBOX_STORE_KEY = "shohojlimitedtest001";
    public static String SSL_LIVE_STORE_KEY = "shohojlimitedlive001";
    public static String SSL_TESTBOX_STORE_PASSWORD = "qwerty";
    public static String SSL_LIVE_STORE_PASSWORD = "shohojlimitedlive001@ssl";
    public static boolean isDiscountable = false;
    public static int  N_TH_ORDER = 0;
    public static boolean isSoudia = false;
    public static boolean isKolkataTrip = false;

}
