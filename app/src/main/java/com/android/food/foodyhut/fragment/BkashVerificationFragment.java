package com.android.food.foodyhut.fragment;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.android.food.foodyhut.R;
import com.android.food.foodyhut.api.data.item.bkash.BkashVerificationMessage;
import com.android.food.foodyhut.api.data.item.ticketWinning.TicketWinningMessage;
import com.android.food.foodyhut.application.AppController;
import com.android.food.foodyhut.fragment.item.TicketSuccessItem;
import com.android.food.foodyhut.toolbox.ObjectRequest;
import com.android.food.foodyhut.util.API;
import com.android.food.foodyhut.util.AppManager;
import com.android.food.foodyhut.util.FrequentFunction;

import java.util.ArrayList;
import java.util.HashMap;

public class BkashVerificationFragment extends BaseFragment {
    private static final String TAG = BkashVerificationFragment.class.getSimpleName();
    ObjectRequest<BkashVerificationMessage> bkashVerificationMessageObjectRequest;
    ObjectRequest<TicketWinningMessage> ticketWinningObjectRequest;
    Tracker mTracker;
    int timeoutCounter = 0;
    private static final String VALID_MOBILE_REG_EX = "^01(1|5|6|7|8|9)\\d{8}$";
    private EditText reservationReferenceEditText;
    private EditText transactionIdEditText;
    private EditText mobileNumberEditText;
    private AppController appController = AppController.getInstance();
    private TextView bkashVerifyButton;
    private TicketSuccessItem ticketSuccessItem;
    private Button tryAgainButton;
    private Button cancelButton,winningCancelButton;
    private ImageView messageTypeImageView;
    private TextView messageTypeTextView,winningMessageTypeTextView;
    private ImageButton backButtonOverride,winningBackButtonOverride;
    private View pnrNumberHolder;
    private TextView pnrNumberTextView;
    private TextView messageTextView;
    private AlertDialog bkashVerificationMessage,winningMessage;
    private boolean isSuccess;
    private TextView tvAppTitle;

    public static BkashVerificationFragment newInstance(ArrayList<Integer> previousFragmentIds, Parcelable parcelable) {
        Log.d(TAG, "AppTermsFragment newInstance(@FragmentId int previousFragmentId)");
        BkashVerificationFragment bkashVerificationFragment = new BkashVerificationFragment();
        Bundle bundle = new Bundle();
        bundle.putIntegerArrayList(PREVIOUS_FRAGMENT_ID_TAG, previousFragmentIds);
        bundle.putParcelable(PARCELABLE_FRAGMENT_ITEM_ID_TAG, parcelable);
        bkashVerificationFragment.setArguments(bundle);
        return bkashVerificationFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (parcelable instanceof TicketSuccessItem) {
            ticketSuccessItem = (TicketSuccessItem) parcelable;
        }

        // Obtain the shared Tracker instance.
        AppController application = (AppController) getActivity().getApplication();
        mTracker = application.getDefaultTracker();
        mTracker.setScreenName("bKas Verify");
        // Send a screen view.
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)");

        if (rootView == null) {
            rootView = inflater.inflate(R.layout.fragment_bkash_verification, container, false);
        }

        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    void initializeEditTextComponents() {
        reservationReferenceEditText = findViewById(R.id.order_id_edit_text);
        if (ticketSuccessItem != null) {
            reservationReferenceEditText.setText(ticketSuccessItem.getReservationReference());
        }
        transactionIdEditText = findViewById(R.id.transaction_id_edit_text);
        mobileNumberEditText = findViewById(R.id.mobile_number_edit_text);


    }

    @Override
    void initializeButtonComponents() {
        bkashVerifyButton = findViewById(R.id.bkash_verify_button);
    }

    @Override
    void initializeTextViewComponents() {


    }

    @Override
    void initializeOtherViewComponents() {
        createDialogView();
        createDialogViewWinningMessage();
        createProgressDialogView();
    }

    private void createDialogView() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());
        View view = View.inflate(getContext(), R.layout.bkash_verification_message, null);
        alertDialogBuilder.setView(view);

        messageTextView = (TextView) view.findViewById(R.id.message_text_view);
        messageTypeTextView = (TextView) view.findViewById(R.id.message_type_text_view);
        pnrNumberTextView = (TextView) view.findViewById(R.id.pnr_number_text_view);

        messageTypeImageView = (ImageView) view.findViewById(R.id.message_type_image_view);

        backButtonOverride = (ImageButton) view.findViewById(R.id.back_button_override);

        tryAgainButton = (Button) view.findViewById(R.id.retry_button);
        cancelButton = (Button) view.findViewById(R.id.cancel_button);

        pnrNumberHolder = view.findViewById(R.id.pnr_number_holder);
        alertDialogBuilder.setCancelable(false);
        bkashVerificationMessage = alertDialogBuilder.create();
    }

    private void createDialogViewWinningMessage() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());
        View view = View.inflate(getContext(), R.layout.winning_ticket_message, null);
        alertDialogBuilder.setView(view);


        winningMessageTypeTextView = (TextView) view.findViewById(R.id.winning_message_type_text_view);


        winningBackButtonOverride = (ImageButton) view.findViewById(R.id.winning_back_button_override);

        winningCancelButton = (Button) view.findViewById(R.id.winning_cancel_button);


        alertDialogBuilder.setCancelable(false);
        winningMessage = alertDialogBuilder.create();
    }

    @Override
    void initializeOnclickListener() {
        bkashVerifyButton.setOnClickListener(this);
        tryAgainButton.setOnClickListener(this);
        backButtonOverride.setOnClickListener(this);
        winningBackButtonOverride.setOnClickListener(this);
        cancelButton.setOnClickListener(this);
        winningCancelButton.setOnClickListener(this);
    }

    @Override
    void removeOnclickListener() {
        bkashVerifyButton.setOnClickListener(null);
        tryAgainButton.setOnClickListener(null);
        backButtonOverride.setOnClickListener(null);
        winningBackButtonOverride.setOnClickListener(null);
        cancelButton.setOnClickListener(null);
        winningCancelButton.setOnClickListener(null);
    }

    @Override
    public void onClick(final View view) {
        super.onClick(view);
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

        switch (view.getId()) {
            case R.id.winning_cancel_button:
            case R.id.winning_back_button_override:
                winningMessage.cancel();
                onBackPressed();
                break;
            case R.id.cancel_button:
            case R.id.back_button_override:
                bkashVerificationMessage.cancel();
                if(isSuccess) {
                    ticketWinningCall("bKash");
                }
                //onBackPressed();
                break;
            case R.id.retry_button:
            case R.id.bkash_verify_button:
                if (bkashVerificationMessage.isShowing()) {
                    bkashVerificationMessage.cancel();
                }
                isSuccess = false;
                bkashVerificationCall();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if (bkashVerificationMessage.isShowing()) {
            bkashVerificationMessage.cancel();
        } else if (winningMessage != null && winningMessage.isShowing()) {
            winningMessage.cancel();
        } else {
            super.onBackPressed();
        }
    }

    public void ticketWinningCall(String tranaction_type) {

        AppManager appManager = new AppManager(getActivity());
        String url = API.TICKET_WINNING_API_URL;
        HashMap<String, String> postParams = new HashMap<>();
        postParams.put(API.Parameter.ANDROID_DEVICE_ID, appManager.getDeviceId());
        postParams.put(API.Parameter.ANDROID_APP_VERSION, appManager.getAppVersion());
        if(tranaction_type.compareTo("card")==0) {
            postParams.put(API.Parameter.ORDER_ID, String.valueOf(ticketSuccessItem.getOrderId()));
        }else {
            postParams.put(API.Parameter.PNR, pnrNumberTextView.getText().toString().trim());
        }

        Log.e(TAG, url);
        Log.e(TAG, postParams.toString());
        ticketWinningObjectRequest = new ObjectRequest<>(API.Method.TICKET_WINNING_API_METHOD, url, postParams, new Response.Listener<TicketWinningMessage>() {
            @Override
            public void onResponse(TicketWinningMessage response) {
                progressBarDialog.cancel();
                if (response.getData() != null) {

                    winningMessage.show();
                    FrequentFunction.convertHtml(winningMessageTypeTextView,response.getData().getMessage());
                } else {

                    String message = "";
                    int i = 0;
                    for (String m : response.getError().getMessages()) {
                        if (i > 0) {
                            message += "\n";
                        }
                        message += m;
                    }
                    onBackPressed();
                    // winningMessageTypeTextView.setText(message);
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                try {
                    Log.e(TAG, Integer.toString(error.networkResponse.statusCode));
                    Log.e(TAG, error.networkResponse.toString());
                } catch (Exception e) {
                    if (timeoutCounter != 2) {
                        timeoutCounter++;
                        appController.addToRequestQueue(bkashVerificationMessageObjectRequest);
                        return;
                    } else {
                        timeoutCounter = 0;
                    }
                }
                progressBarDialog.cancel();
                String message = "";
                try {
                    Gson gson = new GsonBuilder().create();
                    TicketWinningMessage response = gson.fromJson(new String(error.networkResponse.data, "UTF-8"), TicketWinningMessage.class);
                    int i = 0;
                    for (String m : response.getError().getMessages()) {
                        if (i > 0) {
                            message += "\n";
                        }
                        message += m;
                    }
                } catch (Exception e) {
                    message = "Something went wrong.Please Check your Internet Connection";
                    Log.e(TAG, e.getMessage());
                }

            }
        }, TicketWinningMessage.class);
        progressBarDialog.show();
        appController.addToRequestQueue(ticketWinningObjectRequest);
//        }
    }

    private void bkashVerificationCall() {
        if (isZeroLengthText(transactionIdEditText) || isZeroLengthText(reservationReferenceEditText) || isZeroLengthText(mobileNumberEditText)) {
            makeAToast("Fill up all the field above", Toast.LENGTH_SHORT);
        }else if (!mobileNumberEditText.getText().toString().trim().matches(VALID_MOBILE_REG_EX)) {
            makeAToast("Please insert a valid mobile number", Toast.LENGTH_SHORT);
        }else {

            AppManager appManager = new AppManager(getActivity());
            String url = API.BKASH_VERIFICATION_API_URL;
            HashMap<String, String> postParams = new HashMap<>();
            postParams.put(API.Parameter.ANDROID_DEVICE_ID, appManager.getDeviceId());
            postParams.put(API.Parameter.ANDROID_APP_VERSION, appManager.getAppVersion());
            postParams.put(API.Parameter.RESERVATION_REFERENCES, getText(reservationReferenceEditText));
            postParams.put(API.Parameter.CONTACT_NUMBER, getText(mobileNumberEditText));
            postParams.put(API.Parameter.BKASH_TRANSACTION_ID, getText(transactionIdEditText));
            Log.e(TAG, url);
            Log.e(TAG, postParams.toString());
            bkashVerificationMessageObjectRequest = new ObjectRequest<>(API.Method.BKASH_VERIFICATION_API_METHOD, url, postParams, new Response.Listener<BkashVerificationMessage>() {
                @Override
                public void onResponse(BkashVerificationMessage response) {
                    progressBarDialog.cancel();
                    if (response.getData() != null) {
                        messageTypeImageView.setImageResource(R.mipmap.ic_done_amber_48dp);
                        messageTypeTextView.setText("SUCCESS");
                        isSuccess = true;
                        reservationReferenceEditText.setText("");
                        transactionIdEditText.setText("");
                        mobileNumberEditText.setText("");
                        pnrNumberTextView.setText(response.getData().getPnr());
                        pnrNumberHolder.setVisibility(View.VISIBLE);
                        tryAgainButton.setVisibility(View.GONE);
                        bkashVerificationMessage.show();
                      //  messageTextView.setText(Html.fromHtml(response.getData().getMessage()));
                        FrequentFunction.convertHtml(messageTextView,response.getData().getMessage());
                    } else {
                        messageTypeImageView.setImageResource(R.mipmap.ic_warning_amber_48dp);
                        messageTypeTextView.setText("ERROR");
                        pnrNumberHolder.setVisibility(View.GONE);
                        tryAgainButton.setVisibility(View.VISIBLE);
                        bkashVerificationMessage.show();
                        String message = "";
                        int i = 0;
                        for (String m : response.getError().getMessages()) {
                            if (i > 0) {
                                message += "\n";
                            }
                            message += m;
                        }
                        messageTextView.setText(message);
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    try {
                        Log.e(TAG, Integer.toString(error.networkResponse.statusCode));
                        Log.e(TAG, error.networkResponse.toString());
                    } catch (Exception e) {
                        if (timeoutCounter != 2) {
                            timeoutCounter++;
                            appController.addToRequestQueue(bkashVerificationMessageObjectRequest);
                            return;
                        } else {
                            timeoutCounter = 0;
                        }
                    }
                    progressBarDialog.cancel();
                    String message = "";
                    try {
                        Gson gson = new GsonBuilder().create();
                        BkashVerificationMessage response = gson.fromJson(new String(error.networkResponse.data, "UTF-8"), BkashVerificationMessage.class);
                        int i = 0;
                        for (String m : response.getError().getMessages()) {
                            if (i > 0) {
                                message += "\n";
                            }
                            message += m;
                        }
                    } catch (Exception e) {
                        message = "Something went wrong.Please Check your Internet Connection";
                        Log.e(TAG, e.getMessage());
                    }
                    messageTypeImageView.setImageResource(R.mipmap.ic_warning_amber_48dp);
                    messageTypeTextView.setText("ERROR");
                    messageTextView.setText(message);
                    pnrNumberHolder.setVisibility(View.GONE);
                    tryAgainButton.setVisibility(View.VISIBLE);
                    bkashVerificationMessage.show();
                }
            }, BkashVerificationMessage.class);
            progressBarDialog.show();
            appController.addToRequestQueue(bkashVerificationMessageObjectRequest);
        }
    }


}
