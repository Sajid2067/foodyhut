package com.android.food.foodyhut.fragment;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TabWidget;

import com.android.food.foodyhut.fragment.item.BookingData;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.android.food.foodyhut.R;
import com.android.food.foodyhut.adapter.tabspager.SearchResultTabsPagerAdapterFH;
import com.android.food.foodyhut.api.data.item.user.UserStatus;
import com.android.food.foodyhut.application.AppController;
import com.android.food.foodyhut.util.AppManager;

import java.util.ArrayList;


public class SearchResultContainerFragmentFH extends BaseFragment {

	@Override
	void initializeEditTextComponents() {

	}

	@Override
	void initializeButtonComponents() {

	}

	@Override
	void initializeTextViewComponents() {

	}

	@Override
	void initializeOtherViewComponents() {

	}

	@Override
	void initializeOnclickListener() {

	}

	@Override
	void removeOnclickListener() {

	}

	private String[] tabs;
	private ViewPager viewPager;
	private TabWidget tabWidget;
	private TabLayout tabLayout;
	Tracker mTracker;
	private AppManager appManager;
    private BookingData bookingData;

	public static SearchResultContainerFragmentFH newInstance(ArrayList<Integer> previousFragmentIds, Parcelable parcelable) {
		SearchResultContainerFragmentFH tripDetailsFragment = new SearchResultContainerFragmentFH();
		Bundle bundle = new Bundle();
		bundle.putIntegerArrayList(PREVIOUS_FRAGMENT_ID_TAG, previousFragmentIds);
		bundle.putParcelable(PARCELABLE_FRAGMENT_ITEM_ID_TAG, parcelable);
		tripDetailsFragment.setArguments(bundle);
		return tripDetailsFragment;
	}

	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

        if(bookingData==null){
            bookingData=new BookingData();
        }

        if (parcelable instanceof BookingData) {

            bookingData = (BookingData) parcelable;
        }

        appManager = new AppManager(getActivity());

		// Obtain the shared Tracker instance.
		AppController application = (AppController) getActivity().getApplication();
		mTracker = application.getDefaultTracker();
		mTracker.setScreenName("TripDetails");
		// Send a screen view.
		mTracker.send(new HitBuilders.ScreenViewBuilder().build());
	}

	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
	}


	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

		rootView = inflater.inflate(R.layout.search_details, container, false);

        tabLayout = (TabLayout) findViewById(R.id.tabs);

		ViewPager pager = (ViewPager) findViewById(R.id.pager);
	//	pager.setOffscreenPageLimit(1);

		SearchResultTabsPagerAdapterFH adapter = new SearchResultTabsPagerAdapterFH(getChildFragmentManager(),bookingData);
       	pager.setAdapter(adapter);
		pager.setCurrentItem(0);

		tabLayout.setBackgroundResource(R.drawable.orange_button);

		tabLayout.setupWithViewPager(pager);

		tabLayout.setSelectedTabIndicatorColor(ContextCompat.getColor(getActivity(), R.color.indicator));
		return super.onCreateView(inflater, container, savedInstanceState);
	}



}
