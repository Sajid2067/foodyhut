package com.android.food.foodyhut.api.data.item.handshake.cib;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by sajid on 11/7/2015.
 */
public class CardNames {

    @SerializedName("cards")
    private ArrayList<CibData> cards;
    @SerializedName("internet_banking")
    private ArrayList<CibData> internetBanking;

    public CardNames() {
    }

    public CardNames(ArrayList<CibData> cards, ArrayList<CibData> internetBanking) {
        this.cards = cards;
        this.internetBanking = internetBanking;
    }

    public ArrayList<CibData> getCards() {
        return cards;
    }

    public void setCards(ArrayList<CibData> cards) {
        this.cards = cards;
    }

    public ArrayList<CibData> getInternetBanking() {
        return internetBanking;
    }

    public void setInternetBanking(ArrayList<CibData> internetBanking) {
        this.internetBanking = internetBanking;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CardNames)) return false;

        CardNames cardNames = (CardNames) o;

        if (!getCards().equals(cardNames.getCards())) return false;
        return getInternetBanking().equals(cardNames.getInternetBanking());

    }

    @Override
    public int hashCode() {
        int result = getCards().hashCode();
        result = 31 * result + getInternetBanking().hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "CardNames{" +
                "cards=" + cards +
                ", internetBanking=" + internetBanking +
                '}';
    }
}
