package com.android.food.foodyhut.database.data.item;

import com.android.food.foodyhut.api.data.item.handshake.CodCoverage;

/**
 * Created by sajid on 11/7/2015.
 */
public class CodCoverageDataItem implements Item {

    private int areaId;
    private String areaName;
    private String cityId;
    private float codFees;

    public CodCoverageDataItem() {
    }

    public CodCoverageDataItem(int areaId, String areaName, String cityId, float codFees) {

        this.areaId = areaId;
        this.areaName = areaName;
        this.cityId = cityId;
        this.codFees = codFees;
    }

    public CodCoverageDataItem(CodCoverage codCoverage) {
        this.areaId = codCoverage.getAreaId();
        this.areaName = codCoverage.getAreaName();
        this.cityId = codCoverage.getCityId();
        this.codFees = codCoverage.getCodFees();
    }


    public int getAreaId() {

        return areaId;
    }

    public void setAreaId(int areaId) {
        this.areaId = areaId;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public float getCodFees() {
        return codFees;
    }

    public void setCodFees(float codFees) {
        this.codFees = codFees;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CodCoverageDataItem)) return false;

        CodCoverageDataItem that = (CodCoverageDataItem) o;

        return (this.getAreaId() == that.getAreaId())
                && (Float.compare(that.getCodFees(), this.getCodFees()) == 0)
                && this.getAreaName().equals(that.getAreaName())
                && this.getCityId().equals(that.getCityId());

    }

    @Override
    public int hashCode() {
        int result = getAreaId();
        result = 31 * result + getAreaName().hashCode();
        result = 31 * result + getCityId().hashCode();
        result = 31 * result + (getCodFees() != +0.0f ? Float.floatToIntBits(getCodFees()) : 0);
        return result;
    }

    @Override
    public String toString() {
        return areaName;
    }
}
