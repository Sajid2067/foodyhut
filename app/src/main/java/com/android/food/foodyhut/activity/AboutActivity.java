package com.android.food.foodyhut.activity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.android.food.foodyhut.R;
import com.android.food.foodyhut.util.AppManager;

/**
 * Created by sajid on 10/21/2015.
 */
public class AboutActivity extends BaseActivity implements View.OnClickListener {


    private static final String TAG = AboutActivity.class.getSimpleName();
    private ImageButton backButton;
    AppManager appManager;
    FirebaseAnalytics firebaseAnalytics;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate(Bundle savedInstanceState)");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);

        appManager = new AppManager(this);
        // Obtain the Firebase Analytics instance.
        firebaseAnalytics = FirebaseAnalytics.getInstance(this);

        backButton = (ImageButton) findViewById(R.id.back_button);
        backButton.setOnClickListener(this);

        // Obtain the shared Tracker instance.
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, appManager.getDeviceId());
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, appManager.getKeyUserMail());
    }

    @Override
    public void onClick(View view) {
        Log.d(TAG, "onClick(View view)");
        switch (view.getId()) {
            case R.id.back_button:
                Log.d(TAG, "Back Button Pressed");
                finish();
                break;
        }
    }
}
