package com.android.food.foodyhut.activity;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.android.food.foodyhut.fragment.AccountFragmentFH;
import com.android.food.foodyhut.fragment.FavouritesFragmentFH;
import com.android.food.foodyhut.fragment.LocationFragmentFH;
import com.android.food.foodyhut.fragment.MapFragment;
import com.android.food.foodyhut.fragment.TimelineFragmentFH;
import com.android.food.foodyhut.fragment.TimelineVisitFavouritesFragmentFH;
import com.android.food.foodyhut.fragment.WillVisitFragmentFH;
import com.facebook.appevents.AppEventsConstants;
import com.facebook.appevents.AppEventsLogger;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.firebase.messaging.FirebaseMessaging;
import com.android.food.foodyhut.R;
import com.android.food.foodyhut.activity.annotation.FragmentId;
import com.android.food.foodyhut.activity.listener.OnFragmentInteractionListener;
import com.android.food.foodyhut.activity.listener.OnSlideMenuItemClickListener;
import com.android.food.foodyhut.api.data.item.user.UserStatus;
import com.android.food.foodyhut.application.AppController;
import com.android.food.foodyhut.fragment.AppTermsFragment;
import com.android.food.foodyhut.fragment.BaseFragment;
import com.android.food.foodyhut.fragment.BkashVerificationFragment;
import com.android.food.foodyhut.fragment.BoardingPointFragment;
import com.android.food.foodyhut.fragment.BookTicketFragment;
import com.android.food.foodyhut.fragment.CancelTicketDeatailsFragment;
import com.android.food.foodyhut.fragment.CancelTicketFragment;
import com.android.food.foodyhut.fragment.CardDetailsFragment;
import com.android.food.foodyhut.fragment.CashOnDeliveryFragment;
import com.android.food.foodyhut.fragment.DatePickerFragment;
import com.android.food.foodyhut.fragment.FilterFragment;
import com.android.food.foodyhut.fragment.FullSeatLayoutFragment;
import com.android.food.foodyhut.fragment.InternetBankingDetailsFragment;
import com.android.food.foodyhut.fragment.OffersFragment;
import com.android.food.foodyhut.fragment.PassengerInfoFragment;
import com.android.food.foodyhut.fragment.PaymentDetailsFragment;
import com.android.food.foodyhut.fragment.CuisineTypeFragment;
import com.android.food.foodyhut.fragment.SeatLayoutFragment;
import com.android.food.foodyhut.fragment.SlideMenuFragment;
import com.android.food.foodyhut.fragment.TicketDetailsFragment;
import com.android.food.foodyhut.fragment.TicketSearchFragment;
import com.android.food.foodyhut.fragment.TicketSuccessFragment;
import com.android.food.foodyhut.fragment.TopSearchFragment;
import com.android.food.foodyhut.fragment.SearchResultContainerFragmentFH;
import com.android.food.foodyhut.gcm.Config;
import com.android.food.foodyhut.util.AppManager;
import com.android.food.foodyhut.util.Constant;

import java.util.ArrayList;

//import bd.com.gandr.apptracker.GNRTracker;

/**
 * Created by sajid on 10/27/2015.
 */
public class MainAppActivity extends BaseActivity implements OnFragmentInteractionListener, OnSlideMenuItemClickListener {

    private static final String TAG = MainAppActivity.class.getSimpleName();
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    BroadcastReceiver mRegistrationBroadcastReceiver;
    Tracker mTracker;
//    MenuItem tripDetailsHistoryMenu;
    private BaseFragment baseFragment;
    private DrawerLayout mainAppDrawerLayout;
    private DrawerToggle mainAppDrawerToggle;
    private Toolbar toolbar;
    private AppManager appManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate(Bundle savedInstanceState)");
        super.onCreate(savedInstanceState);
        if (savedInstanceState == null) {
            baseFragment = new LocationFragmentFH();
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.shifting_fragment_container, baseFragment)
                    .commit();
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.slide_menu_container, new SlideMenuFragment())
                    .commit();

        } else {
            baseFragment = (BaseFragment) getSupportFragmentManager().getFragment(savedInstanceState, "mFragment");
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.shifting_fragment_container, baseFragment)
                    .commit();

        }
        appManager = new AppManager(this);
        initializeDrawerLayout();

        initFCM();

        // Obtain the shared Tracker instance.
        AppController application = (AppController) getApplication();
        mTracker = application.getDefaultTracker();

        // Initialize the Facebook SDK before executing any other operations,
      //  FacebookSdk.sdkInitialize(getApplicationContext());
     //   AppEventsLogger.activateApp(this);

        AppEventsLogger logger = AppEventsLogger.newLogger(this);
        logger.logEvent(AppEventsConstants.EVENT_NAME_INITIATED_CHECKOUT);

        // Initialize the GnR SDK
//        GNRTracker tracker = GNRTracker.getInstance(this,MainAppActivity.class);
//        tracker.requestAppConversion();
    }


    private void initFCM() {
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                // checking for type intent filter
                if (intent.getAction().equals(Config.REGISTRATION_COMPLETE)) {
                    // gcm successfully registered
                    // now subscribe to `global` topic to receive app wide notifications
                    FirebaseMessaging.getInstance().subscribeToTopic(Config.TOPIC_GLOBAL);
                    SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
                    String regId = pref.getString("regId", null);

                    Log.e(TAG, "Firebase reg id: " + regId);

                } else if (intent.getAction().equals(Config.PUSH_NOTIFICATION)) {
                    // new push notification is received

                    String message = intent.getStringExtra("message");
                    Log.e(TAG, "Firebase reg id: " + message);
//                    Toast.makeText(getApplicationContext(), "Push notification: " + message, Toast.LENGTH_LONG).show();

                }
            }
        };
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        getSupportFragmentManager().putFragment(outState, "mFragment", baseFragment);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Log.d(TAG, "onCreateOptionsMenu(Menu menu)");
        getMenuInflater().inflate(R.menu.main_menu, menu);
//        tripDetailsHistoryMenu = menu.findItem(R.id.action_trip_details_history);
//        UserStatus userStatus = new UserStatus(MainAppActivity.this);
//        if (userStatus.isLogin()) {
//            tripDetailsHistoryMenu.setVisible(true);
//        } else {
//            tripDetailsHistoryMenu.setVisible(false);
//        }

        return true;
    }


    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onPostCreate(Bundle savedInstanceState)");
        super.onPostCreate(savedInstanceState);
//        mainAppDrawerToggle.syncState();
    }

    @Override
    protected void setContentView() {
        Log.d(TAG, "setContentView()");
        setContentView(R.layout.activity_main_app);
    }

    @Override
    protected void setupActionBar() {
        Log.d(TAG, "setupActionBar()");
        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        super.setupActionBar();
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setHomeButtonEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);

        }

    }

    private void initializeDrawerLayout() {
        Log.d(TAG, "initializeDrawerLayout()");
        mainAppDrawerLayout = (DrawerLayout) findViewById(R.id.main_app_drawer_layout);
        mainAppDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        mainAppDrawerToggle = new DrawerToggle(this, mainAppDrawerLayout, toolbar, R.string.app_name, R.string.app_name);
        mainAppDrawerLayout.setDrawerListener(mainAppDrawerToggle);
    }

    @Override
    public void fragmentChange(@FragmentId final int fragmentId, ArrayList<Integer> previousFragmentIds) {
        Log.d(TAG, "fragmentChange(@FragmentId final int fragmentId) fragmentId = " + fragmentId);
        switch (fragmentId) {
            case LOCATION_FRAGMENT_FH:
                baseFragment = new LocationFragmentFH();
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.shifting_fragment_container, baseFragment)
                        .commit();
                break;
            case CALENDER_FRAGMENT_ID:
                baseFragment = new DatePickerFragment();
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.shifting_fragment_container, baseFragment)
                        .commit();
                break;
            case BOOK_TICKET_FRAGMENT_ID:
                baseFragment = new BookTicketFragment();
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.shifting_fragment_container, baseFragment)
                        .commit();
                break;
            case TOP_SEARCH_FRAGMENT_ID:
                baseFragment = new TopSearchFragment();
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.shifting_fragment_container, baseFragment)
                        .commit();
            case TERMS_AND_CONDITION_FRAGMENT_ID:
                baseFragment = new AppTermsFragment();
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.shifting_fragment_container, baseFragment)
                        .commit();
                break;
            case SEARCH_RESULT_CONTAINER_FRAGMENT_ID_FH:
                baseFragment = new SearchResultContainerFragmentFH();
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.shifting_fragment_container, baseFragment)
                        .commit();
                break;

        }
    }

    @Override
    public void fragmentChange(@FragmentId final int fragmentId, Parcelable parcelable, ArrayList<Integer> previousFragmentIds) {
        //  Log.d(TAG, "fragmentChange(@FragmentId final int fragmentId, Parcelable parcelable) " +
        //        "fragmentId = " + fragmentId + " parcelable = " + parcelable.toString());
        fragmentChangeWithAnimation(fragmentId, parcelable, previousFragmentIds, R.anim.slide_in_right, R.anim.slide_out_right);
    }

    private void fragmentChangeWithAnimation(@FragmentId int fragmentId, Parcelable parcelable, ArrayList<Integer> previousFragmentIds, int animIn, int animOut) {
        switch (fragmentId) {
            case LOCATION_FRAGMENT_FH:
                getSupportFragmentManager().beginTransaction().setCustomAnimations(animIn, animOut)
                        .replace(R.id.shifting_fragment_container, baseFragment = LocationFragmentFH.newInstance(previousFragmentIds, parcelable))
                        .commit();
                break;
            case BOOK_TICKET_FRAGMENT_ID:
                getSupportFragmentManager().beginTransaction().setCustomAnimations(animIn, animOut)
                        .replace(R.id.shifting_fragment_container, baseFragment = BookTicketFragment.newInstance(previousFragmentIds, parcelable))
                        .commit();
                break;
            case SEAT_LAYOUT_FRAGMENT_ID:
                getSupportFragmentManager().beginTransaction().setCustomAnimations(animIn, animOut)
                        .replace(R.id.shifting_fragment_container, baseFragment = SeatLayoutFragment.newInstance(previousFragmentIds, parcelable))
                        .commit();
                break;
            case CALENDER_FRAGMENT_ID:
                getSupportFragmentManager().beginTransaction().setCustomAnimations(animIn, animOut)
                        .replace(R.id.shifting_fragment_container, baseFragment = DatePickerFragment.newInstance(parcelable, previousFragmentIds))
                        .commit();
                break;
            case SEAT_LAYOUT_FULL_FRAGMENT_ID:
                getSupportFragmentManager().beginTransaction().setCustomAnimations(animIn, animOut)
                        .replace(R.id.shifting_fragment_container, baseFragment = FullSeatLayoutFragment.newInstance(previousFragmentIds, parcelable))
                        .commit();
                break;
            case BOARDING_POINT_FRAGMENT_ID:

                getSupportFragmentManager().beginTransaction().setCustomAnimations(animIn, animOut)
                        .replace(R.id.shifting_fragment_container, baseFragment = BoardingPointFragment.newInstance(previousFragmentIds, parcelable))
                        .commit();
                break;
            case PASSENGER_INFO_FRAGMENT_ID:
                getSupportFragmentManager().beginTransaction().setCustomAnimations(animIn, animOut)
                        .replace(R.id.shifting_fragment_container, baseFragment = PassengerInfoFragment.newInstance(previousFragmentIds, parcelable))
                        .commit();
                break;
            case PAYMENT_DETAILS_FRAGMENT_ID:
                getSupportFragmentManager().beginTransaction().setCustomAnimations(animIn, animOut)
                        .replace(R.id.shifting_fragment_container, baseFragment = PaymentDetailsFragment.newInstance(previousFragmentIds, parcelable))
                        .commit();
                break;
            case PAYMENT_DETAILS_COD_FRAGMENT_ID:
                getSupportFragmentManager().beginTransaction().setCustomAnimations(animIn, animOut)
                        .replace(R.id.shifting_fragment_container, baseFragment = CashOnDeliveryFragment.newInstance(previousFragmentIds, parcelable))
                        .commit();
                break;
            case TOP_SEARCH_FRAGMENT_ID:
                getSupportFragmentManager().beginTransaction().setCustomAnimations(animIn, animOut)
                        .replace(R.id.shifting_fragment_container, baseFragment = TopSearchFragment.newInstance(previousFragmentIds, parcelable))
                        .commit();
                break;
            case FILTER_FRAGMENT_ID:
                getSupportFragmentManager().beginTransaction().setCustomAnimations(animIn, animOut)
                        .replace(R.id.shifting_fragment_container, baseFragment = FilterFragment.newInstance(previousFragmentIds, parcelable))
                        .commit();
                break;
            case TICKET_DETAILS_FRAGMENT_ID:
                getSupportFragmentManager().beginTransaction().setCustomAnimations(animIn, animOut)
                        .replace(R.id.shifting_fragment_container, baseFragment = TicketDetailsFragment.newInstance(previousFragmentIds, parcelable))
                        .commit();
                break;
            case TICKET_BUY_SUCCESS_FRAGMENT_ID:
                getSupportFragmentManager().beginTransaction().setCustomAnimations(animIn, animOut)
                        .replace(R.id.shifting_fragment_container, baseFragment = TicketSuccessFragment.newInstance(previousFragmentIds, parcelable))
                        .commit();
                break;
            case CUISINE_TYPE_FRAGMENT_ID:
                getSupportFragmentManager().beginTransaction().setCustomAnimations(animIn, animOut)
                        .replace(R.id.shifting_fragment_container, baseFragment = CuisineTypeFragment.newInstance(previousFragmentIds, parcelable))
                        .commit();
                break;
            case BKASH_PAYMENT_VERIFICATION_FRAGMENT_ID:
                getSupportFragmentManager().beginTransaction().setCustomAnimations(animIn, animOut)
                        .replace(R.id.shifting_fragment_container, baseFragment = BkashVerificationFragment.newInstance(previousFragmentIds, parcelable))
                        .commit();
                break;
            case OFFERS_FRAGMENT_ID:
                getSupportFragmentManager().beginTransaction().setCustomAnimations(animIn, animOut)
                        .replace(R.id.shifting_fragment_container, baseFragment = OffersFragment.newInstance(previousFragmentIds, parcelable))
                        .commit();
                break;
            case CARD_DETAILS_FRAGMENT_ID:
                getSupportFragmentManager().beginTransaction().setCustomAnimations(animIn, animOut)
                        .replace(R.id.shifting_fragment_container, baseFragment = CardDetailsFragment.newInstance(previousFragmentIds, parcelable))
                        .commit();
                break;
            case INTERNET_BANKING_DETAILS_FRAGMENT_ID:
                getSupportFragmentManager().beginTransaction().setCustomAnimations(animIn, animOut)
                        .replace(R.id.shifting_fragment_container, baseFragment = InternetBankingDetailsFragment.newInstance(previousFragmentIds, parcelable))
                        .commit();
                break;
            case SEARCH_RESULT_CONTAINER_FRAGMENT_ID_FH:
                getSupportFragmentManager().beginTransaction().setCustomAnimations(animIn, animOut)
                        .replace(R.id.shifting_fragment_container, baseFragment = SearchResultContainerFragmentFH.newInstance(previousFragmentIds, parcelable))
                        .commit();
                break;
            case TICKET_SEARCH_FRAGMENT_ID:
                getSupportFragmentManager().beginTransaction().setCustomAnimations(animIn, animOut)
                        .replace(R.id.shifting_fragment_container, baseFragment = TicketSearchFragment.newInstance(previousFragmentIds, parcelable))
                        .commit();
                break;
            case CANCEL_TICKET_FRAGMENT_ID:
                getSupportFragmentManager().beginTransaction().setCustomAnimations(animIn, animOut)
                        .replace(R.id.shifting_fragment_container, baseFragment = CancelTicketFragment.newInstance(previousFragmentIds, parcelable))
                        .commit();
                break;
            case CANCEL_TICKET_DETAILS_FRAGMENT_ID:
                getSupportFragmentManager().beginTransaction().setCustomAnimations(animIn, animOut)
                        .replace(R.id.shifting_fragment_container, baseFragment = CancelTicketDeatailsFragment.newInstance(previousFragmentIds, parcelable))
                        .commit();
                break;
            case MAP_FRAGMENT_ID:
                getSupportFragmentManager().beginTransaction().setCustomAnimations(animIn, animOut)
                        .replace(R.id.shifting_fragment_container, baseFragment = MapFragment.newInstance(previousFragmentIds, parcelable))
                        .commit();
                break;
            case TIMELINE_VISIT_FAVOURITE_FRAGMENT_ID:
                getSupportFragmentManager().beginTransaction().setCustomAnimations(animIn, animOut)
                        .replace(R.id.shifting_fragment_container, baseFragment = TimelineFragmentFH.newInstance(previousFragmentIds, parcelable))
                        .commit();
                break;
            case TIMELINE_FRAGMENT_ID:
                getSupportFragmentManager().beginTransaction().setCustomAnimations(animIn, animOut)
                        .replace(R.id.shifting_fragment_container, baseFragment = TimelineFragmentFH.newInstance(previousFragmentIds, parcelable))
                        .commit();
                break;
            case VISIT_FRAGMENT_ID:
                getSupportFragmentManager().beginTransaction().setCustomAnimations(animIn, animOut)
                        .replace(R.id.shifting_fragment_container, baseFragment = WillVisitFragmentFH.newInstance(previousFragmentIds, parcelable))
                        .commit();
                break;
            case FAVOURITE_FRAGMENT_ID:
                getSupportFragmentManager().beginTransaction().setCustomAnimations(animIn, animOut)
                        .replace(R.id.shifting_fragment_container, baseFragment = FavouritesFragmentFH.newInstance(previousFragmentIds, parcelable))
                        .commit();
                break;
            case ACCOUNT_FRAGMENT_ID:
                getSupportFragmentManager().beginTransaction().setCustomAnimations(animIn, animOut)
                        .replace(R.id.shifting_fragment_container, baseFragment = AccountFragmentFH.newInstance(previousFragmentIds, parcelable))
                        .commit();
                break;


        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        baseFragment.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, "onActivityResult(int requestCode, int resultCode, Intent data) " +
                "requestCode = " + requestCode + " resultCode = " + resultCode);
        super.onActivityResult(requestCode, resultCode, data);

        if (mainAppDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mainAppDrawerLayout.closeDrawer(GravityCompat.START);
        }

//        if (requestCode == Constant.RESPONSE_CODE_SSL) {
//            TicketDetailsFragment fragment = (TicketDetailsFragment) getSupportFragmentManager().findFragmentById(R.id.shifting_fragment_container);
//            fragment.onActivityResult(requestCode, resultCode, data);
//        }
    }

    @Override
    public void onItemSelect(@FragmentId int newFragmentId) {
        Log.d(TAG, "onItemSelect(@FragmentId int newFragmentId, @FragmentId int oldFragmentId) " +
                "newFragmentId = " + newFragmentId);
        @FragmentId
        int oldFragmentId = 0;
        oldFragmentId = getOldFragmentId(oldFragmentId);

        mainAppDrawerLayout.closeDrawer(GravityCompat.START);

        Parcelable parcelable = baseFragment.getParcelable();
        ArrayList<Integer> previousFragmentIds = baseFragment.getPreviousFragmentIds();
        if (!toBeOrNotToBeOldFragment(oldFragmentId)) {
            previousFragmentIds.add(oldFragmentId);
        }
        switch (newFragmentId) {
            case LOCATION_FRAGMENT_FH:
                if (baseFragment instanceof LocationFragmentFH) {
                    return;
                }
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.shifting_fragment_container, baseFragment = new LocationFragmentFH())
                        .commit();
                break;
            case OFFERS_FRAGMENT_ID:
                if (baseFragment instanceof OffersFragment) {
                    return;
                }
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.shifting_fragment_container, baseFragment = OffersFragment.newInstance(previousFragmentIds, parcelable))
                        .commit();
                break;

            case TERMS_AND_CONDITION_FRAGMENT_ID:
                if (baseFragment instanceof AppTermsFragment) {
                    return;
                }
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.shifting_fragment_container, baseFragment = AppTermsFragment.newInstance(previousFragmentIds, parcelable))
                        .commit();
                break;
            case SEARCH_RESULT_CONTAINER_FRAGMENT_ID_FH:
                if (baseFragment instanceof SearchResultContainerFragmentFH) {
                    return;
                }
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.shifting_fragment_container, baseFragment = SearchResultContainerFragmentFH.newInstance(previousFragmentIds, parcelable))
                        .commit();
                break;
            case TIMELINE_VISIT_FAVOURITE_FRAGMENT_ID:
                if (baseFragment instanceof TimelineVisitFavouritesFragmentFH) {
                    return;
                }
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.shifting_fragment_container, baseFragment = TimelineVisitFavouritesFragmentFH.newInstance(previousFragmentIds, parcelable))
                        .commit();
                break;
            case ACCOUNT_FRAGMENT_ID:
                if (baseFragment instanceof AccountFragmentFH) {
                    return;
                }
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.shifting_fragment_container, baseFragment = AccountFragmentFH.newInstance(previousFragmentIds, parcelable))
                        .commit();
                break;
        }
    }

    private boolean checkIsApplicable() {
        return baseFragment instanceof CancelTicketFragment || baseFragment instanceof OffersFragment || baseFragment instanceof AppTermsFragment;
    }

    private boolean toBeOrNotToBeOldFragment(int oldFragmentId) {
        return oldFragmentId == OFFERS_FRAGMENT_ID || oldFragmentId == TERMS_AND_CONDITION_FRAGMENT_ID || oldFragmentId == CANCEL_TICKET_FRAGMENT_ID;
    }

    @FragmentId
    private int getOldFragmentId(int oldFragmentId) {
        if (baseFragment instanceof LocationFragmentFH) {
            oldFragmentId = LOCATION_FRAGMENT_FH;
        } else if (baseFragment instanceof OffersFragment) {
            oldFragmentId = OFFERS_FRAGMENT_ID;
        } else if (baseFragment instanceof AppTermsFragment) {
            oldFragmentId = TERMS_AND_CONDITION_FRAGMENT_ID;
        } else if (baseFragment instanceof CancelTicketFragment) {
            oldFragmentId = CANCEL_TICKET_FRAGMENT_ID;
        } else if (baseFragment instanceof TopSearchFragment) {
            oldFragmentId = TOP_SEARCH_FRAGMENT_ID;
        } else if (baseFragment instanceof BookTicketFragment) {
            oldFragmentId = BOOK_TICKET_FRAGMENT_ID;
        } else if (baseFragment instanceof SeatLayoutFragment) {
            oldFragmentId = SEAT_LAYOUT_FRAGMENT_ID;
        } else if (baseFragment instanceof FullSeatLayoutFragment) {
            oldFragmentId = SEAT_LAYOUT_FULL_FRAGMENT_ID;
        } else if (baseFragment instanceof PassengerInfoFragment) {
            oldFragmentId = PASSENGER_INFO_FRAGMENT_ID;
        } else if (baseFragment instanceof PaymentDetailsFragment) {
            oldFragmentId = PAYMENT_DETAILS_FRAGMENT_ID;
        } else if (baseFragment instanceof CashOnDeliveryFragment) {
            oldFragmentId = PAYMENT_DETAILS_COD_FRAGMENT_ID;
        } else if (baseFragment instanceof DatePickerFragment) {
            oldFragmentId = CALENDER_FRAGMENT_ID;
        } else if (baseFragment instanceof FilterFragment) {
            oldFragmentId = FILTER_FRAGMENT_ID;
        } else if (baseFragment instanceof TicketDetailsFragment) {
            oldFragmentId = TICKET_DETAILS_FRAGMENT_ID;
        } else if (baseFragment instanceof TicketSuccessFragment) {
            oldFragmentId = TICKET_BUY_SUCCESS_FRAGMENT_ID;
        } else if (baseFragment instanceof BkashVerificationFragment) {
            oldFragmentId = BKASH_PAYMENT_VERIFICATION_FRAGMENT_ID;
        } else if (baseFragment instanceof CuisineTypeFragment) {
            oldFragmentId = CUISINE_TYPE_FRAGMENT_ID;
        } else if (baseFragment instanceof CardDetailsFragment) {
            oldFragmentId = CARD_DETAILS_FRAGMENT_ID;
        } else if (baseFragment instanceof InternetBankingDetailsFragment) {
            oldFragmentId = INTERNET_BANKING_DETAILS_FRAGMENT_ID;
        } else if (baseFragment instanceof TicketSearchFragment) {
            oldFragmentId = TOP_SEARCH_FRAGMENT_ID;
        }

        return oldFragmentId;
    }

    @Override
    public void closeDrawer() {
        Log.d(TAG, "closeDrawer()");

//        UserStatus userStatus = new UserStatus(MainAppActivity.this);
//        if (userStatus.isLogin()) {
//            tripDetailsHistoryMenu.setVisible(true);
//        } else {
//            tripDetailsHistoryMenu.setVisible(false);
//        }
        mainAppDrawerLayout.closeDrawer(GravityCompat.START);
    }


    @Override
    public void onBackPressed(Parcelable parcelable, @FragmentId int fragmentId, ArrayList<Integer> previousfragmentIds) {
        Log.d(TAG, "onBackPressed(@FragmentId int fragmentId) fragmentId = " + fragmentId);
        fragmentChangeWithAnimation(fragmentId, parcelable, previousfragmentIds, R.anim.slide_in_left, R.anim.slide_out_left);
    }

    @Override
    public void onBackPressed() {
        try {
            Log.d(TAG, "onBackPressed()");
            if (mainAppDrawerLayout.isDrawerOpen(GravityCompat.START)) {
                mainAppDrawerLayout.closeDrawer(GravityCompat.START);
            } else if (baseFragment.getPreviousFragmentIds().size() != 0) {
                baseFragment.onBackPressed();
            } else {
                super.onBackPressed();
            }
        } catch (Exception e) {
            super.onBackPressed();
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        Log.d(TAG, "onConfigurationChanged()");
        super.onConfigurationChanged(newConfig);
        mainAppDrawerToggle.onConfigurationChanged(newConfig);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.d(TAG, "onOptionsItemSelected(MenuItem item)");
        ArrayList<Integer> previousFragmentIds = baseFragment.getPreviousFragmentIds();
        switch (item.getItemId()) {
//            case R.id.action_bkash_verify:
//                if (getOldFragmentId(LOCATION_FRAGMENT_FH) == BKASH_PAYMENT_VERIFICATION_FRAGMENT_ID) {
//                    return false;
//                }
//                previousFragmentIds.add(getOldFragmentId(LOCATION_FRAGMENT_FH));
//                fragmentChange(BKASH_PAYMENT_VERIFICATION_FRAGMENT_ID, baseFragment.getParcelable(), previousFragmentIds);
//                return true;
//            case R.id.action_offers:
////                downloadPDFTicket();
//                if (getOldFragmentId(LOCATION_FRAGMENT_FH) == OFFERS_FRAGMENT_ID) {
//                    return false;
//                }
//                Log.d(TAG, "offer menu selected");
//                previousFragmentIds.add(getOldFragmentId(LOCATION_FRAGMENT_FH));
//                fragmentChange(OFFERS_FRAGMENT_ID, baseFragment.getParcelable(), previousFragmentIds);
//                return true;
//            case R.id.action_Search_ticket:
//                if (getOldFragmentId(LOCATION_FRAGMENT_FH) == TICKET_SEARCH_FRAGMENT_ID) {
//                    return false;
//                }
//                Log.d(TAG, "offer menu selected");
//                previousFragmentIds.add(getOldFragmentId(LOCATION_FRAGMENT_FH));
//                fragmentChange(TICKET_SEARCH_FRAGMENT_ID, baseFragment.getParcelable(), previousFragmentIds);
//                return true;
//            case R.id.action_Cancel_Ticket:
//                if (getOldFragmentId(LOCATION_FRAGMENT_FH) == CANCEL_TICKET_FRAGMENT_ID) {
//                    return false;
//                }
//                Log.d(TAG, "Cancel Ticket menu selected");
//                previousFragmentIds.add(getOldFragmentId(LOCATION_FRAGMENT_FH));
//                fragmentChange(CANCEL_TICKET_FRAGMENT_ID, baseFragment.getParcelable(), previousFragmentIds);
//                return true;
//            case R.id.action_Refund_Status:
//                if (getOldFragmentId(LOCATION_FRAGMENT_FH) == TICKET_SEARCH_FRAGMENT_ID) {
//                    return false;
//                }
//                Log.d(TAG, "Refund Status menu selected");
//                previousFragmentIds.add(getOldFragmentId(LOCATION_FRAGMENT_FH));
//                fragmentChange(TICKET_SEARCH_FRAGMENT_ID, baseFragment.getParcelable(), previousFragmentIds);
//                return true;
//            case R.id.action_trip_details_history:
//                if (getOldFragmentId(LOCATION_FRAGMENT_FH) == SEARCH_RESULT_CONTAINER_FRAGMENT_ID_FH) {
//                    return false;
//                }
//                Log.d(TAG, "trip details history menu selected");
//                previousFragmentIds.add(getOldFragmentId(LOCATION_FRAGMENT_FH));
//                fragmentChange(SEARCH_RESULT_CONTAINER_FRAGMENT_ID_FH, baseFragment.getParcelable(), previousFragmentIds);
//                return true;

//            case R.id.action_open_launch:
//                final AlertDialog alertDialog = new AlertDialog.Builder(MainAppActivity.this).create();
//                alertDialog.setTitle("Shohoz Launch");
//                alertDialog.setMessage("Open Shohoz Launch App!");
//                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "YES",
//                        new DialogInterface.OnClickListener() {
//                            public void onClick(DialogInterface dialog, int which) {
//                                dialog.dismiss();
//                                Intent i;
//                                PackageManager manager = getPackageManager();
//                                i = manager.getLaunchIntentForPackage("com.shohoz.launch.consumer");
//                                if (i == null) {
//                                    i = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.shohoz.launch.consumer"));
//                                    startActivity(i);
//                                } else {
//                                    i.addCategory(Intent.CATEGORY_LAUNCHER);
//                                    startActivity(i);
//                                }
//                            }
//                        });
//                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "NO",
//                        new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialogInterface, int i) {
//                                alertDialog.dismiss();
//                            }
//                        });
//                alertDialog.show();
//                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Constant.REGISTRATION_COMPLETE));

//        Log.i(TAG, "Setting screen name: " + "MainAppActivity");
//        mTracker.setScreenName("Image~" + "MainAppActivity");
//        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }

    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Log.i(TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }


    private class DrawerToggle extends ActionBarDrawerToggle {

        public DrawerToggle(Activity activity, DrawerLayout drawerLayout, Toolbar toolbar, int openDrawerContentDescRes, int closeDrawerContentDescRes) {
            super(activity, drawerLayout, toolbar, openDrawerContentDescRes, closeDrawerContentDescRes);
        }

        @Override
        public void onDrawerOpened(View drawerView) {
            Log.d(TAG, "onDrawerOpened(View drawerView)");
            super.onDrawerOpened(drawerView);
            invalidateOptionsMenu();
        }

        @Override
        public void onDrawerClosed(View drawerView) {
            Log.d(TAG, "onDrawerClosed(View drawerView)");
            super.onDrawerClosed(drawerView);
            invalidateOptionsMenu();
        }

        @Override
        public void onDrawerSlide(View drawerView, float slideOffset) {
            Log.d(TAG, "onDrawerClosed(View drawerView, float slideOffset) slideOffset = " + slideOffset);
            super.onDrawerSlide(drawerView, slideOffset);
        }
    }

}
