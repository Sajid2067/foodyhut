package com.android.food.foodyhut.fragment.item;

import android.os.Parcel;
import android.os.Parcelable;

import com.android.food.foodyhut.api.data.item.seat.SeatNumber;

/**
 * Created by sajid on 12/6/2015.
 */
public class SeatNumberData implements Parcelable {


    private int ticketId;
    private String seatNumber;
    private byte seatAvailability;
    private int seatType;
    private String seatFareType;
    private float seatFare;

    public SeatNumberData() {

    }

    public SeatNumberData(SeatNumber seatNumber) {
        this.ticketId = seatNumber.getTicketId();
        this.seatNumber = seatNumber.getSeatNumber();
        this.seatAvailability = seatNumber.getSeatAvailability();
        this.seatType = seatNumber.getSeatType();
        this.seatFareType = seatNumber.getSeatFareType();
        this.seatFare = seatNumber.getSeatFare();
    }

    protected SeatNumberData(Parcel in) {
        ticketId = in.readInt();
        seatNumber = in.readString();
        seatAvailability = in.readByte();
        seatType = in.readInt();
        seatFareType = in.readString();
        seatFare = in.readFloat();
    }

    public static final Creator<SeatNumberData> CREATOR = new Creator<SeatNumberData>() {
        @Override
        public SeatNumberData createFromParcel(Parcel in) {
            return new SeatNumberData(in);
        }

        @Override
        public SeatNumberData[] newArray(int size) {
            return new SeatNumberData[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    public int getTicketId() {
        return ticketId;
    }

    public void setTicketId(int ticketId) {
        this.ticketId = ticketId;
    }

    public String getSeatNumber() {
        return seatNumber;
    }

    public void setSeatNumber(String seatNumber) {
        this.seatNumber = seatNumber;
    }

    public byte getSeatAvailability() {
        return seatAvailability;
    }

    public void setSeatAvailability(byte seatAvailability) {
        this.seatAvailability = seatAvailability;
    }

    public int getSeatType() {
        return seatType;
    }

    public void setSeatType(int seatType) {
        this.seatType = seatType;
    }

    public String getSeatFareType() {
        return seatFareType;
    }

    public void setSeatFareType(String seatFareType) {
        this.seatFareType = seatFareType;
    }

    public float getSeatFare() {
        return seatFare;
    }

    public void setSeatFare(float seatFare) {
        this.seatFare = seatFare;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SeatNumberData)) return false;

        SeatNumberData that = (SeatNumberData) o;

        if (getTicketId() != that.getTicketId()) return false;
        if (getSeatAvailability() != that.getSeatAvailability()) return false;
        if (getSeatType() != that.getSeatType()) return false;
        if (Float.compare(that.getSeatFare(), getSeatFare()) != 0) return false;
        if (!getSeatNumber().equals(that.getSeatNumber())) return false;
        return getSeatFareType().equals(that.getSeatFareType());

    }

    @Override
    public int hashCode() {
        int result = getTicketId();
        result = 31 * result + getSeatNumber().hashCode();
        result = 31 * result + (int) getSeatAvailability();
        result = 31 * result + getSeatType();
        result = 31 * result + getSeatFareType().hashCode();
        result = 31 * result + (getSeatFare() != +0.0f ? Float.floatToIntBits(getSeatFare()) : 0);
        return result;
    }

    @Override
    public String toString() {
        return "SeatNumberData{" +
                "ticketId=" + ticketId +
                ", seatNumber='" + seatNumber + '\'' +
                ", seatAvailability=" + seatAvailability +
                ", seatType=" + seatType +
                ", seatFareType='" + seatFareType + '\'' +
                ", seatFare=" + seatFare +
                '}';
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(ticketId);
        dest.writeString(seatNumber);
        dest.writeByte(seatAvailability);
        dest.writeInt(seatType);
        dest.writeString(seatFareType);
        dest.writeFloat(seatFare);
    }
}
