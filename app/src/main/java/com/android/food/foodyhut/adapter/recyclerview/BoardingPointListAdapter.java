package com.android.food.foodyhut.adapter.recyclerview;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.android.food.foodyhut.R;
import com.android.food.foodyhut.adapter.recyclerview.item.BoardingPointItem;
import com.android.food.foodyhut.adapter.recyclerview.item.RecyclerViewBaseItem;
import com.android.food.foodyhut.util.AppManager;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sajid on 10/28/2015.
 */
public class BoardingPointListAdapter extends RecyclerView.Adapter<BoardingPointListAdapter.ViewHolder> implements Filterable {

    private Activity activity;
    private List<RecyclerViewBaseItem> recyclerViewBaseItems;
    private List<RecyclerViewBaseItem> backupRecyclerViewBaseItems;
    private LayoutInflater layoutInflater;
    private TextView noResultTextView;
    private AppManager appManager;

    public BoardingPointListAdapter(Activity activity, List<RecyclerViewBaseItem> recyclerViewBaseItems, TextView noResultTextView) {
        this.activity = activity;
        this.backupRecyclerViewBaseItems = recyclerViewBaseItems;
        this.recyclerViewBaseItems = new ArrayList<>(backupRecyclerViewBaseItems);
        layoutInflater = (LayoutInflater) this.activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.noResultTextView = noResultTextView;
        appManager = new AppManager(this.activity);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView;
        rootView = layoutInflater.inflate(R.layout.list_item_boarding_point, parent, false);
        return new ViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        BoardingPointItem boardingPointItem = (BoardingPointItem) recyclerViewBaseItems.get(position);
        holder.boardingPointTextView.setText(boardingPointItem.getLocationName());
        holder.boardingTimeTextView.setText(appManager.getTime(boardingPointItem.getLocationTime()));

    }


    @Override
    public int getItemCount() {
        return recyclerViewBaseItems.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence filterValue) {
                final FilterResults results = new FilterResults();
                List<RecyclerViewBaseItem> recyclerViewBaseItems = new ArrayList<>();
                for (int i = 0; i < backupRecyclerViewBaseItems.size(); i++) {
                    BoardingPointItem boardingPointItem = (BoardingPointItem) backupRecyclerViewBaseItems.get(i);
                    if (boardingPointItem.getLocationName().trim().toLowerCase().contains(filterValue.toString().trim().toLowerCase())) {
                        recyclerViewBaseItems.add(boardingPointItem);
                    }
                }
                if (filterValue.toString().trim().length() == 0) {
                    results.count = backupRecyclerViewBaseItems.size();
                    results.values = backupRecyclerViewBaseItems;
                } else {
                    results.count = recyclerViewBaseItems.size();
                    results.values = recyclerViewBaseItems;
                }
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (results.count == 0) {
                            noResultTextView.setVisibility(View.VISIBLE);
                        } else {
                            noResultTextView.setVisibility(View.GONE);
                        }
                    }
                });

                Log.e("VALUES", results.values.toString());
                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                recyclerViewBaseItems = (List<RecyclerViewBaseItem>) results.values;
                if (recyclerViewBaseItems == null) {
                    recyclerViewBaseItems = backupRecyclerViewBaseItems;
                }
                notifyDataSetChanged();
            }
        };
    }

    public RecyclerViewBaseItem getItem(int position) {
        return recyclerViewBaseItems.get(position);
    }


    static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView boardingTimeTextView;
        private TextView boardingPointTextView;

        public ViewHolder(View itemView) {
            super(itemView);
            boardingPointTextView = (TextView) itemView.findViewById(R.id.boarding_point_text_view);
            boardingTimeTextView = (TextView) itemView.findViewById(R.id.boarding_time_text_view);
        }
    }
}
