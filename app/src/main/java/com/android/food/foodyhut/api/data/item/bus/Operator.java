package com.android.food.foodyhut.api.data.item.bus;

import com.google.gson.annotations.SerializedName;

/**
 * Created by sajid on 11/3/2015.
 */
public class Operator {

    @SerializedName("company_id")
    private int companyId;
    @SerializedName("company_logo_url")
    private String companyLogoUrl;
    @SerializedName("company_name")
    private String companyName;
    @SerializedName("company_address")
    private String companyAddress;
    @SerializedName("company_short_name")
    private String companyShortName;
    @SerializedName("display_company_name")
    private String displayCompanyName;
    @SerializedName("address_line1")
    private String firstAddressLine;
    @SerializedName("address_line2")
    private String secondAddressLine;
    @SerializedName("postal_code")
    private String postalCode;
    @SerializedName("city_id")
    private int cityId;
    @SerializedName("area_id")
    private int areaId;
    @SerializedName("company_description")
    private String companyDescription;

    public Operator() {
    }

    public Operator(int companyId, String companyLogoUrl, String companyName, String companyAddress, String companyShortName, String displayCompanyName, String firstAddressLine, String secondAddressLine, String postalCode, int cityId, int areaId, String companyDescription) {
        this.companyId = companyId;
        this.companyLogoUrl = companyLogoUrl;
        this.companyName = companyName;
        this.companyAddress = companyAddress;
        this.companyShortName = companyShortName;
        this.displayCompanyName = displayCompanyName;
        this.firstAddressLine = firstAddressLine;
        this.secondAddressLine = secondAddressLine;
        this.postalCode = postalCode;
        this.cityId = cityId;
        this.areaId = areaId;
        this.companyDescription = companyDescription;
    }

    public int getCompanyId() {

        return companyId;
    }

    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }

    public String getCompanyLogoUrl() {
        return companyLogoUrl;
    }

    public void setCompanyLogoUrl(String companyLogoUrl) {
        this.companyLogoUrl = companyLogoUrl;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCompanyAddress() {
        return companyAddress;
    }

    public void setCompanyAddress(String companyAddress) {
        this.companyAddress = companyAddress;
    }

    public String getCompanyShortName() {
        return companyShortName;
    }

    public void setCompanyShortName(String companyShortName) {
        this.companyShortName = companyShortName;
    }

    public String getFirstAddressLine() {
        return firstAddressLine;
    }

    public void setFirstAddressLine(String firstAddressLine) {
        this.firstAddressLine = firstAddressLine;
    }

    public String getSecondAddressLine() {
        return secondAddressLine;
    }

    public void setSecondAddressLine(String secondAddressLine) {
        this.secondAddressLine = secondAddressLine;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public int getCityId() {
        return cityId;
    }

    public void setCityId(int cityId) {
        this.cityId = cityId;
    }

    public int getAreaId() {
        return areaId;
    }

    public void setAreaId(int areaId) {
        this.areaId = areaId;
    }

    public String getCompanyDescription() {
        return companyDescription;
    }

    public void setCompanyDescription(String companyDescription) {
        this.companyDescription = companyDescription;
    }

    @Override
    public String toString() {
        return "Operator{" +
                "companyId=" + companyId +
                ", companyLogoUrl='" + companyLogoUrl + '\'' +
                ", companyName='" + companyName + '\'' +
                ", companyAddress='" + companyAddress + '\'' +
                ", companyShortName='" + companyShortName + '\'' +
                ", displayCompanyName='" + displayCompanyName + '\'' +
                ", firstAddressLine='" + firstAddressLine + '\'' +
                ", secondAddressLine='" + secondAddressLine + '\'' +
                ", postalCode='" + postalCode + '\'' +
                ", cityId=" + cityId +
                ", areaId=" + areaId +
                ", companyDescription='" + companyDescription + '\'' +
                '}';
    }
}
