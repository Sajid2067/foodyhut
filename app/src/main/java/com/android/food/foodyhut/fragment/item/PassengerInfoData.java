package com.android.food.foodyhut.fragment.item;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by sajid on 11/6/2015.
 */
public class PassengerInfoData implements Parcelable {

    private SeatLayoutData seatLayoutData;

    public PassengerInfoData() {
    }

    public PassengerInfoData(SeatLayoutData seatLayoutData) {
        this.seatLayoutData = seatLayoutData;
    }

    protected PassengerInfoData(Parcel in) {
        seatLayoutData = in.readParcelable(SeatLayoutData.class.getClassLoader());
    }

    public static final Creator<PassengerInfoData> CREATOR = new Creator<PassengerInfoData>() {
        @Override
        public PassengerInfoData createFromParcel(Parcel in) {
            return new PassengerInfoData(in);
        }

        @Override
        public PassengerInfoData[] newArray(int size) {
            return new PassengerInfoData[size];
        }
    };

    public SeatLayoutData getSeatLayoutData() {
        return seatLayoutData;
    }

    public void setSeatLayoutData(SeatLayoutData seatLayoutData) {
        this.seatLayoutData = seatLayoutData;
    }

    @Override
    public String toString() {
        return "PassengerInfoData{" +
                "seatLayoutData=" + seatLayoutData +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(seatLayoutData, flags);
    }
}
