package com.android.food.foodyhut.api.data.item.confirmticket;

import com.google.gson.annotations.SerializedName;

/**
 * Created by sajid on 11/7/2015.
 */
public class TicketConfirmErrorData {

    @SerializedName("code")
    private String code;
    @SerializedName("messages")
    private String message;

    public TicketConfirmErrorData() {
        this(null, null);
    }

    public TicketConfirmErrorData(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "TicketConfirmErrorData{" +
                "code='" + code + '\'' +
                ", message='" + message + '\'' +
                '}';
    }
}
