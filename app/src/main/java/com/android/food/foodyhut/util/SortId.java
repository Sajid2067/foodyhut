package com.android.food.foodyhut.util;

import android.support.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;


@IntDef({SortBookTicketDataButtonUIUpdate.DEPARTURE_SORT,
        SortBookTicketDataButtonUIUpdate.FARE_SORT,
        SortBookTicketDataButtonUIUpdate.OPERATOR_SORT})
@Retention(RetentionPolicy.SOURCE)
public @interface SortId {
}
