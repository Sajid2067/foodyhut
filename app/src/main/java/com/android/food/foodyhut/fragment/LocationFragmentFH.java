package com.android.food.foodyhut.fragment;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.food.foodyhut.fragment.item.BookingData;
import com.android.food.foodyhut.location.AppLocationService;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.android.food.foodyhut.R;
import com.android.food.foodyhut.activity.listener.OnFragmentInteractionListener;
import com.android.food.foodyhut.application.AppController;
import com.android.food.foodyhut.fragment.item.CalendarData;
import com.android.food.foodyhut.util.AppManager;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.model.LatLng;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;

import static com.android.food.foodyhut.fragment.MapFragment.MY_PERMISSIONS_REQUEST_LOCATION;

public class LocationFragmentFH extends BaseFragment implements   GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener, ResultCallback<LocationSettingsResult>, OnClickListener{

    private static final String TAG = LocationFragmentFH.class.getSimpleName();

    private static final SimpleDateFormat monthFormat = new SimpleDateFormat("MMM", Locale.US);
    private static final SimpleDateFormat dayFormat = new SimpleDateFormat("EEEE", Locale.US);
//    private static String[] PERMISSIONS_PHONECALL = {Manifest.permission.CALL_PHONE};
    private static final int PERMISSIONS_REQUEST_PHONE_CALL = 100;
    Tracker mTracker;
    private TextView fromCityTextView;
    //   private TextView toCityEditText;
    private LinearLayout mainSearchButton;
    GoogleApiClient mGoogleApiClient;
    protected LocationSettingsRequest mLocationSettingsRequest;
    LocationRequest mLocationRequest;
    Location mLastLocation;
    protected static final int REQUEST_CHECK_SETTINGS = 0x1;
    public static final long UPDATE_INTERVAL_IN_MILLISECONDS = 10000;

    /**
     * The fastest rate for active location updates. Exact. Updates will never be more frequent
     * than this value.
     */
    public static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS =
            UPDATE_INTERVAL_IN_MILLISECONDS / 2;
 //   private Button recentSearchFragmentButton;

 //   private View changeJourneyDateButton;
  //  private TextView journeyDateTextView;
    // private TextView journeyDayTextView;
    //  private TextView journeyMonthTextView;
    //   private TextView journeyYearTextView;
    private TextView specialTextView;
  //  private TextView helpLineNumberTextView;
 //   private TextView tvAppTitle;
    //  private View toCityHolder;
    private View fromCityHolder,nearMeLL;
    private AppManager appManager;
    private Calendar calendar;
    private BookingData bookingData;

    AppLocationService appLocationService;
    final private int GPS_PROVIDER_OPTION = 10;

    //   private ImageButton interchangeImageButton;

    public static LocationFragmentFH newInstance(ArrayList<Integer> previousFragmentIds, Parcelable parcelable) {
        Log.d(TAG, "newInstance(@FragmentId int previousFragmentId, Parcelable parcelable)");
        Log.i(TAG, "previousFragmentIds - " + previousFragmentIds.toString());
        if (parcelable != null) {
            Log.i(TAG, "parcelable - " + parcelable.toString());
        } else {
            Log.i(TAG, "parcelable is null");
        }
        LocationFragmentFH mainSearchFragment = new LocationFragmentFH();
        Bundle bundle = new Bundle();
        bundle.putIntegerArrayList(PREVIOUS_FRAGMENT_ID_TAG, previousFragmentIds);
        bundle.putParcelable(PARCELABLE_FRAGMENT_ITEM_ID_TAG, parcelable);
        mainSearchFragment.setArguments(bundle);
        return mainSearchFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onCreate(@Nullable Bundle savedInstanceState)");
        super.onCreate(savedInstanceState);
        calendar = new GregorianCalendar();
        appManager = new AppManager(getActivity());
        if(bookingData==null){
            bookingData=new BookingData();
        }

        if (parcelable instanceof BookingData) {

            bookingData = (BookingData) parcelable;
        }
        // Obtain the shared Tracker instance.
        AppController application = (AppController) getActivity().getApplication();
        mTracker = application.getDefaultTracker();
        mTracker.setScreenName("Main Search");
        // Send a screen view.
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(getActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                buildGoogleApiClient();

            }
        }
        else {
            buildGoogleApiClient();

        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)");
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.fragment_location_fh, container, false);
        }
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onActivityCreated(@Nullable Bundle savedInstanceState)");
        super.onActivityCreated(savedInstanceState);
        if (parcelable != null && parcelable instanceof CalendarData) {
            updateUIComponent((CalendarData) parcelable);
        } else if (parcelable != null && parcelable instanceof BookingData) {
           // updateUIComponent(((BookingData) parcelable).getCalendarData());
        }
       // setCurrentLocation();
    }

    @Override
    void initializeEditTextComponents() {
        Log.d(TAG, "initializeEditTextComponents()");
        fromCityTextView = (TextView) rootView.findViewById(R.id.from_city_text_view);
        fromCityTextView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                  //  moveToCuisineTypeFragment();

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        //   toCityEditText = (TextView) rootView.findViewById(R.id.to_city_edit_text);
        if (bookingData != null) {
            fromCityTextView.setText(bookingData.getSearchPoint());
            // toCityEditText.setText(bookingData.getToCity());
        }
    }

    @Override
    void initializeButtonComponents() {
        Log.d(TAG, "initializeButtonComponents()");
     //   changeJourneyDateButton = rootView.findViewById(R.id.change_journey_date_button);
        mainSearchButton = (LinearLayout) rootView.findViewById(R.id.main_search_button);
       // recentSearchFragmentButton = (Button) rootView.findViewById(R.id.recent_search_fragment_button);
        // interchangeImageButton = (ImageButton) rootView.findViewById(R.id.interchange_image_button);
    }

    @Override
    void initializeTextViewComponents() {
        Log.d(TAG, "initializeTextViewComponents()");
      //  enterLoactionTextView = findViewById(R.id.enterLoactionTextView);
     //   int date = calendar.get(Calendar.DAY_OF_MONTH);
     //   String htmlDate = appManager.useDateTerminology(date);
    //    journeyDateTextView.setText(Html.fromHtml(htmlDate));

        //  journeyDayTextView = findViewById(R.id.journey_day_text_view);
        // journeyDayTextView.setText(dayFormat.format(calendar.getTime()));

        //  journeyMonthTextView = findViewById(R.id.journey_month_text_view);
        //  journeyMonthTextView.setText(monthFormat.format(calendar.getTime()));

        //  journeyYearTextView = findViewById(R.id.journey_year_text_view);
        //  journeyYearTextView.setText(String.format("%d", calendar.get(Calendar.YEAR)));

        nearMeLL = findViewById(R.id.near_me_holder);
      //  helpLineNumberTextView = findViewById(R.id.help_line_number_text_view);

//        tvAppTitle = findViewById(R.id.tvAppTitle);
//        if (appManager.getDiscount()) {
//            tvAppTitle.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_card_giftcard_white_24dp, 0, 0, 0);
//            tvAppTitle.setText("Double Discount on 1st Order");
//        } else {
//            tvAppTitle.setText("Search and Buy Bus Tickets");
//        }

    }


    @Override
    void initializeOtherViewComponents() {
        Log.d(TAG, "initializeOtherViewComponents()");
        fromCityHolder = findViewById(R.id.from_city_holder);
        //  toCityHolder = findViewById(R.id.to_city_holder);
    }

    @Override
    void initializeOnclickListener() {
        Log.d(TAG, "initializeOnclickListener()");
        fromCityHolder.setOnClickListener(this);
        //  toCityHolder.setOnClickListener(this);
     //   changeJourneyDateButton.setOnClickListener(this);
        mainSearchButton.setOnClickListener(this);
      //  recentSearchFragmentButton.setOnClickListener(this);
        nearMeLL.setOnClickListener(this);

    //    helpLineNumberTextView.setOnClickListener(this);
        //  interchangeImageButton.setOnClickListener(this);
    }

    @Override
    void removeOnclickListener() {
        Log.d(TAG, "removeOnclickListener()");
        fromCityHolder.setOnClickListener(null);
        //   toCityHolder.setOnClickListener(null);
     //   changeJourneyDateButton.setOnClickListener(null);
        mainSearchButton.setOnClickListener(null);
     //   recentSearchFragmentButton.setOnClickListener(null);
        nearMeLL.setOnClickListener(null);
     //   helpLineNumberTextView.setOnClickListener(null);
        // interchangeImageButton.setOnClickListener(null);
    }

    @Override
    public void onClick(View v) {
        Log.d(TAG, "removeOnclickListener()");
//        if (bookingData != null) {
//            bookingData.setCalendarData(bookingData.getCalendarData());
//        } else {
//            bookingData = new BookingData();
//        }
        switch (v.getId()) {
//            case R.id.interchange_image_button:
//                interchangeCities();
//                break;
//            case R.id.to_city_holder:
//                openTopCityFragment(bookingData, "change:" + toCityEditText.getText().toString().trim(), fromCityTextView.getText().toString().trim());
//                break;
            case R.id.from_city_holder:
               // openTopCityFragment(bookingData, "change:" + fromCityTextView.getText().toString().trim());
                openTopCityFragment(bookingData,fromCityTextView.getText().toString().trim());
                break;
            case R.id.change_journey_date_button:
              //  openDatePickerFragment(bookingData);
             //   break;
            case R.id.main_search_button:
                if(fromCityTextView.getText().toString().trim().compareTo("")==0){
                    makeAToast("Please "+getResources().getString(R.string.location),Toast.LENGTH_LONG);
                }else {
                   // bookingData.setSearchType("0");
                    moveToCuisineTypeFragment();
                }

                break;
            case R.id.help_line_number_text_view:
               // call();
                break;
            case R.id.near_me_holder:
                setCurrentLocation();
                break;
            default:
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted. Do the
                    // contacts-related task you need to do.
                    if (ContextCompat.checkSelfPermission(getActivity(),
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {

                        if (mGoogleApiClient == null) {
                            buildGoogleApiClient();
                        }

                    }

                } else {

                    // Permission denied, Disable the functionality that depends on this permission.
                    Toast.makeText(getActivity(), "permission denied", Toast.LENGTH_LONG).show();
                }
                return;
            }

            // other 'case' lines to check for other permissions this app might request.
            // You can add here other case statements according to your requirement.
        }
    }

    private void updateUIComponent(@Nullable CalendarData calendarData) {
        Log.d(TAG, "updateUIComponent(CalendarData calendarData)");

        if (calendarData != null) {
            Log.i(TAG, calendarData.toString());
            Calendar newJourneyDate = new GregorianCalendar(calendarData.getYear(), calendarData.getMonth(), calendarData.getDay());
            int date = newJourneyDate.get(Calendar.DAY_OF_MONTH);
            String htmlDate = appManager.useDateTerminology(date);
            // journeyDateTextView.setText(Html.fromHtml(htmlDate));
        //    FrequentFunction.convertHtml(journeyDateTextView, htmlDate);
            //  journeyDayTextView.setText(dayFormat.format(newJourneyDate.getTime()));

            //  journeyMonthTextView.setText(monthFormat.format(newJourneyDate.getTime()));


            //    journeyYearTextView.setText(String.format("%d", newJourneyDate.get(Calendar.YEAR)));
        } else {
            Log.i(TAG, CalendarData.class.getSimpleName() + " is null ");
        }
    }



    private void openRecentSearchFragment() {
        Log.d(TAG, "openRecentSearchFragment()");
        previousFragmentIds.add(OnFragmentInteractionListener.LOCATION_FRAGMENT_FH);
        onFragmentChangeCallListener.fragmentChange(OnFragmentInteractionListener.CUISINE_TYPE_FRAGMENT_ID, bookingData, previousFragmentIds);
    }

    private void openTopCityFragment(BookingData bookingData, String fromCity) {
        Log.d(TAG, "openTopCityFragment(BookingData bookingData, String toCity, String fromCity)");
        Log.i(TAG, "bookingData - " + bookingData.toString());
        //  Log.i(TAG, "toCity - " + fromCity);
        Log.i(TAG, "fromCity - " + fromCity);
        //  bookingData.setToCity(toCity);
        bookingData.setSearchPoint(fromCity);
        previousFragmentIds.add(OnFragmentInteractionListener.LOCATION_FRAGMENT_FH);
        onFragmentChangeCallListener.fragmentChange(OnFragmentInteractionListener.TOP_SEARCH_FRAGMENT_ID, bookingData, previousFragmentIds);
    }

    public void moveToCuisineTypeFragment(){
        previousFragmentIds.add(OnFragmentInteractionListener.LOCATION_FRAGMENT_FH);
        onFragmentChangeCallListener.fragmentChange(OnFragmentInteractionListener.CUISINE_TYPE_FRAGMENT_ID, bookingData, previousFragmentIds);
    }

    private void setCurrentLocation() {


        if (mLastLocation != null) {
            double latitude = mLastLocation.getLatitude();
            double longitude = mLastLocation.getLongitude();
            Log.d(TAG,"mLastLocation: "+mLastLocation);
            bookingData.setSearchPointLatLngAddress(mLastLocation.getLatitude()+","+mLastLocation.getLongitude());
            bookingData.setSearchType("1");
            moveToCuisineTypeFragment();
        } else {
            showSettingsAlert();
        }
    }

    public void showSettingsAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                getActivity()
        );
        alertDialog.setTitle("SETTINGS");
        alertDialog.setMessage("Enable Location Provider! Go to settings menu?");
        alertDialog.setPositiveButton("Settings",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(
                                Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                      startActivityForResult(intent,GPS_PROVIDER_OPTION);
                    }
                });
        alertDialog.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        alertDialog.show();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);

        // Sets the fastest rate for active location updates. This interval is exact, and your
        // application will never receive updates faster than this value.
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);

        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }

        //https://github.com/googlesamples/android-play-location/tree/8163f9f91bbb1ca6ed6470f5458b7c6c7e6f7ef7/LocationSettings

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        mLocationSettingsRequest = builder.build();


        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(
                        mGoogleApiClient,
                        mLocationSettingsRequest
                );
        result.setResultCallback(this);
    }

    @Override
    public void onResult(@NonNull LocationSettingsResult locationSettingsResult) {
        final Status status = locationSettingsResult.getStatus();
        Log.i(TAG, "status: "+status);
        switch (status.getStatusCode()) {
            case LocationSettingsStatusCodes.SUCCESS:
                Log.i(TAG, "All location settings are satisfied.");
                //startLocationUpdates();
                break;
            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                Log.i(TAG, "Location settings are not satisfied. Show the user a dialog to" +
                        "upgrade location settings ");

                try {
                    // Show the dialog by calling startResolutionForResult(), and check the result
                    // in onActivityResult().
                    status.startResolutionForResult(getActivity(), REQUEST_CHECK_SETTINGS);
                } catch (IntentSender.SendIntentException e) {
                    Log.i(TAG, "PendingIntent unable to execute request.");
                }
                break;
            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                Log.i(TAG, "Location settings are inadequate, and cannot be fixed here. Dialog " +
                        "not created.");
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            // Check for the integer request code originally supplied to startResolutionForResult().
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        Log.i(TAG, "User agreed to make required location settings changes.");
                        //  startLocationUpdates();
                        break;
                    case Activity.RESULT_CANCELED:
                        Log.i(TAG, "User chose not to make required location settings changes.");
                        break;
                }
                break;
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        mLastLocation = location;


        Log.e(TAG, "onLocationChanged");

        //Place current location marker
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        //    MarkerOptions markerOptions = new MarkerOptions();
        //   markerOptions.position(latLng);
        //   markerOptions.title("Current Position");
        //   markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA));
        //   mCurrLocationMarker = mMap.addMarker(markerOptions);

        //move map camera
//        MarkerOptions marker = new MarkerOptions().position(
//                                new LatLng(latitude, longitude)).title("Hello Maps");


//        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
//        mMap.animateCamera(CameraUpdateFactory.zoomTo(11));

        //stop location updates
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();

    }




    /*
    * In this method, Start PlaceAutocomplete activity
    * PlaceAutocomplete activity provides--
    * a search box to search Google places
    */
//    private void openTopCityFragment(BookingData bookingData, String unloadingPoint, String loadingPoint) {
//        Log.d(TAG, "openTopCityFragment(BookingData bookingData, String unloadingPoint, String loadingPoint)");
//        Log.i(TAG, "bookingData - " + bookingData.toString());
//        Log.i(TAG, "unloadingPoint - " + loadingPoint);
//        Log.i(TAG, "loadingPoint - " + loadingPoint);
//        bookingData.set(unloadingPoint);
//        bookingData.setloadingPoint(loadingPoint);
//        previousFragmentIds.add(OnFragmentInteractionListener.LOCATION_FRAGMENT_FH);
//        onFragmentChangeCallListener.fragmentChange(OnFragmentInteractionListener.TOP_SEARCH_FRAGMENT_ID, bookingData, previousFragmentIds);
//    }

    // A place has been received; use requestCode to track the request.

}
