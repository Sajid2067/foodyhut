package com.android.food.foodyhut.fragment;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.food.foodyhut.R;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.android.food.foodyhut.activity.listener.OnFragmentInteractionListener;
import com.android.food.foodyhut.adapter.listview.PassengerInfoAdapter;
import com.android.food.foodyhut.application.AppController;
import com.android.food.foodyhut.fragment.item.BookTicketData;
import com.android.food.foodyhut.fragment.item.CalendarData;
import com.android.food.foodyhut.fragment.item.PassengerInfoData;
import com.android.food.foodyhut.fragment.item.PaymentDetailsData;
import com.android.food.foodyhut.fragment.item.SeatLayoutData;
import com.android.food.foodyhut.fragment.item.SeatNumberData;
import com.android.food.foodyhut.util.AppManager;
import com.android.food.foodyhut.util.Constant;
import com.android.food.foodyhut.util.FrequentFunction;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;

import static com.android.food.foodyhut.util.Constant.VALID_MOBILE_REG_EXP;

/**
 * Created by sajid on 11/6/2015.
 */
public class PassengerInfoFragment extends BaseFragment implements PassengerInfoAdapter.UpdateContactPerson {

    private static final String TAG = PassengerInfoFragment.class.getSimpleName();

    private PassengerInfoData passengerInfoData;
    private SeatLayoutData seatLayoutData;
    private BookTicketData bookTicketData;
    private CalendarData calendarData;
    private Calendar calendar;

    Tracker mTracker;

    private TextView journeyRouteTextView;
    private TextView journeyDateTextView;

    private List<Parcelable> selectedSeats;
    private ListView passengerInfoListView;
    private PassengerInfoAdapter passengerInfoAdapter;

    private EditText contactPersonEditText;

    private EditText emailTextView;

    private EditText mobileNumberEditText;

    private CheckBox agreementCheckBox;

    private TextView termsConditionButton;

    private Button continueBookingButton;

    private AlertDialog termsConditionDialog;

    private AppManager appManager;

    private PaymentDetailsData paymentDetailsData;

    private String previousNumber = "";

    public static PassengerInfoFragment newInstance(ArrayList<Integer> previousFragmentIds, Parcelable parcelable) {
        PassengerInfoFragment passengerInfoFragment = new PassengerInfoFragment();
        Bundle bundle = new Bundle();
        bundle.putIntegerArrayList(PREVIOUS_FRAGMENT_ID_TAG, previousFragmentIds);
        bundle.putParcelable(PARCELABLE_FRAGMENT_ITEM_ID_TAG, parcelable);
        passengerInfoFragment.setArguments(bundle);
        return passengerInfoFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (parcelable instanceof PassengerInfoData) {
            this.passengerInfoData = (PassengerInfoData) parcelable;
            seatLayoutData = passengerInfoData.getSeatLayoutData();
            bookTicketData = seatLayoutData.getBookTicketData();
            calendarData = bookTicketData.getCalendarData();
        } else if (parcelable instanceof PaymentDetailsData) {
            paymentDetailsData = (PaymentDetailsData) parcelable;
            this.passengerInfoData = new PassengerInfoData();
            this.passengerInfoData.setSeatLayoutData(paymentDetailsData.getSeatLayoutData());
            parcelable = this.passengerInfoData;
            seatLayoutData = passengerInfoData.getSeatLayoutData();
            bookTicketData = seatLayoutData.getBookTicketData();
            calendarData = bookTicketData.getCalendarData();
            parcelable = passengerInfoData;
        }
        calendar = new GregorianCalendar(calendarData.getYear(), calendarData.getMonth(), calendarData.getDay());
        appManager = new AppManager(getActivity());
        // Obtain the shared Tracker instance.
        AppController application = (AppController) getActivity().getApplication();
        mTracker = application.getDefaultTracker();
        mTracker.setScreenName("Passenger Info");
        // Send a screen view.
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.fragment_passenger_info, container, false);
        }
        createTermsConditionDialog(inflater);
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    private void createTermsConditionDialog(LayoutInflater inflater) {
        AlertDialog.Builder termsConditionDialogBuilder = new AlertDialog.Builder(getContext());
        View termsConditionView = inflater.inflate(R.layout.fragment_terms_and_conditions, null);
        TextView generalTermsTextView;
        TextView communicationPolicyTextView;
        {
            generalTermsTextView = (TextView) termsConditionView.findViewById(R.id.general_terms_text_view);
            generalTermsTextView.setText(getParsedHTML("general_terms.html"));
            generalTermsTextView.setMovementMethod(LinkMovementMethod.getInstance());
        }

        {
            communicationPolicyTextView = (TextView) termsConditionView.findViewById(R.id.communication_policy_text_view);
            communicationPolicyTextView.setText(getParsedHTML("communication_policy.html"));
            communicationPolicyTextView.setMovementMethod(LinkMovementMethod.getInstance());
        }
        termsConditionDialogBuilder.setView(termsConditionView);
        ImageButton backButton = (ImageButton) termsConditionView.findViewById(R.id.back_button);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                termsConditionDialog.dismiss();
            }
        });
        termsConditionDialogBuilder.setCancelable(false);
        termsConditionDialog = termsConditionDialogBuilder.create();
    }

    public Spanned getParsedHTML(String fileName) {
        Log.d(TAG, "getParsedHTML(String fileName)");
        InputStream inputStream;
        String htmlText = "";
        try {
            inputStream = getActivity().getAssets().open(fileName);
            int fileSize = inputStream.available();
            byte[] fileByteData = new byte[fileSize];
            inputStream.read(fileByteData);
            inputStream.close();
            htmlText = new String(fileByteData, "UTF-8");
        } catch (IOException e) {
            Log.e(TAG, Log.getStackTraceString(e));
        }
        return Html.fromHtml(htmlText);
    }

    @Override
    void initializeEditTextComponents() {
        contactPersonEditText = (EditText) rootView.findViewById(R.id.contact_person_edit_text);
        emailTextView = (EditText) rootView.findViewById(R.id.email_text_view);
        mobileNumberEditText = (EditText) rootView.findViewById(R.id.mobile_number_edit_text);

        if (Constant.KEY_IS_DEVELOPMENT_BUILD) {
            contactPersonEditText.setText("Muktadir");
            emailTextView.setText("a@a.com");
            mobileNumberEditText.setText("01715052924");
        }

        if (paymentDetailsData != null) {
            contactPersonEditText.setText(paymentDetailsData.getContactPersonName());
            emailTextView.setText(paymentDetailsData.getEmail());
            previousNumber = paymentDetailsData.getMobileNumber();
            mobileNumberEditText.setText(previousNumber);
        }
    }

    @Override
    void initializeButtonComponents() {
        continueBookingButton = (Button) rootView.findViewById(R.id.continue_booking_button);
        termsConditionButton = (TextView) rootView.findViewById(R.id.terms_condition_button);
    }

    @Override
    void initializeTextViewComponents() {
        journeyDateTextView = (TextView) rootView.findViewById(R.id.journey_date_text_view);

        String htmlDate = appManager.getHTMLFormatDate(calendarData, calendar);
        FrequentFunction.convertHtml(journeyDateTextView,htmlDate);
       // journeyDateTextView.setText(Html.fromHtml(htmlDate));
        journeyRouteTextView = (TextView) rootView.findViewById(R.id.journey_route_text_view);
        String journeyRoute = bookTicketData.getFromCity() + " to " + bookTicketData.getToCity();
        journeyRouteTextView.setText(journeyRoute);
        String seats = "";
        for (int i = 0; i < seatLayoutData.getSelectedSeat().size(); i++) {
            SeatNumberData seatNumberData = (SeatNumberData) seatLayoutData.getSelectedSeat().get(i);
            seats += seatNumberData.getSeatNumber();
            if (i != seatLayoutData.getSelectedSeat().size() - 1) {
                seats += ",";
            }
        }
    }

    @Override
    void initializeOtherViewComponents() {
        agreementCheckBox = (CheckBox) rootView.findViewById(R.id.agreement_check_box);
        if (Constant.KEY_IS_DEVELOPMENT_BUILD) {
            agreementCheckBox.setChecked(true);
        }
        passengerInfoListView = findViewById(R.id.passenger_info_list_view);
        selectedSeats = seatLayoutData.getSelectedSeat();
        passengerInfoAdapter = new PassengerInfoAdapter(selectedSeats, getActivity(), this);
        if (paymentDetailsData != null) {
//            passengerInfoAdapter.setAges(getHashMap(paymentDetailsData.getAge(), "ages"));
            passengerInfoAdapter.setPassport(getHashMap(paymentDetailsData.getPassport(), "passport"));
            passengerInfoAdapter.setFullNames(getHashMap(paymentDetailsData.getFullName(), "fullNames"), true);
            passengerInfoAdapter.setGenders(getHashMap(paymentDetailsData.getGender(), "genders"));
            agreementCheckBox.setChecked(true);
            contactPersonEditText.setText(paymentDetailsData.getContactPersonName());
        }
        passengerInfoListView.setAdapter(passengerInfoAdapter);
        setListViewHeightBasedOnChildren(passengerInfoListView);
    }

    private HashMap<String, String> getHashMap(List<String> list, String tag) {
        HashMap<String, String> params = new HashMap<>();
        for (int i = 0; i < list.size(); i++) {
            params.put(tag + ":" + i, list.get(i));
        }
        return params;
    }

    @Override
    void initializeOnclickListener() {
        continueBookingButton.setOnClickListener(this);
        termsConditionButton.setOnClickListener(this);
    }

    @Override
    void removeOnclickListener() {
        continueBookingButton.setOnClickListener(null);
        termsConditionButton.setOnClickListener(null);
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        switch (view.getId()) {
            case R.id.terms_condition_button:
                termsConditionDialog.show();
                break;
            case R.id.continue_booking_button:
                if (!isPassengerSeatInfoEntered()) {
                    Toast.makeText(getContext(), "You must Fill up all the Passengers info", Toast.LENGTH_SHORT).show();
                } else if (isZeroLengthText(contactPersonEditText)
                        || isZeroLengthText(mobileNumberEditText) || isZeroLengthText(emailTextView)) {
                    Toast.makeText(getContext(), "All the field above must be filled.", Toast.LENGTH_SHORT).show();
                } else if (!agreementCheckBox.isChecked()) {
                    Toast.makeText(getContext(), "You must Agree with our terms and conditions.", Toast.LENGTH_SHORT).show();
                } else if (!(getMobileNumber(mobileNumberEditText)).matches(VALID_MOBILE_REG_EXP)) {
                    Toast.makeText(getContext(), "Enter a Valid mobile number.", Toast.LENGTH_SHORT).show();
                } else if (!validateEmailAddress(getText(emailTextView))) {
                    Toast.makeText(getContext(), "Enter a Valid email address.", Toast.LENGTH_SHORT).show();
                } else {

                    PaymentDetailsData paymentDetailsData = new PaymentDetailsData();
                    paymentDetailsData.setSeatLayoutData(seatLayoutData);
                    addPassengerInfo(paymentDetailsData);
                    paymentDetailsData.setContactPersonName(getText(contactPersonEditText));
                    paymentDetailsData.setEmail(getText(emailTextView));
                    String currentNumber = getText(mobileNumberEditText);
                    paymentDetailsData.setMobileNumber(currentNumber);
                    if (Constant.KEY_IS_COUPON_APPLIED && !currentNumber.equalsIgnoreCase(previousNumber)) {
                        Constant.KEY_IS_COUPON_APPLIED = false;
                        Constant.KEY_APPLIED_COUPON_CODE = "";
                        Constant.KEY_COUPON_DISCOUNT = 0.00f;
                    }
                    previousFragmentIds.add(OnFragmentInteractionListener.PASSENGER_INFO_FRAGMENT_ID);
                    onFragmentChangeCallListener.fragmentChange(OnFragmentInteractionListener.PAYMENT_DETAILS_FRAGMENT_ID, paymentDetailsData, previousFragmentIds);
                }
                break;
        }

    }

    private boolean validateEmailAddress(String email) {
        boolean gotDot = false;
        if (email.contains("@")) {
            int n = email.length();
            for (int i = email.indexOf('@') + 2; i < n; i++) {
                if (!gotDot && email.charAt(i) == '.') {
                    gotDot = true;
                } else if (gotDot && email.charAt(i) == '.') {
                    gotDot = false;
                    break;
                } else if (email.charAt(i) == '@') {
                    break;
                }
            }
        }
        return gotDot;
    }

    public void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return;
        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 0;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, ViewGroup.LayoutParams.WRAP_CONTENT));

            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight;
        listView.setLayoutParams(params);
    }

    private void addPassengerInfo(PaymentDetailsData paymentDetailsData) {
        HashMap<String, String> fullNames = passengerInfoAdapter.getFullNames();
//        HashMap<String, String> ages = passengerInfoAdapter.getAges();
        HashMap<String, String> genders = passengerInfoAdapter.getGenders();
        HashMap<String, String> passport = passengerInfoAdapter.getPassport();
        int totalSeats = seatLayoutData.getSelectedSeat().size();
        ArrayList<String> fullNameList = new ArrayList<>();
        ArrayList<String> ageList = new ArrayList<>();
        ArrayList<String> genderList = new ArrayList<>();
        ArrayList<String> passportList = new ArrayList<>();
        for (int i = 0; i < totalSeats; i++) {
            fullNameList.add((fullNames.get("fullNames:" + i).trim()));
//            ageList.add(ages.get("ages:" + i).trim());
            if(Constant.isKolkataTrip){
                passportList.add(passport.get("passport:"+i).trim());
            }
            genderList.add(genders.get("genders:" + i).trim());
        }
        paymentDetailsData.setAge(ageList);
        paymentDetailsData.setPassport(passportList);
        paymentDetailsData.setFullName(fullNameList);
        paymentDetailsData.setGender(genderList);
        Log.d(TAG, ageList.toString());
        Log.d(TAG, fullNameList.toString());
        Log.d(TAG, genderList.toString());
        Log.d(TAG,passportList.toString());
    }

    private boolean isPassengerSeatInfoEntered() {
        try {
            int totalSeats = seatLayoutData.getSelectedSeat().size();
            if(Constant.isKolkataTrip){
                if (passengerInfoAdapter.getFullNames().size() != totalSeats && passengerInfoAdapter.getPassport().size() != totalSeats) {
                    return false;
                } else {
                    HashMap<String, String> fullNames = passengerInfoAdapter.getFullNames();
//                HashMap<String, String> ages = passengerInfoAdapter.getAges();
                    HashMap<String, String> genders = passengerInfoAdapter.getGenders();
                    HashMap<String, String> passport = passengerInfoAdapter.getPassport();
                    for (int i = 0; i < totalSeats; i++) {
                        if ((fullNames.get("fullNames:" + i).trim().length() == 0) || (genders.get("genders:" + i).trim().length() == 0)||(passport.get("passport:" + i).trim().length() == 0)) {
                            return false;
                        }
                    }
                }
            }else{
                if (passengerInfoAdapter.getFullNames().size() != totalSeats) {
                    return false;
                } else {
                    HashMap<String, String> fullNames = passengerInfoAdapter.getFullNames();
//                HashMap<String, String> ages = passengerInfoAdapter.getAges();
                    HashMap<String, String> genders = passengerInfoAdapter.getGenders();
                    for (int i = 0; i < totalSeats; i++) {
                        if ((fullNames.get("fullNames:" + i).trim().length() == 0) || (genders.get("genders:" + i).trim().length() == 0)) {
                            return false;
                        }
                    }
                }
            }

            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private String getText(EditText mobileNumberEditText) {
        return mobileNumberEditText.getText().toString().trim();
    }

    private String getMobileNumber(EditText mobileNumberEditText) {
        return mobileNumberEditText.getText().toString().trim();
    }

    @Override
    public void updateName(final String name) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                contactPersonEditText.setText(name);
            }
        });
    }

//    public boolean isMobileValid() {
//
//        if (getText(mobileNumberEditText).contains("00000")) {
//            return false;
//        } else if (getText(mobileNumberEditText).contains("11111")) {
//            return false;
//        } else if (getText(mobileNumberEditText).contains("22222")) {
//            return false;
//        } else if (getText(mobileNumberEditText).contains("33333")) {
//            return false;
//        } else if (getText(mobileNumberEditText).contains("44444")) {
//            return false;
//        } else if (getText(mobileNumberEditText).contains("55555")) {
//            return false;
//        } else if (getText(mobileNumberEditText).contains("66666")) {
//            return false;
//        } else if (getText(mobileNumberEditText).contains("77777")) {
//            return false;
//        } else if (getText(mobileNumberEditText).contains("88888")) {
//            return false;
//        } else if (getText(mobileNumberEditText).contains("99999")) {
//            return false;
//        } else {
//            return true;
//        }
//
//    }
}
