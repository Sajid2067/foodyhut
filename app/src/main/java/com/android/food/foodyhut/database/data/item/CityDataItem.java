package com.android.food.foodyhut.database.data.item;

import com.android.food.foodyhut.api.data.item.handshake.City;

/**
 * Created by tuman on 4/5/2015.
 */
public class CityDataItem implements Item {
    private String cityId;
    private String cityName;
    private String citySequence;
    private boolean isTopCity;

    public CityDataItem() {
    }

    public CityDataItem(String cityId, String cityName, String citySequence, boolean isTopCity) {

        this.cityId = cityId;
        this.cityName = cityName;
        this.citySequence = citySequence;
        this.isTopCity = isTopCity;
    }

    public CityDataItem(City city) {

        this.cityId = city.getCityId();
        this.cityName = city.getCityName();
        this.citySequence = city.getCitySequence();
        this.isTopCity = city.isTopCity();
    }

    public String getCityId() {

        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getCitySequence() {
        return citySequence;
    }

    public void setCitySequence(String citySequence) {
        this.citySequence = citySequence;
    }

    public boolean isTopCity() {
        return isTopCity;
    }

    public void setIsTopCity(boolean isTopCity) {
        this.isTopCity = isTopCity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof City)) return false;

        CityDataItem that = (CityDataItem) o;

        return this.isTopCity() == that.isTopCity()
                && this.getCityId().equals(that.getCityId())
                && this.getCityName().equals(that.getCityName())
                && this.getCitySequence().equals(that.getCitySequence());

    }

    @Override
    public int hashCode() {
        int result = getCityId().hashCode();
        result = 31 * result + getCityName().hashCode();
        result = 31 * result + getCitySequence().hashCode();
        result = 31 * result + (isTopCity() ? 1 : 0);
        return result;
    }

    @Override
    public String toString() {
        return this.cityName;
    }
}
