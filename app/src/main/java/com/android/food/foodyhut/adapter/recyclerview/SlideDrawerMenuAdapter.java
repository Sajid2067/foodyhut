package com.android.food.foodyhut.adapter.recyclerview;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.food.foodyhut.adapter.recyclerview.item.MenuItem;
import com.android.food.foodyhut.adapter.recyclerview.item.RecyclerViewBaseItem;
import com.android.food.foodyhut.R;

import java.util.List;

/**
 * Created by sajid on 10/28/2015.
 */
public class SlideDrawerMenuAdapter extends RecyclerView.Adapter<SlideDrawerMenuAdapter.ViewHolder> {

    private static final int HEADER_VIEW = 0;
    private static final int MENU_VIEW = 1;

    private Context context;
    private List<RecyclerViewBaseItem> recyclerViewBaseItems;
    private LayoutInflater layoutInflater;

    public SlideDrawerMenuAdapter(Context context, List<RecyclerViewBaseItem> recyclerViewBaseItems) {
        this.context = context;
        this.recyclerViewBaseItems = recyclerViewBaseItems;
        layoutInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View rootView;

        switch (viewType) {
            case MENU_VIEW:
            default:
                rootView = layoutInflater.inflate(R.layout.list_item_sliding_menu, parent, false);
                return new ViewHolder(rootView, MENU_VIEW);
        }
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        switch (holder.holderItemId) {
            case MENU_VIEW:
            default:
                ViewHolder.MenuItemViewHolder menuItemViewHolder = holder.menuItemViewHolder;
                MenuItem menuItem = (MenuItem) recyclerViewBaseItems.get(position);
                menuItemViewHolder.menuNameTextView.setText(menuItem.getMenuItemName());
                menuItemViewHolder.menuIconImageView.setImageResource(menuItem.getIconResourceId());
                break;
        }

    }


    @Override
    public int getItemCount() {
        return recyclerViewBaseItems.size();
    }

    @Override
    public int getItemViewType(int position) {
        switch (position) {
            case MENU_VIEW:
            default:
                return MENU_VIEW;
        }
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private final int holderItemId;

        private MenuItemViewHolder menuItemViewHolder;

        public ViewHolder(View itemView, int holderItemId) {
            super(itemView);
            this.holderItemId = holderItemId;
            switch (holderItemId) {
                case MENU_VIEW:
                default:
                    initMenuItemViewHolder(itemView);
                    break;
            }
        }

        private void initMenuItemViewHolder(View itemView) {
            menuItemViewHolder = new MenuItemViewHolder();
            menuItemViewHolder.menuButton = itemView.findViewById(R.id.menu_button);
            menuItemViewHolder.menuIconImageView = (ImageView) itemView.findViewById(R.id.menu_icon_image_view);
            menuItemViewHolder.menuNameTextView = (TextView) itemView.findViewById(R.id.menu_name_text_view);
        }


        private static class MenuItemViewHolder {
            private View menuButton;
            private ImageView menuIconImageView;
            private TextView menuNameTextView;

        }
    }
}
