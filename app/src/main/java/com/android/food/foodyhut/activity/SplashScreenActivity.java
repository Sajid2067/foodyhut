package com.android.food.foodyhut.activity;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.util.Patterns;
import android.view.Display;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.food.foodyhut.api.data.item.user.UserStatus;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsConstants;
import com.facebook.appevents.AppEventsLogger;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.android.food.foodyhut.R;
import com.android.food.foodyhut.api.data.item.handshake.City;
import com.android.food.foodyhut.api.data.item.handshake.CodCoverage;
import com.android.food.foodyhut.api.data.item.handshake.HandShake;
import com.android.food.foodyhut.api.data.item.handshake.cib.CibData;
import com.android.food.foodyhut.api.data.item.handshake.paymentmethods.PaymentMethodData;
import com.android.food.foodyhut.api.data.item.handshake.paymentmethods.PaymentMethods;
import com.android.food.foodyhut.application.AppController;
import com.android.food.foodyhut.database.DataBaseOpenHelper;
import com.android.food.foodyhut.database.data.item.CibDataItem;
import com.android.food.foodyhut.database.data.item.CityDataItem;
import com.android.food.foodyhut.database.data.item.CodCoverageDataItem;
import com.android.food.foodyhut.database.data.item.PaymentMethodDataItem;
import com.android.food.foodyhut.database.data.source.CibDataSource;
import com.android.food.foodyhut.database.data.source.CityDataSource;
import com.android.food.foodyhut.database.data.source.CodCoverageDataSource;
import com.android.food.foodyhut.database.data.source.PaymentMethodDataSource;
import com.android.food.foodyhut.database.exception.DataSourceException;
import com.android.food.foodyhut.gcm.Config;
import com.android.food.foodyhut.toolbox.GetUrlBuilder;
import com.android.food.foodyhut.toolbox.ObjectRequest;
import com.android.food.foodyhut.util.API;
import com.android.food.foodyhut.util.AppManager;
import com.android.food.foodyhut.util.Constant;
import com.android.food.foodyhut.util.OffersPreference;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

public class SplashScreenActivity extends AppCompatActivity implements DialogInterface.OnClickListener, Response.Listener<HandShake>, Response.ErrorListener {

    private static final String TAG = SplashScreenActivity.class.getSimpleName();

    private static final int SPLASH_TIME_OUT = 1500;
    ProgressBar progressBar;
  //  TextView tvLoadText;
    private AppController appController = AppController.getInstance();
    private AppManager appManager;
    private AlertDialog noInternetAlertDialog;
    private AlertDialog errorAlertDialog;
    private boolean sentToSettings = false;
    SharedPreferences fcmTokenPref;
    private static final int PERMISSION_CALLBACK_CONSTANT = 100;
    private static final int REQUEST_PERMISSION_SETTING = 101;
    String[] permissionsRequired = new String[]{Manifest.permission.READ_CONTACTS,
            Manifest.permission.CALL_PHONE,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE};
    private SharedPreferences permissionStatus;
    String deviceName;
    String deviceMan;
    String carrierName;
    int width;
    int height;
    private ObjectRequest<HandShake> handShakeObjectRequest;
    private int requestNumber = 0;
    String FCM_TOKEN="";
    String gmail = "";

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        Log.d(TAG, "onCreate(Bundle savedInstanceState)");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        appManager = new AppManager(this);

        permissionStatus = getSharedPreferences("permissionStatus",MODE_PRIVATE);
        fcmTokenPref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        FCM_TOKEN = fcmTokenPref.getString("regId","");
        if(FCM_TOKEN.compareTo("")==0){
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    afterFCMTokenGeneration(savedInstanceState);
                }
            }, 5000);
        }else {
            afterFCMTokenGeneration(savedInstanceState);
        }
//        Log.e(TAG, "FCM TOKEN"+FCM_TOKEN);
//        if(permissionStatus.getBoolean(permissionsRequired[0],false)){

    }

    private void afterFCMTokenGeneration(Bundle savedInstanceState){
        runTimePermissionCheck();
//        }

        Pattern gmailPattern = Patterns.EMAIL_ADDRESS; // API level 8+
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.GET_ACCOUNTS) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        Account[] accounts = AccountManager.get(this).getAccounts();
        for (Account account : accounts) {
            if (gmailPattern.matcher(account.name).matches()) {
                gmail = account.name;
            }
        }
        appManager.setKeyUserMail(gmail);
        deviceName = android.os.Build.MODEL;
        deviceMan = android.os.Build.MANUFACTURER;
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        width = size.x;
        height = size.y;
        TelephonyManager manager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        carrierName = manager.getNetworkOperatorName();
        // Initialize the SDK before executing any other operations,
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);

        AppEventsLogger logger = AppEventsLogger.newLogger(this);
        logger.logEvent(AppEventsConstants.EVENT_NAME_INITIATED_CHECKOUT);

        progressBar = (ProgressBar) findViewById(R.id.progressBar);
     //  tvLoadText = (TextView) findViewById(R.id.tvLoadText);

        progressDialog("Establishing Connection ...", 10);

        if (appManager.getDeviceId().equals("?")) {
            appManager.setDeviceId(Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID));
        }
        createAlertDialog();
//        if (savedInstanceState == null) {
//            new Handler().postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    handshakeRequest();
//                    progressDialog("Syncing Data ...", 40);
//                }
//            }, SPLASH_TIME_OUT);
//        }

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                UserStatus userStatus=new UserStatus(SplashScreenActivity.this);
                if(userStatus.isLogin()){
                    LoginActivity.token=userStatus.getToken();
                    startActivity(new Intent(SplashScreenActivity.this, MainAppActivity.class));
                    finish();
                }else {
                    startActivity(new Intent(SplashScreenActivity.this, LoginActivity.class));
                    finish();
                }
            }
        }, 10);
//            }
//        }, 2000);

    }

    private AlertDialog.Builder getNoInternetAlertDialogBuilder() {
        AlertDialog.Builder alertDialogBuilder;
        alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setNeutralButton(R.string.open_settings, this);
        alertDialogBuilder.setPositiveButton(R.string.try_again, this);
        alertDialogBuilder.setNegativeButton(R.string.close_app, this);
        alertDialogBuilder.setTitle(R.string.error);
        alertDialogBuilder.setMessage(R.string.no_internet_connection);
        return alertDialogBuilder;
    }

    private void createAlertDialog() {
        Log.d(TAG, "createAlertDialog()");
        noInternetAlertDialog = getNoInternetAlertDialogBuilder().create();
        errorAlertDialog = getErrorAlertDialog().create();
    }

    private AlertDialog.Builder getErrorAlertDialog() {
        AlertDialog.Builder alertDialogBuilder;
        alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setPositiveButton(R.string.try_again, this);
        alertDialogBuilder.setNegativeButton(R.string.close_app, this);
        alertDialogBuilder.setTitle(R.string.error);
        alertDialogBuilder.setMessage(R.string.server_connection_fail);
        return alertDialogBuilder;
    }

    private void handshakeRequest() {
        Log.d(TAG, "handshakeRequest()");
        FCM_TOKEN = fcmTokenPref.getString("regId","");
        Log.i(TAG, "FCM Tok: "+FCM_TOKEN);
        if (appManager.isNetworkAvailable()&&FCM_TOKEN!="") {
            Log.d(TAG, "handshakeRequest done");
            createHandshakeRequest();
            appController.addToRequestQueue(handShakeObjectRequest, "handshake-request-" + requestNumber++);
        } else {
            noInternetAlertDialog.show();
        }
    }

    private void createHandshakeRequest() {
        Map<String, String> params = new HashMap<>();
        params.put(API.Parameter.ANDROID_APP_VERSION, appManager.getAppVersion());
        params.put(API.Parameter.ANDROID_DEVICE_ID, appManager.getDeviceId());
        params.put(API.Parameter.HASH, appManager.getHashkey());
        params.put(API.Parameter.EMAIL_ADDRESS, appManager.getKeyUserMail());
        params.put(API.Parameter.DEVICE_MANUFACTURER,deviceMan);
        params.put(API.Parameter.DEVICE_MODEL,deviceName);
        params.put(API.Parameter.DEVICE_RESOLUTION,width+"x"+height);
        params.put(API.Parameter.CARRIER,carrierName);
        params.put(API.Parameter.FCM_REGISTRATION_ID,FCM_TOKEN);
        GetUrlBuilder getUrlBuilder = new GetUrlBuilder(API.HANDSHAKE_API_URL, params);
        String Url = getUrlBuilder.getQueryUrl();
        Log.d(TAG, "URL:" + Url);
        Log.d(TAG, "PARAMS:" + params.toString());
        handShakeObjectRequest = new ObjectRequest<>(API.Method.HANDSHAKE_API_METHOD, Url, null, this, this, HandShake.class);
    }

    @Override
    protected void onDestroy() {
        Log.d(TAG, "onDestroy()");
        appController.cancelPendingRequests(API.HANDSHAKE_API_TAG);
        super.onDestroy();
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        Log.d(TAG, "onClick(DialogInterface dialog, int which)");
        if (dialog.equals(noInternetAlertDialog)) {
            switch (which) {
                case Dialog.BUTTON_POSITIVE:
                    Log.d(TAG, "noInternetAlertDialog Positive Button Pressed");
                    if (appManager.isNetworkAvailable()) {
                        noInternetAlertDialog.cancel();
                        noInternetAlertDialog.dismiss();
                      //  handshakeRequest();
                    } else {
                        noInternetAlertDialog.cancel();
                        noInternetAlertDialog = getNoInternetAlertDialogBuilder().create();
                        noInternetAlertDialog.show();
                    }
                    break;
                case Dialog.BUTTON_NEUTRAL:
                    Log.d(TAG, "noInternetAlertDialog Neutral Button Pressed");
                    noInternetAlertDialog.cancel();
                    startActivityForResult(new Intent(Settings.ACTION_SETTINGS), 0);
                    break;
                case Dialog.BUTTON_NEGATIVE:
                    Log.d(TAG, "noInternetAlertDialog Negative Button Pressed");
                    finish();
                    break;
            }
        } else if (dialog.equals(errorAlertDialog)) {
            switch (which) {
                case Dialog.BUTTON_POSITIVE:
                    Log.d(TAG, "errorAlertDialog Positive Button Pressed");
                  //  handshakeRequest();
                    break;
                case Dialog.BUTTON_NEUTRAL:
                    Log.d(TAG, "errorAlertDialog Neutral Button Pressed");
                    break;
                case Dialog.BUTTON_NEGATIVE:
                    Log.d(TAG, "errorAlertDialog Negative Button Pressed");
                    errorAlertDialog.cancel();
                    errorAlertDialog.dismiss();
                    finish();
                    break;
            }
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        try {
            Log.e(TAG, "Handshake Request CuisineTypeResponse- " + error.networkResponse.statusCode);
            Gson gson = new GsonBuilder().create();
            HandShake handShake = gson.fromJson(new String(error.networkResponse.data, "UTF-8"), HandShake.class);
            if (handShake.getError() != null) {
                if (handShake.getError().getCode() == 801) {
                    updateAppMessage(handShake.getError().getMessages());
                }
            }
        } catch (Exception e) {
            createAlertDialog();
            errorAlertDialog.show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "onActivityResult(int requestCode, int resultCode, Intent data) " +
                "requestCode = " + requestCode + " resultCode = " + resultCode);
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode == 0) {
            finish();
            return;
        }
        if (requestCode == REQUEST_PERMISSION_SETTING) {
            if (ActivityCompat.checkSelfPermission(SplashScreenActivity.this, permissionsRequired[0]) == PackageManager.PERMISSION_GRANTED) {
                //Got Permission
//                proceedAfterPermission();
//                Intent intent = getIntent();
//                finish();
//                startActivity(intent);
            }
        }
      //  handshakeRequest();
    }

    @Override
    public void onResponse(HandShake handShake) {
        Log.d(TAG, " onResponse(HandShake handShake)");

        if (handShake.getError() != null) {
            if (handShake.getError().getCode() == 801) {
                updateAppMessage(handShake.getError().getMessages());
            }
            return;
        }
        if (handShake.getHandShakeData().getHash() != null) {
            appManager.setDiscount(handShake.getHandShakeData().isApp_specific_discount());
            Log.e(TAG,""+handShake.getHandShakeData().isApp_specific_discount());
            appManager.setHashkey(handShake.getHandShakeData().getHash());
            Log.d(TAG, "HASH:" + handShake.getHandShakeData().getHash());
            appManager.setBKashFee(handShake.getHandShakeData().getBkashFee());
            appManager.setShohozFee(handShake.getHandShakeData().getShohozFee());
            OffersPreference offersPreference = new OffersPreference(handShake.getHandShakeData().getShohozOfferList());
            Gson gson = new GsonBuilder().create();
            appManager.setOffers(gson.toJson(offersPreference));

            if (appManager.getAppStartFirstTime()) {
                createDataBase(handShake);
            } else {
                appManager.setDiscount(handShake.getHandShakeData().isApp_specific_discount());
                Log.e(TAG,""+handShake.getHandShakeData().isApp_specific_discount());
                DataBaseOpenHelper dataBaseOpenHelper = new DataBaseOpenHelper(this);
                SQLiteDatabase sqLiteDatabase = dataBaseOpenHelper.getWritableDatabase();
                dataBaseOpenHelper.dropDatabase(sqLiteDatabase);
                dataBaseOpenHelper.createDatabase(sqLiteDatabase);
                sqLiteDatabase.close();
                createDataBase(handShake);
            }
        }
//        appManager.setBKashFee(handShake.getHandShakeData().getBkashFee());
//        appManager.setShohozFee(handShake.getHandShakeData().getShohozFee());
//        if (appManager.getAppStartFirstTime()) {
//            appManager.setHashkey(handShake.getHandShakeData().getHash());
//
//        } else {
//            if (!handShake.getHandShakeData().getHash().equals(appManager.getHashkey())) {
//                appManager.setHashkey(handShake.getHandShakeData().getHash());
//                DataBaseOpenHelper dataBaseOpenHelper = new DataBaseOpenHelper(this);
//                SQLiteDatabase sqLiteDatabase = dataBaseOpenHelper.getWritableDatabase();
//                dataBaseOpenHelper.dropDatabase(sqLiteDatabase);
//                dataBaseOpenHelper.createDatabase(sqLiteDatabase);
//                sqLiteDatabase.close();
//                createDataBase(handShake);
//            }
//        }
//        OffersPreference offersPreference = new OffersPreference(handShake.getHandShakeData().getShohozOfferList());
//        Gson gson = new GsonBuilder().create();
//        appManager.setOffers(gson.toJson(offersPreference));
        progressDialog("Complete", 99);
        startActivity(new Intent(SplashScreenActivity.this, MainAppActivity.class));
        finish();
    }

    private void updateAppMessage(ArrayList<String> messages) {
        final String appPackageName = getPackageName();
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setPositiveButton("Update", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                try {
                    startActivityForResult(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)), 1);
                } catch (android.content.ActivityNotFoundException e) {
                    startActivityForResult(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)), 1);
                }
            }
        });
        alertDialogBuilder.setNegativeButton("Close App", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        String alertMessage = "";
        for (String message : messages) {
            alertMessage += message;
            alertMessage += "\n";
        }
        alertDialogBuilder.setMessage(alertMessage);
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.create().show();
    }

    private void createDataBase(HandShake handShake) {
        appManager.setAppStartFirstTime(false);

        CibDataSource cibDataSource = new CibDataSource(this);
        cibDataSource.open();
        for (CibData cib : handShake.getHandShakeData().getCardNames().getCards()) {
            CibDataItem cibDataItem = new CibDataItem(cib.getShort_code(), cib.getName(), cib.getPercentage(), Constant.CONST_CIB_TYPE_CARDS);
            try {
                cibDataSource.insertCibDataItem(cibDataItem);
            } catch (DataSourceException e) {
                Log.e(TAG, Log.getStackTraceString(e));
            }
        }

        for (CibData cib : handShake.getHandShakeData().getCardNames().getInternetBanking()) {
            CibDataItem cibDataItem = new CibDataItem(cib.getShort_code(), cib.getName(), cib.getPercentage(), Constant.CONST_CIB_TYPE_INTERNET_BANKING);
            try {
                cibDataSource.insertCibDataItem(cibDataItem);
            } catch (DataSourceException e) {
                Log.e(TAG, Log.getStackTraceString(e));
            }
        }
        cibDataSource.close();

        PaymentMethodDataSource paymentMethodDataSource = new PaymentMethodDataSource(this);
        paymentMethodDataSource.open();
        PaymentMethods paymentMethods = handShake.getHandShakeData().getPaymentMethods();

        PaymentMethodData cashOnDeliveryData = paymentMethods.getCashOnDelivery();
        PaymentMethodData creditOrDebitCardData = paymentMethods.getCreditOrDebitCard();
        PaymentMethodData internetBankingData = paymentMethods.getInternetBanking();
        PaymentMethodData mobileBankingData = paymentMethods.getMobileBanking();

        PaymentMethodDataItem cashOnDeliveryDataItem = new PaymentMethodDataItem(Constant.CONST_CASH_ON_DELIVERY_NAME, cashOnDeliveryData.getHours(), cashOnDeliveryData.isActive());
        PaymentMethodDataItem creditOrDebitCardDataItem = new PaymentMethodDataItem(Constant.CONST_CREDIT_OR_DEBIT_CARD_NAME, creditOrDebitCardData.getHours(), creditOrDebitCardData.isActive());
        PaymentMethodDataItem internetBankingDataItem = new PaymentMethodDataItem(Constant.CONST_INTERNET_BANKING_NAME, internetBankingData.getHours(), internetBankingData.isActive());
        PaymentMethodDataItem mobileBankingDataItem = new PaymentMethodDataItem(Constant.CONST_MOBILE_BANKING_NAME, mobileBankingData.getHours(), mobileBankingData.isActive());

        try {
            paymentMethodDataSource.insertPaymentMethodDataItem(cashOnDeliveryDataItem);
        } catch (DataSourceException e) {
            Log.e(TAG, Log.getStackTraceString(e));
        }
        try {
            paymentMethodDataSource.insertPaymentMethodDataItem(creditOrDebitCardDataItem);
        } catch (DataSourceException e) {
            Log.e(TAG, Log.getStackTraceString(e));
        }
        try {
            paymentMethodDataSource.insertPaymentMethodDataItem(internetBankingDataItem);
        } catch (DataSourceException e) {
            Log.e(TAG, Log.getStackTraceString(e));
        }
        try {
            paymentMethodDataSource.insertPaymentMethodDataItem(mobileBankingDataItem);
        } catch (DataSourceException e) {
            Log.e(TAG, Log.getStackTraceString(e));
        }
        paymentMethodDataSource.close();

        CityDataSource cityDataSource = new CityDataSource(this);
        cityDataSource.open();
        if (cityDataSource.getSize() != handShake.getHandShakeData().getCities().size()) {
            for (City city : handShake.getHandShakeData().getCities()) {
                CityDataItem cityDataItem = new CityDataItem(city);
                try {
                    cityDataSource.insertCityDataItem(cityDataItem);
                } catch (DataSourceException e) {
                    Log.e(TAG, Log.getStackTraceString(e));
                }
            }
        }
        cityDataSource.close();

        CodCoverageDataSource codCoverageDataSource = new CodCoverageDataSource(this);
        codCoverageDataSource.open();
        if (codCoverageDataSource.getSize() != handShake.getHandShakeData().getCodCoverages().size()) {
            for (CodCoverage codCoverage : handShake.getHandShakeData().getCodCoverages()) {
                CodCoverageDataItem coverageDataItem = new CodCoverageDataItem(codCoverage);
                codCoverageDataSource.open();
                try {
                    codCoverageDataSource.insertCodCoverageDataItem(coverageDataItem);
                } catch (DataSourceException e) {
                    Log.e(TAG, Log.getStackTraceString(e));
                }
            }
        }
        codCoverageDataSource.close();
    }

    private void progressDialog(String tvText, int timeInterval) {

        //tvLoadText.setText(tvText);
        progressBar.setProgress(timeInterval);
    }

    public void runTimePermissionCheck(){
        if(ActivityCompat.checkSelfPermission(SplashScreenActivity.this, permissionsRequired[0]) != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(SplashScreenActivity.this, permissionsRequired[1]) != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(SplashScreenActivity.this, permissionsRequired[2]) != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(SplashScreenActivity.this, permissionsRequired[3]) != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(SplashScreenActivity.this, permissionsRequired[4]) != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(SplashScreenActivity.this, permissionsRequired[5]) != PackageManager.PERMISSION_GRANTED){
            if(ActivityCompat.shouldShowRequestPermissionRationale(SplashScreenActivity.this,permissionsRequired[0])
                    || ActivityCompat.shouldShowRequestPermissionRationale(SplashScreenActivity.this,permissionsRequired[1])
                    || ActivityCompat.shouldShowRequestPermissionRationale(SplashScreenActivity.this,permissionsRequired[2])
                    || ActivityCompat.shouldShowRequestPermissionRationale(SplashScreenActivity.this,permissionsRequired[3])
                    || ActivityCompat.shouldShowRequestPermissionRationale(SplashScreenActivity.this,permissionsRequired[4])
                    || ActivityCompat.shouldShowRequestPermissionRationale(SplashScreenActivity.this,permissionsRequired[5])){
                //Show Information about why you need the permission
                AlertDialog.Builder builder = new AlertDialog.Builder(SplashScreenActivity.this);
                builder.setTitle("Need Multiple Permissions");
                builder.setMessage("This app needs Phone, Contacts and Location permissions.");
                builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        ActivityCompat.requestPermissions(SplashScreenActivity.this,permissionsRequired,PERMISSION_CALLBACK_CONSTANT);
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.show();
            } else if (permissionStatus.getBoolean(permissionsRequired[0],false)) {
                //Previously Permission Request was cancelled with 'Dont Ask Again',
                // Redirect to Settings after showing Information about why you need the permission
                AlertDialog.Builder builder = new AlertDialog.Builder(SplashScreenActivity.this);
                builder.setTitle("Need Multiple Permissions");
                builder.setMessage("This app needs Phone, Contacts and Location permissions.");
                builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        sentToSettings = true;
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", getPackageName(), null);
                        intent.setData(uri);
                        startActivityForResult(intent, REQUEST_PERMISSION_SETTING);
                        Toast.makeText(getBaseContext(), "Go to Permissions to Grant  Phone, Contacts and Location", Toast.LENGTH_LONG).show();
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.show();
            }  else {
                //just request the permission
                ActivityCompat.requestPermissions(SplashScreenActivity.this,permissionsRequired,PERMISSION_CALLBACK_CONSTANT);
            }

//            txtPermissions.setText("Permissions Required");
            Toast.makeText(SplashScreenActivity.this, "Permissions Required", Toast.LENGTH_SHORT).show();
            SharedPreferences.Editor editor = permissionStatus.edit();
            editor.putBoolean(permissionsRequired[0],true);
            editor.commit();
        } else {
            //You already have the permission, just go ahead.
//            proceedAfterPermission();
        }
    }



    private void proceedAfterPermission() {
//        txtPermissions.setText("We've got all permissions");
        Toast.makeText(getBaseContext(), "We got All Permissions", Toast.LENGTH_SHORT).show();

    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        if (sentToSettings) {
            if (ActivityCompat.checkSelfPermission(SplashScreenActivity.this, permissionsRequired[0]) == PackageManager.PERMISSION_GRANTED) {
                //Got Permission
//                proceedAfterPermission();
                Intent intent = getIntent();
                finish();
                startActivity(intent);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode == PERMISSION_CALLBACK_CONSTANT){
            //check if all permissions are granted
            boolean allgranted = false;
            for(int i=0;i<grantResults.length;i++){
                if(grantResults[i]==PackageManager.PERMISSION_GRANTED){
                    allgranted = true;
                } else {
                    allgranted = false;
                    break;
                }
            }

            if(allgranted){
                proceedAfterPermission();
                Intent intent = getIntent();
                finish();
                startActivity(intent);
            } else if(ActivityCompat.shouldShowRequestPermissionRationale(SplashScreenActivity.this,permissionsRequired[0])
                    || ActivityCompat.shouldShowRequestPermissionRationale(SplashScreenActivity.this,permissionsRequired[1])
                    || ActivityCompat.shouldShowRequestPermissionRationale(SplashScreenActivity.this,permissionsRequired[2])
                    || ActivityCompat.shouldShowRequestPermissionRationale(SplashScreenActivity.this,permissionsRequired[3])
                    || ActivityCompat.shouldShowRequestPermissionRationale(SplashScreenActivity.this,permissionsRequired[4])){
//                txtPermissions.setText("Permissions Required");
                Toast.makeText(SplashScreenActivity.this, "Permissions Required", Toast.LENGTH_SHORT).show();
                AlertDialog.Builder builder = new AlertDialog.Builder(SplashScreenActivity.this);
                builder.setTitle("Need Multiple Permissions");
                builder.setMessage("This app needs Phone, Contacts and Location permissions.");
                builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        ActivityCompat.requestPermissions(SplashScreenActivity.this,permissionsRequired,PERMISSION_CALLBACK_CONSTANT);

                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.show();
            } else {
                Toast.makeText(getBaseContext(),"Unable to get Permission",Toast.LENGTH_LONG).show();
            }
        }
    }



}