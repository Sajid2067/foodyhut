package com.android.food.foodyhut.fragment;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.android.food.foodyhut.R;
import com.android.food.foodyhut.activity.listener.OnFragmentInteractionListener;
import com.android.food.foodyhut.adapter.spinner.InternetBankingAdapter;
import com.android.food.foodyhut.application.AppController;
import com.android.food.foodyhut.database.data.item.CibDataItem;
import com.android.food.foodyhut.database.data.source.CibDataSource;
import com.android.food.foodyhut.fragment.item.BkashPaymentData;
import com.android.food.foodyhut.fragment.item.CalendarData;
import com.android.food.foodyhut.fragment.item.CashOnDeliveryData;
import com.android.food.foodyhut.fragment.item.PaymentDetailsData;
import com.android.food.foodyhut.fragment.item.SSLPaymentData;
import com.android.food.foodyhut.fragment.item.SeatLayoutData;
import com.android.food.foodyhut.fragment.item.SeatNumberData;
import com.android.food.foodyhut.util.AppManager;
import com.android.food.foodyhut.util.Constant;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;



public class InternetBankingDetailsFragment extends BaseFragment implements AdapterView.OnItemSelectedListener{
    private static final String TAG = InternetBankingDetailsFragment.class.getSimpleName();

    private Button continueButton;
    private TextView fullNameTextView;
    private TextView journeyTimeTextView;
    private TextView journeyDateTextView;
    private TextView busOperatorNameTextView;
    private TextView journeyRouteTextView;
    private TextView ageTextView;
    private TextView boardingPointTextView;
    private TextView ticketFareTextView;
    private TextView shohozFeeTextView;
    private TextView paymentGatewayFeeTextView;
    private TextView discountTextView;
    private TextView tvDiscountMessage;
    private TextView totalPaymentTextView;
    private TextView cashOnDeliveryFeeTextView;
    private Spinner spinner;
    private CalendarData calendarData;
    private Calendar calendar;
    private AppManager appManager;
    private PaymentDetailsData paymentDetailsData;
    private CashOnDeliveryData cashOnDeliveryData;
    Tracker mTracker;
    private BkashPaymentData bkashPaymentData;
    private float bankPercentage;
    private float bankCharge;
    private String bankName;
    String paymentGateWayFee;
    String ticketFare;
    String shohozFee;
    String cashOnDeliveryFee;
    String discountFee;
    String totalPayment;
    String totalPaymentWithBankCharge;
    int REQUEST_CODE = Constant.RESPONSE_CODE_SSL;
    SSLPaymentData sslPaymentData;
    InternetBankingAdapter internetBankingAdapter;
    private List<CibDataItem> cardList = new ArrayList<CibDataItem>();

    public static InternetBankingDetailsFragment newInstance(ArrayList<Integer> previousFragmentIds, Parcelable parcelable) {
        InternetBankingDetailsFragment paymentDetailsFragment = new InternetBankingDetailsFragment();
        Bundle bundle = new Bundle();
        bundle.putIntegerArrayList(PREVIOUS_FRAGMENT_ID_TAG, previousFragmentIds);
        bundle.putParcelable(PARCELABLE_FRAGMENT_ITEM_ID_TAG, parcelable);
        paymentDetailsFragment.setArguments(bundle);
        return paymentDetailsFragment;

    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        appManager = new AppManager(getActivity());
        if (parcelable instanceof PaymentDetailsData) {
            paymentDetailsData = (PaymentDetailsData) parcelable;
        } else if (parcelable instanceof CashOnDeliveryData) {
            cashOnDeliveryData = (CashOnDeliveryData) parcelable;
            paymentDetailsData = cashOnDeliveryData.getPaymentDetailsData();
            parcelable = paymentDetailsData;
        } else if (parcelable instanceof BkashPaymentData) {
            bkashPaymentData = (BkashPaymentData) parcelable;
            paymentDetailsData = bkashPaymentData.getPaymentDetailsData();
            parcelable = paymentDetailsData;
        }
        sslPaymentData = new SSLPaymentData();
        internetBankingAdapter = new InternetBankingAdapter(getActivity(), cardList);

        calendarData = paymentDetailsData.getSeatLayoutData().getBookTicketData().getCalendarData();
        calendar = new GregorianCalendar(calendarData.getYear(), calendarData.getMonth(), calendarData.getDay());
        // Obtain the shared Tracker instance.
        AppController application = (AppController) getActivity().getApplication();
        mTracker = application.getDefaultTracker();

        mTracker.setScreenName("Internet Banking Details");

        mTracker.setScreenName("Payment Details");

        // Send a screen view.
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (rootView == null) {

            rootView = inflater.inflate(R.layout.fragment_internet_banking_details, container, false);


        }

        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    void initializeEditTextComponents() {

    }

    @Override
    void initializeButtonComponents() {
        continueButton = (Button) rootView.findViewById(R.id.continue_button);
    }

    @Override
    void initializeTextViewComponents() {
        fullNameTextView = findViewById(R.id.full_name_text_view);
        fullNameTextView.setText(getUserFullName());

        journeyTimeTextView = findViewById(R.id.journey_time_text_view);
        journeyTimeTextView.setText(appManager.getTime(paymentDetailsData.getSeatLayoutData().getSelectedBoardingPoint().getLocationTime()));

        journeyDateTextView = findViewById(R.id.journey_date_text_view);
        journeyDateTextView.setText(getJourneyDate());

        busOperatorNameTextView = findViewById(R.id.bus_operator_name_text_view);
        busOperatorNameTextView.setText(paymentDetailsData.getSeatLayoutData().getOperatorName());

        journeyRouteTextView = findViewById(R.id.journey_route_text_view);
        journeyRouteTextView.setText(getJourneyRoute());

        boardingPointTextView = findViewById(R.id.boarding_point_text_view);
        boardingPointTextView.setText(paymentDetailsData.getSeatLayoutData().getSelectedBoardingPoint().getLocationName());

//        ageTextView = findViewById(R.id.age_text_view);
//        ageTextView.setText(getUserAge());

        ticketFareTextView = findViewById(R.id.ticket_fare_text_view);
        ticketFare = getPriceFormat(getTicketFare());
        ticketFareTextView.setText(getLeadingSpace(ticketFare));

        shohozFeeTextView = findViewById(R.id.shohoz_fee_text_view);
        shohozFee = getPriceFormat(calculateShohozFee());
        shohozFeeTextView.setText(getLeadingSpace(shohozFee));

        cashOnDeliveryFeeTextView = findViewById(R.id.cash_on_delivery_fee_text_view);
        cashOnDeliveryFee = getPriceFormat(0.0f);
        if (cashOnDeliveryData != null) {
            cashOnDeliveryFee = getPriceFormat(cashOnDeliveryData.getCashOnDeliveryFee());
        }
        cashOnDeliveryFeeTextView.setText(getLeadingSpace(cashOnDeliveryFee));

        paymentGatewayFeeTextView = findViewById(R.id.payment_gateway_fee_text_view);
        paymentGateWayFee = getPriceFormat(0.0f);
        if (bkashPaymentData != null) {
            if (appManager.getDiscount()){
                paymentGateWayFee = getPriceFormat(getBKashFee(getTicketFare() + calculateShohozFee() - Constant.KEY_DISCOUNT));
            }else {
                paymentGateWayFee = getPriceFormat(getBKashFee(getTicketFare() + calculateShohozFee() - getDiscountAmount()));
            }

        }
        paymentGatewayFeeTextView.setText(getLeadingSpace(paymentGateWayFee));

        discountTextView = findViewById(R.id.discount_text_view);
        if(appManager.getDiscount()){
            discountFee = getPriceFormat(Constant.KEY_DISCOUNT);
        }else{
            discountFee = getPriceFormat(getDiscountAmount());
        }

        discountTextView.setText(getLeadingSpace(discountFee));

        totalPaymentTextView = findViewById(R.id.total_payment_text_view);
        totalPayment = getPriceFormat(calculateTotalFee(ticketFare, shohozFee, cashOnDeliveryFee, paymentGateWayFee, discountFee));
        totalPaymentTextView.setText(getLeadingSpace(totalPayment));

        tvDiscountMessage = findViewById(R.id.tvDiscountMessage);
        if (Constant.isDiscountable){
//            discountTextView.setVisibility(View.INVISIBLE);
            tvDiscountMessage.setVisibility(View.VISIBLE);
            tvDiscountMessage.setText("You've just saved "+getDiscountAmount()+" tk instead of buying tickets from counter.");
        }else{
            tvDiscountMessage.setVisibility(View.GONE);
        }
    }

    @Override
    void initializeOtherViewComponents() {

        CibDataSource cibDataSource = new CibDataSource(getContext());
        cibDataSource.open();
        cardList = cibDataSource.getAllCibDataItem(Constant.CONST_CIB_TYPE_INTERNET_BANKING);
        Log.e(TAG, "INTERNET_BANKING: " + cardList.toString());
        spinner = (Spinner) rootView.findViewById(R.id.spnrInternetBankList);
        spinner.setOnItemSelectedListener(this);

        spinner.setAdapter(internetBankingAdapter);
        internetBankingAdapter.notifyDataSetChanged();
        cibDataSource.close();


    }

    @Override
    void initializeOnclickListener() {
        continueButton.setOnClickListener(this);
    }

    @Override
    void removeOnclickListener() {
        continueButton.setOnClickListener(null);
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        switch (view.getId()){
            case R.id.continue_button:
                if (appManager.getCardName().equalsIgnoreCase("default")){

                    Toast.makeText(getActivity(), "Please choose any gateway.", Toast.LENGTH_SHORT).show();
                }
                else{
                    previousFragmentIds.add(OnFragmentInteractionListener.PAYMENT_DETAILS_FRAGMENT_ID);
                    sslPaymentData.setPaymentDetailsData(paymentDetailsData);
                    onFragmentChangeCallListener.fragmentChange(OnFragmentInteractionListener.TICKET_DETAILS_FRAGMENT_ID, sslPaymentData, previousFragmentIds);
                }

                break;
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        bankName = cardList.get(position).getCibShortCode();
        appManager.setCardName(bankName);
        bankPercentage = cardList.get(position).getCibPercentage();
        sslPaymentData.setmSSLFee(bankCharge);
        getBankPercentage(bankPercentage);
        appManager.setSSLFee(bankCharge);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    public String getUserFullName() {
        return paymentDetailsData.getFullName().get(0);
    }
    private Spanned getJourneyDate() {
        String htmlFormatDate = appManager.getHTMLFormatDate(paymentDetailsData.getSeatLayoutData().getBookTicketData().getCalendarData(), calendar);
        return Html.fromHtml(htmlFormatDate);
    }
    private float calculateTotalFee(String ticketFare, String shohozFee, String cashOnDeliveryFee, String paymentGateWayFee, String discountFee) {
        return Float.parseFloat(ticketFare) + Float.parseFloat(cashOnDeliveryFee) + Float.parseFloat(paymentGateWayFee) + Float.parseFloat(shohozFee) - Float.parseFloat(discountFee);
    }

    private float calculateShohozFee() {
        return (appManager.getShohozFee() * paymentDetailsData.getSeatLayoutData().getSelectedSeat().size());
    }

    @NonNull
    private String getUserAge() {
        return paymentDetailsData.getAge().get(0) + " Years";
    }

    @NonNull
    private String getJourneyRoute() {
        return paymentDetailsData.getSeatLayoutData().getBookTicketData().getFromCity() + " to " + paymentDetailsData.getSeatLayoutData().getBookTicketData().getToCity();
    }
    private String getPriceFormat(float price) {
        return String.format("%.02f", price);
    }
    private float getTicketFare() {
        SeatLayoutData seatLayoutData = paymentDetailsData.getSeatLayoutData();
        List<Parcelable> selectedSeats = seatLayoutData.getSelectedSeat();
        float baseFare = 0.0f;
        for (int i = 0; i < selectedSeats.size(); i++) {
            SeatNumberData seatNumberData = (SeatNumberData) selectedSeats.get(i);
            baseFare = baseFare + seatNumberData.getSeatFare();
        }
        return baseFare;
    }

    private float getDiscountAmount() {
        if (Constant.KEY_IS_COUPON_APPLIED) {
            return Constant.KEY_COUPON_DISCOUNT;
        } else {
            SeatLayoutData seatLayoutData = paymentDetailsData.getSeatLayoutData();
            List<Parcelable> selectedSeats = seatLayoutData.getSelectedSeat();

            return Constant.KEY_DISCOUNT * selectedSeats.size();
        }
    }
    private String getLeadingSpace(String string) {
        String space = "";
        int count = 12 - string.length();
        if (string.length() == 8) {
            count = 1;
        } else if (string.length() == 7) {
            count = 3;
        } else if (string.length() == 6) {
            count = 5;
        } else if (string.length() == 5) {
            count = 7;
        } else if (string.length() == 4) {
            count = 9;
        }
        for (int i = 1; i <= count; i++) {
            space += " ";
        }
        return String.format("৳%s%s", space, string);
    }

    private float getBKashFee(float totalFee) {
        //ceil(ceil(total / (1 - (bkash_fee / 100))) - total)
        return (float) Math.ceil(Math.ceil(totalFee / (1 - (appManager.getBKashFee() / 100f))) - totalFee);
    }

    private void getBankPercentage(float percentage) {
        bankCharge = (float)Math.ceil(Math.ceil(Float.parseFloat(totalPayment) / (1 - (percentage / 100f))) - Float.parseFloat(totalPayment));
        paymentGateWayFee = getPriceFormat((float) Math.ceil(bankCharge));
        paymentGatewayFeeTextView.setText(getLeadingSpace(paymentGateWayFee));
        totalPaymentWithBankCharge = getPriceFormat(calculateTotalFee(ticketFare, shohozFee, cashOnDeliveryFee, paymentGateWayFee, discountFee));
        totalPaymentTextView.setText(getLeadingSpace(totalPaymentWithBankCharge));
    }

}
