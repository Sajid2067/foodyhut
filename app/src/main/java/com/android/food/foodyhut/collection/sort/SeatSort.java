package com.android.food.foodyhut.collection.sort;

import com.android.food.foodyhut.fragment.item.SeatNumberData;

import java.util.Comparator;

/**
 * Created by sajid on 12/6/2015.
 */
public class SeatSort implements Comparator<SeatNumberData> {
    @Override
    public int compare(SeatNumberData lhs, SeatNumberData rhs) {
        if (lhs.getSeatNumber().charAt(0) >= 'A' && lhs.getSeatNumber().charAt(0) <= 'Z') {
            if (lhs.getSeatNumber().charAt(0) > lhs.getSeatNumber().charAt(0)) {
                return 1;
            } else if (lhs.getSeatNumber().charAt(0) < lhs.getSeatNumber().charAt(0)) {
                return -1;
            } else {
                return 0;
            }
        } else {
            String leftSeat = "";
            String rightSeat = "";
            for (int i = 0; i < lhs.getSeatNumber().length(); i++) {
                if (lhs.getSeatNumber().charAt(i) >= '0' && lhs.getSeatNumber().charAt(i) <= '9') {
                    leftSeat += lhs.getSeatNumber().charAt(i);
                } else {
                    break;
                }
            }
            for (int i = 0; i < rhs.getSeatNumber().length(); i++) {
                if (rhs.getSeatNumber().charAt(i) >= '0' && rhs.getSeatNumber().charAt(i) <= '9') {
                    rightSeat += rhs.getSeatNumber().charAt(i);
                } else {
                    break;
                }
            }
            if (Integer.parseInt(leftSeat) > Integer.parseInt(rightSeat)) {
                return 1;
            } else if (Integer.parseInt(leftSeat) < Integer.parseInt(rightSeat)) {
                return -1;
            } else {
                return 0;
            }
        }
    }
}
