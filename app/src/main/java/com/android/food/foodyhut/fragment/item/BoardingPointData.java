package com.android.food.foodyhut.fragment.item;

import android.os.Parcel;
import android.os.Parcelable;

import com.android.food.foodyhut.adapter.recyclerview.item.BoardingPointItem;
import com.android.food.foodyhut.api.data.item.BoardingPoint;

public class BoardingPointData implements Parcelable {

    public static final Creator<BoardingPointData> CREATOR = new Creator<BoardingPointData>() {
        @Override
        public BoardingPointData createFromParcel(Parcel in) {
            return new BoardingPointData(in);
        }

        @Override
        public BoardingPointData[] newArray(int size) {
            return new BoardingPointData[size];
        }
    };
    private int tripPointId;
    private int locationId;
    private String locationName;
    private int locationType;
    private String locationDate;
    private String locationTime;
    private String locationDescription;

    public BoardingPointData() {
    }

    public BoardingPointData(BoardingPoint boardingPoint) {
        this.tripPointId = boardingPoint.getTripPointId();
        this.locationId = boardingPoint.getLocationId();
        this.locationName = boardingPoint.getLocationName();
        this.locationType = boardingPoint.getLocationType();
        this.locationDate = boardingPoint.getLocationDate();
        this.locationTime = boardingPoint.getLocationTime();
        this.locationDescription = boardingPoint.getLocationDescription();
    }

    public BoardingPointData(BoardingPointItem boardingPointItem) {
        this.tripPointId = boardingPointItem.getTripPointId();
        this.locationId = boardingPointItem.getLocationId();
        this.locationName = boardingPointItem.getLocationName();
        this.locationType = boardingPointItem.getLocationType();
        this.locationDate = boardingPointItem.getLocationDate();
        this.locationTime = boardingPointItem.getLocationTime();
        this.locationDescription = boardingPointItem.getLocationDescription();
    }

    protected BoardingPointData(Parcel in) {
        setTripPointId(in.readInt());
        setLocationId(in.readInt());
        setLocationName(in.readString());
        setLocationType(in.readInt());
        setLocationDate(in.readString());
        setLocationTime(in.readString());
        setLocationDescription(in.readString());
    }

    public BoardingPointData(int tripPointId, int locationId, String locationName, int locationType, String locationDate, String locationTime, String locationDescription) {
        this.tripPointId = tripPointId;
        this.locationId = locationId;
        this.locationName = locationName;
        this.locationType = locationType;
        this.locationDate = locationDate;
        this.locationTime = locationTime;
        this.locationDescription = locationDescription;
    }

    public int getTripPointId() {
        return tripPointId;
    }

    public void setTripPointId(int tripPointId) {
        this.tripPointId = tripPointId;
    }

    public int getLocationId() {
        return locationId;
    }

    public void setLocationId(int locationId) {
        this.locationId = locationId;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public int getLocationType() {
        return locationType;
    }

    public void setLocationType(int locationType) {
        this.locationType = locationType;
    }

    public String getLocationDate() {
        return locationDate;
    }

    public void setLocationDate(String locationDate) {
        this.locationDate = locationDate;
    }

    public String getLocationTime() {
        return locationTime;
    }

    public void setLocationTime(String locationTime) {
        this.locationTime = locationTime;
    }

    public String getLocationDescription() {
        return locationDescription;
    }

    public void setLocationDescription(String locationDescription) {
        this.locationDescription = locationDescription;
    }

    @Override
    public String toString() {
        return "BoardingPointData{" +
                "tripPointId=" + tripPointId +
                ", locationId=" + locationId +
                ", locationName='" + locationName + '\'' +
                ", locationType=" + locationType +
                ", locationDate='" + locationDate + '\'' +
                ", locationTime='" + locationTime + '\'' +
                ", locationDescription='" + locationDescription + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(tripPointId);
        dest.writeInt(locationId);
        dest.writeString(locationName);
        dest.writeInt(locationType);
        dest.writeString(locationDate);
        dest.writeString(locationTime);
        dest.writeString(locationDescription);
    }
}