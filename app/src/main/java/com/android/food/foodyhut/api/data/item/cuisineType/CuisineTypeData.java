package com.android.food.foodyhut.api.data.item.cuisineType;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by sajid on 12/14/2015.
 */
public class CuisineTypeData {

    @SerializedName("status")
    private CusineTypeStatus status;

    @SerializedName("response")
    private List<CuisineTypeResponse> response;

    public CuisineTypeData() {
    }

    public CusineTypeStatus getStatus() {
        return status;
    }

    public void setStatus(CusineTypeStatus status) {
        this.status = status;
    }

    public List<CuisineTypeResponse> getResponse() {
        return response;
    }

    public void setResponse(List<CuisineTypeResponse> response) {
        this.response = response;
    }
}
