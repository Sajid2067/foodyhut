package com.android.food.foodyhut.api.data.item.bookticket;

import com.google.gson.annotations.SerializedName;
import com.android.food.foodyhut.api.data.item.filter.Filter;
import com.android.food.foodyhut.api.data.item.policy.Policy;
import com.android.food.foodyhut.api.data.item.trip.Trip;

/**
 * Created by sajid on 11/3/2015.
 */
public class BookTicketData {

    @SerializedName("query")
    private BookTicketQuery bookTicketQuery;
    @SerializedName("trips")
    private Trip trip;
    @SerializedName("policies")
    private Policy policy;
    @SerializedName("filters")
    private Filter filter;

    public BookTicketData() {
    }

    public BookTicketData(BookTicketQuery bookTicketQuery, Trip trip, Policy policy, Filter filter) {

        this.bookTicketQuery = bookTicketQuery;
        this.trip = trip;
        this.policy = policy;
        this.filter = filter;
    }

    public BookTicketQuery getBookTicketQuery() {

        return bookTicketQuery;
    }

    public void setBookTicketQuery(BookTicketQuery bookTicketQuery) {
        this.bookTicketQuery = bookTicketQuery;
    }

    public Trip getTrip() {
        return trip;
    }

    public void setTrip(Trip trip) {
        this.trip = trip;
    }

    public Policy getPolicy() {
        return policy;
    }

    public void setPolicy(Policy policy) {
        this.policy = policy;
    }

    public Filter getFilter() {
        return filter;
    }

    public void setFilter(Filter filter) {
        this.filter = filter;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BookTicketData)) return false;

        BookTicketData that = (BookTicketData) o;

        if (!getBookTicketQuery().equals(that.getBookTicketQuery())) return false;
        if (!getTrip().equals(that.getTrip())) return false;
        if (!getPolicy().equals(that.getPolicy())) return false;
        return getFilter().equals(that.getFilter());

    }

    @Override
    public int hashCode() {
        int result = getBookTicketQuery().hashCode();
        result = 31 * result + getTrip().hashCode();
        result = 31 * result + getPolicy().hashCode();
        result = 31 * result + getFilter().hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "BookTicketData{" +
                "bookTicketQuery=" + bookTicketQuery +
                ", trip=" + trip +
                ", policy=" + policy +
                ", filter=" + filter +
                '}';
    }
}
