package com.android.food.foodyhut.api.data.item.googlesearchdetailshistory;

import com.google.gson.annotations.SerializedName;

/**
 * Created by sajid on 12/14/2015.
 */
public class GoogleSearchDetailListResponseOpeningHours {

    @SerializedName("open_now")
    private String open_now;
    @SerializedName("weekday_text")
    private String[] weekday_text;

    public GoogleSearchDetailListResponseOpeningHours() {
    }

    public String getOpen_now() {
        return open_now;
    }

    public void setOpen_now(String open_now) {
        this.open_now = open_now;
    }

    public String[] getWeekday_text() {
        return weekday_text;
    }

    public void setWeekday_text(String[] weekday_text) {
        this.weekday_text = weekday_text;
    }
}
