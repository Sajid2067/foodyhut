package com.android.food.foodyhut.api.data.item.coupon;

import com.google.gson.annotations.SerializedName;

/**
 * Created by sajid on 12/14/2015.
 */
public class Data {
    @SerializedName("amount")
    private String amount;
    @SerializedName("message")
    private String message;
    @SerializedName("discount")
    private String discount;
    @SerializedName("shohoz_discount")
    private String shohoz_discount;
    @SerializedName("operator_discount")
    private String operator_discount;
    @SerializedName("tp_discount")
    private String tp_discount;

    public Data() {
    }

    public Data(String amount, String message, String discount, String shohoz_discount, String operator_discount, String tp_discount) {
        this.amount = amount;
        this.message = message;
        this.discount = discount;
        this.shohoz_discount = shohoz_discount;
        this.operator_discount = operator_discount;
        this.tp_discount = tp_discount;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getShohoz_discount() {
        return shohoz_discount;
    }

    public void setShohoz_discount(String shohoz_discount) {
        this.shohoz_discount = shohoz_discount;
    }

    public String getOperator_discount() {
        return operator_discount;
    }

    public void setOperator_discount(String operator_discount) {
        this.operator_discount = operator_discount;
    }

    public String getTp_discount() {
        return tp_discount;
    }

    public void setTp_discount(String tp_discount) {
        this.tp_discount = tp_discount;
    }

    @Override
    public String toString() {
        return "Data{" +
                "amount='" + amount + '\'' +
                ", message='" + message + '\'' +
                ", discount='" + discount + '\'' +
                ", shohoz_discount='" + shohoz_discount + '\'' +
                ", operator_discount='" + operator_discount + '\'' +
                ", tp_discount='" + tp_discount + '\'' +
                '}';
    }

    //    public Data(String pnr, String message) {
//        this.pnr = pnr;
//        this.message = message;
//    }
//
//    public String getPnr() {
//        return pnr;
//    }
//
//    public void setPnr(String pnr) {
//        this.pnr = pnr;
//    }
//
//    public String getMessage() {
//
//        return message;
//    }
//
//    public void setMessage(String message) {
//        this.message = message;
//    }
//
//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (!(o instanceof Data)) return false;
//
//        Data data = (Data) o;
//
//        if (!getPnr().equals(data.getPnr())) return false;
//        return getMessage().equals(data.getMessage());
//
//    }
//
//    @Override
//    public int hashCode() {
//        int result = getPnr().hashCode();
//        result = 31 * result + getMessage().hashCode();
//        return result;
//    }
//
//    @Override
//    public String toString() {
//        return "Data{" +
//                "pnr='" + pnr + '\'' +
//                ", message='" + message + '\'' +
//                '}';
//    }
}
