package com.android.food.foodyhut.fragment;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.android.food.foodyhut.R;
import com.android.food.foodyhut.activity.listener.OnFragmentInteractionListener;
import com.android.food.foodyhut.application.AppController;
import com.android.food.foodyhut.fragment.item.CancelTicketRequestData;

import java.util.ArrayList;

/**
 * Created by sajid on 10/30/2015.
 */
public class CancelTicketFragment extends BaseFragment {
    Tracker mTracker;
    private Button getCancelTicketButton;
    private Button getCalcellationRuleButton;
    private EditText phnNumberEditText;
    private EditText PNREditText;
    private CancelTicketRequestData cancelTicketRequestData;
    private static final String VALID_MOBILE_REG_EX = "^01(1|5|6|7|8|9)\\d{8}$";


    public static CancelTicketFragment newInstance(ArrayList<Integer> previousFragmentIds, Parcelable parcelable) {
        CancelTicketFragment cancelTicketFragment = new CancelTicketFragment();
        Bundle bundle = new Bundle();
        bundle.putIntegerArrayList(PREVIOUS_FRAGMENT_ID_TAG, previousFragmentIds);
        bundle.putParcelable(PARCELABLE_FRAGMENT_ITEM_ID_TAG, parcelable);
        cancelTicketFragment.setArguments(bundle);
        return cancelTicketFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Obtain the shared Tracker instance.
        if (parcelable instanceof CancelTicketRequestData) {
            cancelTicketRequestData = (CancelTicketRequestData) parcelable;
            PNREditText.setText(cancelTicketRequestData.getPnr());
            phnNumberEditText.setText(cancelTicketRequestData.getMobile());
        }


        AppController application = (AppController) getActivity().getApplication();
        mTracker = application.getDefaultTracker();
        mTracker.setScreenName("CancelTicket");
        // Send a screen view.
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.fragment_cancel_ticket, container, false);
        }

        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    void initializeEditTextComponents() {
        PNREditText = (EditText) rootView.findViewById(R.id.ticket_pnr_edit_text);
        phnNumberEditText = (EditText) rootView.findViewById(R.id.mobile_number_edit_text);

    }

    @Override
    void initializeButtonComponents() {
        getCancelTicketButton = (Button) rootView.findViewById(R.id.getCancelTickets);
        getCalcellationRuleButton = (Button) rootView.findViewById(R.id.cancellation_policy_button);

        getCancelTicketButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String mobile = phnNumberEditText.getText().toString().trim();
                String pnr = PNREditText.getText().toString().trim();

                if (phnNumberEditText.getText().toString().isEmpty() || PNREditText.getText().toString().isEmpty()) {
                    Toast.makeText(getActivity(), "Please fill up those informations", Toast.LENGTH_LONG).show();
                } else if (!phnNumberEditText.getText().toString().trim().matches(VALID_MOBILE_REG_EX)) {
                    Toast.makeText(getActivity(), "Please enter a valid mobile number", Toast.LENGTH_LONG).show();
                } else {
                    CancelTicketRequestData cancelTicketRequestData = new CancelTicketRequestData(pnr, mobile);
                    previousFragmentIds.add(OnFragmentInteractionListener.CANCEL_TICKET_FRAGMENT_ID);
                    onFragmentChangeCallListener.fragmentChange(OnFragmentInteractionListener.CANCEL_TICKET_DETAILS_FRAGMENT_ID, cancelTicketRequestData, previousFragmentIds);
                }

            }
        });
    }

    @Override
    void initializeTextViewComponents() {

    }

    @Override
    void initializeOtherViewComponents() {

    }

    @Override
    void initializeOnclickListener() {

    }

    @Override
    void removeOnclickListener() {

    }
}
