package com.android.food.foodyhut.api.data.item.bookticket;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by sajid on 12/14/2015.
 */
public class BookTicketError {
    @SerializedName("code")
    private int code;
    @SerializedName("messages")
    private ArrayList<String> messages;


    public BookTicketError() {
    }

    public BookTicketError(int code, ArrayList<String> messages) {

        this.code = code;
        this.messages = messages;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public ArrayList<String> getMessages() {
        return messages;
    }

    public void setMessages(ArrayList<String> messages) {
        this.messages = messages;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BookTicketError)) return false;

        BookTicketError that = (BookTicketError) o;

        if (getCode() != that.getCode()) return false;
        return getMessages().equals(that.getMessages());

    }

    @Override
    public int hashCode() {
        int result = getCode();
        result = 31 * result + getMessages().hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "BookTicketError{" +
                "code=" + code +
                ", messages=" + messages +
                '}';
    }
}
