package com.android.food.foodyhut.activity.annotation;

import android.support.annotation.IntDef;

import com.android.food.foodyhut.activity.listener.OnFragmentInteractionListener;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;


@IntDef({OnFragmentInteractionListener.NULL_FRAGMENT_ID,
        OnFragmentInteractionListener.LOCATION_FRAGMENT_FH,
        OnFragmentInteractionListener.CALENDER_FRAGMENT_ID,
        OnFragmentInteractionListener.CUISINE_TYPE_FRAGMENT_ID,
        OnFragmentInteractionListener.BOOK_TICKET_FRAGMENT_ID,
        OnFragmentInteractionListener.TOP_SEARCH_FRAGMENT_ID,
        OnFragmentInteractionListener.TERMS_AND_CONDITION_FRAGMENT_ID,
        OnFragmentInteractionListener.CANCEL_TICKET_FRAGMENT_ID,
        OnFragmentInteractionListener.OFFERS_FRAGMENT_ID,
        OnFragmentInteractionListener.SEAT_LAYOUT_FRAGMENT_ID,
        OnFragmentInteractionListener.SEAT_LAYOUT_FULL_FRAGMENT_ID,
        OnFragmentInteractionListener.BOARDING_POINT_FRAGMENT_ID,
        OnFragmentInteractionListener.PASSENGER_INFO_FRAGMENT_ID,
        OnFragmentInteractionListener.PAYMENT_DETAILS_FRAGMENT_ID,
        OnFragmentInteractionListener.PAYMENT_DETAILS_COD_FRAGMENT_ID,
        OnFragmentInteractionListener.FILTER_FRAGMENT_ID,
        OnFragmentInteractionListener.TICKET_DETAILS_FRAGMENT_ID,
        OnFragmentInteractionListener.TICKET_BUY_SUCCESS_FRAGMENT_ID,
        OnFragmentInteractionListener.BKASH_PAYMENT_VERIFICATION_FRAGMENT_ID,
        OnFragmentInteractionListener.CARD_DETAILS_FRAGMENT_ID,
        OnFragmentInteractionListener.INTERNET_BANKING_DETAILS_FRAGMENT_ID,
        OnFragmentInteractionListener.SEARCH_RESULT_CONTAINER_FRAGMENT_ID_FH,
        OnFragmentInteractionListener.TICKET_SEARCH_FRAGMENT_ID,
        OnFragmentInteractionListener.CANCEL_TICKET_DETAILS_FRAGMENT_ID,
        OnFragmentInteractionListener.MAP_FRAGMENT_ID,
        OnFragmentInteractionListener.TIMELINE_VISIT_FAVOURITE_FRAGMENT_ID,
        OnFragmentInteractionListener.TIMELINE_FRAGMENT_ID,
        OnFragmentInteractionListener.VISIT_FRAGMENT_ID,
        OnFragmentInteractionListener.FAVOURITE_FRAGMENT_ID,
        OnFragmentInteractionListener.ACCOUNT_FRAGMENT_ID
})
@Retention(RetentionPolicy.SOURCE)
public @interface FragmentId {
}
