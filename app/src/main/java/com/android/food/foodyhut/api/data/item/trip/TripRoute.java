package com.android.food.foodyhut.api.data.item.trip;

import com.google.gson.annotations.SerializedName;

/**
 * Created by sajid on 11/3/2015.
 */
public class TripRoute {

    @SerializedName("trip_route_id")
    private String tripRouteId;
    @SerializedName("trip_id")
    private String tripId;
    @SerializedName("company_id")
    private String companyId;
    @SerializedName("route_id")
    private String routeId;
    @SerializedName("origin_city_id")
    private String originCityId;
    @SerializedName("destination_city_id")
    private String destinationCityId;
    @SerializedName("parent_trip_route_id")
    private String parentTripRouteId;
    @SerializedName("business_class_fare")
    private float businessClassFare;
    @SerializedName("economy_class_fare")
    private float economyClassFare;
    @SerializedName("special_class_fare")
    private float specialClassFare;
    @SerializedName("departure_date")
    private String departureDate;
    @SerializedName("departure_time")
    private String departureTime;
    @SerializedName("departure_date_time")
    private String departureDateTime;
    @SerializedName("arrival_date")
    private String arrivalDate;
    @SerializedName("arrival_time")
    private String arrivalTime;
    @SerializedName("arrival_date_time")
    private String arrivalDateTime;
    @SerializedName("available_till_datetime")
    private String availableTillDatetime;
    @SerializedName("discount")
    private float discount;

    public TripRoute() {
    }

    public TripRoute(String tripRouteId, String tripId, String companyId, String routeId, String originCityId, String destinationCityId, String parentTripRouteId, float businessClassFare, float economyClassFare, float specialClassFare, String departureDate, String departureDateTime, String arrivalDate, String arrivalTime, String arrivalDateTime, String availableTillDatetime, float discount) {

        this.tripRouteId = tripRouteId;
        this.tripId = tripId;
        this.companyId = companyId;
        this.routeId = routeId;
        this.originCityId = originCityId;
        this.destinationCityId = destinationCityId;
        this.parentTripRouteId = parentTripRouteId;
        this.businessClassFare = businessClassFare;
        this.economyClassFare = economyClassFare;
        this.specialClassFare = specialClassFare;
        this.departureDate = departureDate;
        this.departureDateTime = departureDateTime;
        this.arrivalDate = arrivalDate;
        this.arrivalTime = arrivalTime;
        this.arrivalDateTime = arrivalDateTime;
        this.availableTillDatetime = availableTillDatetime;
        this.discount = discount;
    }

    public String getTripRouteId() {

        return tripRouteId;
    }

    public void setTripRouteId(String tripRouteId) {
        this.tripRouteId = tripRouteId;
    }

    public String getTripId() {
        return tripId;
    }

    public void setTripId(String tripId) {
        this.tripId = tripId;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getRouteId() {
        return routeId;
    }

    public void setRouteId(String routeId) {
        this.routeId = routeId;
    }

    public String getOriginCityId() {
        return originCityId;
    }

    public void setOriginCityId(String originCityId) {
        this.originCityId = originCityId;
    }

    public String getDestinationCityId() {
        return destinationCityId;
    }

    public void setDestinationCityId(String destinationCityId) {
        this.destinationCityId = destinationCityId;
    }

    public String getParentTripRouteId() {
        return parentTripRouteId;
    }

    public void setParentTripRouteId(String parentTripRouteId) {
        this.parentTripRouteId = parentTripRouteId;
    }

    public float getBusinessClassFare() {
        return businessClassFare;
    }

    public void setBusinessClassFare(float businessClassFare) {
        this.businessClassFare = businessClassFare;
    }

    public float getEconomyClassFare() {
        return economyClassFare;
    }

    public void setEconomyClassFare(float economyClassFare) {
        this.economyClassFare = economyClassFare;
    }

    public float getSpecialClassFare() {
        return specialClassFare;
    }

    public void setSpecialClassFare(float specialClassFare) {
        this.specialClassFare = specialClassFare;
    }

    public String getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(String departureDate) {
        this.departureDate = departureDate;
    }

    public String getDepartureDateTime() {
        return departureDateTime;
    }

    public void setDepartureDateTime(String departureDateTime) {
        this.departureDateTime = departureDateTime;
    }

    public String getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(String arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    public String getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(String arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public String getArrivalDateTime() {
        return arrivalDateTime;
    }

    public void setArrivalDateTime(String arrivalDateTime) {
        this.arrivalDateTime = arrivalDateTime;
    }

    public String getAvailableTillDatetime() {
        return availableTillDatetime;
    }

    public void setAvailableTillDatetime(String availableTillDatetime) {
        this.availableTillDatetime = availableTillDatetime;
    }

    public float getDiscount() {
        return discount;
    }

    public String getDepartureTime() {
        return departureTime;
    }

    public void setDiscount(float discount) {
        this.discount = discount;
    }

    public void setDepartureTime(String departureTime) {
        this.departureTime = departureTime;
    }

    @Override
    public String toString() {
        return "TripRoute{" +
                "tripRouteId='" + tripRouteId + '\'' +
                ", tripId='" + tripId + '\'' +
                ", companyId='" + companyId + '\'' +
                ", routeId='" + routeId + '\'' +
                ", originCityId='" + originCityId + '\'' +
                ", destinationCityId='" + destinationCityId + '\'' +
                ", parentTripRouteId='" + parentTripRouteId + '\'' +
                ", businessClassFare=" + businessClassFare +
                ", economyClassFare=" + economyClassFare +
                ", specialClassFare=" + specialClassFare +
                ", departureTime='" + departureTime + '\'' +
                ", departureDate='" + departureDate + '\'' +
                ", departureDateTime='" + departureDateTime + '\'' +
                ", arrivalDate='" + arrivalDate + '\'' +
                ", arrivalTime='" + arrivalTime + '\'' +
                ", arrivalDateTime='" + arrivalDateTime + '\'' +
                ", availableTillDatetime='" + availableTillDatetime + '\'' +
                ", discount=" + discount +
                '}';
    }
}
