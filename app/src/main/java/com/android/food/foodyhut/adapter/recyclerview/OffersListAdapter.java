package com.android.food.foodyhut.adapter.recyclerview;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.android.food.foodyhut.R;
import com.android.food.foodyhut.api.data.item.handshake.offer.ShohozOffer;
import com.android.food.foodyhut.application.AppController;
import com.android.food.foodyhut.util.FrequentFunction;

import net.nightwhistler.htmlspanner.HtmlSpanner;

import java.util.List;

/**
 * Created by sajid on 10/28/2015.
 */
public class OffersListAdapter extends RecyclerView.Adapter<OffersListAdapter.ViewHolder> {

    private Context context;
    private List<ShohozOffer> shohozOffers;
    private LayoutInflater layoutInflater;
    private AppController appController = AppController.getInstance();

    public OffersListAdapter(Context context, List<ShohozOffer> shohozOffers) {
        this.context = context;
        this.shohozOffers = shohozOffers;
        layoutInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView;
        rootView = layoutInflater.inflate(R.layout.list_item_offer, parent, false);
        return new ViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ShohozOffer shohozOffer = shohozOffers.get(position);
        if (getItemCount() - 1 == position) {
            holder.listDivider.setVisibility(View.GONE);
        } else {
            holder.listDivider.setVisibility(View.VISIBLE);
        }
        holder.offersBodyTextView.setText((new HtmlSpanner()).fromHtml(shohozOffer.getBody()));//Html
        FrequentFunction.convertHtml(holder.offersTitleTextView,shohozOffer.getTitle());
       // holder.offersTitleTextView.setText(Html.fromHtml(shohozOffer.getTitle()));
        Glide.with(appController).
                load(shohozOffer.getImageUrl()).
                into(holder.offersImageView);
    }

    @Override
    public int getItemCount() {
        return shohozOffers.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private TextView offersTitleTextView;
        private TextView offersBodyTextView;
        private ImageView offersImageView;
        private View listDivider;

        public ViewHolder(View itemView) {
            super(itemView);
            offersTitleTextView = (TextView) itemView.findViewById(R.id.offers_title_text_view);
            offersBodyTextView = (TextView) itemView.findViewById(R.id.offers_body_text_view);
            offersImageView = (ImageView) itemView.findViewById(R.id.offers_image_view);
            listDivider = (View) itemView.findViewById(R.id.list_divider);
        }
    }
}
