package com.android.food.foodyhut.fragment;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.android.food.foodyhut.R;
import com.android.food.foodyhut.activity.annotation.FragmentId;
import com.android.food.foodyhut.adapter.recyclerview.BoardingPointListAdapter;
import com.android.food.foodyhut.adapter.recyclerview.item.BoardingPointItem;
import com.android.food.foodyhut.adapter.recyclerview.item.RecyclerViewBaseItem;
import com.android.food.foodyhut.application.AppController;
import com.android.food.foodyhut.fragment.item.BoardingPointData;
import com.android.food.foodyhut.fragment.item.SeatLayoutData;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sajid on 11/6/2015.
 */
public class BoardingPointFragment extends BaseFragment implements RecyclerView.OnItemTouchListener, TextWatcher {

    private static final String TAG = BoardingPointFragment.class.getSimpleName();
    Tracker mTracker;
    private RecyclerView boardingPointRecyclerView;
    private LinearLayoutManager linearLayoutManager;
    private BoardingPointListAdapter boardingPointListAdapter;
    private GestureDetector mGestureDetector;
    private TextView noResultTextView;
    private EditText boardingPointSearchEditText;

    private List<RecyclerViewBaseItem> recyclerViewBaseItems;

    private SeatLayoutData seatLayoutData;

    public static BoardingPointFragment newInstance(ArrayList<Integer> previousFragmentIds, Parcelable parcelable) {
        BoardingPointFragment boardingPointFragment = new BoardingPointFragment();
        Bundle bundle = new Bundle();
        bundle.putIntegerArrayList(PREVIOUS_FRAGMENT_ID_TAG, previousFragmentIds);
        bundle.putParcelable(PARCELABLE_FRAGMENT_ITEM_ID_TAG, parcelable);
        boardingPointFragment.setArguments(bundle);
        return boardingPointFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (parcelable instanceof SeatLayoutData) {
            seatLayoutData = (SeatLayoutData) parcelable;
            recyclerViewBaseItems = new ArrayList<>();
            for (Parcelable boardingPointData : seatLayoutData.getBoardingPointDataList()) {
                recyclerViewBaseItems.add(new BoardingPointItem((BoardingPointData) boardingPointData));
            }


        }
        // Obtain the shared Tracker instance.
        AppController application = (AppController) getActivity().getApplication();
        mTracker = application.getDefaultTracker();
        mTracker.setScreenName("Boarding Point");
        // Send a screen view.
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.fragment_add_boarding_point, container, false);
        }

        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    void initializeEditTextComponents() {
        boardingPointSearchEditText = findViewById(R.id.boarding_point_search_edit_text);
        boardingPointSearchEditText.addTextChangedListener(this);

    }

    @Override
    void initializeButtonComponents() {

    }

    @Override
    void initializeTextViewComponents() {
        noResultTextView = findViewById(R.id.no_result_text_view);
    }

    @Override
    void initializeOtherViewComponents() {
        boardingPointRecyclerView = (RecyclerView) rootView.findViewById(R.id.boarding_point_recycler_view);
        linearLayoutManager = new LinearLayoutManager(getContext());
        boardingPointListAdapter = new BoardingPointListAdapter(getActivity(), recyclerViewBaseItems, noResultTextView);
        boardingPointRecyclerView.setAdapter(boardingPointListAdapter);
        boardingPointRecyclerView.setLayoutManager(linearLayoutManager);
        boardingPointRecyclerView.setHasFixedSize(true);
        boardingPointRecyclerView.addOnItemTouchListener(this);
        mGestureDetector = new GestureDetector(getActivity(), new GestureDetector.SimpleOnGestureListener() {

            @Override
            public boolean onSingleTapUp(MotionEvent e) {
                return true;
            }
        });
    }

    @Override
    void initializeOnclickListener() {

    }

    @Override
    void removeOnclickListener() {

    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView recyclerView, MotionEvent motionEvent) {
        if (recyclerView.equals(boardingPointRecyclerView)) {
            View child = recyclerView.findChildViewUnder(motionEvent.getX(), motionEvent.getY());
            if (child != null && mGestureDetector.onTouchEvent(motionEvent)) {
                int position = recyclerView.getChildAdapterPosition(child);

                BoardingPointData boardingPointData = new BoardingPointData((BoardingPointItem) boardingPointListAdapter.getItem(position));
                seatLayoutData.setSelectedBoardingPoint(boardingPointData);
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(rootView.getWindowToken(), 0);
                @FragmentId
                int previousFragmentId = previousFragmentIds.remove(previousFragmentIds.size() - 1);
                onFragmentChangeCallListener.fragmentChange(previousFragmentId, seatLayoutData, previousFragmentIds);
            }
        }
        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {

    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        Log.e(TAG, s.toString());
        boardingPointListAdapter.getFilter().filter(s);
    }

    @Override
    public void afterTextChanged(Editable s) {

    }
}
