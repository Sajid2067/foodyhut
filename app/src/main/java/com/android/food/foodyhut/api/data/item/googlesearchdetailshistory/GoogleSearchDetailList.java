package com.android.food.foodyhut.api.data.item.googlesearchdetailshistory;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by sajid on 11/3/2015.
 */
public class GoogleSearchDetailList {

    @SerializedName("status")
    private GoogleSearchDetailListStatus status;
    @SerializedName("response")
    private List<GoogleSearchDetailListResponse> response;

    public GoogleSearchDetailListStatus getStatus() {
        return status;
    }

    public void setStatus(GoogleSearchDetailListStatus status) {
        this.status = status;
    }

    public List<GoogleSearchDetailListResponse> getResponse() {
        return response;
    }

    public void setResponse(List<GoogleSearchDetailListResponse> response) {
        this.response = response;
    }
}
