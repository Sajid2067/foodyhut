package com.android.food.foodyhut.api.data.item.googlesearchdetailshistory;

import com.google.gson.annotations.SerializedName;

/**
 * Created by sajid on 12/14/2015.
 */
public class GoogleSearchDetailListResponse {

    @SerializedName("address")
    private String address;
    @SerializedName("geoLocation")
    private GoogleSearchDetailListResponseGeo geoLocation;
    @SerializedName("name")
    private String name;
    @SerializedName("openingHours")
    private GoogleSearchDetailListResponseOpeningHours openingHours;
    @SerializedName("rating")
    private String rating;
    @SerializedName("placeId")
    private String placeId;

    public GoogleSearchDetailListResponse() {
    }

    public String getPlaceId() {
        return placeId;
    }

    public void setPlaceId(String placeId) {
        this.placeId = placeId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public GoogleSearchDetailListResponseGeo getGeoLocation() {
        return geoLocation;
    }

    public void setGeoLocation(GoogleSearchDetailListResponseGeo geoLocation) {
        this.geoLocation = geoLocation;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public GoogleSearchDetailListResponseOpeningHours getOpeningHours() {
        return openingHours;
    }

    public void setOpeningHours(GoogleSearchDetailListResponseOpeningHours openingHours) {
        this.openingHours = openingHours;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }
}
