package com.android.food.foodyhut.fragment.item;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by sajid on 11/7/2015.
 */
public class PaymentDetailsData implements Parcelable {

    public static final Creator<PaymentDetailsData> CREATOR = new Creator<PaymentDetailsData>() {
        @Override
        public PaymentDetailsData createFromParcel(Parcel in) {
            return new PaymentDetailsData(in);
        }

        @Override
        public PaymentDetailsData[] newArray(int size) {
            return new PaymentDetailsData[size];
        }
    };
    private List<String> fullName;
    private List<String> age;
    private List<String> passport;
    private List<String> gender;
    private String contactPersonName;
    private String mobileNumber;
    private String email;
    private SeatLayoutData seatLayoutData;

    public PaymentDetailsData() {
    }

    public PaymentDetailsData(List<String> fullName, List<String> age, List<String> gender, String contactPersonName, String mobileNumber, String email, SeatLayoutData seatLayoutData) {
        this.fullName = fullName;
        this.age = age;
        this.gender = gender;
        this.contactPersonName = contactPersonName;
        this.mobileNumber = mobileNumber;
        this.email = email;
        this.seatLayoutData = seatLayoutData;
    }

    protected PaymentDetailsData(Parcel in) {
        fullName = in.createStringArrayList();
        age = in.createStringArrayList();
        gender = in.createStringArrayList();
        contactPersonName = in.readString();
        mobileNumber = in.readString();
        email = in.readString();
        seatLayoutData = in.readParcelable(SeatLayoutData.class.getClassLoader());
        passport = in.createStringArrayList();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public List<String> getFullName() {
        return fullName;
    }

    public void setFullName(List<String> fullName) {
        this.fullName = fullName;
    }

    public List<String> getAge() {
        return age;
    }

    public void setAge(List<String> age) {
        this.age = age;
    }

    public List<String> getGender() {
        return gender;
    }

    public void setGender(List<String> gender) {
        this.gender = gender;
    }

    public String getContactPersonName() {
        return contactPersonName;
    }

    public void setContactPersonName(String contactPersonName) {
        this.contactPersonName = contactPersonName;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public SeatLayoutData getSeatLayoutData() {
        return seatLayoutData;
    }

    public void setSeatLayoutData(SeatLayoutData seatLayoutData) {
        this.seatLayoutData = seatLayoutData;
    }

    public List<String> getPassport() {
        return passport;
    }

    public void setPassport(List<String> passport) {
        this.passport = passport;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PaymentDetailsData)) return false;

        PaymentDetailsData that = (PaymentDetailsData) o;

        if (!getFullName().equals(that.getFullName())) return false;
        if (!getAge().equals(that.getAge())) return false;
        if (!getPassport().equals(that.getPassport())) return false;
        if (!getGender().equals(that.getGender())) return false;
        if (!getContactPersonName().equals(that.getContactPersonName())) return false;
        if (!getMobileNumber().equals(that.getMobileNumber())) return false;
        if (!getEmail().equals(that.getEmail())) return false;
        return getSeatLayoutData().equals(that.getSeatLayoutData());

    }

    @Override
    public int hashCode() {
        int result = getFullName().hashCode();
        result = 31 * result + getAge().hashCode();
        result = 31 * result + getPassport().hashCode();
        result = 31 * result + getGender().hashCode();
        result = 31 * result + getContactPersonName().hashCode();
        result = 31 * result + getMobileNumber().hashCode();
        result = 31 * result + getEmail().hashCode();
        result = 31 * result + getSeatLayoutData().hashCode();
        return result;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeStringList(fullName);
        dest.writeStringList(age);
        dest.writeStringList(passport);
        dest.writeStringList(gender);
        dest.writeString(contactPersonName);
        dest.writeString(mobileNumber);
        dest.writeString(email);
        dest.writeParcelable(seatLayoutData, flags);
    }

    @Override
    public String toString() {
        return "PaymentDetailsData{" +
                "fullName=" + fullName +
                ", passport=" + passport +
                ", gender=" + gender +
                ", contactPersonName='" + contactPersonName + '\'' +
                ", mobileNumber='" + mobileNumber + '\'' +
                ", email='" + email + '\'' +
                ", seatLayoutData=" + seatLayoutData +
                '}';
    }
}
