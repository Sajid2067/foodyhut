package com.android.food.foodyhut.util;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.android.food.foodyhut.api.data.item.BoardingPoint;
import com.android.food.foodyhut.api.data.item.bookticket.BookTicket;
import com.android.food.foodyhut.api.data.item.bus.Operator;
import com.android.food.foodyhut.api.data.item.trip.TripList;
import com.android.food.foodyhut.fragment.item.FilterData;

import java.util.List;

/**
 * Created by sajid on 12/7/2015.
 */
public class FilterBookTicketData {
    private FilterData filterData;

    private BookTicket bookTicket;

    public FilterBookTicketData(FilterData filterData) {
        this.filterData = filterData;
        Gson gson = new GsonBuilder().create();
        bookTicket = gson.fromJson(filterData.getBookTicketAsJson(), BookTicket.class);
        if (filterData.getTime() != null && filterData.getTime().length() != 0) {
            filterByTime();
        }
        if (filterData.getBusType() != -1) {
            filterByBusType();
        }
        if (filterData.getBoardingPoint() != null & filterData.getBoardingPoint().length() != 0) {
            filterByBoardingPoint();
        }
        if (filterData.getOperator() != null && filterData.getOperator().length() != 0) {
            filterByOperator();
        }
    }

    public FilterData getFilterData() {
        return filterData;
    }

    public void setFilterData(FilterData filterData) {
        this.filterData = filterData;
        if (filterData.getTime().length() != 0) {
            filterByTime();
        }
        if (filterData.getBusType() != -1) {
            filterByBusType();
        }
        if (filterData.getBoardingPoint().length() != 0) {
            filterByBoardingPoint();
        }
        if (filterData.getOperator().length() != 0) {
            filterByOperator();
        }
    }

    private void filterByOperator() {
        Gson gson = new GsonBuilder().create();
        Operator operator = gson.fromJson(filterData.getOperator(), Operator.class);
        List<TripList> tripLists = bookTicket.getBookTicketData().getTrip().getTripLists();
        Log.e("BE filterByOperator()", tripLists.size() + " ");
        for (int i = 0; i < tripLists.size(); i++) {
            Log.e("TAG " + i, tripLists.get(i).getOperator().getCompanyName());
            if (!tripLists.get(i).getOperator().getCompanyName().equals(operator.getCompanyName())) {
                Log.e("TAG REMOVE", tripLists.get(i).getOperator().getCompanyName() + " " + operator.getCompanyName());
                tripLists.remove(i);
                i -= 1;
            } else {
                Log.e("TAG", tripLists.get(i).getOperator().getCompanyName() + " " + operator.getCompanyName());
            }
        }
        bookTicket.getBookTicketData().getTrip().setTripLists(tripLists);
        Log.e("AF filterByOperator()", tripLists.size() + " ");
    }

    private void filterByBoardingPoint() {
        Gson gson = new GsonBuilder().create();
        BoardingPoint boardingPoint = gson.fromJson(filterData.getBoardingPoint(), BoardingPoint.class);
        List<TripList> tripLists = bookTicket.getBookTicketData().getTrip().getTripLists();
        for (int i = 0; i < tripLists.size(); i++) {
            List<BoardingPoint> boardingPoints = tripLists.get(i).getBoardingPoints();
            boolean hasLocation = false;
            for (int j = 0; j < boardingPoints.size(); j++) {
                if (boardingPoints.get(j).getLocationName().equals(boardingPoint.getLocationName())) {
                    hasLocation = true;
                }
            }
            if (!hasLocation) {
                tripLists.remove(i);
                i -= 1;
            }
        }
        bookTicket.getBookTicketData().getTrip().setTripLists(tripLists);
        Log.e("filterByBoardingPoint()", tripLists.size() + " ");
    }

    private void filterByBusType() {
        List<TripList> tripLists = bookTicket.getBookTicketData().getTrip().getTripLists();
        for (int i = 0; i < tripLists.size(); i++) {
            if (tripLists.get(i).getBus().getBusType() != filterData.getBusType()) {
                tripLists.remove(i);
                i -= 1;
            }
        }
        bookTicket.getBookTicketData().getTrip().setTripLists(tripLists);
    }

    private static final String MORNING_FILTER = "06:00 AM- 11:59 AM";
    private static final String NOON_FILTER = "12:00 PM - 05:59 PM";
    private static final String EVENING_FILTER = "06:00 PM - 11:59 PM";
    private static final String NIGHT_FILTER = "12:00 AM - 05:59 AM";

    private void filterByTime() {
        List<TripList> tripLists = bookTicket.getBookTicketData().getTrip().getTripLists();
        for (int i = 0; i < tripLists.size(); i++) {
            String tripTime;
            String subTime[] = tripLists.get(i).getTripDetail().getTripOriginTime().split(":");
            String time = subTime[0];
            switch (filterData.getTime()) {
                case MORNING_FILTER:
                    if (!(Integer.parseInt(time) >= 6 && Integer.parseInt(time) < 12)) {
                        tripLists.remove(i);
                        i -= 1;
                    }
                    break;
                case NOON_FILTER:
                    if (!(Integer.parseInt(time) >= 12 && Integer.parseInt(time) < 18)) {
                        tripLists.remove(i);
                        i -= 1;
                    }
                    break;
                case EVENING_FILTER:
                    if (!(Integer.parseInt(time) >= 18 && Integer.parseInt(time) <= 23)) {
                        tripLists.remove(i);
                        i -= 1;
                    }
                    break;
                case NIGHT_FILTER:
                    if (!(Integer.parseInt(time) >= 0 && Integer.parseInt(time) < 6)) {
                        tripLists.remove(i);
                        i -= 1;
                    }
                    break;

            }
        }
        bookTicket.getBookTicketData().getTrip().setTripLists(tripLists);
    }

    public BookTicket getBookTicket() {
        return bookTicket;
    }

    public void setBookTicket(BookTicket bookTicket) {
        this.bookTicket = bookTicket;
    }

    @Override
    public String toString() {
        return "FilterBookTicketData{" +
                "filterData=" + filterData +
                ", bookTicket=" + bookTicket +
                '}';
    }
}
