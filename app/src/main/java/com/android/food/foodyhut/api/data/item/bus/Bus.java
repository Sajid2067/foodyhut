package com.android.food.foodyhut.api.data.item.bus;

import com.google.gson.annotations.SerializedName;

/**
 * Created by sajid on 11/3/2015.
 */
public class Bus {

    @SerializedName("bus_make")
    private String busCompany;
    @SerializedName("bus_model")
    private String busModel;
    @SerializedName("bus_type")
    private int busType;

    public Bus() {
    }

    public Bus(String busCompany, String busModel, int busType) {
        this.busCompany = busCompany;
        this.busModel = busModel;
        this.busType = busType;
    }

    public String getBusCompany() {
        return busCompany;
    }

    public void setBusCompany(String busCompany) {
        this.busCompany = busCompany;
    }

    public String getBusModel() {
        return busModel;
    }

    public void setBusModel(String busModel) {
        this.busModel = busModel;
    }

    public int getBusType() {
        return busType;
    }

    public void setBusType(int busType) {
        this.busType = busType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Bus)) return false;

        Bus bus = (Bus) o;

        if (getBusType() != bus.getBusType()) return false;
        if (!getBusCompany().equals(bus.getBusCompany())) return false;
        return getBusModel().equals(bus.getBusModel());

    }

    @Override
    public int hashCode() {
        int result = getBusCompany().hashCode();
        result = 31 * result + getBusModel().hashCode();
        result = 31 * result + getBusType();
        return result;
    }

    @Override
    public String toString() {
        return "Bus{" +
                "busCompany='" + busCompany + '\'' +
                ", busModel='" + busModel + '\'' +
                ", busType=" + busType +
                '}';
    }
}
