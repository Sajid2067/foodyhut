package com.android.food.foodyhut.adapter.spinner;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.food.foodyhut.R;
import com.android.food.foodyhut.database.data.item.CibDataItem;
import com.android.food.foodyhut.database.data.source.CibDataSource;
import com.android.food.foodyhut.util.Constant;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Droid on 02-Apr-16.
 */
public class InternetBankingAdapter extends BaseAdapter {
    private Activity activity;
    private LayoutInflater inflater;
    private List<CibDataItem> bankNames;
    ArrayList<CibDataItem> cibDataItemList;
    CibDataSource cibDataSource;

    public InternetBankingAdapter(Activity activity, List<CibDataItem> bankNames) {
        this.activity = activity;
        this.bankNames = bankNames;
        cibDataSource = new CibDataSource(activity);
        cibDataSource.open();
        cibDataItemList = cibDataSource.getAllCibDataItem(Constant.CONST_CIB_TYPE_INTERNET_BANKING);
    }

    @Override
    public int getCount() {
        return cibDataItemList.size();
    }

    @Override
    public Object getItem(int position) {
        return cibDataItemList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.spinner_item_row, null);


//        Log.e("SPINNER ADAPTER>>", "cibDataItemArrayList: " + cibDataItemArrayList.toString());

//        CibDataItem items = bankNames.get(position);

        TextView tvSpinner = (TextView) convertView.findViewById(R.id.tvSpinner);

        tvSpinner.setText(cibDataItemList.get(position).getCibName());
        cibDataSource.close();

        return convertView;
    }
}
