package com.android.food.foodyhut.util;

import android.app.Activity;
import android.content.res.Resources;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.food.foodyhut.R;

/**
 * Created by sajid on 12/8/2015.
 */
public class SortBookTicketDataButtonUIUpdate {

    private static final int NO_SORT = 0;
    private static final int ASC_SORT = 1;
    private static final int DESC_SORT = 2;

    public static final int OPERATOR_SORT = 0;
    public static final int DEPARTURE_SORT = 1;
    public static final int FARE_SORT = 2;

    private int operator = 0;
    private int departure = 0;
    private int fare = 0;


    private Activity activity;

    private TextView busFareSortButtonTextView;
    private ImageView busFareSortButtonImageView;

    private TextView departureSortButtonTextView;
    private ImageView departureSortButtonImageView;


    private TextView operationSortButtonTextView;
    private ImageView operationSortButtonImageView;

    public SortBookTicketDataButtonUIUpdate(Activity activity) {
        this.activity = activity;
    }

    public void setOperationSortButton(TextView operationSortButtonTextView, ImageView operationSortButtonImageView) {
        this.operationSortButtonTextView = operationSortButtonTextView;
        this.operationSortButtonImageView = operationSortButtonImageView;
    }

    public void setDepartureSortButton(TextView departureSortButtonTextView, ImageView departureSortButtonImageView) {
        this.departureSortButtonTextView = departureSortButtonTextView;
        this.departureSortButtonImageView = departureSortButtonImageView;
    }

    public void setBusFareSortButton(TextView busFareSortButtonTextView, ImageView busFareSortButtonImageView) {
        this.busFareSortButtonTextView = busFareSortButtonTextView;
        this.busFareSortButtonImageView = busFareSortButtonImageView;
    }

    public void sortCase(@SortId int sortCase) {
        View[] sortButtonViews = getSortButtonViews(sortCase);
        if (sortButtonViews[0].equals(operationSortButtonTextView)) {
            busFareSortButtonTextView.setTextColor(getResources().getColor(R.color.table_bar_text_color));
            busFareSortButtonImageView.setColorFilter(getResources().getColor(R.color.table_bar_text_color));

            departureSortButtonTextView.setTextColor(getResources().getColor(R.color.table_bar_text_color));
            departureSortButtonImageView.setColorFilter(getResources().getColor(R.color.table_bar_text_color));

            busFareSortButtonImageView.setImageResource(R.mipmap.ic_unfold_more_grey600_18dp);
            departureSortButtonImageView.setImageResource(R.mipmap.ic_unfold_more_grey600_18dp);

            departure = 0;
            fare = 0;

        } else if (sortButtonViews[0].equals(departureSortButtonTextView)) {

            operationSortButtonTextView.setTextColor(getResources().getColor(R.color.table_bar_text_color));
            operationSortButtonImageView.setColorFilter(getResources().getColor(R.color.table_bar_text_color));

            busFareSortButtonTextView.setTextColor(getResources().getColor(R.color.table_bar_text_color));
            busFareSortButtonImageView.setColorFilter(getResources().getColor(R.color.table_bar_text_color));

            busFareSortButtonImageView.setImageResource(R.mipmap.ic_unfold_more_grey600_18dp);
            operationSortButtonImageView.setImageResource(R.mipmap.ic_unfold_more_grey600_18dp);

            operator = 0;
            fare = 0;
        } else if (sortButtonViews[0].equals(busFareSortButtonTextView)) {

            operationSortButtonTextView.setTextColor(getResources().getColor(R.color.table_bar_text_color));
            operationSortButtonImageView.setColorFilter(getResources().getColor(R.color.table_bar_text_color));

            departureSortButtonTextView.setTextColor(getResources().getColor(R.color.table_bar_text_color));
            departureSortButtonImageView.setColorFilter(getResources().getColor(R.color.table_bar_text_color));

            departureSortButtonImageView.setImageResource(R.mipmap.ic_unfold_more_grey600_18dp);
            operationSortButtonImageView.setImageResource(R.mipmap.ic_unfold_more_grey600_18dp);

            operator = 0;
            departure = 0;
        }

    }

    private View[] getSortButtonViews(@SortId int sortCase) {
        View[] sortButtonViews = new View[2];
        switch (sortCase) {
            case OPERATOR_SORT:
                operator++;
                if (operator == 3) {
                    operator = 0;
                }
                sortButtonViews[0] = operationSortButtonTextView;
                sortButtonViews[1] = operationSortButtonImageView;

                switch (operator) {
                    case NO_SORT:
                        operationSortButtonImageView.setImageResource(R.mipmap.ic_unfold_more_grey600_18dp);
                        break;
                    case ASC_SORT:
                        operationSortButtonImageView.setImageResource(R.mipmap.ic_expand_more_grey600_18dp);
                        break;
                    case DESC_SORT:
                        operationSortButtonImageView.setImageResource(R.mipmap.ic_expand_less_grey600_18dp);
                        break;
                }
                operationSortButtonTextView.setTextColor(getResources().getColor(R.color.text_color_special));
                operationSortButtonImageView.setColorFilter(getResources().getColor(R.color.text_color_special));
                break;
            case DEPARTURE_SORT:
                departure++;
                if (departure == 3) {
                    departure = 0;
                }
                sortButtonViews[0] = departureSortButtonTextView;
                sortButtonViews[1] = departureSortButtonImageView;
                switch (departure) {
                    case NO_SORT:
                        departureSortButtonImageView.setImageResource(R.mipmap.ic_unfold_more_grey600_18dp);
                        break;
                    case ASC_SORT:
                        departureSortButtonImageView.setImageResource(R.mipmap.ic_expand_more_grey600_18dp);
                        break;
                    case DESC_SORT:
                        departureSortButtonImageView.setImageResource(R.mipmap.ic_expand_less_grey600_18dp);
                        break;
                }
                departureSortButtonTextView.setTextColor(getResources().getColor(R.color.text_color_special));
                departureSortButtonImageView.setColorFilter(getResources().getColor(R.color.text_color_special));
                break;
            case FARE_SORT:
                fare++;
                if (fare == 3) {
                    fare = 0;
                }
                sortButtonViews[0] = busFareSortButtonTextView;
                sortButtonViews[1] = busFareSortButtonImageView;
                switch (fare) {
                    case NO_SORT:
                        busFareSortButtonImageView.setImageResource(R.mipmap.ic_unfold_more_grey600_18dp);
                        break;
                    case ASC_SORT:
                        busFareSortButtonImageView.setImageResource(R.mipmap.ic_expand_more_grey600_18dp);
                        break;
                    case DESC_SORT:
                        busFareSortButtonImageView.setImageResource(R.mipmap.ic_expand_less_grey600_18dp);
                        break;
                }
                busFareSortButtonTextView.setTextColor(getResources().getColor(R.color.text_color_special));
                busFareSortButtonImageView.setColorFilter(getResources().getColor(R.color.text_color_special));
                break;
        }
        return sortButtonViews;
    }

    public TextView getBusFareSortButtonTextView() {
        return busFareSortButtonTextView;
    }

    public void setBusFareSortButtonTextView(TextView busFareSortButtonTextView) {
        this.busFareSortButtonTextView = busFareSortButtonTextView;
    }

    public ImageView getBusFareSortButtonImageView() {
        return busFareSortButtonImageView;
    }

    public void setBusFareSortButtonImageView(ImageView busFareSortButtonImageView) {
        this.busFareSortButtonImageView = busFareSortButtonImageView;
    }

    public TextView getDepartureSortButtonTextView() {
        return departureSortButtonTextView;
    }

    public void setDepartureSortButtonTextView(TextView departureSortButtonTextView) {
        this.departureSortButtonTextView = departureSortButtonTextView;
    }

    public ImageView getDepartureSortButtonImageView() {
        return departureSortButtonImageView;
    }

    public void setDepartureSortButtonImageView(ImageView departureSortButtonImageView) {
        this.departureSortButtonImageView = departureSortButtonImageView;
    }

    public TextView getOperationSortButtonTextView() {
        return operationSortButtonTextView;
    }

    public void setOperationSortButtonTextView(TextView operationSortButtonTextView) {
        this.operationSortButtonTextView = operationSortButtonTextView;
    }

    public ImageView getOperationSortButtonImageView() {
        return operationSortButtonImageView;
    }

    public void setOperationSortButtonImageView(ImageView operationSortButtonImageView) {
        this.operationSortButtonImageView = operationSortButtonImageView;
    }

    private Resources getResources() {
        return activity.getResources();
    }

    public int getOperator() {
        return operator;
    }

    public void setOperator(int operator) {
        this.operator = operator;
    }

    public int getDeparture() {
        return departure;
    }

    public void setDeparture(int departure) {
        this.departure = departure;
    }

    public int getFare() {
        return fare;
    }

    public void setFare(int fare) {
        this.fare = fare;
    }
}