package com.android.food.foodyhut.activity.listener;

import com.android.food.foodyhut.activity.annotation.FragmentId;

/**
 * Created by sajid on 10/29/2015.
 */
public interface OnSlideMenuItemClickListener {

    int HOME = 0;
    int PROFILE = 1;
    int ACCOUNT = 2;
    int TERMS_AND_PRIVACY = 3;
    int ABOUT = 4;
    int MY_BUSINESS = 5;
    int LOGIN_LOGOUT = 6;
    int SIGN_UP = 7;
    int SHARE_FH =8;

    void onItemSelect(@FragmentId int newFragmentId);

    void closeDrawer();

}
