package com.android.food.foodyhut.adapter.recyclerview.item;

import com.google.gson.annotations.SerializedName;

/**
 * Created by sajid on 11/2/2015.
 */
public class UpcomingTripsItem implements RecyclerViewBaseItem {

    @SerializedName("pnr")
    private int pnr;
    @SerializedName("journey_date")
    private String journey_date;
    @SerializedName("origin_city")
    private String origin_city;
    @SerializedName("destination_city")
    private String destination_city;
    @SerializedName("company_name")
    private String company_name;
    @SerializedName("booking_date")
    private String booking_date;
    @SerializedName("trip_origin_date")
    private String trip_origin_date;
    @SerializedName("order_value")
    private String order_value;

    public int getPnr() {
        return pnr;
    }

    public void setPnr(int pnr) {
        this.pnr = pnr;
    }

    public String getJourney_date() {
        return journey_date;
    }

    public void setJourney_date(String journey_date) {
        this.journey_date = journey_date;
    }

    public String getOrigin_city() {
        return origin_city;
    }

    public void setOrigin_city(String origin_city) {
        this.origin_city = origin_city;
    }

    public String getDestination_city() {
        return destination_city;
    }

    public void setDestination_city(String destination_city) {
        this.destination_city = destination_city;
    }

    public String getCompany_name() {
        return company_name;
    }

    public void setCompany_name(String company_name) {
        this.company_name = company_name;
    }

    public String getBooking_date() {
        return booking_date;
    }

    public void setBooking_date(String booking_date) {
        this.booking_date = booking_date;
    }

    public String getTrip_origin_date() {
        return trip_origin_date;
    }

    public void setTrip_origin_date(String trip_origin_date) {
        this.trip_origin_date = trip_origin_date;
    }

    public String getOrder_value() {
        return order_value;
    }

    public void setOrder_value(String order_value) {
        this.order_value = order_value;
    }

    public UpcomingTripsItem(int pnr, String journey_date, String origin_city, String destination_city, String company_name, String booking_date, String trip_origin_date, String order_value) {
        this.pnr = pnr;
        this.journey_date = journey_date;
        this.origin_city = origin_city;
        this.destination_city = destination_city;
        this.company_name = company_name;
        this.booking_date = booking_date;
        this.trip_origin_date = trip_origin_date;
        this.order_value = order_value;
    }

    public UpcomingTripsItem() {
    }


}
