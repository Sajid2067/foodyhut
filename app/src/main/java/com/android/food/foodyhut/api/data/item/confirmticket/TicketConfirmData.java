package com.android.food.foodyhut.api.data.item.confirmticket;

import com.google.gson.annotations.SerializedName;

/**
 * Created by sajid on 11/7/2015.
 */
public class TicketConfirmData {

    @SerializedName("order_id")
    private String orderId;
    @SerializedName("reservation_ref")
    private String reservationReference;
    @SerializedName("message")
    private String message;
    @SerializedName("payment_id")
    private String paymentId;
    @SerializedName("order_value")
    private String orderValue;

    public TicketConfirmData() {
        this(null, null, null);
    }

    public TicketConfirmData(String orderId, String reservationReference, String message) {
        this.orderId = orderId;
        this.reservationReference = reservationReference;
        this.message = message;
    }

    public String getOrderId() {

        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getReservationReference() {
        return reservationReference;
    }

    public void setReservationReference(String reservationReference) {
        this.reservationReference = reservationReference;
    }

    public String getOrderValue() {
        return orderValue;
    }

    public void setOrderValue(String orderValue) {
        this.orderValue = orderValue;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    @Override
    public String toString() {
        return "TicketConfirmData{" +
                "orderId='" + orderId + '\'' +
                ", reservationReference='" + reservationReference + '\'' +
                ", message='" + message + '\'' +
                '}';
    }
}
