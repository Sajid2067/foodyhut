package com.android.food.foodyhut.fragment;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.analytics.ecommerce.Product;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.android.food.foodyhut.R;
import com.android.food.foodyhut.activity.listener.OnFragmentInteractionListener;
import com.android.food.foodyhut.api.data.item.bkash.BkashVerificationMessage;
import com.android.food.foodyhut.api.data.item.ticketWinning.TicketWinningMessage;
import com.android.food.foodyhut.application.AppController;
import com.android.food.foodyhut.fragment.item.TicketSuccessItem;
import com.android.food.foodyhut.toolbox.ObjectRequest;
import com.android.food.foodyhut.util.API;
import com.android.food.foodyhut.util.AppManager;
import com.android.food.foodyhut.util.FrequentFunction;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * Created by sajid on 12/3/2015.
 */
public class TicketSuccessFragment extends BaseFragment {
    private static final String TAG = TicketSuccessFragment.class.getSimpleName();
    Tracker mTracker;
    private TicketSuccessItem ticketSuccessItem;
  //  private TextView orderIdTextView;
   // private TextView reservationReferenceTextView;
    private EditText transactionIdEditText;
    private TextView buySuccessMessageTextView;
  //  private TextView amountDueTextView;
 //   private TextView amountInfoTextView;
    private TextView mainMessage;
    private TextView confirmationMobileTextView;
    private TextView merchantNoTextView;
    private TextView amountDueTextView;
    private TextView reservationTextView;
    private TextView reservationLabel;
    private Button bookAgainButton;
    private Button bkashVerifyButton;
    private Button tryAgainButton;
    private Button cancelButton,winningCancelButton;
    private ImageView messageTypeImageView;
    private TextView messageTypeTextView,winningMessageTypeTextView;
    private ImageButton backButtonOverride,winningBackButtonOverride;
    private View pnrNumberHolder;
    private TextView pnrNumberTextView;
    private TextView messageTextView;
    private AlertDialog bkashVerificationMessage,winningMessage;
    private TextView rule1,rule2,rule3,rule33,reservationRefCODTextView,amountDueCODTextView,addressCODTextView;
    private TableLayout tL;
    private LinearLayout lLRule2, linearLayoutCOD,rule3Holder;

    private static final String VALID_MOBILE_REG_EX = "^01(1|5|6|7|8|9)\\d{8}$";

    ObjectRequest<TicketWinningMessage> ticketWinningObjectRequest;
    ObjectRequest<BkashVerificationMessage> bkashVerificationMessageObjectRequest;
    int timeoutCounter = 0;
    private AppController appController = AppController.getInstance();
    private boolean isSuccess;

    public static TicketSuccessFragment newInstance(ArrayList<Integer> previousFragmentIds, Parcelable parcelable) {
        Log.d(TAG, "FilterFragment newInstance(@FragmentId int previousFragmentId)");
        TicketSuccessFragment filterFragment = new TicketSuccessFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(PARCELABLE_FRAGMENT_ITEM_ID_TAG, parcelable);
        bundle.putIntegerArrayList(PREVIOUS_FRAGMENT_ID_TAG, previousFragmentIds);
        filterFragment.setArguments(bundle);
        return filterFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (parcelable instanceof TicketSuccessItem) {
            ticketSuccessItem = (TicketSuccessItem) parcelable;
        }

        Product product = new Product()
                .setId(ticketSuccessItem.getPaymentId())
                .setName(TAG)
                .setCategory(ticketSuccessItem.getOrderTypeTag())
                .setBrand("Shohoz")
                .setVariant(ticketSuccessItem.getMessage())
                .setPosition(1)
                .setCustomDimension(1, ticketSuccessItem.getOrderValue());
        HitBuilders.ScreenViewBuilder builder = new HitBuilders.ScreenViewBuilder()
                .addImpression(product, "Payment Result").addProduct(product);

        // Obtain the shared Tracker instance.
        AppController application = (AppController) getActivity().getApplication();
        mTracker = application.getDefaultTracker();
        mTracker.setScreenName("Ticket Success");
        // Send a screen view.
        mTracker.send(builder.build());
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory(ticketSuccessItem.getOrderTypeTag())
                .setAction(ticketSuccessItem.getOrderValue())
                .setLabel(ticketSuccessItem.getMessage())
                .build());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)");

        if (rootView == null) {
            rootView = inflater.inflate(R.layout.fragment_ticket_buy_success, container, false);
        }
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    void initializeEditTextComponents() {
        transactionIdEditText = findViewById(R.id.transaction_id_edit_text);
    }

    @Override
    void initializeButtonComponents() {
        bookAgainButton = findViewById(R.id.book_again_button);
        bkashVerifyButton = findViewById(R.id.bkash_verify_button);
        if (ticketSuccessItem.getOrderTypeTag().equals(TicketSuccessItem.TYPE_CASH_ON_DELIVERY)) {
            bkashVerifyButton.setVisibility(View.GONE);
        }
        else if (ticketSuccessItem.getOrderTypeTag().equals(TicketSuccessItem.TYPE_CARD)) {
            bkashVerifyButton.setVisibility(View.GONE);
        }

    }

    private String getPriceFormat(String price) {
        return String.format("৳ %.02f", Float.parseFloat(price));
    }

    @Override
    void initializeTextViewComponents() {

        reservationLabel = findViewById(R.id.reservation_lebel_text_view);
        buySuccessMessageTextView = findViewById(R.id.message_text_view);
        rule1 =findViewById(R.id.rule1_text_view);
        rule2 =findViewById(R.id.rule2_text_view);
        rule3 =findViewById(R.id.rule3_text_view);
        rule33=findViewById(R.id.rule33_text_view);
        reservationLabel=findViewById(R.id.reservation_lebel_text_view);
        reservationRefCODTextView=findViewById(R.id.reservationRefCODTextView);
        amountDueCODTextView=findViewById(R.id.amountDueCODTextView);
        addressCODTextView=findViewById(R.id.addressCODTextView);

        FrequentFunction.convertHtml(rule1,"1. "+ticketSuccessItem.getMessage());
        FrequentFunction.convertHtml(rule33,"• Your ticket will be emailed to you in a few hours but emails <b>MAY GET DELAYED</b>. Hence Shohoz.com encourages you to <b>PRINT YOUR TICKET YOURSELF</b>.");

        if (ticketSuccessItem.getOrderTypeTag().equals(TicketSuccessItem.TYPE_CARD)) {
     //       amountInfoTextView.setText("Amount: ");
            isSuccess=true;
        }

        mainMessage = findViewById(R.id.main_message_text_view);
        confirmationMobileTextView = findViewById(R.id.confirmation_mobile_text_view);

        merchantNoTextView = findViewById(R.id.merchantNoTextView);
        amountDueTextView = findViewById(R.id.amountDueTextView);
        reservationTextView = findViewById(R.id.reservationTextView);

        if (ticketSuccessItem.getMessage().contains("not successful")){
            mainMessage.setText("Sorry, Transaction Failed");
            reservationLabel.setVisibility(View.GONE);
            amountDueTextView.setVisibility(View.GONE);
        //    amountInfoTextView.setVisibility(View.GONE);
            isSuccess=true;
        }
        else{
//            mainMessage.setText(ticketSuccessItem.getName()+" , your ticket is reserved!");
//            confirmationMobileTextView.setText("Reservation confirmation SMS has been sent to "+ticketSuccessItem.getMobileNumber());
//            String wholeMessage = ticketSuccessItem.getMessage();
//            String tempMessage="";
//            List<String> wholeMessageList = Arrays.asList(wholeMessage.split(" "));
//            for(int i=0;i<wholeMessageList.size();i++){
//                tempMessage=wholeMessageList.get(i);
//                if(tempMessage.trim().matches(VALID_MOBILE_REG_EX)){
//                    break;
//                }
//            }
//            merchantNoTextView.setText(tempMessage);
//            amountDueTextView.setText(getPriceFormat(ticketSuccessItem.getOrderValue()));
//            reservationTextView.setText(ticketSuccessItem.getReservationReference());

        }


    }

    @Override
    void initializeOtherViewComponents() {

        tL=(TableLayout)findViewById(R.id.tl);
        lLRule2=(LinearLayout)findViewById(R.id.lLRule2);
        linearLayoutCOD =(LinearLayout)findViewById(R.id.linearLayoutCOD);
        rule3Holder =(LinearLayout)findViewById(R.id.rule3Holder);

        createDialogView();
        createDialogViewWinningMessage();
        createProgressDialogView();

        if (ticketSuccessItem.getOrderTypeTag().equals(TicketSuccessItem.TYPE_CASH_ON_DELIVERY)) {
            transactionIdEditText.setVisibility(View.GONE);
            mainMessage.setText(ticketSuccessItem.getName()+" , your ticket is reserved!");
            confirmationMobileTextView.setText("Reservation confirmation SMS has been sent to "+ticketSuccessItem.getMobileNumber());
            reservationLabel.setText("Our support executives would call you in few hours to verify your order and delivery address. In case we are not able to reach you, your order would be cancelled automatically.");
            reservationRefCODTextView.setText(ticketSuccessItem.getReservationReference());
            amountDueCODTextView.setText(getPriceFormat(ticketSuccessItem.getOrderValue()));
            FrequentFunction.convertHtml(addressCODTextView,ticketSuccessItem.getCashOnDeliveryData().getFirstName()+" "+
            ticketSuccessItem.getCashOnDeliveryData().getLastName()+"<br>"+ticketSuccessItem.getCashOnDeliveryData().getAddressLineOne()
            +"<br>"+ticketSuccessItem.getCashOnDeliveryData().getAreaName()+"<br>"+ticketSuccessItem.getCashOnDeliveryData().getCityName());
          //  addressCODTextView.setText(ticketSuccessItem.getCashOnDeliveryData().getAddressLineOne());

            linearLayoutCOD.setVisibility(View.VISIBLE);
            rule3Holder.setVisibility(View.GONE);
            rule1.setVisibility(View.GONE);
            tL.setVisibility(View.GONE);
            rule2.setVisibility(View.GONE);
            lLRule2.setVisibility(View.GONE);

        }else if (ticketSuccessItem.getOrderTypeTag().equals(TicketSuccessItem.TYPE_CARD)){

            if (ticketSuccessItem.getMessage().contains("not successful")){
                mainMessage.setText("Sorry, Transaction Failed");
                mainMessage.setTextColor(Color.RED);
                mainMessage.setTextSize(TypedValue.COMPLEX_UNIT_PX,
                        getResources().getDimension(R.dimen.horizontal_20dip_margin));
                confirmationMobileTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX,
                        getResources().getDimension(R.dimen.horizontal_15dip_margin));
                confirmationMobileTextView.setText("Transaction was not successful");
                reservationLabel.setVisibility(View.GONE);
                linearLayoutCOD.setVisibility(View.GONE);
                rule1.setVisibility(View.GONE);
                tL.setVisibility(View.GONE);
                rule2.setVisibility(View.GONE);
                lLRule2.setVisibility(View.GONE);
                rule3Holder.setVisibility(View.GONE);
                isSuccess=true;
            }else {
                transactionIdEditText.setVisibility(View.GONE);
                mainMessage.setText(ticketSuccessItem.getName() + " , your ticket is confirmed!");
                confirmationMobileTextView.setText("Successfully completed your booking and confirmation SMS has been sent to " + ticketSuccessItem.getMobileNumber());
                reservationLabel.setText("What to do now : ");
                rule3.setText("• Print your ticket !");
                linearLayoutCOD.setVisibility(View.GONE);
                rule1.setVisibility(View.GONE);
                tL.setVisibility(View.GONE);
                rule2.setVisibility(View.GONE);
                lLRule2.setVisibility(View.GONE);

                ticketWinningCall("card");
            }

        }else {

            //bKash
            linearLayoutCOD.setVisibility(View.GONE);
            rule1.setVisibility(View.VISIBLE);
            tL.setVisibility(View.VISIBLE);
            rule2.setVisibility(View.VISIBLE);
            lLRule2.setVisibility(View.VISIBLE);

            mainMessage.setText(ticketSuccessItem.getName()+" , your ticket is reserved!");
            confirmationMobileTextView.setText("Reservation confirmation SMS has been sent to "+ticketSuccessItem.getMobileNumber());
            String wholeMessage = ticketSuccessItem.getMessage();
            String tempMessage="";
            List<String> wholeMessageList = Arrays.asList(wholeMessage.split(" "));
            for(int i=0;i<wholeMessageList.size();i++){
                tempMessage=wholeMessageList.get(i);
                if(tempMessage.trim().matches(VALID_MOBILE_REG_EX)){
                    break;
                }
            }
            merchantNoTextView.setText(tempMessage);
            amountDueTextView.setText(getPriceFormat(ticketSuccessItem.getOrderValue()));
            reservationTextView.setText(ticketSuccessItem.getReservationReference());

        }
    }

    private void createDialogView() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());
        View view = View.inflate(getContext(), R.layout.bkash_verification_message, null);
        alertDialogBuilder.setView(view);

        messageTextView = (TextView) view.findViewById(R.id.message_text_view);
        messageTypeTextView = (TextView) view.findViewById(R.id.message_type_text_view);
        pnrNumberTextView = (TextView) view.findViewById(R.id.pnr_number_text_view);

        messageTypeImageView = (ImageView) view.findViewById(R.id.message_type_image_view);

        backButtonOverride = (ImageButton) view.findViewById(R.id.back_button_override);

        tryAgainButton = (Button) view.findViewById(R.id.retry_button);
        cancelButton = (Button) view.findViewById(R.id.cancel_button);

        pnrNumberHolder = view.findViewById(R.id.pnr_number_holder);
        alertDialogBuilder.setCancelable(false);
        bkashVerificationMessage = alertDialogBuilder.create();
    }

    private void createDialogViewWinningMessage() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());
        View view = View.inflate(getContext(), R.layout.winning_ticket_message, null);
        alertDialogBuilder.setView(view);


        winningMessageTypeTextView = (TextView) view.findViewById(R.id.winning_message_type_text_view);


        winningBackButtonOverride = (ImageButton) view.findViewById(R.id.winning_back_button_override);

        winningCancelButton = (Button) view.findViewById(R.id.winning_cancel_button);


        alertDialogBuilder.setCancelable(false);
        winningMessage = alertDialogBuilder.create();
    }

    @Override
    void initializeOnclickListener() {
        bkashVerifyButton.setOnClickListener(this);
        bookAgainButton.setOnClickListener(this);
        if (bkashVerificationMessage != null) {
            tryAgainButton.setOnClickListener(this);
            backButtonOverride.setOnClickListener(this);
            winningBackButtonOverride.setOnClickListener(this);
            cancelButton.setOnClickListener(this);
            winningCancelButton.setOnClickListener(this);
        }
    }

    @Override
    void removeOnclickListener() {
        bkashVerifyButton.setOnClickListener(null);
        bookAgainButton.setOnClickListener(null);
        if (bkashVerificationMessage != null) {
            tryAgainButton.setOnClickListener(null);
            backButtonOverride.setOnClickListener(null);
            winningBackButtonOverride.setOnClickListener(null);
            cancelButton.setOnClickListener(null);
            winningCancelButton.setOnClickListener(null);
        }
    }

    @Override
    public void onBackPressed() {
        if (bkashVerificationMessage != null && bkashVerificationMessage.isShowing()) {
            bkashVerificationMessage.cancel();
        } else if (winningMessage != null && winningMessage.isShowing()) {
            winningMessage.cancel();
        }else if (isSuccess) {
            previousFragmentIds = new ArrayList<>();
            onFragmentChangeCallListener.onBackPressed(null, OnFragmentInteractionListener.LOCATION_FRAGMENT_FH, previousFragmentIds);
        } else {
//            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
//            imm.hideSoftInputFromWindow(null, 0);
//            super.onBackPressed();
        }
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

        switch (view.getId()) {
            case R.id.winning_cancel_button:
            case R.id.winning_back_button_override:
                winningMessage.cancel();
                onBackPressed();
                break;
            case R.id.cancel_button:
            case R.id.back_button_override:
                bkashVerificationMessage.cancel();
                if(isSuccess) {
                    ticketWinningCall("bKash");
                }
                //onBackPressed();
                break;
            case R.id.retry_button:
            case R.id.bkash_verify_button:
                if (bkashVerificationMessage != null) {
                    if (bkashVerificationMessage.isShowing()) {
                        bkashVerificationMessage.cancel();
                    }
                }
                isSuccess = false;
                bkashVerification();
                break;
            case R.id.book_again_button:
                isSuccess = true;
                onBackPressed();
                break;
        }
    }

    private void bkashVerification() {
        if (isZeroLengthText(transactionIdEditText) || isZeroLengthText(reservationTextView)) {
            makeAToast("Fill up all the field above", Toast.LENGTH_SHORT);
        } else {
            AppManager appManager = new AppManager(getActivity());
            String url = API.BKASH_VERIFICATION_API_URL;

            HashMap<String, String> postParams = new HashMap<>();
            postParams.put(API.Parameter.ANDROID_DEVICE_ID, appManager.getDeviceId());
            postParams.put(API.Parameter.ANDROID_APP_VERSION, appManager.getAppVersion());
            postParams.put(API.Parameter.RESERVATION_REFERENCES, getText(reservationTextView));
            postParams.put(API.Parameter.BKASH_TRANSACTION_ID, getText(transactionIdEditText));
            postParams.put(API.Parameter.CONTACT_NUMBER, ticketSuccessItem.getMobileNumber());
            Log.e(TAG, url);
            Log.e(TAG, postParams.toString());
            bkashVerificationMessageObjectRequest = new ObjectRequest<>(API.Method.BKASH_VERIFICATION_API_METHOD, url, postParams, new Response.Listener<BkashVerificationMessage>() {
                @Override
                public void onResponse(BkashVerificationMessage response) {
                    progressBarDialog.cancel();
                    if (response.getData() != null) {
                        messageTypeImageView.setImageResource(R.mipmap.ic_done_amber_48dp);
                        messageTypeTextView.setText("SUCCESS");
                        isSuccess = true;
                        pnrNumberTextView.setText(response.getData().getPnr());
                        pnrNumberHolder.setVisibility(View.VISIBLE);
                        tryAgainButton.setVisibility(View.GONE);
                        bkashVerificationMessage.show();
                        FrequentFunction.convertHtml(messageTextView,response.getData().getMessage());
                    } else {
                        messageTypeImageView.setImageResource(R.mipmap.ic_warning_amber_48dp);
                        messageTypeTextView.setText("ERROR");
                        pnrNumberHolder.setVisibility(View.GONE);
                        tryAgainButton.setVisibility(View.VISIBLE);
                        bkashVerificationMessage.show();
                        String message = "";
                        int i = 0;
                        for (String m : response.getError().getMessages()) {
                            if (i > 0) {
                                message += "\n";
                            }
                            message += m;
                        }
                        messageTextView.setText(message);
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    try {
                        Log.e(TAG, Integer.toString(error.networkResponse.statusCode));
                        Log.e(TAG, error.networkResponse.toString());
                    } catch (Exception e) {
                        if (timeoutCounter != 2) {
                            timeoutCounter++;
                            appController.addToRequestQueue(bkashVerificationMessageObjectRequest);
                            return;
                        } else {
                            timeoutCounter = 0;
                        }
                    }
                    progressBarDialog.cancel();
                    String message = "";
                    try {
                        Gson gson = new GsonBuilder().create();
                        BkashVerificationMessage response = gson.fromJson(new String(error.networkResponse.data, "UTF-8"), BkashVerificationMessage.class);
                        int i = 0;
                        for (String m : response.getError().getMessages()) {
                            if (i > 0) {
                                message += "\n";
                            }
                            message += m;
                        }
                    } catch (Exception e) {
                        message = "Something went wrong.Please Check your Internet Connection";
                        Log.e(TAG, e.getMessage());
                    }
                    messageTypeImageView.setImageResource(R.mipmap.ic_warning_amber_48dp);
                    messageTypeTextView.setText("ERROR");
                    messageTextView.setText(message);
                    pnrNumberHolder.setVisibility(View.GONE);
                    tryAgainButton.setVisibility(View.VISIBLE);
                    bkashVerificationMessage.show();
                }
            }, BkashVerificationMessage.class);
            progressBarDialog.show();
            appController.addToRequestQueue(bkashVerificationMessageObjectRequest);
        }
    }

    public void ticketWinningCall(final String tranaction_type) {

        AppManager appManager = new AppManager(getActivity());
        String url = API.TICKET_WINNING_API_URL;
        HashMap<String, String> postParams = new HashMap<>();
        postParams.put(API.Parameter.ANDROID_DEVICE_ID, appManager.getDeviceId());
        postParams.put(API.Parameter.ANDROID_APP_VERSION, appManager.getAppVersion());
        if(tranaction_type.compareTo("card")==0) {
            postParams.put(API.Parameter.ORDER_ID, String.valueOf(ticketSuccessItem.getOrderId()));
        }else {
            postParams.put(API.Parameter.PNR, pnrNumberTextView.getText().toString().trim());
        }

        Log.e(TAG, url);
        Log.e(TAG, postParams.toString());
        ticketWinningObjectRequest = new ObjectRequest<>(API.Method.TICKET_WINNING_API_METHOD, url, postParams, new Response.Listener<TicketWinningMessage>() {
            @Override
            public void onResponse(TicketWinningMessage response) {
                progressBarDialog.cancel();
                if (response.getData() != null) {

                    winningMessage.show();
                    FrequentFunction.convertHtml(winningMessageTypeTextView,response.getData().getMessage());
                } else {

                    String message = "";
                    int i = 0;
                    for (String m : response.getError().getMessages()) {
                        if (i > 0) {
                            message += "\n";
                        }
                        message += m;
                    }
                    if(tranaction_type.compareTo("card")!=0) {
                        onBackPressed();
                    }
                   // winningMessageTypeTextView.setText(message);
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                try {
                    Log.e(TAG, Integer.toString(error.networkResponse.statusCode));
                    Log.e(TAG, error.networkResponse.toString());
                } catch (Exception e) {
                    if (timeoutCounter != 2) {
                        timeoutCounter++;
                        appController.addToRequestQueue(bkashVerificationMessageObjectRequest);
                        return;
                    } else {
                        timeoutCounter = 0;
                    }
                }
                progressBarDialog.cancel();
                String message = "";
                try {
                    Gson gson = new GsonBuilder().create();
                    TicketWinningMessage response = gson.fromJson(new String(error.networkResponse.data, "UTF-8"), TicketWinningMessage.class);
                    int i = 0;
                    for (String m : response.getError().getMessages()) {
                        if (i > 0) {
                            message += "\n";
                        }
                        message += m;
                    }
                } catch (Exception e) {
                    message = "Something went wrong.Please Check your Internet Connection";
                    Log.e(TAG, e.getMessage());
                }

            }
        }, TicketWinningMessage.class);
        progressBarDialog.show();
        appController.addToRequestQueue(ticketWinningObjectRequest);
//        }
    }

}
