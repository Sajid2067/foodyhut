package com.android.food.foodyhut.fragment.item;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by sajid on 11/3/2015.
 */
public class CancelTicketRequestData implements Parcelable {

    public static final Creator<CancelTicketRequestData> CREATOR = new Creator<CancelTicketRequestData>() {
        @Override
        public CancelTicketRequestData createFromParcel(Parcel in) {
            return new CancelTicketRequestData(in);
        }

        @Override
        public CancelTicketRequestData[] newArray(int size) {
            return new CancelTicketRequestData[size];
        }
    };
   
    private String pnr;
    private String mobile;

   
    public CancelTicketRequestData(Parcel in) {
        setPnr(in.readString());
        setMobile(in.readString());
    }

    public CancelTicketRequestData(String pnr, String mobile) {
       
        this.pnr = pnr;
        this.mobile = mobile;
    }

  

   

    public String getPnr() {
        return pnr;
    }

    public void setPnr(String pnr) {
        this.pnr = pnr;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
       
        dest.writeString(getPnr());
        dest.writeString(getMobile());
    }

    @Override
    public String toString() {
        return "BookTicketData{" +
               
                ", pnr='" + pnr + '\'' +
                ", mobile='" + mobile + '\'' +
                '}';
    }
}
