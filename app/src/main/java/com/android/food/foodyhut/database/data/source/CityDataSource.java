package com.android.food.foodyhut.database.data.source;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.android.food.foodyhut.database.DataBaseOpenHelper;
import com.android.food.foodyhut.database.data.item.CityDataItem;
import com.android.food.foodyhut.database.exception.DataSourceException;

import java.util.ArrayList;

/**
 * Created by tuman on 4/5/2015.
 */
public class CityDataSource {

    protected Context context;
    protected SQLiteDatabase database;
    protected DataBaseOpenHelper dbHelper;
    private String[] allColumns = {
            DataBaseOpenHelper.CITY_ID_COLUMN,
            DataBaseOpenHelper.CITY_NAME_COLUMN,
            DataBaseOpenHelper.CITY_SEQUENCE_COLUMN,
            DataBaseOpenHelper.IS_TOP_CITY_COLUMN};

    public CityDataSource(Context context) {
        this.context = context;
        dbHelper = new DataBaseOpenHelper(this.context);
    }

    public CityDataItem insertCityDataItem(String cityId, String cityName, String citySequence, boolean isTopCity)
            throws DataSourceException {
        try {
            ContentValues values = new ContentValues();
            values.put(DataBaseOpenHelper.CITY_ID_COLUMN, cityId);
            values.put(DataBaseOpenHelper.CITY_NAME_COLUMN, cityName);
            values.put(DataBaseOpenHelper.CITY_SEQUENCE_COLUMN, citySequence);
            values.put(DataBaseOpenHelper.IS_TOP_CITY_COLUMN, Boolean.toString(isTopCity));
            database.insert(DataBaseOpenHelper.CITY_TABLE, null, values);
            Cursor cursor = database.query(DataBaseOpenHelper.CITY_TABLE,
                    allColumns, DataBaseOpenHelper.CITY_ID_COLUMN + " LIKE "
                            + "'" + cityId + "'", null, null, null, null);
            if (cursor == null) {
                return null;
            }
            cursor.moveToFirst();
            CityDataItem cityDataItem = cursorToCityDataItem(cursor);
            cursor.close();
            return cityDataItem;
        } catch (Exception e) {
            return null;
        }
    }

    public CityDataItem insertCityDataItem(CityDataItem item)
            throws DataSourceException {
        return insertCityDataItem(item.getCityId(), item.getCityName(), item.getCitySequence(), item.isTopCity());
    }

    public CityDataItem getCityDataItem(String cityName) {
        Cursor cursor = database.query(DataBaseOpenHelper.CITY_TABLE,
                allColumns, DataBaseOpenHelper.CITY_NAME_COLUMN + " LIKE "
                        + "'" + cityName + "'", null, null, null, null);
        cursor.moveToFirst();
        CityDataItem cityDataItem = cursorToCityDataItem(cursor);
        cursor.close();
        return cityDataItem;
    }

    public void updateCityDataItem(CityDataItem item) {
        ContentValues values = new ContentValues();
//        values.put(DataBaseOpenHelper.LATEST_COLUMN, item.getLatestOne());
        //      database.update(DataBaseOpenHelper.DATA_TABLE, values, DataBaseOpenHelper.ID_COLUMN + " = " + item.getId(), null);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public void deleteCityItem(CityDataItem item) {
        String cityId = item.getCityId();
        database.delete(DataBaseOpenHelper.CITY_TABLE,
                DataBaseOpenHelper.CITY_ID_COLUMN + " LIKE "
                        + "'" + cityId + "'", null);
    }

    public ArrayList<CityDataItem> getAllCityItem() {
        ArrayList<CityDataItem> items = new ArrayList<>();
        Cursor cursor = database.query(DataBaseOpenHelper.CITY_TABLE,
                allColumns, null, null, null, null, null);
        cursor.moveToFirst();
        do {
            CityDataItem item = cursorToCityDataItem(cursor);
            items.add(item);
        } while (cursor.moveToNext());
        cursor.close();
        return items;
    }

    public ArrayList<CityDataItem> getAllCityItem(CityDataItem cityDataItem) {
        ArrayList<CityDataItem> items = new ArrayList<>();
        Cursor cursor = database.query(DataBaseOpenHelper.CITY_TABLE,
                allColumns, DataBaseOpenHelper.CITY_NAME_COLUMN + " LIKE "
                        + "'" + cityDataItem.getCityName() + "'", null, null, null, null);
        cursor.moveToFirst();
        do {
            CityDataItem item = cursorToCityDataItem(cursor);
            items.add(item);
        } while (cursor.moveToNext());
        cursor.close();
        return items;
    }

    public ArrayList<CityDataItem> getTopCityItem() {
        ArrayList<CityDataItem> items = new ArrayList<>();
        Cursor cursor = database.query(DataBaseOpenHelper.CITY_TABLE,
                allColumns, DataBaseOpenHelper.IS_TOP_CITY_COLUMN + " LIKE "
                        + "'" + "true" + "'", null, null, null, null);
        cursor.moveToFirst();
        do {
            CityDataItem item = cursorToCityDataItem(cursor);
            items.add(item);
        } while (cursor.moveToNext());
        cursor.close();
        return items;
    }

    public int getSize() {
        Cursor cursor = database.query(DataBaseOpenHelper.CITY_TABLE,
                allColumns, null, null, null, null, null);
        cursor.moveToFirst();
        cursor.close();
        return cursor.getCount();
    }

    private CityDataItem cursorToCityDataItem(Cursor cursor) {
        CityDataItem item = new CityDataItem();
        if (cursor.getCount() == 0)
            return item;
        item.setCityId(cursor.getString(cursor
                .getColumnIndex(DataBaseOpenHelper.CITY_ID_COLUMN)));
        item.setCityName(cursor.getString(cursor
                .getColumnIndex(DataBaseOpenHelper.CITY_NAME_COLUMN)));
        item.setCitySequence(cursor.getString(cursor
                .getColumnIndex(DataBaseOpenHelper.CITY_SEQUENCE_COLUMN)));
        item.setIsTopCity(Boolean.parseBoolean(cursor.getString(cursor
                .getColumnIndex(DataBaseOpenHelper.IS_TOP_CITY_COLUMN))));
        return item;
    }
}
