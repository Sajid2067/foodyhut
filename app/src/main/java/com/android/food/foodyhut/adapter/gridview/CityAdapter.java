package com.android.food.foodyhut.adapter.gridview;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.android.food.foodyhut.R;
import com.android.food.foodyhut.database.data.item.CityDataItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sajid on 10/22/2015.
 */
public class CityAdapter extends BaseAdapter implements Filterable {

    private static final String TAG = CityAdapter.class.getSimpleName();
    private List<CityDataItem> cityDataItems;
    private ViewHolder viewHolder;
    private Activity activity;
    private LayoutInflater layoutInflater;
    private List<CityDataItem> backUpCityDataItems;
    private List<CityDataItem> topCityDataItems;
    private TextView noResultTextView;

    public CityAdapter(Activity activity, List<CityDataItem> cityDataItems, List<CityDataItem> topCityDataItems, TextView noResultTextView) {
        this.activity = activity;
        this.backUpCityDataItems = cityDataItems;
        this.topCityDataItems = topCityDataItems;
        this.cityDataItems = new ArrayList<>(this.topCityDataItems);
        this.noResultTextView = noResultTextView;
        if (cityDataItems.size() == 0) {
            noResultTextView.setVisibility(View.VISIBLE);
        } else {
            noResultTextView.setVisibility(View.GONE);
        }
        layoutInflater = (LayoutInflater) this.activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return cityDataItems.size();
    }

    @Override
    public Object getItem(int position) {
        return cityDataItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        CityDataItem cityDataItem = cityDataItems.get(position);
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.grid_item_district, parent, false);
            inflatesView(convertView);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.districtLabelTextView.setText(cityDataItem.getCityName());
        return convertView;
    }

    private void inflatesView(final View convertView) {
        viewHolder = new ViewHolder();
        viewHolder.districtLabelTextView = (TextView) convertView.findViewById(R.id.district_label_text_view);
        convertView.setTag(viewHolder);

    }


    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence filterValue) {
                final FilterResults results = new FilterResults();
                List<CityDataItem> cityDataItems = new ArrayList<>();
                for (int i = 0; i < backUpCityDataItems.size(); i++) {
                    CityDataItem cityDataItem = backUpCityDataItems.get(i);
                    if (cityDataItem.getCityName().trim().toLowerCase().contains(filterValue.toString().trim().toLowerCase())) {
                        cityDataItems.add(cityDataItem);
                    }
                }
                if (filterValue.toString().trim().length() == 0) {
                    results.count = topCityDataItems.size();
                    results.values = topCityDataItems;
                } else {
                    results.count = cityDataItems.size();
                    results.values = cityDataItems;
                }
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (results.count == 0) {
                            noResultTextView.setVisibility(View.VISIBLE);
                        } else {
                            noResultTextView.setVisibility(View.GONE);
                        }
                    }
                });

                Log.e("VALUES", results.values.toString());
                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                cityDataItems = (List<CityDataItem>) results.values;
                if (cityDataItems == null) {
                    cityDataItems = topCityDataItems;
                }
                notifyDataSetChanged();
            }
        };
    }

    private static class ViewHolder {
        private TextView districtLabelTextView;
    }
}
