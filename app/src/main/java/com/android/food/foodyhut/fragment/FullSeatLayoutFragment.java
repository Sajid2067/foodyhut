package com.android.food.foodyhut.fragment;

import android.graphics.Paint;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.android.food.foodyhut.R;
import com.android.food.foodyhut.activity.listener.OnFragmentInteractionListener;
import com.android.food.foodyhut.api.data.item.seat.SeatNumber;
import com.android.food.foodyhut.api.data.item.seatlayout.SeatLayout;
import com.android.food.foodyhut.application.AppController;
import com.android.food.foodyhut.fragment.item.SeatLayoutData;
import com.android.food.foodyhut.fragment.item.SeatNumberData;
import com.android.food.foodyhut.toolbox.GetUrlBuilder;
import com.android.food.foodyhut.util.API;
import com.android.food.foodyhut.util.AppManager;
import com.android.food.foodyhut.util.Constant;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by sajid on 11/6/2015.
 */
public class FullSeatLayoutFragment extends BaseFragment {

    private static final String TAG = FullSeatLayoutFragment.class.getSimpleName();
    Gson gson = new GsonBuilder().create();
    SeatLayout seatLayout;
    AppController appController = AppController.getInstance();
    AppManager appManager;
    Tracker mTracker;
    private LinearLayout seatOneView;
    private LinearLayout seatTwoView;
    private LinearLayout seatThreeView;
    private LinearLayout seatFourView;
    private LinearLayout seatMiddleView;
    private LinearLayout seatMiddleViewRight;
    private TextView addSeatTextView;
    private Button doneButton;
    private TextView newBaseFareTextView;
    private TextView oldBaseFareTextView;
    private SeatLayoutData seatLayoutData;
    private List<Parcelable> selectedSeats;
    private boolean buttonOff = false;

    public static FullSeatLayoutFragment newInstance(ArrayList<Integer> previousFragmentIds, Parcelable parcelable) {
        FullSeatLayoutFragment fullSeatLayoutFragment = new FullSeatLayoutFragment();
        Bundle bundle = new Bundle();
        bundle.putIntegerArrayList(PREVIOUS_FRAGMENT_ID_TAG, previousFragmentIds);
        bundle.putParcelable(PARCELABLE_FRAGMENT_ITEM_ID_TAG, parcelable);
        fullSeatLayoutFragment.setArguments(bundle);
        return fullSeatLayoutFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (parcelable != null && parcelable instanceof SeatLayoutData) {
            seatLayoutData = (SeatLayoutData) parcelable;
            seatLayout = gson.fromJson(seatLayoutData.getSeatLayoutAsJson(), SeatLayout.class);
        }
        appManager = new AppManager(getActivity());
        selectedSeats = new ArrayList<>();

        Constant.isSoudia = false;
        // Obtain the shared Tracker instance.
        AppController application = (AppController) getActivity().getApplication();
        mTracker = application.getDefaultTracker();
        mTracker.setScreenName("Full Seat Layout");
        // Send a screen view.
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.fragment_full_seat_layout, container, false);
        }
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    void initializeEditTextComponents() {

    }

    @Override
    void initializeButtonComponents() {
        doneButton = (Button) rootView.findViewById(R.id.done_button);
        doneButton.setEnabled(false);
    }

    private void initializeSeats() {
        Gson gson = new GsonBuilder().create();
        SeatLayout seatLayout = gson.fromJson(seatLayoutData.getSeatLayoutAsJson(), SeatLayout.class);
        ArrayList<SeatNumberData> firstRowSeats = new ArrayList<>();
        ArrayList<SeatNumberData> secondRowSeats = new ArrayList<>();
        ArrayList<SeatNumberData> thirdRowSeats = new ArrayList<>();
        ArrayList<SeatNumberData> forthRowSeats = new ArrayList<>();
        ArrayList<SeatNumberData> middleRowSeats = new ArrayList<>();
        ArrayList<SeatNumberData> middleRowSeatsRight = new ArrayList<>();
        addSeatSystemTwo(seatLayout, firstRowSeats, secondRowSeats, thirdRowSeats, forthRowSeats, middleRowSeats, middleRowSeatsRight);
        for (SeatNumberData seatNumberData : firstRowSeats) {
            seatOneView.addView(getSeatImageButton(seatNumberData));
        }
        for (SeatNumberData seatNumberData : secondRowSeats) {
            seatTwoView.addView(getSeatImageButton(seatNumberData));
        }
        for (SeatNumberData seatNumberData : thirdRowSeats) {
            seatThreeView.addView(getSeatImageButton(seatNumberData));
        }
        for (SeatNumberData seatNumberData : forthRowSeats) {
            seatFourView.addView(getSeatImageButton(seatNumberData));
        }
        for (SeatNumberData seatNumberData : middleRowSeats) {
            seatMiddleView.addView(getSeatImageButton(seatNumberData));
        }
        for (SeatNumberData seatNumberData : middleRowSeatsRight) {
            seatMiddleViewRight.addView(getSeatImageButton(seatNumberData));
        }
        if (selectedSeats.size() > 0) {
            doneButton.setEnabled(true);
        }
    }

    private int addSeatSystemTwo(SeatLayout seatLayout, ArrayList<SeatNumberData> firstRowSeats, ArrayList<SeatNumberData> secondRowSeats, ArrayList<SeatNumberData> thirdRowSeats, ArrayList<SeatNumberData> forthRowSeats, ArrayList<SeatNumberData> middleRowSeats, ArrayList<SeatNumberData> middleRowRight) {
        int totalSeat = 0;
        int totalColumn = seatLayout.getSeatLayoutData().getSeat().getGrid().getColumn();
        Log.d(TAG, "totalColumn - " + totalColumn);
        for (int i = 0; i < seatLayout.getSeatLayoutData().getSeat().getGrid().getColumn(); i++) {
            ArrayList<SeatNumber> seatNumbers = seatLayout.getSeatLayoutData().getSeat().getSeatNumbers().get(i);
            for (int j = 0; j < seatLayout.getSeatLayoutData().getSeat().getGrid().getRow(); j++) {
                SeatNumber seatNumber = seatNumbers.get(j);
                if (seatNumber.getSeatNumber().trim().length() != 0) {
                    totalSeat++;
                    if (totalColumn == 6) {
                        switch (i) {
                            case 0:
                                Log.d(TAG, "first row-" + seatNumber.getSeatNumber());
                                firstRowSeats.add(new SeatNumberData(seatNumber));
                                break;
                            case 1:
                                Log.d(TAG, "second row-" + seatNumber.getSeatNumber());
                                secondRowSeats.add(new SeatNumberData(seatNumber));
                                break;
                            case 2:
                                Log.d(TAG, "middle row-" + seatNumber.getSeatNumber());
                                middleRowSeats.add(new SeatNumberData(seatNumber));
                                break;
                            case 3:
                                Log.d(TAG, "middle Right row-" + seatNumber.getSeatNumber());
                                middleRowRight.add(new SeatNumberData(seatNumber));
                                break;
                            case 4:
                                Log.d(TAG, "third row-" + seatNumber.getSeatNumber());
                                thirdRowSeats.add(new SeatNumberData(seatNumber));
                                break;
                            case 5:
                                Log.d(TAG, "forth row-" + seatNumber.getSeatNumber());
                                forthRowSeats.add(new SeatNumberData(seatNumber));
                                break;
                        }

                    } else if (totalColumn == 5) {
                        switch (i) {
                            case 0:
                                Log.d(TAG, "first row-" + seatNumber.getSeatNumber());
                                firstRowSeats.add(new SeatNumberData(seatNumber));
                                break;
                            case 1:
                                Log.d(TAG, "second row-" + seatNumber.getSeatNumber());
                                secondRowSeats.add(new SeatNumberData(seatNumber));
                                break;
                            case 2:
                                Log.d(TAG, "middle row-" + seatNumber.getSeatNumber());
                                middleRowSeats.add(new SeatNumberData(seatNumber));
                                break;
                            case 3:
                                Log.d(TAG, "third row-" + seatNumber.getSeatNumber());
                                thirdRowSeats.add(new SeatNumberData(seatNumber));
                                break;
                            case 4:
                                Log.d(TAG, "forth row-" + seatNumber.getSeatNumber());
                                forthRowSeats.add(new SeatNumberData(seatNumber));
                                break;
                        }
                    } else if (totalColumn == 4) {
                        if (i == 0) {
                            Log.d(TAG, "first row-" + seatNumber.getSeatNumber());
                            firstRowSeats.add(new SeatNumberData(seatNumber));
                        } else if (i == 1) {
                            Log.d(TAG, "second row-" + seatNumber.getSeatNumber());
                            secondRowSeats.add(new SeatNumberData(seatNumber));
                        } else if (i == 2) {
                            Log.d(TAG, "third row-" + seatNumber.getSeatNumber());
                            thirdRowSeats.add(new SeatNumberData(seatNumber));
                        } else if (i == 3) {
                            Log.d(TAG, "forth row-" + seatNumber.getSeatNumber());
                            forthRowSeats.add(new SeatNumberData(seatNumber));
                        }
                    }
                }
            }
        }
        return totalSeat;
    }


    private void reserveSeat(final ImageButton seatButton, final SeatNumberData seatNumberData) {
        String tripRouteId = seatLayout.getSeatLayoutData().getSeatLayoutQuery().getTripRouteId();
        String ticketId = Integer.toString(seatNumberData.getTicketId());
        String appVersion = appManager.getAppVersion();
        String deviceId = appManager.getDeviceId();
        Map<String, String> params = new HashMap<>();
        params.put(API.Parameter.ROUTE_ID, tripRouteId);
        params.put(API.Parameter.TICKET_ID, ticketId);
        params.put(API.Parameter.ANDROID_DEVICE_ID, deviceId);
        params.put(API.Parameter.ANDROID_APP_VERSION, appVersion);
        progressBarMessageTextView.setText("Reserving your Seat...");
        GetUrlBuilder getUrlBuilder = new GetUrlBuilder(API.RESERVE_SEAT_API_URL, params);
        Log.e(TAG, getUrlBuilder.getQueryUrl());
        Log.e(TAG, params.toString());
//        Toast.makeText(getActivity(), ">>>> \t" + seatLayoutData.getCompanyId(), Toast.LENGTH_SHORT).show();
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, getUrlBuilder.getQueryUrl(),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e(TAG, response.toString());
                        buttonOff = false;
                        progressBarDialog.cancel();
                        try {
                            if (response.toString().contains("data")) {

                                reserveSeatUIUpdate(seatButton, seatNumberData);


                            } else {
                                seatButton.setImageResource(R.mipmap.ic_economy_seat_white_booked_36dp);
                                seatButton.setEnabled(false);
                                JSONArray messages = response.getJSONObject("error").getJSONArray("messages");
                                makeAToast(messages.get(0).toString(), Toast.LENGTH_SHORT);
                            }
                        } catch (Exception e) {
                            makeAToast("Something went wrong, please try again later.", Toast.LENGTH_SHORT);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                buttonOff = false;
                progressBarDialog.cancel();
                makeAToast("Something went wrong, please try again later.", Toast.LENGTH_SHORT);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<String, String>();
                String creds = String.format("%s:%s", "guest", "33615sua");
                String auth = "Basic " + Base64.encodeToString(creds.getBytes(), Base64.DEFAULT);
                params.put("Authorization", auth);
                return params;
            }
        };
        appController.addToRequestQueue(jsonObjectRequest);
        progressBarDialog.show();
    }

    private void releaseSeat(final ImageButton seatButton, final SeatNumberData seatNumberData) {
        progressBarMessageTextView.setText("Releasing your Seat...");
        String tripRouteId = seatLayout.getSeatLayoutData().getSeatLayoutQuery().getTripRouteId();
        String ticketId = Integer.toString(seatNumberData.getTicketId());
        String appVersion = appManager.getAppVersion();
        String deviceId = appManager.getDeviceId();
        Map<String, String> params = new HashMap<>();
        params.put(API.Parameter.ROUTE_ID, tripRouteId);
        params.put(API.Parameter.TICKET_ID, ticketId);
        params.put(API.Parameter.ANDROID_DEVICE_ID, deviceId);
        params.put(API.Parameter.ANDROID_APP_VERSION, appVersion);

        GetUrlBuilder getUrlBuilder = new GetUrlBuilder(API.RELEASE_SEAT_API_URL, params);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, getUrlBuilder.getQueryUrl(),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        buttonOff = false;
                        if (seatButton != null) {
                            progressBarDialog.cancel();
                            try {
                                if (response.toString().contains("data")) {
                                    releaseSeatUIUpdate(seatButton, seatNumberData);

                                } else {
                                    JSONArray messages = response.getJSONObject("error").getJSONArray("messages");
                                    if (Constant.isSoudia) {

                                    } else {
                                        makeAToast(messages.get(0).toString(), Toast.LENGTH_SHORT);
                                    }

                                }
                            } catch (Exception e) {
                                makeAToast("Something went wrong, please try again later.", Toast.LENGTH_SHORT);
                            }
                        } else {
                            selectedSeats.remove(0);
                            if (selectedSeats.size() != 0) {
                                releaseSeat(null, (SeatNumberData) selectedSeats.get(0));
                            } else {
                                progressBarDialog.cancel();
                                FullSeatLayoutFragment.super.onBackPressed();
                            }
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                buttonOff = false;
                progressBarDialog.cancel();
                if (seatButton == null) {
                    makeAToast("Something went wrong, please try again later.", Toast.LENGTH_SHORT);
                    FullSeatLayoutFragment.super.onBackPressed();
                } else {
                    makeAToast("Something went wrong, please try again later.", Toast.LENGTH_SHORT);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<String, String>();
                String creds = String.format("%s:%s", "guest", "33615sua");
                String auth = "Basic " + Base64.encodeToString(creds.getBytes(), Base64.DEFAULT);
                params.put("Authorization", auth);
                return params;
            }
        };
        appController.addToRequestQueue(jsonObjectRequest);
        if (!progressBarDialog.isShowing()) {
            progressBarDialog.show();
        }
    }

    @Override
    void initializeTextViewComponents() {
        addSeatTextView = (TextView) rootView.findViewById(R.id.add_seat_text_view);
        newBaseFareTextView = (TextView) rootView.findViewById(R.id.new_base_fare_text_view);
        oldBaseFareTextView = (TextView) rootView.findViewById(R.id.old_base_fare_text_view);
        oldBaseFareTextView.setPaintFlags(oldBaseFareTextView.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        if (Constant.KEY_DISCOUNT > 0) {
            oldBaseFareTextView.setVisibility(View.VISIBLE);
        }
        if (seatLayoutData.getSelectedSeat().size() != 0) {
            selectedSeats = seatLayoutData.getSelectedSeat();
            updateBaseFareTextView();
            updateSelectedSeatsTextView();
            doneButton.setEnabled(true);
        }
    }

    @Override
    void initializeOtherViewComponents() {
        seatOneView = (LinearLayout) rootView.findViewById(R.id.seat_one_view);
        seatTwoView = (LinearLayout) rootView.findViewById(R.id.seat_two_view);
        seatThreeView = (LinearLayout) rootView.findViewById(R.id.seat_three_view);
        seatFourView = (LinearLayout) rootView.findViewById(R.id.seat_four_view);
        seatMiddleView = (LinearLayout) rootView.findViewById(R.id.seat_middle_view);
        seatMiddleViewRight = (LinearLayout) rootView.findViewById(R.id.seat_middle_view_right);
        initializeSeats();
        createProgressDialogView();
    }

    @Override
    void initializeOnclickListener() {
        doneButton.setOnClickListener(this);
    }

    @Override
    void removeOnclickListener() {
        doneButton.setOnClickListener(null);

    }

    private ImageButton getSeatImageButton(SeatNumberData seatNumberData) {
        boolean enable = seatNumberData.getSeatAvailability() == 1;
        ImageButton imageButton = new ImageButton(getContext());
        imageButton.setPadding(
                getResources().getDimensionPixelSize(R.dimen.seat_padding),
                getResources().getDimensionPixelSize(R.dimen.seat_padding),
                getResources().getDimensionPixelSize(R.dimen.seat_padding),
                getResources().getDimensionPixelSize(R.dimen.seat_padding));
        imageButton.setTag(seatNumberData);
        imageButton.setBackgroundResource(R.drawable.rect_flat_button);
        imageButton.setImageResource(enable ? R.mipmap.ic_economy_seat_white_normal_36dp : R.mipmap.ic_economy_seat_white_booked_36dp);
        if (seatLayoutData.getSelectedSeat().size() != 0) {
            if (seatLayoutData.getSelectedSeat().contains(seatNumberData)) {
                imageButton.setImageResource(enable ? R.mipmap.ic_economy_seat_white_picked_36dp : R.mipmap.ic_economy_seat_white_booked_36dp);
            }
        }
        imageButton.setOnClickListener(this);
        imageButton.setEnabled(enable);
        return imageButton;
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        switch (view.getId()) {
            case R.id.back_button:

                return;
            case R.id.done_button:
                if (selectedSeats.size() == 0) {
                    Toast.makeText(getContext(), "You must select at least 1 seat.", Toast.LENGTH_SHORT).show();
                } else {
                    seatLayoutData.setSelectedSeat(selectedSeats);
                    previousFragmentIds.remove(previousFragmentIds.size() - 1);
                    onFragmentChangeCallListener.fragmentChange(OnFragmentInteractionListener.SEAT_LAYOUT_FRAGMENT_ID, seatLayoutData, previousFragmentIds);
                }
                return;
        }
        if (view.isEnabled()) {
            if (selectedSeats != null) {

                if ((view instanceof ImageButton) && !buttonOff) {
                    buttonOff = true;
                    ImageButton tempView = (ImageButton) view;
                    SeatNumberData seatNumberData = (SeatNumberData) tempView.getTag();
                    if (selectedSeats.size() != 4) {
                        if (!selectedSeats.contains(seatNumberData)) {
                            if (seatLayoutData.getBookTicketData().getToCity().equalsIgnoreCase("Kolkata")) {
                                if (seatLayoutData.getCompanyId() == 8 && selectedSeats.size() == 1) {

                                    Toast.makeText(getContext(), "You can select maximum 1 Seats.", Toast.LENGTH_SHORT).show();
                                    buttonOff = false;

                                } else {
                                    reserveSeat(tempView, seatNumberData);
                                }
                            }else if(seatLayoutData.getCompanyId() == 64 && selectedSeats.size() == 1){
                                Toast.makeText(getContext(), "You can select maximum 1 Seats.", Toast.LENGTH_SHORT).show();
                                buttonOff = false;
                            } else {
                                reserveSeat(tempView, seatNumberData);

                            }

                        } else {
                            releaseSeat(tempView, seatNumberData);
                        }
                    } else if (selectedSeats.size() == 4) {
                        if (!selectedSeats.contains(seatNumberData)) {
                            Toast.makeText(getContext(), "You can select maximum 4 Seats.", Toast.LENGTH_SHORT).show();
                            buttonOff = false;
                        } else {
                            releaseSeat(tempView, seatNumberData);
                        }
                    }
                }
            }
        }
    }

    private void reserveSeatUIUpdate(ImageButton tempView, SeatNumberData seatNumberData) {
        tempView.setImageResource(R.mipmap.ic_economy_seat_white_picked_36dp);
        selectedSeats.add(seatNumberData);
        updateSelectedSeatsTextView();
        updateBaseFareTextView();
        doneButton.setEnabled(true);
        if (selectedSeats.size() > 0) {
            doneButton.setEnabled(true);
        }
    }

    private void releaseSeatUIUpdate(ImageButton tempView, SeatNumberData seatNumberData) {
        tempView.setImageResource(R.mipmap.ic_economy_seat_white_normal_36dp);
        selectedSeats.remove(seatNumberData);
        updateSelectedSeatsTextView();
        updateBaseFareTextView();
        if (selectedSeats.size() == 0) {
            doneButton.setEnabled(false);
        }
    }

    private void updateBaseFareTextView() {
        String newBaseFareString = "0.00";
        String oldBaseFareString = "0.00";
        for (int i = 0; i < selectedSeats.size(); i++) {
            SeatNumberData seatNumberData = (SeatNumberData) selectedSeats.get(i);
            newBaseFareString = String.format("%.02f", Float.parseFloat(newBaseFareString) + (seatNumberData.getSeatFare() - Constant.KEY_DISCOUNT));
            oldBaseFareString = String.format("%.02f", Float.parseFloat(oldBaseFareString) + seatNumberData.getSeatFare());
        }
        newBaseFareString = "৳ " + newBaseFareString;
        newBaseFareTextView.setText(newBaseFareString);

        if (Constant.KEY_DISCOUNT > 0) {
            oldBaseFareString = "৳ " + oldBaseFareString;
            oldBaseFareTextView.setText(oldBaseFareString);
        }
    }

    @Override
    public void onBackPressed() {
        if (selectedSeats.size() != 0) {
            releaseSeat(null, (SeatNumberData) selectedSeats.get(0));
        } else {
            super.onBackPressed();
        }
    }

    private void updateSelectedSeatsTextView() {
        Collections.sort(selectedSeats, new Comparator<Parcelable>() {
            @Override
            public int compare(Parcelable leftParcelable, Parcelable rightParcelable) {
                int lhs = ((SeatNumberData) leftParcelable).getTicketId();
                int rhs = ((SeatNumberData) rightParcelable).getTicketId();
                Log.d(TAG, Integer.toString(lhs < rhs ? -1 : (lhs == rhs ? 0 : 1)));
                return lhs < rhs ? -1 : (lhs == rhs ? 0 : 1);
            }
        });
        String selectedSeat = "";
        for (int i = 0; i < selectedSeats.size(); i++) {
            SeatNumberData seatNumberData = (SeatNumberData) selectedSeats.get(i);
            selectedSeat += seatNumberData.getSeatNumber();
            if (i != selectedSeats.size() - 1) {
                selectedSeat += ", ";
            }
        }
        addSeatTextView.setText(selectedSeat.trim());
    }
}
