package com.android.food.foodyhut.database.data.item;

/**
 * Created by tuman on 4/5/2015.
 */
public class CibDataItem implements Item {
    private String cibShortCode;
    private String cibName;
    private float cibPercentage;
    private String cibType;

    public CibDataItem() {
    }

    public CibDataItem(String cibShortCode, String cibName, float cibPercentage, String cibType) {
        this.cibShortCode = cibShortCode;
        this.cibName = cibName;
        this.cibPercentage = cibPercentage;
        this.cibType = cibType;
    }

    public String getCibShortCode() {
        return cibShortCode;
    }

    public void setCibShortCode(String cibShortCode) {
        this.cibShortCode = cibShortCode;
    }

    public String getCibName() {
        return cibName;
    }

    public void setCibName(String cibName) {
        this.cibName = cibName;
    }

    public float getCibPercentage() {
        return cibPercentage;
    }

    public void setCibPercentage(float cibPercentage) {
        this.cibPercentage = cibPercentage;
    }

    public String getCibType() {
        return cibType;
    }

    public void setCibType(String cibType) {
        this.cibType = cibType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CibDataItem)) return false;

        CibDataItem that = (CibDataItem) o;

        if (Float.compare(that.getCibPercentage(), getCibPercentage()) != 0) return false;
        if (!getCibShortCode().equals(that.getCibShortCode())) return false;
        if (!getCibName().equals(that.getCibName())) return false;
        return getCibType().equals(that.getCibType());

    }

    @Override
    public int hashCode() {
        int result = getCibShortCode().hashCode();
        result = 31 * result + getCibName().hashCode();
        result = 31 * result + (getCibPercentage() != +0.0f ? Float.floatToIntBits(getCibPercentage()) : 0);
        result = 31 * result + getCibType().hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "CibDataItem{" +
                "cibShortCode='" + cibShortCode + '\'' +
                ", cibName='" + cibName + '\'' +
                ", cibPercentage=" + cibPercentage +
                ", cibType='" + cibType + '\'' +
                '}';
    }
}
