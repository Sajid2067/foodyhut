package com.android.food.foodyhut.api.data.item.policy;

import com.google.gson.annotations.SerializedName;

/**
 * Created by sajid on 11/3/2015.
 */
public class Policy {

    @SerializedName("tickets")
    private TicketPolicy ticketPolicy;

    public Policy() {
    }

    public Policy(TicketPolicy ticketPolicy) {

        this.ticketPolicy = ticketPolicy;
    }

    public TicketPolicy getTicketPolicy() {

        return ticketPolicy;
    }

    public void setTicketPolicy(TicketPolicy ticketPolicy) {
        this.ticketPolicy = ticketPolicy;
    }

    @Override
    public String toString() {
        return "Policy{" +
                "ticketPolicy=" + ticketPolicy +
                '}';
    }
}
