package com.android.food.foodyhut.fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.TypedArray;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.food.foodyhut.R;
import com.android.food.foodyhut.activity.AboutActivity;
import com.android.food.foodyhut.activity.LoginActivity;
import com.android.food.foodyhut.activity.MainAppActivity;
import com.android.food.foodyhut.activity.SignUpActivity;
import com.android.food.foodyhut.activity.annotation.FragmentId;
import com.android.food.foodyhut.activity.listener.OnFragmentInteractionListener;
import com.android.food.foodyhut.activity.listener.OnSlideMenuItemClickListener;
import com.android.food.foodyhut.adapter.recyclerview.SlideDrawerMenuAdapter;
import com.android.food.foodyhut.adapter.recyclerview.item.MenuItem;
import com.android.food.foodyhut.adapter.recyclerview.item.RecyclerViewBaseItem;
import com.android.food.foodyhut.api.data.item.user.UserStatus;

import java.util.ArrayList;
import java.util.List;

import static android.widget.Toast.LENGTH_SHORT;
import static android.widget.Toast.makeText;
import static com.android.food.foodyhut.activity.listener.OnSlideMenuItemClickListener.ABOUT;
import static com.android.food.foodyhut.activity.listener.OnSlideMenuItemClickListener.ACCOUNT;
import static com.android.food.foodyhut.activity.listener.OnSlideMenuItemClickListener.HOME;
import static com.android.food.foodyhut.activity.listener.OnSlideMenuItemClickListener.LOGIN_LOGOUT;
import static com.android.food.foodyhut.activity.listener.OnSlideMenuItemClickListener.MY_BUSINESS;
import static com.android.food.foodyhut.activity.listener.OnSlideMenuItemClickListener.PROFILE;
import static com.android.food.foodyhut.activity.listener.OnSlideMenuItemClickListener.SHARE_FH;
import static com.android.food.foodyhut.activity.listener.OnSlideMenuItemClickListener.SIGN_UP;
import static com.android.food.foodyhut.activity.listener.OnSlideMenuItemClickListener.TERMS_AND_PRIVACY;

/**
 * Created by sajid on 10/27/2015.
 */
public class SlideMenuFragment extends BaseFragment implements View.OnClickListener, RecyclerView.OnItemTouchListener {

    private static final String TAG = SlideMenuFragment.class.getSimpleName();

    private RecyclerView mainMenuRecyclerView;
   // private TextView fullName;
  //  private TextView mobile;
  //  private ImageView profileImage;
 //   private RelativeLayout profileBoxRelativeLayout;
    private View aboutButton;
    private AlertDialog errorAlertDialog;

    private SlideDrawerMenuAdapter slideDrawerMenuAdapter;
    private List<RecyclerViewBaseItem> recyclerViewBaseItems;
    private LinearLayoutManager linearLayoutManager;


    private GestureDetector mGestureDetector;
    private OnSlideMenuItemClickListener onSlideMenuItemClickListener;
    private UserStatus userStatus;


    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);

    }

    @Override
    public void onStart() {
        //setTextViewValues();
        super.onStart();
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        createSlidingMenu();

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        onSlideMenuItemClickListener = (OnSlideMenuItemClickListener) getActivity();
    }

    private void createSlidingMenu() {
        recyclerViewBaseItems = new ArrayList<>();
        String[] names = getResources().getStringArray(R.array.slide_drawer_item_names);
        TypedArray icons = getResources().obtainTypedArray(R.array.slide_drawer_icons);
        MenuItem item;
        for (int i = 0; i < names.length; i++) {
            item = new MenuItem();
            item.setMenuItemName(names[i]);
            item.setIconResourceId(icons.getResourceId(i, -1));
            recyclerViewBaseItems.add(item);
        }
        icons.recycle();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.fragment_slide_menu, container, false);
        }
        userStatus=new UserStatus(getContext());
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    void initializeEditTextComponents() {

    }

    @Override
    void initializeButtonComponents() {
        aboutButton = rootView.findViewById(R.id.about_button);

    }

    @Override
    void initializeTextViewComponents() {
      //  fullName=(TextView)findViewById(R.id.userName);
     //   mobile=(TextView)findViewById(R.id.mobile);
      //  profileImage=(ImageView) findViewById(R.id.profile_image);

    }

    @Override
    void initializeOtherViewComponents() {
     //   profileBoxRelativeLayout=(RelativeLayout)findViewById(R.id.profileBox);
        mainMenuRecyclerView = (RecyclerView) rootView.findViewById(R.id.main_menu_recycler_view);
        linearLayoutManager = new LinearLayoutManager(getContext());
        mainMenuRecyclerView.setLayoutManager(linearLayoutManager);
        mainMenuRecyclerView.setHasFixedSize(true);
        slideDrawerMenuAdapter = new SlideDrawerMenuAdapter(getContext(), recyclerViewBaseItems);
        mainMenuRecyclerView.setAdapter(slideDrawerMenuAdapter);
        mGestureDetector = new GestureDetector(getActivity(), new GestureDetector.SimpleOnGestureListener() {

            @Override
            public boolean onSingleTapUp(MotionEvent e) {
                return true;
            }

        });
        mainMenuRecyclerView.addOnItemTouchListener(this);
    }

    @Override
    void initializeOnclickListener() {
        aboutButton.setOnClickListener(this);
     //   profileBoxRelativeLayout.setOnClickListener(this);
      //  profileBoxRelativeLayout.setFocusable(true);
    }

    @Override
    void removeOnclickListener() {
        aboutButton.setOnClickListener(null);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.about_button:
                startActivity(new Intent(getActivity(), AboutActivity.class));
                onSlideMenuItemClickListener.closeDrawer();
                break;
//            case R.id.profileBox:
//                userStatus=new UserStatus(getContext());
//                if(userStatus.isLogin()){
//                    createAlertDialog();
//                }else {
//
//                    finishActivity();
//                }
//                onSlideMenuItemClickListener.closeDrawer();
//                break;
        }
    }



    @Override
    public boolean onInterceptTouchEvent(RecyclerView recyclerView, MotionEvent motionEvent) {
        View child = recyclerView.findChildViewUnder(motionEvent.getX(), motionEvent.getY());
        if (child != null && mGestureDetector.onTouchEvent(motionEvent)) {
            @FragmentId
            int oldFragmentId = OnFragmentInteractionListener.LOCATION_FRAGMENT_FH;

            switch (recyclerView.getChildAdapterPosition(child)) {

                case HOME:
                    onSlideMenuItemClickListener.onItemSelect(OnFragmentInteractionListener.LOCATION_FRAGMENT_FH);
                    return true;
                case PROFILE:
                    onSlideMenuItemClickListener.onItemSelect(OnFragmentInteractionListener.TIMELINE_VISIT_FAVOURITE_FRAGMENT_ID);
                    return true;
                case ACCOUNT:
                    onSlideMenuItemClickListener.onItemSelect(OnFragmentInteractionListener.ACCOUNT_FRAGMENT_ID);
                    return true;
                case TERMS_AND_PRIVACY:
                    onSlideMenuItemClickListener.onItemSelect(OnFragmentInteractionListener.TERMS_AND_CONDITION_FRAGMENT_ID);
                    return true;
                case ABOUT:
                    onSlideMenuItemClickListener.onItemSelect(OnFragmentInteractionListener.TERMS_AND_CONDITION_FRAGMENT_ID);
                    return true;
                case MY_BUSINESS:
                    onSlideMenuItemClickListener.onItemSelect(OnFragmentInteractionListener.LOCATION_FRAGMENT_FH);
                    return true;
                case LOGIN_LOGOUT:
                    createAlertDialog();
                    return true;
                case SIGN_UP:
                    signUp();
                    return true;
                case SHARE_FH:
                    onSlideMenuItemClickListener.onItemSelect(OnFragmentInteractionListener.TERMS_AND_CONDITION_FRAGMENT_ID);
                    return true;


            }
        }

        return false;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        onSlideMenuItemClickListener.closeDrawer();
    }


    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {

    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

    }

    private void createAlertDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setMessage("Do you want to Log Out?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        userStatus=new UserStatus(getContext());
                        userStatus.setLogin(false);
                        userStatus.setUser_id("");
                    //    userStatus.setUser_full_name("");
                        userStatus.setUser_mobile("");
                        //setTextViewValues();
                        dialog.cancel();
                        makeText(getActivity(), "Logged Out successfully.", LENGTH_SHORT).show();

                        finishActivity();
                        //Back to LocationFragmentFH
                      //  onSlideMenuItemClickListener.onItemSelect(OnFragmentInteractionListener.LOCATION_FRAGMENT_FH);
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //  Action for 'NO' Button
                        dialog.cancel();
                    }
                });

        //Creating dialog box
        AlertDialog alert = builder.create();
        alert.show();


    }

    void signUp(){
        onSlideMenuItemClickListener.closeDrawer();
        startActivity(new Intent(getActivity(), SignUpActivity.class));
        getActivity().finish();
    }

    void finishActivity(){
        onSlideMenuItemClickListener.closeDrawer();
        startActivity(new Intent(getActivity(), LoginActivity.class));
        getActivity().finish();
    }

//      public void setTextViewValues(){
//          userStatus=new UserStatus(getContext());
//        if(userStatus.isLogin()) {
//         //   fullName.setText(userStatus.getUser_full_name());
//            mobile.setVisibility(View.VISIBLE);
//            mobile.setText(userStatus.getUser_mobile());
//        }else {
//            fullName.setText("Log In");
//            ViewGroup.MarginLayoutParams mlp = (ViewGroup.MarginLayoutParams) fullName
//                    .getLayoutParams();
//
//            mlp.setMargins(0, 0, 0, 4);
//            fullName.setLayoutParams(mlp);
//            mobile.setText("");
//            mobile.setVisibility(View.GONE);
//
//        }
//    }


}
