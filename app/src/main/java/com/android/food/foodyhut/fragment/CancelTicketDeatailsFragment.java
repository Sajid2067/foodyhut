package com.android.food.foodyhut.fragment;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.android.food.foodyhut.R;
import com.android.food.foodyhut.activity.listener.OnFragmentInteractionListener;
import com.android.food.foodyhut.adapter.recyclerview.CancelTicketListAdapter;
import com.android.food.foodyhut.api.data.item.BoardingPoint;
import com.android.food.foodyhut.api.data.item.bookticket.BookTicket;
import com.android.food.foodyhut.api.data.item.trip.TripList;
import com.android.food.foodyhut.application.AppController;
import com.android.food.foodyhut.fragment.item.BoardingPointData;
import com.android.food.foodyhut.fragment.item.CancelTicketRequestData;
import com.android.food.foodyhut.fragment.item.FilterData;
import com.android.food.foodyhut.fragment.item.SeatLayoutData;
import com.android.food.foodyhut.toolbox.GetUrlBuilder;
import com.android.food.foodyhut.toolbox.ObjectRequest;
import com.android.food.foodyhut.util.API;
import com.android.food.foodyhut.util.AppManager;
import com.android.food.foodyhut.util.Constant;
import com.android.food.foodyhut.util.SortBookTicketDataButtonUIUpdate;
import com.android.food.foodyhut.view.widget.ProgressBar;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static com.android.food.foodyhut.util.SortBookTicketDataButtonUIUpdate.DEPARTURE_SORT;
import static com.android.food.foodyhut.util.SortBookTicketDataButtonUIUpdate.FARE_SORT;
import static com.android.food.foodyhut.util.SortBookTicketDataButtonUIUpdate.OPERATOR_SORT;

/**
 * Created by sajid on 10/21/2015.
 */
public class CancelTicketDeatailsFragment extends BaseFragment implements Response.Listener<BookTicket>, Response.ErrorListener, RecyclerView.OnItemTouchListener {
    private static String[] PERMISSIONS_PHONECALL = {Manifest.permission.CALL_PHONE};
    private static final int PERMISSIONS_REQUEST_PHONE_CALL = 100;
    private static final String TAG = CancelTicketDeatailsFragment.class.getSimpleName();
    private static final SimpleDateFormat monthFormat = new SimpleDateFormat("MMM", Locale.US);
    private static final SimpleDateFormat dayFormat = new SimpleDateFormat("EEE", Locale.US);
    private static final String dateFormat = "%s %s %s %d";
    private RecyclerView bookTicketRecyclerView;
    private ProgressBar bookTicketProgressBar;
    private ObjectRequest<BookTicket> bookTicketObjectRequest;
    private CancelTicketListAdapter bookTicketListAdapter;
    private List<TripList> tripLists;

    private View bookTicketView;
    Tracker mTracker;
    private TextView totalTripListTextView;
    private BookTicket bookTicket;
    private GestureDetector mGestureDetector;
    private SortBookTicketDataButtonUIUpdate sortBookTicketDataButtonUIUpdate;
    private Calendar calendar;
    private LinearLayoutManager linearLayoutManager;
    private AppController appController = AppController.getInstance();

    private CancelTicketRequestData cancelTicketRequestData;
    private AppManager appManager;
    private FilterData filterData;
    private TextView noResultTextView;

    private boolean listUpdated = false;

    public static CancelTicketDeatailsFragment newInstance(ArrayList<Integer> previousFragmentIds, Parcelable parcelable) {
        CancelTicketDeatailsFragment bookTicketFragment = new CancelTicketDeatailsFragment();
        Bundle bundle = new Bundle();
        bundle.putIntegerArrayList(PREVIOUS_FRAGMENT_ID_TAG, previousFragmentIds);
        bundle.putParcelable(PARCELABLE_FRAGMENT_ITEM_ID_TAG, parcelable);
        bookTicketFragment.setArguments(bundle);
        return bookTicketFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        appManager = new AppManager(getActivity());
        if (parcelable instanceof CancelTicketRequestData) {
            cancelTicketRequestData = (CancelTicketRequestData) parcelable;

        }

        // Obtain the shared Tracker instance.
        AppController application = (AppController) getActivity().getApplication();
        mTracker = application.getDefaultTracker();
        mTracker.setScreenName("BookTicket");
        // Send a screen view.
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.fragment_cancel_ticket_details, container, false);
            Constant.isSoudia = false;
//            Constant.isKolkataTrip = false;
        }

        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    private void requestTripList() {
        noResultTextView.setVisibility(GONE);
        connectionErrorView.setVisibility(GONE);
      //  calendar = new GregorianCalendar(bookTicketData.getCalendarData().getYear(), bookTicketData.getCalendarData().getMonth(), bookTicketData.getCalendarData().getDay());
      //  String date = appManager.getFormatDate(calendar.getTime());
        Map<String, String> params = new HashMap<>();
        params.put(API.Parameter.ANDROID_APP_VERSION, appManager.getAppVersion());
        params.put(API.Parameter.ANDROID_DEVICE_ID, appManager.getDeviceId());
        params.put(API.Parameter.MOBILE_NUMBER, cancelTicketRequestData.getMobile());
        params.put(API.Parameter.PNR, cancelTicketRequestData.getPnr());

        GetUrlBuilder getUrlBuilder = new GetUrlBuilder(API.SEARCH_TRIPS_API_URL, params);
        String url = getUrlBuilder.getQueryUrl();
        bookTicketObjectRequest = new ObjectRequest<>(API.Method.SEARCH_TRIPS_API_METHOD,
                url, null, this, this, BookTicket.class);
        bookTicket = null;
        appController.addToRequestQueue(bookTicketObjectRequest, "bookTicket");
        Log.d(TAG, url);
    }

    @Override
    void initializeEditTextComponents() {

    }

    @Override
    void initializeButtonComponents() {

    }

    @Override
    void initializeTextViewComponents() {
        noResultTextView = findViewById(R.id.no_result_text_view);
        totalTripListTextView = (TextView) rootView.findViewById(R.id.total_trip_list_text_view);



    }

    @Override
    void initializeOtherViewComponents() {

        tripLists = new ArrayList<>();
        bookTicketListAdapter = new CancelTicketListAdapter(getActivity(), tripLists, noResultTextView);
        bookTicketRecyclerView = (RecyclerView) rootView.findViewById(R.id.book_ticket_recycler_view);
        linearLayoutManager = new LinearLayoutManager(getContext());
        bookTicketRecyclerView.setHasFixedSize(true);
        bookTicketRecyclerView.setLayoutManager(linearLayoutManager);
        bookTicketRecyclerView.setAdapter(bookTicketListAdapter);
        bookTicketRecyclerView.addOnItemTouchListener(this);
        mGestureDetector = new GestureDetector(getActivity(), new GestureDetector.SimpleOnGestureListener() {

            @Override
            public boolean onSingleTapUp(MotionEvent e) {
                return true;
            }

        });
        connectionErrorView = findViewById(R.id.connection_error_view);
        mGestureDetector.setIsLongpressEnabled(true);
        bookTicketProgressBar = (ProgressBar) rootView.findViewById(R.id.book_ticket_progress_bar);
        bookTicketView = rootView.findViewById(R.id.book_ticket_view);
        if (bookTicket != null) {
            bookTicketView.setVisibility(VISIBLE);
            bookTicketProgressBar.setVisibility(GONE);
            this.tripLists = bookTicket.getBookTicketData().getTrip().getTripLists();
            bookTicketListAdapter.setItems(this.tripLists);
            bookTicketListAdapter.notifyDataSetChanged();
            String totalTrips = bookTicket.getBookTicketData().getTrip().getTripLists().size() + "";
            totalTripListTextView.setText(totalTrips);
        } else {
            if (appManager.isNetworkAvailable()) {
                requestTripList();
            } else {
                connectionErrorView.setVisibility(VISIBLE);
                bookTicketProgressBar.setVisibility(GONE);
            }
        }
    }

    @Override
    void initializeOnclickListener() {

    }

    @Override
    void removeOnclickListener() {

    }

    @Override
    public void onClick(View view) {
      //  Calendar calendar;
      //  calendar = new GregorianCalendar(bookTicketData.getCalendarData().getYear(), bookTicketData.getCalendarData().getMonth(), bookTicketData.getCalendarData().getDay());
        super.onClick(view);
        switch (view.getId()) {
            case R.id.filter_button:
                if (bookTicket != null) {
                    FilterData filterData = new FilterData();
                    Gson gson = new GsonBuilder().create();
                    if (listUpdated) {
                        filterData.setBookTicketAsJson(gson.toJson(bookTicket));
                        listUpdated = false;
                    } else {
                        filterData.setBookTicketAsJson(this.filterData.getBookTicketAsJson());
                    }
                  //  filterData.setBookTicketData(bookTicketData);
                    previousFragmentIds.add(OnFragmentInteractionListener.BOOK_TICKET_FRAGMENT_ID);
                    onFragmentChangeCallListener.fragmentChange(OnFragmentInteractionListener.FILTER_FRAGMENT_ID, filterData, previousFragmentIds);
                    break;
                } else {
                    makeAToast("Please Wait a while!", Toast.LENGTH_SHORT);
                    break;
                }
            case R.id.operation_sort_button:
                sortBookTicketDataButtonUIUpdate.sortCase(OPERATOR_SORT);
                int operator = sortBookTicketDataButtonUIUpdate.getOperator();
                Log.e(TAG, "operation:" + operator);
                bookTicketListAdapter.getFilter().filter("operator:" + operator);
                break;
            case R.id.departure_sort_button:
                sortBookTicketDataButtonUIUpdate.sortCase(DEPARTURE_SORT);
                int departure = sortBookTicketDataButtonUIUpdate.getDeparture();
                Log.e(TAG, "departure:" + departure);
                bookTicketListAdapter.getFilter().filter("departure:" + departure);
                break;
            case R.id.bus_fare_sort_button:
                sortBookTicketDataButtonUIUpdate.sortCase(FARE_SORT);
                int fare = sortBookTicketDataButtonUIUpdate.getFare();
                Log.e(TAG, "fare:" + fare);
                bookTicketListAdapter.getFilter().filter("fare:" + fare);
                break;
            case R.id.retry_button:
                noResultTextView.setVisibility(GONE);
                connectionErrorView.setVisibility(GONE);
                bookTicketProgressBar.setVisibility(VISIBLE);
                appController.cancelPendingRequests("bookTicket");
                if (appManager.isNetworkAvailable()) {
                    requestTripList();
                } else {
                    connectionErrorView.setVisibility(VISIBLE);
                    bookTicketProgressBar.setVisibility(GONE);
                }
                break;
            case R.id.back_date_button:
                calendar.add(Calendar.DAY_OF_YEAR, -1);
                Calendar todayCalendar = Calendar.getInstance();
                if (todayCalendar.get(Calendar.YEAR) == calendar.get(Calendar.YEAR) && todayCalendar.get(Calendar.MONTH) == calendar.get(Calendar.MONTH) && todayCalendar.get(Calendar.DAY_OF_MONTH) == calendar.get(Calendar.DAY_OF_MONTH)) {

                } else if (todayCalendar.getTimeInMillis() > calendar.getTimeInMillis()) {
                    Toast.makeText(getActivity(), "You can't Select an old date for your Trip.", Toast.LENGTH_SHORT).show();
                    return;
                }
                updateDataForTripListSearch(calendar);
                if (appManager.isNetworkAvailable()) {
                    requestTripList();
                } else {
                    connectionErrorView.setVisibility(VISIBLE);
                    bookTicketProgressBar.setVisibility(GONE);
                }
                break;
            case R.id.forward_date_button:
                calendar.add(Calendar.DAY_OF_YEAR, 1);
                updateDataForTripListSearch(calendar);
                if (appManager.isNetworkAvailable()) {
                    requestTripList();
                } else {
                    connectionErrorView.setVisibility(VISIBLE);
                    bookTicketProgressBar.setVisibility(GONE);
                }
                break;
        }

    }

    private void updateDataForTripListSearch(Calendar calendar) {
      //  bookTicketData.setCalendarData(new CalendarData(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)));
      //  String htmlDate = appManager.getHTMLFormatDate(bookTicketData.getCalendarData(), calendar);
        appController.cancelPendingRequests("bookTicket");
        bookTicketView.setVisibility(GONE);
        connectionErrorView.setVisibility(GONE);
        bookTicketProgressBar.setVisibility(VISIBLE);
    }

    @Override
    public void onResponse(BookTicket bookTicket) {
        if (bookTicket.getBookTicketData() != null) {
            Log.e(TAG, bookTicket.toString());
            listUpdated = true;
            this.bookTicket = bookTicket;
            bookTicketView.setVisibility(VISIBLE);
            connectionErrorView.setVisibility(GONE);
            bookTicketProgressBar.setVisibility(GONE);
            this.tripLists = bookTicket.getBookTicketData().getTrip().getTripLists();

            if (sortBookTicketDataButtonUIUpdate != null) {
                int departure = sortBookTicketDataButtonUIUpdate.getDeparture();
                int fare = sortBookTicketDataButtonUIUpdate.getFare();
                int operator = sortBookTicketDataButtonUIUpdate.getOperator();
                if (departure != 0) {
                    bookTicketListAdapter.setItems(this.tripLists, "departure:" + departure);
                } else if (fare != 0) {
                    bookTicketListAdapter.setItems(this.tripLists, "fare:" + fare);
                } else if (operator != 0) {
                    bookTicketListAdapter.setItems(this.tripLists, "operator:" + operator);
                } else {
                    bookTicketListAdapter.setItems(this.tripLists);
                }
            } else {
                bookTicketListAdapter.setItems(this.tripLists);
            }
            bookTicketListAdapter.notifyDataSetChanged();
            String totalTrips = bookTicket.getBookTicketData().getTrip().getTotal() + "";
            totalTripListTextView.setText(totalTrips);
        } else {
            Log.e(TAG, "error");
            bookTicketProgressBar.setVisibility(GONE);
            bookTicketView.setVisibility(GONE);
            connectionErrorView.setVisibility(VISIBLE);
            String message = "";
            int i = 0;
            for (String m : bookTicket.getBookTicketError().getMessages()) {
                if (i > 0) {
                    message += "\n";
                }
                message += m;
            }
            connectionErrorTextView.setText(message);
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Log.e(TAG, "error");
        bookTicketProgressBar.setVisibility(GONE);
        bookTicketView.setVisibility(GONE);
        connectionErrorView.setVisibility(VISIBLE);

        String message = "";
        try {
            Gson gson = new GsonBuilder().create();
            BookTicket response = gson.fromJson(new String(error.networkResponse.data, "UTF-8"), BookTicket.class);
            int i = 0;
            for (String m : response.getBookTicketError().getMessages()) {
                if (i > 0) {
                    message += "\n";
                }
                message += m;
            }
        } catch (Exception e) {
            message = "Something went wrong. Please check your Internet connection";
        }
        connectionErrorTextView.setText(message);
    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView recyclerView, MotionEvent motionEvent) {
        View child = recyclerView.findChildViewUnder(motionEvent.getX(), motionEvent.getY());
        mGestureDetector.setIsLongpressEnabled(true);

        if (child != null && mGestureDetector.onTouchEvent(motionEvent)) {
            int position = recyclerView.getChildAdapterPosition(child);
            TripList tripList = bookTicketListAdapter.getItem(position);

            if (tripList.getTripDetail().getMobileBookingEnabled().equalsIgnoreCase("1")) {
                List<Parcelable> boardingPointsData = new ArrayList<>();
                for (BoardingPoint boardingPoint : tripList.getBoardingPoints()) {
                    boardingPointsData.add(new BoardingPointData(boardingPoint));
                }
                SeatLayoutData seatLayoutData = getSeatLayoutData(tripList, boardingPointsData);
                previousFragmentIds.add(OnFragmentInteractionListener.BOOK_TICKET_FRAGMENT_ID);
                Constant.KEY_DISCOUNT = tripList.getTripRoute().getDiscount();
                Constant.KEY_DISCOUNT_AMOUNT = tripList.getTripRoute().getDiscount();
                if (seatLayoutData.getBookTicketData().getToCity().equalsIgnoreCase("Kolkata")){
                    Constant.isKolkataTrip = true;
                    Log.e(TAG,"Kolkata Trip = "+true);
                }else{
                    Constant.isKolkataTrip = false;
                }
                onFragmentChangeCallListener.fragmentChange(OnFragmentInteractionListener.SEAT_LAYOUT_FRAGMENT_ID, seatLayoutData, previousFragmentIds);
            } else {
//                appManager.callOrProcessTheNumber("16374", Intent.ACTION_CALL);
                call();
            }
            return true;
        }
        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {
    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

    }

    @NonNull
    private SeatLayoutData getSeatLayoutData(TripList tripList, List<Parcelable> boardingPointsData) {
        SeatLayoutData seatLayoutData;
        seatLayoutData = new SeatLayoutData();
        seatLayoutData.setTripId(tripList.getTripRoute().getTripId());
       // seatLayoutData.setBookTicketData(bookTicketData);
      //  Log.e(TAG, bookTicketData.toString());
        seatLayoutData.setTripRouteId(tripList.getTripRoute().getTripRouteId());
        seatLayoutData.setJourneyTime(tripList.getTripDetail().getTripOriginTime());
        seatLayoutData.setAvailableSeats(tripList.getTripDetail().getAvailableSeats());
        seatLayoutData.setOperatorName(tripList.getOperator().getCompanyName());
        seatLayoutData.setCompanyId(tripList.getOperator().getCompanyId());
        seatLayoutData.setBusType(tripList.getTripDetail().getBusDescription());
        seatLayoutData.setMinSeat(bookTicket.getBookTicketData().getPolicy().getTicketPolicy().getMin());
        seatLayoutData.setMaxSeat(bookTicket.getBookTicketData().getPolicy().getTicketPolicy().getMax());
        seatLayoutData.setBusinessClassFare(tripList.getTripRoute().getBusinessClassFare());
        seatLayoutData.setEconomyClassFare(tripList.getTripRoute().getEconomyClassFare());
        seatLayoutData.setSpecialClassFare(tripList.getTripRoute().getSpecialClassFare());
        seatLayoutData.setBoardingPointDataList(boardingPointsData);
        return seatLayoutData;
    }
    private void call() {
        // Check the SDK version and whether the permission is already granted or not.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && getActivity().checkSelfPermission(Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.CALL_PHONE}, PERMISSIONS_REQUEST_PHONE_CALL);
        } else {
            //Open call function
            String number = "16374";
            Intent intent = new Intent(Intent.ACTION_CALL);
            intent.setData(Uri.parse("tel:" + number));
            startActivity(intent);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        if (requestCode == PERMISSIONS_REQUEST_PHONE_CALL) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission is granted
                call();
            } else {
                Toast.makeText(getActivity(), "Sorry!!! Permission Denied", Toast.LENGTH_SHORT).show();
            }
        }
    }
}