package com.android.food.foodyhut.api.data.item.handshake;

import com.google.gson.annotations.SerializedName;
import com.android.food.foodyhut.api.data.item.bkash.Error;

/**
 * Created by sajid on 11/7/2015.
 */
public class HandShake {

    @SerializedName("data")
    private HandShakeData handShakeData;
    @SerializedName("error")
    private Error error;

    public HandShake() {
    }

    public HandShake(HandShakeData handShakeData, Error error) {

        this.handShakeData = handShakeData;
        this.error = error;
    }

    public HandShakeData getHandShakeData() {

        return handShakeData;
    }

    public void setHandShakeData(HandShakeData handShakeData) {
        this.handShakeData = handShakeData;
    }

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) return true;
        if (!(o instanceof HandShake)) return false;

        HandShake handShake = (HandShake) o;

        if (!handShakeData.equals(handShake.handShakeData)) return false;
        return error.equals(handShake.error);

    }

    @Override
    public int hashCode() {
        int result = handShakeData.hashCode();
        result = 31 * result + error.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "HandShake{" +
                "handShakeData=" + handShakeData +
                ", error=" + error +
                '}';
    }
}
