package com.android.food.foodyhut.api.data.item.seat;

import com.google.gson.annotations.SerializedName;
import com.android.food.foodyhut.api.data.item.Grid;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sajid on 11/4/2015.
 */
public class Seat {

    @SerializedName("layout")
    private List<ArrayList<SeatNumber>> seatNumbers;

    @SerializedName("grid")
    private Grid grid;

    public Seat() {
    }

    public Seat(List<ArrayList<SeatNumber>> seatNumbers, Grid grid) {
        this.seatNumbers = seatNumbers;
        this.grid = grid;
    }

    public List<ArrayList<SeatNumber>> getSeatNumbers() {

        return seatNumbers;
    }

    public void setSeatNumbers(List<ArrayList<SeatNumber>> seatNumbers) {
        this.seatNumbers = seatNumbers;
    }

    public Grid getGrid() {
        return grid;
    }

    public void setGrid(Grid grid) {
        this.grid = grid;
    }

    @Override
    public String toString() {
        return "Seat{" +
                "seatNumbers=" + seatNumbers +
                ", grid=" + grid +
                '}';
    }
}
