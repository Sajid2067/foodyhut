package com.android.food.foodyhut.fragment.item;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by sajid on 11/3/2015.
 */
public class CancelTicketData implements Parcelable {

    public static final Creator<CancelTicketData> CREATOR = new Creator<CancelTicketData>() {
        @Override
        public CancelTicketData createFromParcel(Parcel in) {
            return new CancelTicketData(in);
        }

        @Override
        public CancelTicketData[] newArray(int size) {
            return new CancelTicketData[size];
        }
    };
    private CalendarData calendarData;
    private String from;
    private String to;
    private String journeyTime;
    private String seatsNo;
    private String ticketPrice;
    private String shohozFee;
    private String bankCharges;
    private String discount;
    private String total;
    private String refundableAmount;

    public CancelTicketData() {
        calendarData = new CalendarData();
    }

    public CancelTicketData(Parcel in) {
        setCalendarData((CalendarData) in.readParcelable(CalendarData.class.getClassLoader()));
        setFrom(in.readString());
        setTo(in.readString());
        setJourneyTime(in.readString());
        setTicketPrice(in.readString());
        setShohozFee(in.readString());
        setBankCharges(in.readString());
        setDiscount(in.readString());
        setTotal(in.readString());
        setRefundableAmount(in.readString());

    }


    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getJourneyTime() {
        return journeyTime;
    }

    public void setJourneyTime(String journeyTime) {
        this.journeyTime = journeyTime;
    }

    public String getSeatsNo() {
        return seatsNo;
    }

    public void setSeatsNo(String seatsNo) {
        this.seatsNo = seatsNo;
    }

    public String getTicketPrice() {
        return ticketPrice;
    }

    public void setTicketPrice(String ticketPrice) {
        this.ticketPrice = ticketPrice;
    }

    public String getShohozFee() {
        return shohozFee;
    }

    public void setShohozFee(String shohozFee) {
        this.shohozFee = shohozFee;
    }

    public String getBankCharges() {
        return bankCharges;
    }

    public void setBankCharges(String bankCharges) {
        this.bankCharges = bankCharges;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getRefundableAmount() {
        return refundableAmount;
    }

    public void setRefundableAmount(String refundableAmount) {
        this.refundableAmount = refundableAmount;
    }

    public CalendarData getCalendarData() {
        return calendarData;
    }

    public void setCalendarData(CalendarData calendarData) {
        this.calendarData = calendarData;
    }



    @Override
    public int describeContents() {
        return 0;
    }


    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(calendarData, flags);
        dest.writeString(getFrom());
        dest.writeString(getTo());
        dest.writeString(getJourneyTime());
        dest.writeString(getTicketPrice());
        dest.writeString(getShohozFee());
        dest.writeString(getBankCharges());
        dest.writeString(getDiscount());
        dest.writeString(getTotal());
        dest.writeString(getRefundableAmount());

    }


}
