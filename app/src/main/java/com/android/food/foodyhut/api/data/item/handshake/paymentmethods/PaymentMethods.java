package com.android.food.foodyhut.api.data.item.handshake.paymentmethods;

import com.google.gson.annotations.SerializedName;

public class PaymentMethods {
    @SerializedName("mobile_banking")
    private PaymentMethodData mobileBanking;
    @SerializedName("cash_on_delivery")
    private PaymentMethodData cashOnDelivery;
    @SerializedName("credit_or_debit_card")
    private PaymentMethodData creditOrDebitCard;
    @SerializedName("internet_banking")
    private PaymentMethodData internetBanking;

    public PaymentMethods() {
    }

    public PaymentMethods(PaymentMethodData mobileBanking, PaymentMethodData cashOnDelivery, PaymentMethodData creditOrDebitCard, PaymentMethodData internetBanking) {
        this.mobileBanking = mobileBanking;
        this.cashOnDelivery = cashOnDelivery;
        this.creditOrDebitCard = creditOrDebitCard;
        this.internetBanking = internetBanking;
    }

    public PaymentMethodData getMobileBanking() {
        return mobileBanking;
    }

    public void setMobileBanking(PaymentMethodData mobileBanking) {
        this.mobileBanking = mobileBanking;
    }

    public PaymentMethodData getCashOnDelivery() {
        return cashOnDelivery;
    }

    public void setCashOnDelivery(PaymentMethodData cashOnDelivery) {
        this.cashOnDelivery = cashOnDelivery;
    }

    public PaymentMethodData getCreditOrDebitCard() {
        return creditOrDebitCard;
    }

    public void setCreditOrDebitCard(PaymentMethodData creditOrDebitCard) {
        this.creditOrDebitCard = creditOrDebitCard;
    }

    public PaymentMethodData getInternetBanking() {
        return internetBanking;
    }

    public void setInternetBanking(PaymentMethodData internetBanking) {
        this.internetBanking = internetBanking;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PaymentMethods)) return false;

        PaymentMethods that = (PaymentMethods) o;

        if (!getMobileBanking().equals(that.getMobileBanking())) return false;
        if (!getCashOnDelivery().equals(that.getCashOnDelivery())) return false;
        if (!getCreditOrDebitCard().equals(that.getCreditOrDebitCard())) return false;
        return getInternetBanking().equals(that.getInternetBanking());

    }

    @Override
    public int hashCode() {
        int result = getMobileBanking().hashCode();
        result = 31 * result + getCashOnDelivery().hashCode();
        result = 31 * result + getCreditOrDebitCard().hashCode();
        result = 31 * result + getInternetBanking().hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "PaymentMethods{" +
                "mobileBanking=" + mobileBanking +
                ", cashOnDelivery=" + cashOnDelivery +
                ", creditOrDebitCard=" + creditOrDebitCard +
                ", internetBanking=" + internetBanking +
                '}';
    }
}

