package com.android.food.foodyhut.api.data.item.seatlayout;

import com.google.gson.annotations.SerializedName;

/**
 * Created by sajid on 11/4/2015.
 */
public class SeatLayoutQuery {

    @SerializedName("trip_id")
    private String tripId;
    @SerializedName("trip_route_id")
    private String tripRouteId;

    public SeatLayoutQuery() {
    }

    public SeatLayoutQuery(String tripId, String tripRouteId) {

        this.tripId = tripId;
        this.tripRouteId = tripRouteId;
    }

    public String getTripId() {

        return tripId;
    }

    public void setTripId(String tripId) {
        this.tripId = tripId;
    }

    public String getTripRouteId() {
        return tripRouteId;
    }

    public void setTripRouteId(String tripRouteId) {
        this.tripRouteId = tripRouteId;
    }

    @Override
    public String toString() {
        return "SeatLayoutQuery{" +
                "tripId='" + tripId + '\'' +
                ", tripRouteId='" + tripRouteId + '\'' +
                '}';
    }
}
