package com.android.food.foodyhut.activity;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.food.foodyhut.toolbox.PostUrlBuilder;
import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.android.gms.analytics.Tracker;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Checked;
import com.mobsandgeeks.saripaar.annotation.ConfirmPassword;
import com.mobsandgeeks.saripaar.annotation.Password;
import com.android.food.foodyhut.R;
import com.android.food.foodyhut.api.data.item.bkash.BkashVerificationMessage;
import com.android.food.foodyhut.api.data.item.handshake.HandShake;
import com.android.food.foodyhut.api.data.item.user.UserStatus;
import com.android.food.foodyhut.api.data.item.user.UserVerification;
import com.android.food.foodyhut.application.AppController;
import com.android.food.foodyhut.toolbox.GetUrlBuilder;
import com.android.food.foodyhut.toolbox.ObjectRequest;
import com.android.food.foodyhut.util.API;
import com.android.food.foodyhut.util.AppManager;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.support.design.widget.Snackbar.make;
import static android.view.View.OnClickListener;
import static android.widget.Toast.LENGTH_SHORT;
import static android.widget.Toast.makeText;

/**
 * Created by sajid on 10/20/2015.
 */
public class SignUpActivity extends BaseActivity implements DialogInterface.OnClickListener,OnClickListener, Validator.ValidationListener {

    private static final String TAG = SignUpActivity.class.getSimpleName();

//    private static final String VALID_EMAIL_REG_EX = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
//            + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    private EditText fullNameEditText;

    private EditText emailEditText;

    @Password(min = 6, message = "Your password must contain minimum 6 characters and Maximum 20 characters")
    private EditText passwordEditText;

    @ConfirmPassword(message = "Both Password didn't matched.")
    private EditText confirmPasswordEditText;

    @Checked(message = "You must agree with the Terms and Conditions.")

    private CheckBox agreementCheckBox;
    private Button shohozSignUpButton;
    private Button signInActivityTransferButton;
    private AppManager appManager;
    private ProgressBar signUpProgressBar;
    private View shohozSignUpForm;
    private Validator validator;
    Tracker mTracker;
    private AppController appController = AppController.getInstance();
    private ObjectRequest<HandShake> handShakeObjectRequest;
    ObjectRequest<UserVerification> userVerificationMessageObjectRequest;
    protected AlertDialog progressBarDialog;
    int timeoutCounter = 0;
    protected TextView progressBarMessageTextView;
    private AlertDialog noInternetAlertDialog;
    private AlertDialog errorAlertDialog;
    private String fullName,email,password,passwordConfirmation;
    private Integer fullNameMaximumCharacterSize=63;
    private ImageButton backButton;
    private TextView termsConditionButton;
    private AlertDialog termsConditionDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate(Bundle savedInstanceState)");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        initializeUIViews();
        initializeUIListeners();
        validator = new Validator(this);
        validator.setValidationListener(this);
        appManager = new AppManager(this);
        // Obtain the shared Tracker instance.
        AppController application = (AppController) getApplication();
        mTracker = application.getDefaultTracker();
    }

    private void initializeUIListeners() {
        Log.d(TAG, "initializeUIListeners()");
        shohozSignUpButton.setOnClickListener(this);
        signInActivityTransferButton.setOnClickListener(this);
        backButton.setOnClickListener(this);
    }

    private void initializeUIViews() {
        Log.d(TAG, "initializeUIViews()");
        fullNameEditText = (EditText) findViewById(R.id.full_name_edit_text);
        emailEditText = (EditText) findViewById(R.id.email_edit_text);
        passwordEditText = (EditText) findViewById(R.id.password_edit_text);
        confirmPasswordEditText = (EditText) findViewById(R.id.confirm_password_edit_text);
        agreementCheckBox = (CheckBox) findViewById(R.id.agreement_check_box);
        shohozSignUpButton = (Button) findViewById(R.id.shohoz_sign_up_button);
        backButton=(ImageButton) findViewById(R.id.back_button);
        signInActivityTransferButton = (Button) findViewById(R.id.sign_in_activity_transfer_button);
        signUpProgressBar = (ProgressBar) findViewById(R.id.sign_up_progress_bar);
        shohozSignUpForm = findViewById(R.id.shohoz_sign_up_form);
        termsConditionButton = (TextView) findViewById(R.id.terms_condition_button);
        createTermsConditionDialog();
        createProgressDialogView();
        createAlertDialog();

    }

    private void createTermsConditionDialog() {
        AlertDialog.Builder termsConditionDialogBuilder = new AlertDialog.Builder(SignUpActivity.this);
        View termsConditionView = getLayoutInflater().inflate(R.layout.fragment_terms_and_conditions, null);
        TextView generalTermsTextView;
        TextView communicationPolicyTextView;
        {
            generalTermsTextView = (TextView) termsConditionView.findViewById(R.id.general_terms_text_view);
            generalTermsTextView.setText(getParsedHTML("general_terms.html"));
            generalTermsTextView.setMovementMethod(LinkMovementMethod.getInstance());
        }

        {
            communicationPolicyTextView = (TextView) termsConditionView.findViewById(R.id.communication_policy_text_view);
            communicationPolicyTextView.setText(getParsedHTML("communication_policy.html"));
            communicationPolicyTextView.setMovementMethod(LinkMovementMethod.getInstance());
        }
        termsConditionDialogBuilder.setView(termsConditionView);
        ImageButton backButton = (ImageButton) termsConditionView.findViewById(R.id.back_button);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                termsConditionDialog.dismiss();
            }
        });
        termsConditionDialogBuilder.setCancelable(false);
        termsConditionDialog = termsConditionDialogBuilder.create();
    }

    public Spanned getParsedHTML(String fileName) {
        Log.d(TAG, "getParsedHTML(String fileName)");
        InputStream inputStream;
        String htmlText = "";
        try {
            inputStream = SignUpActivity.this.getAssets().open(fileName);
            int fileSize = inputStream.available();
            byte[] fileByteData = new byte[fileSize];
            inputStream.read(fileByteData);
            inputStream.close();
            htmlText = new String(fileByteData, "UTF-8");
        } catch (IOException e) {
            Log.e(TAG, Log.getStackTraceString(e));
        }
        return Html.fromHtml(htmlText);
    }

    @Override
    public void onClick(View view) {
        Log.d(TAG, "onClick(View view)");
        switch (view.getId()) {
            case R.id.shohoz_sign_up_button:
                if(fullNameEditText.getText().toString().trim().isEmpty() || fullNameEditText.getText().toString().trim().length()>fullNameMaximumCharacterSize){
                    makeText(SignUpActivity.this, "Enter a name which is less than 64 characters.", LENGTH_SHORT).show();

              } else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(emailEditText.getText().toString()).matches()) {
                    makeText(SignUpActivity.this, "Enter a valid EMAIL.", LENGTH_SHORT).show();

                } else {
                    validator.validate();
                }
                break;
            case R.id.sign_in_activity_transfer_button:
                startActivity(new Intent(SignUpActivity.this, LoginActivity.class));
                finish();
                break;
            case R.id.back_button:
                startActivity(new Intent(SignUpActivity.this, LoginActivity.class));
                finish();
                break;
            case R.id.terms_condition_button:
                termsConditionDialog.show();
                break;
        }
    }

    @Override
    public void onValidationSucceeded() {
        Log.d(TAG, "onValidationSucceeded()");
        if (appManager.isNetworkAvailable()) {
            Log.d(TAG, "handshakeRequest done");

            fullName=fullNameEditText.getText().toString().trim();
            email = emailEditText.getText().toString().trim();
            password = passwordEditText.getText().toString().trim();
            passwordConfirmation =confirmPasswordEditText.getText().toString().trim();
            registration(fullName,email,password,passwordConfirmation);

        } else {

            noInternetAlertDialog.show();

        }

    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        Log.d(TAG, "onValidationFailed(List<ValidationError> errors) totalError = " + errors.size());
        for (ValidationError validationError : errors) {
            makeText(SignUpActivity.this, validationError.getCollatedErrorMessage(SignUpActivity.this),
                    LENGTH_SHORT).show();
            break;
        }
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        Log.d(TAG, "onClick(DialogInterface dialog, int which)");
        if (dialog.equals(noInternetAlertDialog)) {
            switch (which) {
                case Dialog.BUTTON_POSITIVE:
                    Log.d(TAG, "noInternetAlertDialog Positive Button Pressed");
                    if (appManager.isNetworkAvailable()) {
                        noInternetAlertDialog.cancel();
                        noInternetAlertDialog.dismiss();
                        registration(fullName,email,password,passwordConfirmation);
                    } else {
                        noInternetAlertDialog.cancel();
                        noInternetAlertDialog = getNoInternetAlertDialogBuilder().create();
                        noInternetAlertDialog.show();
                    }
                    break;
                case Dialog.BUTTON_NEUTRAL:
                    Log.d(TAG, "noInternetAlertDialog Neutral Button Pressed");
                    noInternetAlertDialog.cancel();
                    startActivityForResult(new Intent(Settings.ACTION_SETTINGS), 0);
                    break;
                case Dialog.BUTTON_NEGATIVE:
                    Log.d(TAG, "noInternetAlertDialog Negative Button Pressed");
                    finish();
                    break;
            }
        } else if (dialog.equals(errorAlertDialog)) {
            switch (which) {
                case Dialog.BUTTON_POSITIVE:
                    Log.d(TAG, "errorAlertDialog Positive Button Pressed");
                    registration(fullName,email,password,passwordConfirmation);
                    break;
                case Dialog.BUTTON_NEUTRAL:
                    Log.d(TAG, "errorAlertDialog Neutral Button Pressed");
                    break;
                case Dialog.BUTTON_NEGATIVE:
                    Log.d(TAG, "errorAlertDialog Negative Button Pressed");
                    errorAlertDialog.cancel();
                    errorAlertDialog.dismiss();
                    finish();
                    break;
            }
        }
    }

    private void registration (final String fullName, final String email, String password, String confirmPassword){
        Map<String, String> params = new HashMap<>();
        params.put(API.Parameter.EMAIL_ADDRESS,email);
      //  params.put(API.Parameter.PASSWORD_CONFIRMATION,confirmPassword);
        params.put(API.Parameter.PASSWORD, password);
        params.put(API.Parameter.FULL_NAME,fullName);
//        params.put(API.Parameter.ANDROID_DEVICE_ID, appManager.getDeviceId());
//        params.put(API.Parameter.ANDROID_APP_VERSION, appManager.getAppVersion());

        PostUrlBuilder postUrlBuilder = new PostUrlBuilder(API.USER_REGISTRATION_API_URL, null, null);
        GetUrlBuilder getUrlBuilder = new GetUrlBuilder(API.USER_REGISTRATION_API_URL, params);
        String Url =getUrlBuilder.getQueryUrl();  //only for showing URL and params
        Log.d(TAG, "URL:" + Url);
        Log.d(TAG, "PARAMS:" + params.toString());

        userVerificationMessageObjectRequest = new ObjectRequest<>(API.Method.USER_REGISTRATION_API_METHOD, postUrlBuilder.getUrl(), params, new Response.Listener<UserVerification>() {



            @Override
            public void onResponse(UserVerification userVerification) {
                progressBarDialog.cancel();
                if (userVerification.getResponse() != null) {

                    UserStatus userStatus=new UserStatus(SignUpActivity.this);
                    userStatus.setLogin(true);
                    LoginActivity.token=userVerification.getResponse();
                    userStatus.setToken(userVerification.getResponse());
                 //   userStatus.setUser_id(userVerification.getResponse().getUserId());
                  //  userStatus.setUser_full_name(response.getData().getFirst_name()+" "+response.getData().getLast_name());
                 //   userStatus.setUser_mobile(userVerification.getResponse().getUserMobil());
                    makeText(SignUpActivity.this, userVerification.getStatus().getMessage(), LENGTH_SHORT).show();
                    finishActivity();

                } else {

                    String message = "";
//                    for (String m : response.getError().getMessages()) {
//                        message += (m + "\n");
//                    }
                    message=userVerification.getStatus().getMessage();
                    make(shohozSignUpForm, message, Snackbar.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                try {
                    Log.e(TAG, Integer.toString(error.networkResponse.statusCode));
                } catch (Exception e) {
                    if (timeoutCounter != 2) {
                        timeoutCounter++;
                        appController.addToRequestQueue(userVerificationMessageObjectRequest);
                        return;
                    } else {
                        timeoutCounter = 0;
                    }
                }
                progressBarDialog.cancel();
                String message = "";
                try {
                    Gson gson = new GsonBuilder().create();
                    UserVerification response = gson.fromJson(new String(error.networkResponse.data, "UTF-8"), UserVerification.class);
//                    for (String m : response.getError().getMessages()) {
//                        message += (m + "\n");
//                    }
                } catch (Exception e) {
                    message = "Something went wrong.Please Check your Internet Connection";
                }

                make(shohozSignUpForm, message, Snackbar.LENGTH_SHORT).show();
            }
        },UserVerification.class);

        progressBarDialog.show();
        appController.addToRequestQueue(userVerificationMessageObjectRequest);

    }

    void finishActivity(){
        startActivity(new Intent(SignUpActivity.this, MainAppActivity.class));
        finish();
    }

    protected void createProgressDialogView() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(SignUpActivity.this);
        View view = View.inflate(SignUpActivity.this, R.layout.progressbar_view, null);
        progressBarMessageTextView = (TextView) view.findViewById(R.id.progress_bar_message_text_view);
        alertDialogBuilder.setView(view);
        alertDialogBuilder.setCancelable(false);
        progressBarDialog = alertDialogBuilder.create();
    }

    public AlertDialog.Builder getNoInternetAlertDialogBuilder() {
        AlertDialog.Builder alertDialogBuilder;
        alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setNeutralButton(R.string.action_settings, this);
        alertDialogBuilder.setPositiveButton(R.string.try_again, this);
        alertDialogBuilder.setNegativeButton(R.string.close_app, this);
        alertDialogBuilder.setTitle(R.string.error);
        alertDialogBuilder.setMessage(R.string.no_internet_connection);
        return alertDialogBuilder;
    }

    private void createAlertDialog() {
        Log.d(TAG, "createAlertDialog()");
        noInternetAlertDialog = getNoInternetAlertDialogBuilder().create();
        errorAlertDialog = getErrorAlertDialog().create();
    }

    private AlertDialog.Builder getErrorAlertDialog() {
        AlertDialog.Builder alertDialogBuilder;
        alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setPositiveButton(R.string.try_again, this);
        alertDialogBuilder.setNegativeButton(R.string.close_app, this);
        alertDialogBuilder.setTitle(R.string.error);
        alertDialogBuilder.setMessage(R.string.server_connection_fail);
        return alertDialogBuilder;
    }

}
