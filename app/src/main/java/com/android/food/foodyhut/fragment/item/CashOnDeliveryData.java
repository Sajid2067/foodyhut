package com.android.food.foodyhut.fragment.item;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by sajid on 12/4/2015.
 */
public class CashOnDeliveryData implements Parcelable {

    private PaymentDetailsData paymentDetailsData;
    private int cityId;
    private int areaId;
    private String cityName;
    private String areaName;
    private String addressLineOne;
    private String addressLineTwo;
    private String landmark;
    private String postalCode;
    private String alternateContactNumber;
    private float cashOnDeliveryFee = 0.0f;
    private String firstName;
    private String lastName;

    public CashOnDeliveryData() {

    }

    protected CashOnDeliveryData(Parcel in) {
        paymentDetailsData = in.readParcelable(PaymentDetailsData.class.getClassLoader());
        cityId = in.readInt();
        areaId = in.readInt();
        cityName=in.readString();
        areaName=in.readString();
        addressLineOne = in.readString();
        addressLineTwo = in.readString();
        landmark = in.readString();
        postalCode = in.readString();
        alternateContactNumber = in.readString();
        cashOnDeliveryFee = in.readFloat();
        firstName = in.readString();
        lastName = in.readString();
    }

    public static final Creator<CashOnDeliveryData> CREATOR = new Creator<CashOnDeliveryData>() {
        @Override
        public CashOnDeliveryData createFromParcel(Parcel in) {
            return new CashOnDeliveryData(in);
        }

        @Override
        public CashOnDeliveryData[] newArray(int size) {
            return new CashOnDeliveryData[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    public PaymentDetailsData getPaymentDetailsData() {
        return paymentDetailsData;
    }

    public void setPaymentDetailsData(PaymentDetailsData paymentDetailsData) {
        this.paymentDetailsData = paymentDetailsData;
    }

    public int getCityId() {
        return cityId;
    }

    public void setCityId(int cityId) {
        this.cityId = cityId;
    }

    public int getAreaId() {
        return areaId;
    }

    public void setAreaId(int areaId) {
        this.areaId = areaId;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getAddressLineOne() {
        return addressLineOne;
    }

    public void setAddressLineOne(String addressLineOne) {
        this.addressLineOne = addressLineOne;
    }

    public String getAddressLineTwo() {
        return addressLineTwo;
    }

    public void setAddressLineTwo(String addressLineTwo) {
        this.addressLineTwo = addressLineTwo;
    }

    public String getLandmark() {
        return landmark;
    }

    public void setLandmark(String landmark) {
        this.landmark = landmark;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getAlternateContactNumber() {
        return alternateContactNumber;
    }

    public void setAlternateContactNumber(String alternateContactNumber) {
        this.alternateContactNumber = alternateContactNumber;
    }

    public float getCashOnDeliveryFee() {
        return cashOnDeliveryFee;
    }

    public void setCashOnDeliveryFee(float cashOnDeliveryFee) {
        this.cashOnDeliveryFee = cashOnDeliveryFee;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CashOnDeliveryData)) return false;

        CashOnDeliveryData that = (CashOnDeliveryData) o;

        if (getCityId() != that.getCityId()) return false;
        if (getAreaId() != that.getAreaId()) return false;
        if (getCityName() != that.getCityName()) return false;
        if (getAreaName() != that.getAreaName()) return false;
        if (Float.compare(that.getCashOnDeliveryFee(), getCashOnDeliveryFee()) != 0) return false;
        if (!getPaymentDetailsData().equals(that.getPaymentDetailsData())) return false;
        if (!getAddressLineOne().equals(that.getAddressLineOne())) return false;
        if (!getAddressLineTwo().equals(that.getAddressLineTwo())) return false;
        if (!getLandmark().equals(that.getLandmark())) return false;
        if (!getPostalCode().equals(that.getPostalCode())) return false;
        if (!getAlternateContactNumber().equals(that.getAlternateContactNumber())) return false;
        if (!getFirstName().equals(that.getFirstName())) return false;
        return getLastName().equals(that.getLastName());

    }

    @Override
    public int hashCode() {
        int result = getPaymentDetailsData().hashCode();
        result = 31 * result + getCityId();
        result = 31 * result + getAreaId();
        result = 31 * result + getCityName().hashCode();
        result = 31 * result + getAreaName().hashCode();
        result = 31 * result + getAddressLineOne().hashCode();
        result = 31 * result + getAddressLineTwo().hashCode();
        result = 31 * result + getLandmark().hashCode();
        result = 31 * result + getPostalCode().hashCode();
        result = 31 * result + getAlternateContactNumber().hashCode();
        result = 31 * result + (getCashOnDeliveryFee() != +0.0f ? Float.floatToIntBits(getCashOnDeliveryFee()) : 0);
        result = 31 * result + getFirstName().hashCode();
        result = 31 * result + getLastName().hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "CashOnDeliveryData{" +
                "paymentDetailsData=" + paymentDetailsData +
                ", cityId=" + cityId +
                ", areaId=" + areaId +
                ", addressLineOne='" + addressLineOne + '\'' +
                ", addressLineTwo='" + addressLineTwo + '\'' +
                ", landmark='" + landmark + '\'' +
                ", postalCode='" + postalCode + '\'' +
                ", alternateContactNumber='" + alternateContactNumber + '\'' +
                ", cashOnDeliveryFee=" + cashOnDeliveryFee +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                '}';
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(paymentDetailsData, flags);
        dest.writeInt(cityId);
        dest.writeInt(areaId);
        dest.writeString(cityName);
        dest.writeString(areaName);
        dest.writeString(addressLineOne);
        dest.writeString(addressLineTwo);
        dest.writeString(landmark);
        dest.writeString(postalCode);
        dest.writeString(alternateContactNumber);
        dest.writeFloat(cashOnDeliveryFee);
        dest.writeString(firstName);
        dest.writeString(lastName);
    }
}
