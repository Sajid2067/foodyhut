package com.android.food.foodyhut.adapter.recyclerview;

import android.app.Activity;
import android.content.Context;
import android.graphics.Paint;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.food.foodyhut.R;
import com.android.food.foodyhut.api.data.item.bus.Operator;
import com.android.food.foodyhut.api.data.item.trip.TripList;
import com.android.food.foodyhut.api.data.item.trip.TripRoute;
import com.android.food.foodyhut.util.AppManager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by sajid on 10/28/2015.
 */
public class CancelTicketListAdapter extends RecyclerView.Adapter<CancelTicketListAdapter.ViewHolder> implements Filterable {

    private Activity activity;
    private List<TripList> tripLists;
    private List<TripList> backupTripLists;
    private LayoutInflater layoutInflater;
    private AppManager appManager;
    private TextView noResultTextView;


    public CancelTicketListAdapter(Activity activity, List<TripList> recyclerViewBaseItems, TextView noResultTextView) {
        this.activity = activity;
        backupTripLists = recyclerViewBaseItems;
        this.tripLists = new ArrayList<>(this.backupTripLists);
        this.noResultTextView = noResultTextView;
        layoutInflater = (LayoutInflater) this.activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        appManager = new AppManager(this.activity);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView;
        rootView = layoutInflater.inflate(R.layout.list_item_book_ticket, parent, false);
        return new ViewHolder(rootView);
    }

    public void setItems(final List<TripList> recyclerViewBaseItems) {
        backupTripLists = recyclerViewBaseItems;
        this.tripLists = new ArrayList<>(this.backupTripLists);
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (recyclerViewBaseItems.size() == 0) {
                    noResultTextView.setVisibility(View.VISIBLE);
                } else {
                    noResultTextView.setVisibility(View.GONE);
                }
            }
        });
    }

    public void setItems(final List<TripList> recyclerViewBaseItems, String sortingValue) {
        backupTripLists = recyclerViewBaseItems;
        this.tripLists = getTripListsSorted(sortingValue);
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (recyclerViewBaseItems.size() == 0) {
                    noResultTextView.setVisibility(View.VISIBLE);
                } else {
                    noResultTextView.setVisibility(View.GONE);
                }
            }
        });
    }

    public TripList getItem(int position) {
        return tripLists.get(position);
    }



    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        TripList tripList = tripLists.get(position);

        String availableSeats = activity.getResources().getQuantityString(R.plurals.seat, tripList.getTripDetail().getAvailableSeats(), tripList.getTripDetail().getAvailableSeats());
        holder.availableSeatTextView.setText(availableSeats);

        String time = appManager.getTime(tripList.getTripRoute().getDepartureTime()) + " - " + appManager.getTime(tripList.getTripRoute().getArrivalTime());

        holder.tripTimeTextView.setText(time);
        String busType = appManager.getBusType(tripList.getTripDetail().getBusDescription());
        holder.busTypeTextView.setText(busType);

        String operatorName = getOperatorName(tripList.getOperator());
        holder.busOperatorNameTextView.setText(operatorName);

        holder.routeNameTextView.setText(tripList.getTripDetail().getTripHeading());

        float discount = tripList.getTripRoute().getDiscount();
        float newTicketPrize = getTicketFare(tripList.getTripRoute());
        String newTicketFare = String.format(activity.getResources().getString(R.string.ticket_fare), newTicketPrize - discount);


        if (isMultiplePrice(tripList.getTripRoute())) {
            holder.newTicketPriceTextView2.setVisibility(View.VISIBLE);
            holder.newTicketPriceTextView1.setVisibility(View.GONE);
            holder.newTicketPriceTextView2.setText(newTicketFare);
        } else {
            holder.newTicketPriceTextView1.setVisibility(View.VISIBLE);
            holder.newTicketPriceTextView2.setVisibility(View.GONE);
            holder.newTicketPriceTextView1.setText(newTicketFare);
        }
        if (discount > 0) {
            holder.oldTicketPriceTextView.setVisibility(View.VISIBLE);
            float oldTicketPrize = getTicketFare(tripList.getTripRoute());
            String oldTicketFare = String.format(activity.getResources().getString(R.string.ticket_fare), oldTicketPrize);
            holder.oldTicketPriceTextView.setText(oldTicketFare);
        } else {
            holder.oldTicketPriceTextView.setVisibility(View.INVISIBLE);
        }

        if(tripList.getIsEidDay()==1){
            holder.eid_day_linear_layout.setVisibility(View.VISIBLE);
        }else{
            holder.eid_day_linear_layout.setVisibility(View.GONE);
        }

        if (tripList.getTripDetail().getMobileBookingEnabled().equalsIgnoreCase("1")) {
            holder.dummyTextView.setVisibility(View.GONE);
            holder.availableSeatTextView.setVisibility(View.VISIBLE);
        } else {
            holder.dummyTextView.setVisibility(View.VISIBLE);
            holder.availableSeatTextView.setVisibility(View.GONE);
        }
        if (getItemCount() - 1 == position) {
            holder.listDivider.setVisibility(View.GONE);
        } else {
            holder.listDivider.setVisibility(View.VISIBLE);
        }

    }

    private String getOperatorName(Operator operator) {
        return operator != null ? operator.getCompanyName() : "";
    }

    @Override
    public int getItemCount() {
        return tripLists != null ? tripLists.size() : 0;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();
                List<TripList> tripLists = getTripListsSorted(constraint);
                results.count = tripLists.size();
                results.values = tripLists;
                Log.e("VALUES", results.values.toString());
                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                Log.e("VALUES", results.values.toString());
                tripLists = (List<TripList>) results.values;
                notifyDataSetChanged();
            }
        };
    }

    @NonNull
    private List<TripList> getTripListsSorted(CharSequence constraint) {
        List<TripList> tripLists = new ArrayList<>(CancelTicketListAdapter.this.backupTripLists);
        Log.e("TAG", tripLists.size() + "");
        if (constraint.toString().equals("operator:1")) {
            Log.e("VALUES", "operator:1");
            Collections.sort(tripLists, new Comparator<TripList>() {
                @Override
                public int compare(TripList lhs, TripList rhs) {
                    return lhs.getOperator().getCompanyName().compareTo(rhs.getOperator().getCompanyName());
                }
            });
        } else if (constraint.toString().equals("operator:2")) {
            Collections.sort(tripLists, new Comparator<TripList>() {
                @Override
                public int compare(TripList lhs, TripList rhs) {
                    return rhs.getOperator().getCompanyName().compareTo(lhs.getOperator().getCompanyName());
                }
            });
        } else if (constraint.toString().equals("departure:1")) {
            Collections.sort(tripLists, new Comparator<TripList>() {
                @Override
                public int compare(TripList lhs, TripList rhs) {
                    return lhs.getTripDetail().getTripOriginTime().compareTo(rhs.getTripDetail().getTripOriginTime());
                }
            });
        } else if (constraint.toString().equals("departure:2")) {
            Collections.sort(tripLists, new Comparator<TripList>() {
                @Override
                public int compare(TripList lhs, TripList rhs) {
                    return rhs.getTripDetail().getTripOriginTime().compareTo(lhs.getTripDetail().getTripOriginTime());
                }
            });
        } else if (constraint.toString().equals("fare:1")) {
            Collections.sort(tripLists, new Comparator<TripList>() {
                @Override
                public int compare(TripList lhs, TripList rhs) {
                    return sortFare(lhs, rhs);
                }
            });
        } else if (constraint.toString().equals("fare:2")) {
            Collections.sort(tripLists, new Comparator<TripList>() {
                @Override
                public int compare(TripList lhs, TripList rhs) {
                    return sortFare(rhs, lhs);
                }
            });
        }
        return tripLists;
    }

    private int sortFare(TripList lhs, TripList rhs) {
        if (getTicketFare(lhs.getTripRoute()) < getTicketFare(rhs.getTripRoute())) {
            return -1;
        } else if (getTicketFare(lhs.getTripRoute()) > getTicketFare(rhs.getTripRoute())) {
            return 1;
        } else {
            return 0;
        }
    }

    private float getTicketFare(TripRoute tripRoute) {
        if (tripRoute.getEconomyClassFare() > 0) {
            return tripRoute.getEconomyClassFare();
        } else if (tripRoute.getBusinessClassFare() > 0) {
            return tripRoute.getBusinessClassFare();
        } else {
            return tripRoute.getSpecialClassFare();
        }
    }

    private boolean isMultiplePrice(TripRoute tripRoute) {
        if (tripRoute.getBusinessClassFare() > 0 && tripRoute.getEconomyClassFare() > 0) {
            return true;
        }

        if (tripRoute.getBusinessClassFare() > 0 && tripRoute.getSpecialClassFare() > 0) {
            return true;
        }

        if (tripRoute.getSpecialClassFare() > 0 && tripRoute.getEconomyClassFare() > 0) {
            return true;
        }
        return false;
    }


    private String getAllTicketFare(TripRoute tripRoute) {
        String ticketFare = "";
        if (tripRoute.getBusinessClassFare() != 0) {
            ticketFare += String.format(activity.getResources().getString(R.string.ticket_fare), tripRoute.getBusinessClassFare());
        }
        if (tripRoute.getEconomyClassFare() != 0) {
            if (!ticketFare.equals("")) {
                ticketFare += "\n";
            }
            ticketFare += String.format(activity.getResources().getString(R.string.ticket_fare), tripRoute.getEconomyClassFare());
        }
        if (tripRoute.getSpecialClassFare() != 0) {
            if (!ticketFare.equals("")) {
                ticketFare += "\n";
            }
            ticketFare += String.format(activity.getResources().getString(R.string.ticket_fare), tripRoute.getSpecialClassFare());
        }
        return ticketFare;
    }

    private String getTicketFareString(TripRoute tripRoute) {

        String ticketFare = "";
        if (tripRoute.getBusinessClassFare() > 0) {
            ticketFare += String.format(activity.getResources().getString(R.string.ticket_fare), tripRoute.getBusinessClassFare());
        }
        if (tripRoute.getEconomyClassFare() > 0) {
            if (!ticketFare.isEmpty()) {
                ticketFare += ",   ";
            }
            ticketFare += String.format(activity.getResources().getString(R.string.ticket_fare), tripRoute.getEconomyClassFare());
        }
        if (tripRoute.getSpecialClassFare() > 0) {
            if (!ticketFare.isEmpty()) {
                ticketFare += ",   ";
            }
            ticketFare += String.format(activity.getResources().getString(R.string.ticket_fare), tripRoute.getSpecialClassFare());
        }

        return ticketFare;
    }

    private String getDiscountTicketFareString(TripRoute tripRoute) {

        String discountTicketFare = "";
        float discount = tripRoute.getDiscount();

        if (tripRoute.getBusinessClassFare() > 0) {
            discountTicketFare += String.format(activity.getResources().getString(R.string.ticket_fare), (tripRoute.getBusinessClassFare() - discount));
        }
        if (tripRoute.getEconomyClassFare() > 0) {
            if (!discountTicketFare.isEmpty()) {
                discountTicketFare += ",   ";
            }
            discountTicketFare += String.format(activity.getResources().getString(R.string.ticket_fare), (tripRoute.getEconomyClassFare() - discount));
        }
        if (tripRoute.getSpecialClassFare() > 0) {
            if (!discountTicketFare.isEmpty()) {
                discountTicketFare += ",   ";
            }
            discountTicketFare += String.format(activity.getResources().getString(R.string.ticket_fare), (tripRoute.getSpecialClassFare() - discount));
        }

        return discountTicketFare;
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tripTimeTextView;
        private TextView busOperatorNameTextView;
        private TextView busTypeTextView;
        private TextView oldTicketPriceTextView;
        private TextView newTicketPriceTextView1;
        private TextView newTicketPriceTextView2;
        private TextView availableSeatTextView;
        private TextView dummyTextView;
        private TextView routeNameTextView;
        private View listDivider;
        private LinearLayout eid_day_linear_layout;


        public ViewHolder(View rootView) {
            super(rootView);
            tripTimeTextView = (TextView) rootView.findViewById(R.id.trip_time_text_view);
            busOperatorNameTextView = (TextView) rootView.findViewById(R.id.bus_operator_name_text_view);
            busTypeTextView = (TextView) rootView.findViewById(R.id.bus_type_text_view);
            oldTicketPriceTextView = (TextView) rootView.findViewById(R.id.ticket_price_old_text_view);
            oldTicketPriceTextView.setPaintFlags(oldTicketPriceTextView.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            newTicketPriceTextView1 = (TextView) rootView.findViewById(R.id.ticket_price_new_text_view_1);
            newTicketPriceTextView2 = (TextView) rootView.findViewById(R.id.ticket_price_new_text_view_2);
            availableSeatTextView = (TextView) rootView.findViewById(R.id.available_seat_text_view);
            dummyTextView = (TextView) rootView.findViewById(R.id.dummy_text_view);
            routeNameTextView = (TextView) rootView.findViewById(R.id.route_name_text_view);
            eid_day_linear_layout = (LinearLayout) rootView.findViewById(R.id.eid_day_linear_layout);
            listDivider = (View) itemView.findViewById(R.id.list_divider);
        }
    }
}
