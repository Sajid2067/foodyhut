package com.android.food.foodyhut.api.data.item.login;

import com.android.food.foodyhut.api.data.item.user.UserRegistrationStatus;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sajid on 12/14/2015.
 */
public class LoginVerification {


    @SerializedName("status")
    private LoginVerificationData status;

    @SerializedName("response")
    private String response;

    public LoginVerification() {
    }

    public LoginVerificationData getStatus() {
        return status;
    }

    public void setStatus(LoginVerificationData status) {
        this.status = status;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }
}
