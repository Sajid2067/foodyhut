package com.android.food.foodyhut.fragment;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.android.food.foodyhut.R;
import com.android.food.foodyhut.activity.listener.OnFragmentInteractionListener;
import com.android.food.foodyhut.api.data.item.bkash.BkashVerificationMessage;
import com.android.food.foodyhut.api.data.item.coupon.CouponVerificationMessage;
import com.android.food.foodyhut.api.data.item.discount.Discount;
import com.android.food.foodyhut.application.AppController;
import com.android.food.foodyhut.database.data.item.PaymentMethodDataItem;
import com.android.food.foodyhut.database.data.source.PaymentMethodDataSource;
import com.android.food.foodyhut.fragment.item.BkashPaymentData;
import com.android.food.foodyhut.fragment.item.CalendarData;
import com.android.food.foodyhut.fragment.item.CashOnDeliveryData;
import com.android.food.foodyhut.fragment.item.PaymentDetailsData;
import com.android.food.foodyhut.fragment.item.SSLPaymentData;
import com.android.food.foodyhut.fragment.item.SeatLayoutData;
import com.android.food.foodyhut.fragment.item.SeatNumberData;
import com.android.food.foodyhut.toolbox.ObjectRequest;
import com.android.food.foodyhut.util.API;
import com.android.food.foodyhut.util.AppManager;
import com.android.food.foodyhut.util.Constant;
import com.android.food.foodyhut.util.FrequentFunction;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;

/**
 * Created by sajid on 11/7/2015.
 */
public class PaymentDetailsFragment extends BaseFragment {

    private static final String TAG = PaymentDetailsFragment.class.getSimpleName();
    ObjectRequest<CouponVerificationMessage> couponVerificationMessageObjectRequest;
    ObjectRequest<Discount> discountObjectRequest;
    int timeoutCounter = 0;
    private Button mCouponApplyButton;
    private EditText mCouponCodeEditText;
    private AppController appController = AppController.getInstance();
    private Button tryAgainButton;
    private Button cancelButton;
    private ImageView messageTypeImageView;
    private TextView messageTypeTextView;
    private ImageButton backButtonOverride;
    private View discountAmountHolder;
    private TextView discountAmountTextView;
    private TextView messageTextView;
    private AlertDialog couponVerificationMessage;
    private TextView fullNameTextView;
    private TextView journeyTimeTextView;
    private TextView journeyDateTextView;
    private TextView busOperatorNameTextView;
    private TextView journeyRouteTextView;
    private TextView tvDiscountMessage;
    private TextView ageTextView;
    private TextView boardingPointTextView;
    private TextView ticketFareTextView;
    private TextView shohozFeeTextView;
    private TextView paymentGatewayFeeTextView;
    private TextView discountTextView;
    private TextView totalPaymentTextView;
    private TextView cashOnDeliveryFeeTextView;
    private Button cashOnDeliveryButton;
    private Button mobileBankingButton;
    private Button continueButton;
    private AppManager appManager;
    private PaymentDetailsData paymentDetailsData;
    private CashOnDeliveryData cashOnDeliveryData;
    private BkashPaymentData bkashPaymentData;
    private CalendarData calendarData;
    private Calendar calendar;
    private Button creditOrDebitCardButton;
    private Button internetBankingButton;
    private SSLPaymentData sslPaymentData;
    String discountFee=null;
    Tracker mTracker;
    String totalPayment;
    String paymentGateWayFee;
    String cashOnDeliveryFee;
    String ticketFare;
    String shohozFee;

    public static PaymentDetailsFragment newInstance(ArrayList<Integer> previousFragmentIds, Parcelable parcelable) {
        PaymentDetailsFragment paymentDetailsFragment = new PaymentDetailsFragment();
        Bundle bundle = new Bundle();
        bundle.putIntegerArrayList(PREVIOUS_FRAGMENT_ID_TAG, previousFragmentIds);
        bundle.putParcelable(PARCELABLE_FRAGMENT_ITEM_ID_TAG, parcelable);
        paymentDetailsFragment.setArguments(bundle);
        return paymentDetailsFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        discountFee=null;
        appManager = new AppManager(getActivity());
        if (parcelable instanceof PaymentDetailsData) {
            paymentDetailsData = (PaymentDetailsData) parcelable;
        } else if (parcelable instanceof CashOnDeliveryData) {
            cashOnDeliveryData = (CashOnDeliveryData) parcelable;
            paymentDetailsData = cashOnDeliveryData.getPaymentDetailsData();
            parcelable = paymentDetailsData;
        } else if (parcelable instanceof BkashPaymentData) {
            bkashPaymentData = (BkashPaymentData) parcelable;
            paymentDetailsData = bkashPaymentData.getPaymentDetailsData();
            parcelable = paymentDetailsData;
        }else if (parcelable instanceof SSLPaymentData) {
            sslPaymentData = (SSLPaymentData) parcelable;
            paymentDetailsData = sslPaymentData.getPaymentDetailsData();
            parcelable = paymentDetailsData;
        }
        createDialogView();
        createProgressDialogView();
//        Toast.makeText(getActivity(), ""+appManager.getDiscount(), Toast.LENGTH_SHORT).show();
        if(appManager.getDiscount() && !Constant.KEY_IS_COUPON_APPLIED){
            getDiscountPrice(paymentDetailsData.getMobileNumber(),Constant.KEY_DISCOUNT_AMOUNT);
        }

//        Toast.makeText(getActivity(), ""+Constant.KEY_DISCOUNT, Toast.LENGTH_SHORT).show();
        calendarData = paymentDetailsData.getSeatLayoutData().getBookTicketData().getCalendarData();
        calendar = new GregorianCalendar(calendarData.getYear(), calendarData.getMonth(), calendarData.getDay());
        // Obtain the shared Tracker instance.
        AppController application = (AppController) getActivity().getApplication();
        mTracker = application.getDefaultTracker();
        mTracker.setScreenName("Payment Details");
        // Send a screen view.
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.fragment_payment_details, container, false);
        }

        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    void initializeEditTextComponents() {
        mCouponCodeEditText = (EditText) rootView.findViewById(R.id.fragment_payment_details_coupon_code_et);
        if (Constant.KEY_IS_COUPON_APPLIED) {
            mCouponCodeEditText.setText(Constant.KEY_APPLIED_COUPON_CODE);
            mCouponCodeEditText.setEnabled(false);
        }
    }

    @Override
    void initializeButtonComponents() {
        cashOnDeliveryButton = (Button) rootView.findViewById(R.id.cash_on_delivery_button);
        mobileBankingButton = (Button) rootView.findViewById(R.id.mobile_banking_button);
        creditOrDebitCardButton = (Button) rootView.findViewById(R.id.credit_or_debit_card_button);
        internetBankingButton = (Button) rootView.findViewById(R.id.internet_banking_button);

//        final PaymentMethodDataSource paymentMethodDataSource = new PaymentMethodDataSource(getContext());
//        paymentMethodDataSource.open();
//        cashOnDeliveryButton.setEnabled(paymentMethodDataSource.getPaymentMethodDataItem(Constant.CONST_CASH_ON_DELIVERY_NAME).isPaymentMethodIsActive());
//        mobileBankingButton.setEnabled(paymentMethodDataSource.getPaymentMethodDataItem(Constant.CONST_MOBILE_BANKING_NAME).isPaymentMethodIsActive());
//        creditOrDebitCardButton.setEnabled(paymentMethodDataSource.getPaymentMethodDataItem(Constant.CONST_CREDIT_OR_DEBIT_CARD_NAME).isPaymentMethodIsActive());
//        internetBankingButton.setEnabled(paymentMethodDataSource.getPaymentMethodDataItem(Constant.CONST_INTERNET_BANKING_NAME).isPaymentMethodIsActive());

        if (cashOnDeliveryData != null) {
            cashOnDeliveryButton.setSelected(true);
        } else if (bkashPaymentData != null) {
            mobileBankingButton.setSelected(true);
        }
        continueButton = (Button) rootView.findViewById(R.id.continue_button);
        mCouponApplyButton = (Button) rootView.findViewById(R.id.fragment_payment_details_coupon_apply_bt);
        if (Constant.KEY_IS_COUPON_APPLIED) {
            mCouponApplyButton.setEnabled(false);
        }
    }

    @Override
    void initializeTextViewComponents() {
        fullNameTextView = findViewById(R.id.full_name_text_view);
        fullNameTextView.setText(getUserFullName());

        journeyTimeTextView = findViewById(R.id.journey_time_text_view);
        journeyTimeTextView.setText(appManager.getTime(paymentDetailsData.getSeatLayoutData().getSelectedBoardingPoint().getLocationTime()));

        journeyDateTextView = findViewById(R.id.journey_date_text_view);
        journeyDateTextView.setText(getJourneyDate());

        busOperatorNameTextView = findViewById(R.id.bus_operator_name_text_view);
        busOperatorNameTextView.setText(paymentDetailsData.getSeatLayoutData().getOperatorName());

        journeyRouteTextView = findViewById(R.id.journey_route_text_view);
        journeyRouteTextView.setText(getJourneyRoute());

        boardingPointTextView = findViewById(R.id.boarding_point_text_view);
        boardingPointTextView.setText(paymentDetailsData.getSeatLayoutData().getSelectedBoardingPoint().getLocationName());

//        ageTextView = findViewById(R.id.age_text_view);
//        ageTextView.setText(getUserAge());

        ticketFareTextView = findViewById(R.id.ticket_fare_text_view);
        ticketFare = getPriceFormat(getTicketFare());
        ticketFareTextView.setText(getLeadingSpace(ticketFare));

        shohozFeeTextView = findViewById(R.id.shohoz_fee_text_view);
        shohozFee = getPriceFormat(calculateShohozFee());
        shohozFeeTextView.setText(getLeadingSpace(shohozFee));

        cashOnDeliveryFeeTextView = findViewById(R.id.cash_on_delivery_fee_text_view);
        cashOnDeliveryFee = getPriceFormat(0.0f);
        if (cashOnDeliveryData != null) {
            cashOnDeliveryFee = getPriceFormat(cashOnDeliveryData.getCashOnDeliveryFee());
        }
        cashOnDeliveryFeeTextView.setText(getLeadingSpace(cashOnDeliveryFee));

        paymentGatewayFeeTextView = findViewById(R.id.payment_gateway_fee_text_view);
        paymentGateWayFee = getPriceFormat(0.0f);
        if (bkashPaymentData != null) {
            paymentGateWayFee = getPriceFormat(getBKashFee(getTicketFare() + calculateShohozFee() - getDiscountAmount()));
        }
        paymentGatewayFeeTextView.setText(getLeadingSpace(paymentGateWayFee));

        discountTextView = findViewById(R.id.discount_text_view);
        discountFee = getPriceFormat(getDiscountAmount());
        discountTextView.setText(getLeadingSpace(discountFee));

        totalPaymentTextView = findViewById(R.id.total_payment_text_view);
        totalPayment = getPriceFormat(calculateTotalFee(ticketFare, shohozFee, cashOnDeliveryFee, paymentGateWayFee, discountFee));
        totalPaymentTextView.setText(getLeadingSpace(totalPayment));

        tvDiscountMessage = findViewById(R.id.tvDiscountMessage);
        tvDiscountMessage.setVisibility(View.GONE);

    }

    private String getLeadingSpace(String string) {
        String space = "";
        int count = 12 - string.length();
        if (string.length() == 8) {
            count = 1;
        } else if (string.length() == 7) {
            count = 3;
        } else if (string.length() == 6) {
            count = 5;
        } else if (string.length() == 5) {
            count = 7;
        } else if (string.length() == 4) {
            count = 9;
        }
        for (int i = 1; i <= count; i++) {
            space += " ";
        }
        return String.format("৳%s%s", space, string);
    }

    private float getBKashFee(float totalFee) {
        //ceil(ceil(total / (1 - (bkash_fee / 100))) - total)
        return (float) Math.ceil(Math.ceil(totalFee / (1 - (appManager.getBKashFee() / 100f))) - totalFee);
    }

    private float calculateTotalFee(String ticketFare, String shohozFee, String cashOnDeliveryFee, String paymentGateWayFee, String discountFee) {
        return Float.parseFloat(ticketFare) + Float.parseFloat(cashOnDeliveryFee) + Float.parseFloat(paymentGateWayFee) + Float.parseFloat(shohozFee) - Float.parseFloat(discountFee);
    }

    private float calculateShohozFee() {
        return (appManager.getShohozFee() * paymentDetailsData.getSeatLayoutData().getSelectedSeat().size());
    }

    @NonNull
    private String getUserAge() {
        return paymentDetailsData.getAge().get(0) + " Years";
    }

    @NonNull
    private String getJourneyRoute() {
        return paymentDetailsData.getSeatLayoutData().getBookTicketData().getFromCity() + " to " + paymentDetailsData.getSeatLayoutData().getBookTicketData().getToCity();
    }

    private Spanned getJourneyDate() {
        String htmlFormatDate = appManager.getHTMLFormatDate(paymentDetailsData.getSeatLayoutData().getBookTicketData().getCalendarData(), calendar);
        return Html.fromHtml(htmlFormatDate);
    }

    private float getTicketFare() {
        SeatLayoutData seatLayoutData = paymentDetailsData.getSeatLayoutData();
        List<Parcelable> selectedSeats = seatLayoutData.getSelectedSeat();
        float baseFare = 0.0f;
        for (int i = 0; i < selectedSeats.size(); i++) {
            SeatNumberData seatNumberData = (SeatNumberData) selectedSeats.get(i);
            baseFare = baseFare + seatNumberData.getSeatFare();
        }
        return baseFare;
    }

    private float getDiscountAmount() {
        if (Constant.KEY_IS_COUPON_APPLIED) {
            return Constant.KEY_COUPON_DISCOUNT;
        } else {
            SeatLayoutData seatLayoutData = paymentDetailsData.getSeatLayoutData();
            List<Parcelable> selectedSeats = seatLayoutData.getSelectedSeat();

            return Constant.KEY_DISCOUNT * selectedSeats.size();
        }
    }

    private void resetPaymentDetails(boolean isCoupon) {
        ticketFare = getPriceFormat(getTicketFare());
        ticketFareTextView.setText(getLeadingSpace(ticketFare));

        shohozFee = getPriceFormat(calculateShohozFee());
        shohozFeeTextView.setText(getLeadingSpace(shohozFee));

        cashOnDeliveryFee = getPriceFormat(0.0f);
        if (cashOnDeliveryButton.isSelected()) {
            cashOnDeliveryFee = getPriceFormat(cashOnDeliveryData.getCashOnDeliveryFee());
        }
        cashOnDeliveryFeeTextView.setText(getLeadingSpace(cashOnDeliveryFee));

        paymentGateWayFee = getPriceFormat(0.0f);
        if (mobileBankingButton.isSelected()) {
            paymentGateWayFee = getPriceFormat(getBKashFee(getTicketFare() + calculateShohozFee() - getDiscountAmount()));
        }
        paymentGatewayFeeTextView.setText(getLeadingSpace(paymentGateWayFee));

        if(isCoupon){
            discountFee = getPriceFormat(getDiscountAmount());
        }
        discountTextView.setText(getLeadingSpace(discountFee));

        totalPayment = getPriceFormat(calculateTotalFee(ticketFare, shohozFee, cashOnDeliveryFee, paymentGateWayFee, discountFee));
        totalPaymentTextView.setText(getLeadingSpace(totalPayment));
    }

    @Override
    void initializeOtherViewComponents() {

    }

    @Override
    void initializeOnclickListener() {
        cashOnDeliveryButton.setOnClickListener(this);
        creditOrDebitCardButton.setOnClickListener(this);
        internetBankingButton.setOnClickListener(this);
        continueButton.setOnClickListener(this);
        mCouponApplyButton.setOnClickListener(this);
        mobileBankingButton.setOnClickListener(this);
        tryAgainButton.setOnClickListener(this);
        backButtonOverride.setOnClickListener(this);
        cancelButton.setOnClickListener(this);
    }

    @Override
    void removeOnclickListener() {
        cashOnDeliveryButton.setOnClickListener(null);
        creditOrDebitCardButton.setOnClickListener(null);
        internetBankingButton.setOnClickListener(null);
        continueButton.setOnClickListener(null);
        mCouponApplyButton.setOnClickListener(null);
        mobileBankingButton.setOnClickListener(null);
        tryAgainButton.setOnClickListener(null);
        backButtonOverride.setOnClickListener(null);
        cancelButton.setOnClickListener(null);
    }

    @SuppressWarnings("WrongConstant")
    @Override
    public void onClick(View view) {
        super.onClick(view);
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        switch (view.getId()) {
            case R.id.mobile_banking_button:
                bkashPaymentData = new BkashPaymentData();
                if(appManager.getDiscount()){
                    SeatLayoutData seatLayoutData = paymentDetailsData.getSeatLayoutData();
                    List<Parcelable> selectedSeats = seatLayoutData.getSelectedSeat();
                    if(Constant.N_TH_ORDER>0||Constant.N_TH_ORDER<4){
                        bkashPaymentData.setbKashFee(getBKashFee((getTicketFare() + calculateShohozFee()) - Constant.KEY_DISCOUNT));
                    }
                    else{
                        bkashPaymentData.setbKashFee(getBKashFee((getTicketFare() + calculateShohozFee()) - Constant.KEY_DISCOUNT));
                    }

                }else{
                    bkashPaymentData.setbKashFee(getBKashFee((getTicketFare() + calculateShohozFee()) - getDiscountAmount()));
                }

                bkashPaymentData.setPaymentDetailsData(paymentDetailsData);
                stateChange(R.id.mobile_banking_button);
                break;
            case R.id.cash_on_delivery_button:
                stateChange(R.id.cash_on_delivery_button);
                previousFragmentIds.add(OnFragmentInteractionListener.PAYMENT_DETAILS_FRAGMENT_ID);
                CashOnDeliveryData cashOnDeliveryData = new CashOnDeliveryData();
                cashOnDeliveryData.setPaymentDetailsData(paymentDetailsData);
                stateChange(R.id.cash_on_delivery_button);
                onFragmentChangeCallListener.fragmentChange(OnFragmentInteractionListener.PAYMENT_DETAILS_COD_FRAGMENT_ID, cashOnDeliveryData, previousFragmentIds);
                break;

            case R.id.credit_or_debit_card_button:
                previousFragmentIds.add(OnFragmentInteractionListener.PAYMENT_DETAILS_FRAGMENT_ID);
                onFragmentChangeCallListener.fragmentChange(OnFragmentInteractionListener.CARD_DETAILS_FRAGMENT_ID, paymentDetailsData, previousFragmentIds);
                break;

            case R.id.internet_banking_button:
                previousFragmentIds.add(OnFragmentInteractionListener.PAYMENT_DETAILS_FRAGMENT_ID);
                onFragmentChangeCallListener.fragmentChange(OnFragmentInteractionListener.INTERNET_BANKING_DETAILS_FRAGMENT_ID, paymentDetailsData, previousFragmentIds);
                break;

            case R.id.continue_button:
                if (!cashOnDeliveryButton.isSelected() && !mobileBankingButton.isSelected()) {
                    makeAToast("You must have to select a payment method", Toast.LENGTH_SHORT);
                } else if (cashOnDeliveryButton.isSelected()) {
                    previousFragmentIds.add(OnFragmentInteractionListener.PAYMENT_DETAILS_FRAGMENT_ID);
                    onFragmentChangeCallListener.fragmentChange(OnFragmentInteractionListener.TICKET_DETAILS_FRAGMENT_ID, this.cashOnDeliveryData, previousFragmentIds);
                } else if (mobileBankingButton.isSelected()) {
                    previousFragmentIds.add(OnFragmentInteractionListener.PAYMENT_DETAILS_FRAGMENT_ID);
                    onFragmentChangeCallListener.fragmentChange(OnFragmentInteractionListener.TICKET_DETAILS_FRAGMENT_ID, this.bkashPaymentData, previousFragmentIds);
                }
                break;

            case R.id.cancel_button:
            case R.id.back_button_override:
                couponVerificationMessage.cancel();
                break;
            case R.id.retry_button:
            case R.id.fragment_payment_details_coupon_apply_bt:
                if (couponVerificationMessage.isShowing()) {
                    couponVerificationMessage.cancel();
                }
                if (isZeroLengthText(mCouponCodeEditText)) {
                    makeAToast("Fill up all the coupon code field", Toast.LENGTH_SHORT);
                }
                else if (getText(mCouponCodeEditText).equalsIgnoreCase("CB2016")){
                    makeAToast("Coupon Applied.",Toast.LENGTH_LONG);
                }else {
                    couponVerificationCall();
                }

                break;
        }
    }

    private void stateChange(int buttonId) {
        ticketFare = getPriceFormat(getTicketFare());
        shohozFee = getPriceFormat(calculateShohozFee());
        if (appManager.getDiscount()){
            discountFee = getPriceFormat(Constant.KEY_DISCOUNT);
        }else{
            discountFee = getPriceFormat(getDiscountAmount());
        }

        cashOnDeliveryFee = getPriceFormat(0.0f);
        paymentGateWayFee = getPriceFormat(0.0f);
        switch (buttonId) {
            case R.id.cash_on_delivery_button:
                if (cashOnDeliveryData != null) {
                    cashOnDeliveryFee = getPriceFormat(cashOnDeliveryData.getCashOnDeliveryFee());
                }
                mobileBankingButton.setSelected(false);
                cashOnDeliveryButton.setSelected(true);
                break;
            case R.id.mobile_banking_button:
                if (bkashPaymentData != null) {
                    if(appManager.getDiscount()){
                        SeatLayoutData seatLayoutData = paymentDetailsData.getSeatLayoutData();
                        List<Parcelable> selectedSeats = seatLayoutData.getSelectedSeat();
                        if(Constant.N_TH_ORDER>0||Constant.N_TH_ORDER<4){
                            paymentGateWayFee = getPriceFormat(getBKashFee((getTicketFare() + calculateShohozFee()) - Constant.KEY_DISCOUNT));
                        }
                        else{
                            paymentGateWayFee = getPriceFormat(getBKashFee(getTicketFare() + calculateShohozFee() - Constant.KEY_DISCOUNT));
                        }
                    }else{
                        paymentGateWayFee = getPriceFormat(getBKashFee(getTicketFare() + calculateShohozFee() - getDiscountAmount()));
                    }

                }
                mobileBankingButton.setSelected(true);
                cashOnDeliveryButton.setSelected(false);
                break;
        }
        String totalPayment = getPriceFormat(calculateTotalFee(ticketFare, shohozFee, cashOnDeliveryFee, paymentGateWayFee, discountFee));
        ticketFareTextView.setText(getLeadingSpace(ticketFare));
        shohozFeeTextView.setText(getLeadingSpace(shohozFee));
        paymentGatewayFeeTextView.setText(getLeadingSpace(paymentGateWayFee));
        cashOnDeliveryFeeTextView.setText(getLeadingSpace(cashOnDeliveryFee));
        discountTextView.setText(getLeadingSpace(discountFee));
        totalPaymentTextView.setText(getLeadingSpace(totalPayment));
    }

    private String getPriceFormat(float price) {
        return String.format("%.02f", price);
    }

    public String getUserFullName() {
        return paymentDetailsData.getFullName().get(0);
    }

    private void createDialogView() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());
        View view = View.inflate(getContext(), R.layout.coupon_verification_message, null);
        alertDialogBuilder.setView(view);

        messageTextView = (TextView) view.findViewById(R.id.message_text_view);
        messageTypeTextView = (TextView) view.findViewById(R.id.message_type_text_view);
        discountAmountHolder = view.findViewById(R.id.discount_amount_holder);
        discountAmountTextView = (TextView) view.findViewById(R.id.discount_amount_text_view);

        messageTypeImageView = (ImageView) view.findViewById(R.id.message_type_image_view);

        backButtonOverride = (ImageButton) view.findViewById(R.id.back_button_override);

        tryAgainButton = (Button) view.findViewById(R.id.retry_button);
        cancelButton = (Button) view.findViewById(R.id.cancel_button);

        alertDialogBuilder.setCancelable(false);
        couponVerificationMessage = alertDialogBuilder.create();
    }

    @Override
    public void onBackPressed() {
        if (couponVerificationMessage.isShowing()) {
            couponVerificationMessage.cancel();
        } else {
            super.onBackPressed();
        }
    }

    private void couponVerificationCall() {
        if (isZeroLengthText(mCouponCodeEditText)) {
            makeAToast("Fill up all the coupon code field", Toast.LENGTH_SHORT);
        } else {

//            AppManager appManager = new AppManager(getActivity());
            String url = API.COUPON_VERIFICATION_API_URL;
            HashMap<String, String> postParams = new HashMap<>();
            postParams.put(API.Parameter.ANDROID_DEVICE_ID, appManager.getDeviceId());
            postParams.put(API.Parameter.ANDROID_APP_VERSION, appManager.getAppVersion());
            postParams.put(API.Parameter.COUPON_CODE, getText(mCouponCodeEditText));
            postParams.put(API.Parameter.MOBILE_NUMBER, paymentDetailsData.getMobileNumber());
            postParams.put(API.Parameter.AMOUNT, (int) getTicketFare() + "");
            postParams.put(API.Parameter.TRIP_ID, paymentDetailsData.getSeatLayoutData().getTripId());
            postParams.put(API.Parameter.TOTAL_TICKET, paymentDetailsData.getSeatLayoutData().getSelectedSeat().size() + "");
            Log.d(TAG, "URL:" + url);
            Log.d(TAG, "PARAMS:" + postParams.toString());
            couponVerificationMessageObjectRequest = new ObjectRequest<>(API.Method.COUPON_VERIFICATION_API_METHOD, url, postParams, new Response.Listener<CouponVerificationMessage>() {
                @Override
                public void onResponse(CouponVerificationMessage response) {
                    progressBarDialog.cancel();
                    if (response.getData() != null) {
                        discountAmountHolder.setVisibility(View.VISIBLE);
                        tryAgainButton.setVisibility(View.GONE);
                        messageTypeImageView.setImageResource(R.mipmap.ic_done_amber_48dp);
                        messageTypeTextView.setText("SUCCESS");
                        discountAmountTextView.setText(response.getData().getDiscount());
                        FrequentFunction.convertHtml(messageTextView,response.getData().getMessage());
                      //  messageTextView.setText(Html.fromHtml(response.getData().getMessage()));
                        couponVerificationMessage.show();
                        Constant.KEY_IS_COUPON_APPLIED = true;
                        Constant.KEY_DISCOUNT = Float.parseFloat(response.getData().getDiscount());
                        Constant.KEY_COUPON_DISCOUNT = Float.parseFloat(response.getData().getDiscount());
                        Constant.KEY_APPLIED_COUPON_CODE = getText(mCouponCodeEditText);
                        mCouponCodeEditText.setEnabled(false);
                        mCouponApplyButton.setEnabled(false);
                        resetPaymentDetails(true);
                    } else {
                        discountAmountHolder.setVisibility(View.GONE);
                        tryAgainButton.setVisibility(View.VISIBLE);
                        messageTypeImageView.setImageResource(R.mipmap.ic_warning_amber_48dp);
                        messageTypeTextView.setText("ERROR");
                        String message = "";
                        for (String m : response.getError().getMessages()) {
                            message += (m + "\n");
                        }
                        messageTextView.setText(message);
                        couponVerificationMessage.show();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    try {
                        Log.e(TAG, Integer.toString(error.networkResponse.statusCode));
                    } catch (Exception e) {
                        if (timeoutCounter != 2) {
                            timeoutCounter++;
                            appController.addToRequestQueue(couponVerificationMessageObjectRequest);
                            return;
                        } else {
                            timeoutCounter = 0;
                        }
                    }
                    progressBarDialog.cancel();
                    String message = "";
                    try {
                        Gson gson = new GsonBuilder().create();
                        BkashVerificationMessage response = gson.fromJson(new String(error.networkResponse.data, "UTF-8"), BkashVerificationMessage.class);
                        for (String m : response.getError().getMessages()) {
                            message += (m + "\n");
                        }
                    } catch (Exception e) {
                        message = "Something went wrong.Please Check your Internet Connection";
                    }
                    messageTypeImageView.setImageResource(R.mipmap.ic_warning_amber_48dp);
                    messageTypeTextView.setText("ERROR");
                    messageTextView.setText(message);
                    discountAmountHolder.setVisibility(View.GONE);
                    tryAgainButton.setVisibility(View.VISIBLE);
                    couponVerificationMessage.show();
                }
            }, CouponVerificationMessage.class);
            progressBarDialog.show();
            appController.addToRequestQueue(couponVerificationMessageObjectRequest);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        updatePaymentMethodsButton();
    }

    private void updatePaymentMethodsButton() {
        Calendar c1 = Calendar.getInstance();
        Log.d("Recent Search", "Current time => " + c1.getTime());
        Calendar c2 = Calendar.getInstance();
        CalendarData cd = paymentDetailsData.getSeatLayoutData().getBookTicketData().getCalendarData();
//        String dateTime = cd.getYear() + "-" + cd.getMonth() + "-" + cd.getDay() + " " + paymentDetailsData.getSeatLayoutData().getSelectedBoardingPoint().getLocationTime();
        String dateTime = paymentDetailsData.getSeatLayoutData().getSelectedBoardingPoint().getLocationDate() + " " + paymentDetailsData.getSeatLayoutData().getSelectedBoardingPoint().getLocationTime();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        try {
            c2.setTime(df.parse(dateTime));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Log.d("Recent Search", "Received time => " + c2.getTime());
        long difference = (c2.getTimeInMillis() - c1.getTimeInMillis()) / 1000;
        int hour = (int) difference / 3600;

        final PaymentMethodDataSource paymentMethodDataSource = new PaymentMethodDataSource(getContext());
        paymentMethodDataSource.open();

        PaymentMethodDataItem h1 = paymentMethodDataSource.getPaymentMethodDataItem(Constant.CONST_CASH_ON_DELIVERY_NAME);
        PaymentMethodDataItem h2 = paymentMethodDataSource.getPaymentMethodDataItem(Constant.CONST_MOBILE_BANKING_NAME);
        PaymentMethodDataItem h3 = paymentMethodDataSource.getPaymentMethodDataItem(Constant.CONST_CREDIT_OR_DEBIT_CARD_NAME);
        PaymentMethodDataItem h4 = paymentMethodDataSource.getPaymentMethodDataItem(Constant.CONST_INTERNET_BANKING_NAME);
        Log.d("Recent Search", "CONST_CASH_ON_DELIVERY_NAME => " + hour + " => " + h1.getPaymentMethodHours() + " => " + h1.isPaymentMethodIsActive());
        Log.d("Recent Search", "CONST_MOBILE_BANKING_NAME => " + hour + " => " + h2.getPaymentMethodHours() + " => " + h2.isPaymentMethodIsActive());
        Log.d("Recent Search", "CONST_CREDIT_OR_DEBIT_CARD_NAME => " + hour + " => " + h3.getPaymentMethodHours() + " => " + h3.isPaymentMethodIsActive());
        Log.d("Recent Search", "CONST_INTERNET_BANKING_NAME => " + hour + " => " + h4.getPaymentMethodHours() + " => " + h4.isPaymentMethodIsActive());
        cashOnDeliveryButton.setEnabled(hour >= h1.getPaymentMethodHours() && h1.isPaymentMethodIsActive());
        mobileBankingButton.setEnabled(hour >= h2.getPaymentMethodHours() && h2.isPaymentMethodIsActive());
        creditOrDebitCardButton.setEnabled(hour >= h3.getPaymentMethodHours() && h3.isPaymentMethodIsActive());
        internetBankingButton.setEnabled(hour >= h4.getPaymentMethodHours() && h4.isPaymentMethodIsActive());
    }

    public void getDiscountPrice(String mobileNumber, final Float discountAmount){


        SeatLayoutData seatLayoutData = paymentDetailsData.getSeatLayoutData();
        List<Parcelable> selectedSeats = seatLayoutData.getSelectedSeat();

                String url = API.DISCOUNT_API_URL;
            HashMap<String, String> postParams = new HashMap<>();
            postParams.put(API.Parameter.ANDROID_DEVICE_ID, appManager.getDeviceId());
            postParams.put(API.Parameter.ANDROID_APP_VERSION, appManager.getAppVersion());
            postParams.put(API.Parameter.MOBILE_NUMBER, mobileNumber);
            postParams.put(API.Parameter.COMPANY_ID, String.valueOf(paymentDetailsData.getSeatLayoutData().getCompanyId()));
            postParams.put(API.Parameter.SHOHOZ_DISCOUNT, ""+discountAmount*selectedSeats.size());
            postParams.put("tickets",""+selectedSeats.size());
            Log.d(TAG, "URL:" + url);
            Log.d(TAG, "PARAMS:" + postParams.toString());

            discountObjectRequest = new ObjectRequest<Discount>(Request.Method.POST, url, postParams, new Response.Listener<Discount>() {
                @Override
                public void onResponse(Discount response) {

//                    progressBarDialog.cancel();
//                    discountTextView.setText(getLeadingSpace());
                    discountFee = String.valueOf(response.getData().getShohozDiscount());
                    progressBarDialog.dismiss();
                    if(Constant.KEY_IS_DEVELOPMENT_BUILD){
                        Toast.makeText(getActivity(), response.toString(), Toast.LENGTH_LONG).show();
                    }

                    Constant.N_TH_ORDER = response.getData().getNthOrder();
                    Constant.KEY_DISCOUNT = Float.parseFloat(discountFee);

                    if (response.getData().getShohozDiscount()>0){
                        Constant.isDiscountable=true;
                    }
                    if (response.getData().getNthOrder()>3){
                        Constant.isDiscountable=false;
                        tvDiscountMessage.setVisibility(View.GONE);
                    }
                    else{
//                        discountAmountTextView.setVisibility(View.INVISIBLE);
                        tvDiscountMessage.setVisibility(View.VISIBLE);
                        tvDiscountMessage.setText("You've just saved "+Constant.KEY_DISCOUNT+" tk instead of buying tickets from counter.");
                    }
//                    discountFee = getPriceFormat(Float.parseFloat(discountFee));
//                    discountTextView.setText(getLeadingSpace(discountFee));
//                    totalPayment = getPriceFormat(calculateTotalFee(ticketFare, shohozFee, cashOnDeliveryFee, paymentGateWayFee, discountFee));
//                    totalPaymentTextView.setText(getLeadingSpace(totalPayment));
                    resetPaymentDetails(false);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
//                    progressBarDialog.cancel();
                    progressBarDialog.dismiss();
                    Toast.makeText(getActivity(), "Something went wrong about discount.", Toast.LENGTH_SHORT).show();
                }
            },Discount.class);
        progressBarMessageTextView.setText("Getting discount price...");
        progressBarDialog.show();
            appController.addToRequestQueue(discountObjectRequest);



    }

}
