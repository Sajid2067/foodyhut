package com.android.food.foodyhut.adapter.recyclerview;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.food.foodyhut.R;
import com.android.food.foodyhut.api.data.item.cuisineType.CuisineTypeResponse;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

/**
 * Created by sajid on 10/28/2015.
 */
public class RecentSearchListAdapter extends RecyclerView.Adapter<RecentSearchListAdapter.ViewHolder> {

    private Context context;
    private List<CuisineTypeResponse> cuisineTypes;
    private LayoutInflater layoutInflater;
 //   private SimpleDateFormat journeyDateFormat = new SimpleDateFormat("EEE, dd MMM", Locale.US);

    public RecentSearchListAdapter(Context context, List<CuisineTypeResponse> recyclerViewBaseItems) {
        this.context = context;
        this.cuisineTypes = recyclerViewBaseItems;
        layoutInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView;
        rootView = layoutInflater.inflate(R.layout.list_item_cuisine_type, parent, false);
        return new ViewHolder(rootView);
    }

    public void setItems(List<CuisineTypeResponse> recyclerViewBaseItems) {
        this.cuisineTypes = recyclerViewBaseItems;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        CuisineTypeResponse cuisineType = cuisineTypes.get(position);
//        if (getItemCount() - 1 == position) {
//            holder.listDivider.setVisibility(View.GONE);
//        } else {
//            holder.listDivider.setVisibility(View.VISIBLE);
//        }
     //   holder.fromCityTextView.setText(cuisineType.getFromCity());
     //   holder.toCityTextView.setText(cuisineType.getToCity());
        holder.cuisineNameTextView.setText(cuisineType.getCuisineName());

    }

    @Override
    public int getItemCount() {
        return cuisineTypes.size();
    }


    static class ViewHolder extends RecyclerView.ViewHolder {

        private TextView cuisineNameTextView;
     //   private TextView toCityTextView;
     //   private TextView fromCityTextView;
        private View listDivider;

        public ViewHolder(View rootView) {
            super(rootView);
            cuisineNameTextView = (TextView) rootView.findViewById(R.id.cuisine_name);
//            toCityTextView = (TextView) rootView.findViewById(R.id.to_city_text_view);
//            fromCityTextView = (TextView) rootView.findViewById(R.id.from_city_text_view);
            listDivider = (View) itemView.findViewById(R.id.list_divider);
        }
    }
}
