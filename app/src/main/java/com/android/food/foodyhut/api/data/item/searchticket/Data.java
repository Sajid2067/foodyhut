package com.android.food.foodyhut.api.data.item.searchticket;

import com.google.gson.annotations.SerializedName;

/**
 * Created by sajid on 12/14/2015.
 */
public class Data {
    @SerializedName("name")
    private String name;
    @SerializedName("pnr")
    private String pnr;
    @SerializedName("status")
    private String status;

    public Data() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPnr() {
        return pnr;
    }

    public void setPnr(String pnr) {
        this.pnr = pnr;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
