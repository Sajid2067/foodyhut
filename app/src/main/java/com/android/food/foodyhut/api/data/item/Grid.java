package com.android.food.foodyhut.api.data.item;

import com.google.gson.annotations.SerializedName;

/**
 * Created by sajid on 11/4/2015.
 */
public class Grid {

    @SerializedName("row")
    private int row;
    @SerializedName("column")
    private int column;

    public Grid() {
    }

    public Grid(int row, int column) {

        this.row = row;
        this.column = column;
    }

    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public int getColumn() {
        return column;
    }

    public void setColumn(int column) {
        this.column = column;
    }

    @Override
    public String toString() {
        return "Grid{" +
                "row=" + row +
                ", column=" + column +
                '}';
    }
}
