package com.android.food.foodyhut.adapter.listview;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.food.foodyhut.R;
import com.android.food.foodyhut.api.data.item.BoardingPoint;

import java.util.List;

/**
 * Created by sajid on 12/7/2015.
 */
public class BoardingPointFilterAdapter extends BaseAdapter {


    private Activity activity;
    private List<BoardingPoint> boardingPoints;
    private LayoutInflater layoutInflater;
    private int selectedPosition = -1;
    private View oldSelectedView;
    private ViewGroup parent;

    public BoardingPointFilterAdapter(Activity activity, List<BoardingPoint> boardingPoints) {
        this.activity = activity;
        this.boardingPoints = boardingPoints;
        layoutInflater = (LayoutInflater) this.activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return boardingPoints.size();
    }

    @Override
    public Object getItem(int position) {
        return boardingPoints.get(position);
    }

    @Override
    public long getItemId(int position) {
        return boardingPoints.get(position).getLocationId();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (position == -1) {
            return null;
        }
        final ViewHolder viewHolder;
        if (this.parent == null) {
            this.parent = parent;
        }
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.list_item_single_text, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.textView = (TextView) convertView.findViewById(R.id.textView);
            convertView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        Log.e("TAG", selectedPosition + "");
        if (selectedPosition == position) {

            convertView.setSelected(true);
        }
        BoardingPoint boardingPoint = boardingPoints.get(position);
        viewHolder.textView.setText(boardingPoint.getLocationName());
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (oldSelectedView != null) {
                    oldSelectedView.setSelected(false);
                }
                v.setSelected(true);
                oldSelectedView = v;
                setSelectedPosition(position);
            }
        });
        return convertView;
    }

    public View getOldSelectedView() {
        return oldSelectedView;
    }

    public void setOldSelectedView(View oldSelectedView) {
        this.oldSelectedView = oldSelectedView;
    }

    public ViewGroup getParent() {
        return parent;
    }

    public void setParent(ViewGroup parent) {
        this.parent = parent;
    }

    public int getSelectedPosition() {
        return selectedPosition;
    }

    public void setSelectedPosition(int selectedPosition) {
        this.selectedPosition = selectedPosition;
    }

    private static class ViewHolder {
        private TextView textView;
    }
}
