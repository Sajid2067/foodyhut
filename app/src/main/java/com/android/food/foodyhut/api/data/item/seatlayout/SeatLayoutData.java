package com.android.food.foodyhut.api.data.item.seatlayout;

import com.google.gson.annotations.SerializedName;
import com.android.food.foodyhut.api.data.item.seat.Seat;

/**
 * Created by sajid on 11/4/2015.
 */
public class SeatLayoutData {

    @SerializedName("query")
    private SeatLayoutQuery seatLayoutQuery;
    @SerializedName("seats")
    private Seat seat;


    public SeatLayoutData() {
    }

    public SeatLayoutData(SeatLayoutQuery seatLayoutQuery, Seat seat) {

        this.seatLayoutQuery = seatLayoutQuery;
        this.seat = seat;

    }

    public SeatLayoutQuery getSeatLayoutQuery() {

        return seatLayoutQuery;
    }

    public void setSeatLayoutQuery(SeatLayoutQuery seatLayoutQuery) {
        this.seatLayoutQuery = seatLayoutQuery;
    }

    public Seat getSeat() {
        return seat;
    }

    public void setSeat(Seat seat) {
        this.seat = seat;
    }



    @Override
    public String toString() {
        return "SeatLayoutData{" +
                "seatLayoutQuery=" + seatLayoutQuery +
                ", seat=" + seat +
                '}';
    }
}
