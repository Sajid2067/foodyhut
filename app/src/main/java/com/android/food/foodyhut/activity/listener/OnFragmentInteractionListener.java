package com.android.food.foodyhut.activity.listener;

import android.os.Parcelable;

import com.android.food.foodyhut.activity.annotation.FragmentId;

import java.util.ArrayList;

/**
 * Created by sajid on 10/28/2015.
 */
public interface OnFragmentInteractionListener {

    int NULL_FRAGMENT_ID = -1;
    int LOCATION_FRAGMENT_FH = 0;
    int CALENDER_FRAGMENT_ID = 1;
    int CUISINE_TYPE_FRAGMENT_ID = 2;
    int BOOK_TICKET_FRAGMENT_ID = 3;
    int TOP_SEARCH_FRAGMENT_ID = 4;
    int TERMS_AND_CONDITION_FRAGMENT_ID = 5;
    int CANCEL_TICKET_FRAGMENT_ID = 6;
    int OFFERS_FRAGMENT_ID = 7;
    int SEAT_LAYOUT_FRAGMENT_ID = 8;
    int SEAT_LAYOUT_FULL_FRAGMENT_ID = 9;
    int BOARDING_POINT_FRAGMENT_ID = 10;
    int PASSENGER_INFO_FRAGMENT_ID = 11;
    int PAYMENT_DETAILS_FRAGMENT_ID = 12;
    int PAYMENT_DETAILS_COD_FRAGMENT_ID = 13;
    int FILTER_FRAGMENT_ID = 14;
    int TICKET_DETAILS_FRAGMENT_ID = 15;
    int TICKET_BUY_SUCCESS_FRAGMENT_ID = 16;
    int BKASH_PAYMENT_VERIFICATION_FRAGMENT_ID = 17;
    int CARD_DETAILS_FRAGMENT_ID = 18;
    int INTERNET_BANKING_DETAILS_FRAGMENT_ID = 19;
    int SEARCH_RESULT_CONTAINER_FRAGMENT_ID_FH = 20;
    int TICKET_SEARCH_FRAGMENT_ID = 21;
    int CANCEL_TICKET_DETAILS_FRAGMENT_ID = 23;
    int MAP_FRAGMENT_ID = 24;
    int TIMELINE_VISIT_FAVOURITE_FRAGMENT_ID = 25;
    int TIMELINE_FRAGMENT_ID = 26;
    int VISIT_FRAGMENT_ID = 27;
    int FAVOURITE_FRAGMENT_ID = 28;
    int ACCOUNT_FRAGMENT_ID = 29;

    void fragmentChange(@FragmentId final int fragmentId, ArrayList<Integer> previousFragmentIds);

    void fragmentChange(@FragmentId final int fragmentId, Parcelable parcelable, ArrayList<Integer> previousFragmentIds);


    void onBackPressed(Parcelable parcelable, @FragmentId final int fragmentId, final ArrayList<Integer> previousFragmentIds);

}
