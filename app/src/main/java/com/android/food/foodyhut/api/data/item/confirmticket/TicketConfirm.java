package com.android.food.foodyhut.api.data.item.confirmticket;

import com.google.gson.annotations.SerializedName;

/**
 * Created by sajid on 11/7/2015.
 */
public class TicketConfirm {
    @SerializedName("data")
    private TicketConfirmData data;
    @SerializedName("error")
    private TicketConfirmErrorData error;

    public TicketConfirm() {
        this(new TicketConfirmData(), new TicketConfirmErrorData());
    }

    public TicketConfirm(TicketConfirmData data, TicketConfirmErrorData error) {

        this.data = data;
        this.error = error;
    }

    public TicketConfirmData getData() {

        return data;
    }

    public void setData(TicketConfirmData data) {
        this.data = data;
    }

    public TicketConfirmErrorData getError() {
        return error;
    }

    public void setError(TicketConfirmErrorData error) {
        this.error = error;
    }

    @Override
    public String toString() {
        return "TicketConfirm{" +
                "data=" + data +
                ", error=" + error +
                '}';
    }
}
