package com.android.food.foodyhut.fragment;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.android.food.foodyhut.adapter.recyclerview.OffersListAdapter;
import com.android.food.foodyhut.api.data.item.handshake.offer.ShohozOffer;
import com.android.food.foodyhut.application.AppController;
import com.android.food.foodyhut.util.AppManager;
import com.android.food.foodyhut.util.OffersPreference;
import com.android.food.foodyhut.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by sajid on 10/27/2015.
 */
public class OffersFragment extends BaseFragment {

    private static final String TAG = OffersFragment.class.getSimpleName();

    private RecyclerView offersRecyclerView;

    Tracker mTracker;

    private OffersListAdapter offersListAdapter;
    private List<ShohozOffer> shohozOffers;
    private LinearLayoutManager linearLayoutManager;
    private AppManager appManager;
    private TextView noOffersTextView;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        appManager = new AppManager(getActivity());
        createOffersMenu();
        // Obtain the shared Tracker instance.
        AppController application = (AppController) getActivity().getApplication();
        mTracker = application.getDefaultTracker();
        mTracker.setScreenName("Offers");
        // Send a screen view.
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    private void createOffersMenu() {
        Gson gson = new GsonBuilder().create();
        OffersPreference offersPreference = gson.fromJson(appManager.getOffers(), OffersPreference.class);
        shohozOffers = offersPreference.getShohozOfferList();
        for (int i = 0; i < shohozOffers.size(); i++) {
            String dateTime = shohozOffers.get(i).getValidTill();
            long difference = -1;
            if (!dateTime.equals("null")) {
                Calendar c1 = Calendar.getInstance();
                Log.d("Recent Search", "Current time => " + c1.getTime());
                Calendar c2 = Calendar.getInstance();
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                try {
                    c2.setTime(df.parse(dateTime));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                Log.d("Recent Search", "Received time => " + c2.getTime());
                difference = (c1.getTimeInMillis() - c2.getTimeInMillis()) / 3600000;
                Log.d("Recent Search", "Difference => " + difference);
            }
            if (difference >= 0) {
                shohozOffers.remove(i);
                i--;
            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.fragment_offers, container, false);
        }
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    void initializeEditTextComponents() {

    }

    @Override
    void initializeButtonComponents() {
    }

    @Override
    void initializeTextViewComponents() {
        noOffersTextView = findViewById(R.id.no_offers_text_view);
    }

    @Override
    void initializeOtherViewComponents() {
        offersRecyclerView = (RecyclerView) rootView.findViewById(R.id.offers_recycler_view);
        linearLayoutManager = new LinearLayoutManager(getContext());
        offersRecyclerView.setLayoutManager(linearLayoutManager);
        offersRecyclerView.setHasFixedSize(true);
        offersListAdapter = new OffersListAdapter(getContext(), shohozOffers);
        offersRecyclerView.setAdapter(offersListAdapter);
        if (shohozOffers.size() == 0) {
            offersRecyclerView.setVisibility(View.GONE);
            noOffersTextView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    void initializeOnclickListener() {
    }

    @Override
    void removeOnclickListener() {
    }

    public static OffersFragment newInstance(ArrayList<Integer> previousFragmentIds, Parcelable parcelable) {
        OffersFragment offersFragment = new OffersFragment();
        Bundle bundle = new Bundle();
        bundle.putIntegerArrayList(PREVIOUS_FRAGMENT_ID_TAG, previousFragmentIds);
        bundle.putParcelable(PARCELABLE_FRAGMENT_ITEM_ID_TAG, parcelable);
        offersFragment.setArguments(bundle);
        return offersFragment;
    }
}
