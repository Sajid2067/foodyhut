package com.android.food.foodyhut.fragment;

import android.graphics.Paint;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.android.food.foodyhut.activity.listener.OnFragmentInteractionListener;
import com.android.food.foodyhut.api.data.item.seat.SeatNumber;
import com.android.food.foodyhut.api.data.item.seatlayout.SeatLayout;
import com.android.food.foodyhut.application.AppController;
import com.android.food.foodyhut.fragment.item.BookTicketData;
import com.android.food.foodyhut.fragment.item.CalendarData;
import com.android.food.foodyhut.fragment.item.PassengerInfoData;
import com.android.food.foodyhut.fragment.item.SeatLayoutData;
import com.android.food.foodyhut.fragment.item.SeatNumberData;
import com.android.food.foodyhut.toolbox.GetUrlBuilder;
import com.android.food.foodyhut.toolbox.ObjectRequest;
import com.android.food.foodyhut.util.API;
import com.android.food.foodyhut.util.AppManager;
import com.android.food.foodyhut.util.Constant;
import com.android.food.foodyhut.util.FrequentFunction;
import com.android.food.foodyhut.view.widget.ProgressBar;
import com.android.food.foodyhut.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

/**
 * Created by sajid on 11/4/2015.
 */
public class SeatLayoutFragment extends BaseFragment implements Response.Listener<SeatLayout>, Response.ErrorListener {

    private static final String TAG = SeatLayoutFragment.class.getSimpleName();

    Tracker mTracker;
    private TextView journeyRouteTextView;
    private TextView journeyDateTextView;
    private TextView busOperatorNameTextView;
    private TextView journeyTimeTextView;
    private TextView busTypeTextView;
    private TextView availableSeatTextView;
    private TextView boardingPointTextView;
    private HorizontalScrollView seatLayoutScrollView;

    private LinearLayout seatOneView;
    private LinearLayout seatTwoView;
    private LinearLayout seatThreeView;
    private LinearLayout seatFourView;
    private LinearLayout seatMiddleView;
    private LinearLayout seatMiddleViewRight;
    private View seatLayoutHolder;
    private View addSeatButton;

    private View addBoardingPointButton;
    private ProgressBar recentSearchProgressBar;
    private View recentSearchProgressBarHolder;

    private TextView newBaseFareTextView;
    private TextView oldBaseFareTextView;

    private Button continueBookingButton;

    private ObjectRequest<SeatLayout> seatLayoutObjectRequest;

    private SeatLayoutData seatLayoutData;
    BookTicketData bookTicketData;
    CalendarData calendarData;

    private SeatLayout seatLayout;
    private Calendar calendar = Calendar.getInstance();
    private static final SimpleDateFormat monthFormat = new SimpleDateFormat("MMM", Locale.US);
    private static final SimpleDateFormat dayFormat = new SimpleDateFormat("EEE", Locale.US);
    private static final String dateFormat = "%s %d<sup>%s</sup> %s %d";

    private boolean hasSeatLayout = false;
    private AppController appController = AppController.getInstance();

    private AppManager appManager;

    public static SeatLayoutFragment newInstance(ArrayList<Integer> previousFragmentIds, Parcelable parcelable) {
        SeatLayoutFragment seatLayoutFragment = new SeatLayoutFragment();
        Bundle bundle = new Bundle();
        bundle.putIntegerArrayList(PREVIOUS_FRAGMENT_ID_TAG, previousFragmentIds);
        bundle.putParcelable(PARCELABLE_FRAGMENT_ITEM_ID_TAG, parcelable);
        seatLayoutFragment.setArguments(bundle);
        return seatLayoutFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        appManager = new AppManager(getActivity());
        if (parcelable != null) {
            if (parcelable instanceof SeatLayoutData) {
                seatLayoutData = (SeatLayoutData) parcelable;
            } else if (parcelable instanceof PassengerInfoData) {
                seatLayoutData = ((PassengerInfoData) parcelable).getSeatLayoutData();
                parcelable = seatLayoutData;
            }
        }
        Log.e(TAG, seatLayoutData.getBookTicketData().toString());
        bookTicketData = seatLayoutData.getBookTicketData();
        calendarData = bookTicketData.getCalendarData();
        calendar.set(calendarData.getYear(), calendarData.getMonth(), calendarData.getDay());
        Constant.KEY_IS_COUPON_APPLIED = false;
        Constant.KEY_COUPON_DISCOUNT = 0.00f;
        Constant.KEY_APPLIED_COUPON_CODE = "";

        // Obtain the shared Tracker instance.
        AppController application = (AppController) getActivity().getApplication();
        mTracker = application.getDefaultTracker();
        mTracker.setScreenName("Seat Layout");
        // Send a screen view.
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.fragment_seat_layout, container, false);
        }
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    void initializeEditTextComponents() {

    }

    @Override
    void initializeButtonComponents() {
        addBoardingPointButton = rootView.findViewById(R.id.add_boarding_point_button);
        continueBookingButton = (Button) rootView.findViewById(R.id.continue_booking_button);
    }


    @Override
    void initializeTextViewComponents() {

        journeyRouteTextView = (TextView) rootView.findViewById(R.id.journey_route_text_view);
        String journeyRoute = bookTicketData.getFromCity() + " to " + bookTicketData.getToCity();
        journeyRouteTextView.setText(journeyRoute);
        journeyDateTextView = (TextView) rootView.findViewById(R.id.journey_date_text_view);
        String htmlDate = appManager.getHTMLFormatDate(bookTicketData.getCalendarData(), calendar);
        FrequentFunction.convertHtml(journeyDateTextView,htmlDate);
      //  journeyDateTextView.setText(Html.fromHtml(htmlDate));
        busOperatorNameTextView = (TextView) rootView.findViewById(R.id.bus_operator_name_text_view);
        busOperatorNameTextView.setText(seatLayoutData.getOperatorName());
        journeyTimeTextView = (TextView) rootView.findViewById(R.id.journey_time_text_view);
        journeyTimeTextView.setText(appManager.getTime(seatLayoutData.getJourneyTime()));
        busTypeTextView = (TextView) rootView.findViewById(R.id.bus_type_text_view);
        busTypeTextView.setText(appManager.getBusType(seatLayoutData.getBusType()));
        newBaseFareTextView = (TextView) rootView.findViewById(R.id.new_base_fare_text_view);
        oldBaseFareTextView = (TextView) rootView.findViewById(R.id.old_base_fare_text_view);
        oldBaseFareTextView.setPaintFlags(oldBaseFareTextView.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        if (Constant.KEY_DISCOUNT > 0) {
            oldBaseFareTextView.setVisibility(View.VISIBLE);
        }
        availableSeatTextView = (TextView) rootView.findViewById(R.id.available_seat_text_view);
        String availableSeats = seatLayoutData.getAvailableSeats() + " Seats";
        availableSeatTextView.setText(availableSeats);
        boardingPointTextView = (TextView) rootView.findViewById(R.id.boarding_point_text_view);
        if (seatLayoutData.getSelectedBoardingPoint() != null) {
            String boardingTime = appManager.getTime(seatLayoutData.getSelectedBoardingPoint().getLocationTime());
            String boardingPoint = seatLayoutData.getSelectedBoardingPoint().getLocationName() + " " + boardingTime;
            boardingPointTextView.setText(boardingPoint);
        }
    }

    @Override
    void initializeOtherViewComponents() {
        seatLayoutScrollView = findViewById(R.id.seat_layout_scroll_view);
        seatLayoutScrollView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });
        addSeatButton = rootView.findViewById(R.id.add_seat_button);
        seatLayoutHolder = rootView.findViewById(R.id.seat_layout_holder);
        recentSearchProgressBar = (ProgressBar) rootView.findViewById(R.id.recent_search_progress_bar);
        recentSearchProgressBarHolder = rootView.findViewById(R.id.recent_search_progress_bar_holder);
        seatOneView = (LinearLayout) rootView.findViewById(R.id.seat_one_view);
        seatTwoView = (LinearLayout) rootView.findViewById(R.id.seat_two_view);
        seatThreeView = (LinearLayout) rootView.findViewById(R.id.seat_three_view);
        seatFourView = (LinearLayout) rootView.findViewById(R.id.seat_four_view);
        seatMiddleView = (LinearLayout) rootView.findViewById(R.id.seat_middle_view);
        seatMiddleViewRight = (LinearLayout) rootView.findViewById(R.id.seat_middle_view_right);
        connectionErrorView = findViewById(R.id.connection_error_view);
        if ((seatLayoutData.getSelectedSeat().size() != 0 || seatLayoutData.getSeatLayoutAsJson() != null)) {
            if (!seatLayoutData.getSeatLayoutAsJson().equals("")) {
                Log.e(TAG, seatLayoutData.getSeatLayoutAsJson());
                addSeatButton.setVisibility(View.VISIBLE);
                recentSearchProgressBar.setVisibility(View.GONE);
                recentSearchProgressBarHolder.setVisibility(View.GONE);
                Gson gson = new GsonBuilder().create();
                SeatLayout seatLayout = gson.fromJson(seatLayoutData.getSeatLayoutAsJson(), SeatLayout.class);
                setSeatLayout(seatLayout);
                updateBaseFareTextView(seatLayoutData.getSelectedSeat());
            } else {
                requestSeatLayout();
            }
        } else {
            requestSeatLayout();
        }
    }

    private void requestSeatLayout() {
        if (appManager.isNetworkAvailable()) {

            String url = API.SEAT_LAYOUT_API_URL;
            HashMap<String, String> params = new HashMap<>();
            params.put(API.Parameter.ANDROID_APP_VERSION, appManager.getAppVersion());
            params.put(API.Parameter.ANDROID_DEVICE_ID, appManager.getDeviceId());
            params.put(API.Parameter.TRIP_ID, seatLayoutData.getTripId());
            params.put(API.Parameter.TRIP_ROUTE_ID, seatLayoutData.getTripRouteId());
            GetUrlBuilder getUrlBuilder = new GetUrlBuilder(url, params);
            Log.d(TAG, "requestSeatLayout:" + url);
            Log.d(TAG, "requestSeatLayout:" + params.toString());
            if ((seatLayoutData.getSelectedSeat().size() == 0 && seatLayoutData.getSeatLayoutAsJson() == null) || seatLayoutData.getSeatLayoutAsJson().equals("")) {
                seatLayoutObjectRequest = new ObjectRequest<>(API.Method.SEAT_LAYOUT_API_METHOD,
                        getUrlBuilder.getQueryUrl(), this, this, SeatLayout.class);
                appController.addToRequestQueue(seatLayoutObjectRequest);
                connectionErrorView.setVisibility(View.GONE);
                recentSearchProgressBar.setVisibility(View.VISIBLE);
            }
        } else {
            connectionErrorView.setVisibility(View.VISIBLE);
            recentSearchProgressBar.setVisibility(View.GONE);
        }
    }

    private void updateBaseFareTextView(List<Parcelable> selectedSeats) {
        float newBaseFareString = 0.0f;
        float oldBaseFareString = 0.0f;
        for (int i = 0; i < selectedSeats.size(); i++) {
            SeatNumberData seatNumberData = (SeatNumberData) selectedSeats.get(i);
            newBaseFareString = newBaseFareString + (seatNumberData.getSeatFare() - Constant.KEY_DISCOUNT);
            oldBaseFareString = oldBaseFareString + seatNumberData.getSeatFare();
        }
        newBaseFareTextView.setText(String.format("৳ %.02f", newBaseFareString));
        if (Constant.KEY_DISCOUNT > 0) {
            oldBaseFareTextView.setText(String.format("৳ %.02f", oldBaseFareString));
        }
    }

    @Override
    void initializeOnclickListener() {
        addSeatButton.setOnClickListener(this);
        addBoardingPointButton.setOnClickListener(this);
        continueBookingButton.setOnClickListener(this);
        seatLayoutHolder.setOnClickListener(this);
    }

    @Override
    void removeOnclickListener() {
        addSeatButton.setOnClickListener(null);
        addBoardingPointButton.setOnClickListener(null);
        continueBookingButton.setOnClickListener(null);
        seatLayoutHolder.setOnClickListener(null);
    }


    @Override
    public void onErrorResponse(VolleyError error) {
        connectionErrorView.setVisibility(View.VISIBLE);
        recentSearchProgressBar.setVisibility(View.GONE);
    }

    private ImageButton getSeatImageButton(SeatNumberData seatNumberData) {
        boolean enable = seatNumberData.getSeatAvailability() == 1;
        ImageButton imageButton = new ImageButton(getContext());
        imageButton.setPadding(
                getResources().getDimensionPixelSize(R.dimen.seat_padding),
                getResources().getDimensionPixelSize(R.dimen.seat_padding),
                getResources().getDimensionPixelSize(R.dimen.seat_padding),
                getResources().getDimensionPixelSize(R.dimen.seat_padding));
        imageButton.setTag(seatNumberData);
        imageButton.setBackgroundResource(R.drawable.rect_flat_button);
        imageButton.setImageResource(enable ? R.mipmap.ic_economy_seat_white_normal_18dp : R.mipmap.ic_economy_seat_white_booked_18dp);
        if (seatLayoutData.getSelectedSeat().size() != 0) {
            Log.e(TAG, seatLayoutData.getSelectedSeat().toString());
            if (seatLayoutData.getSelectedSeat().contains(seatNumberData)) {

                imageButton.setImageResource(enable ? R.mipmap.ic_economy_seat_white_picked_18dp : R.mipmap.ic_economy_seat_white_booked_18dp);
            }
        }
        imageButton.setOnClickListener(this);
        imageButton.setEnabled(enable);
        return imageButton;
    }

    @Override
    public void onResponse(SeatLayout seatLayout) {
        Log.e(TAG, seatLayout.toString());
        this.seatLayout = seatLayout;
        addSeatButton.setVisibility(View.VISIBLE);
        setSeatLayout(seatLayout);
        recentSearchProgressBar.setVisibility(View.GONE);
        recentSearchProgressBarHolder.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        Gson gson = new GsonBuilder().create();
        switch (view.getId()) {
            case R.id.retry_button:
                requestSeatLayout();
                break;
            case R.id.seat_layout_holder:
            case R.id.add_seat_button:
                seatLayoutData.setSeatLayoutAsJson(gson.toJson(seatLayout));
                previousFragmentIds.add(OnFragmentInteractionListener.SEAT_LAYOUT_FRAGMENT_ID);
                onFragmentChangeCallListener.fragmentChange(OnFragmentInteractionListener.SEAT_LAYOUT_FULL_FRAGMENT_ID, seatLayoutData, previousFragmentIds);
                break;
            case R.id.add_boarding_point_button:
                if (hasSeatLayout) {
                    seatLayoutData.setSeatLayoutAsJson(gson.toJson(seatLayout));
                    Log.e(TAG, seatLayoutData.getSeatLayoutAsJson());
                    previousFragmentIds.add(OnFragmentInteractionListener.SEAT_LAYOUT_FRAGMENT_ID);
                    onFragmentChangeCallListener.fragmentChange(OnFragmentInteractionListener.BOARDING_POINT_FRAGMENT_ID, seatLayoutData, previousFragmentIds);
                } else {
                    makeAToast("Please wait a while!", Toast.LENGTH_SHORT);
                }
                break;
            case R.id.continue_booking_button:
                if (seatLayoutData.getSelectedSeat() == null || seatLayoutData.getSelectedSeat().size() == 0) {
                    Toast.makeText(getContext(), "You must select minimum 1 Seat and Maximum 4 Seats", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (seatLayoutData.getSelectedBoardingPoint() == null) {
                    Toast.makeText(getContext(), "You must select your boarding point", Toast.LENGTH_SHORT).show();
                    return;
                }
                PassengerInfoData passengerInfoData = new PassengerInfoData();
                passengerInfoData.setSeatLayoutData(this.seatLayoutData);
                previousFragmentIds.add(OnFragmentInteractionListener.SEAT_LAYOUT_FRAGMENT_ID);
                onFragmentChangeCallListener.fragmentChange(OnFragmentInteractionListener.PASSENGER_INFO_FRAGMENT_ID, passengerInfoData, previousFragmentIds);
                break;
        }
        if (view instanceof ImageButton && view.getId() != R.id.back_button) {
            seatLayoutData.setSeatLayoutAsJson(gson.toJson(seatLayout));
            previousFragmentIds.add(OnFragmentInteractionListener.SEAT_LAYOUT_FRAGMENT_ID);
            onFragmentChangeCallListener.fragmentChange(OnFragmentInteractionListener.SEAT_LAYOUT_FULL_FRAGMENT_ID, seatLayoutData, previousFragmentIds);
            Log.d(TAG, view.getTag().toString());
        }

    }

    public void setSeatLayout(SeatLayout seatLayout) {
        hasSeatLayout = true;
        this.seatLayout = seatLayout;
        ArrayList<SeatNumberData> firstRowSeats = new ArrayList<>();
        ArrayList<SeatNumberData> secondRowSeats = new ArrayList<>();
        ArrayList<SeatNumberData> thirdRowSeats = new ArrayList<>();
        ArrayList<SeatNumberData> forthRowSeats = new ArrayList<>();
        ArrayList<SeatNumberData> middleRowSeats = new ArrayList<>();
        ArrayList<SeatNumberData> middleRowSeatsRight = new ArrayList<>();
        addSeatSystemTwo(seatLayout, firstRowSeats, secondRowSeats, thirdRowSeats, forthRowSeats, middleRowSeats, middleRowSeatsRight);
        for (SeatNumberData seatNumberData : firstRowSeats) {
            seatOneView.addView(getSeatImageButton(seatNumberData));
        }
        for (SeatNumberData seatNumberData : secondRowSeats) {
            seatTwoView.addView(getSeatImageButton(seatNumberData));
        }
        for (SeatNumberData seatNumberData : thirdRowSeats) {
            seatThreeView.addView(getSeatImageButton(seatNumberData));
        }
        for (SeatNumberData seatNumberData : forthRowSeats) {
            seatFourView.addView(getSeatImageButton(seatNumberData));
        }
        for (SeatNumberData seatNumberData : middleRowSeats) {
            seatMiddleView.addView(getSeatImageButton(seatNumberData));
        }
        for (SeatNumberData seatNumberData : middleRowSeatsRight) {
            seatMiddleViewRight.addView(getSeatImageButton(seatNumberData));
        }
    }

    private int addSeatSystemTwo(SeatLayout seatLayout, ArrayList<SeatNumberData> firstRowSeats, ArrayList<SeatNumberData> secondRowSeats, ArrayList<SeatNumberData> thirdRowSeats, ArrayList<SeatNumberData> forthRowSeats, ArrayList<SeatNumberData> middleRowSeats,ArrayList<SeatNumberData> middleRowSeatsRight) {
        int totalSeat = 0;
        int totalColumn = seatLayout.getSeatLayoutData().getSeat().getGrid().getColumn();
        Log.d(TAG, "totalColumn - " + totalColumn);
        for (int i = 0; i < seatLayout.getSeatLayoutData().getSeat().getGrid().getColumn(); i++) {
            ArrayList<SeatNumber> seatNumbers = seatLayout.getSeatLayoutData().getSeat().getSeatNumbers().get(i);
            for (int j = 0; j < seatLayout.getSeatLayoutData().getSeat().getGrid().getRow(); j++) {
                SeatNumber seatNumber = seatNumbers.get(j);
                if (seatNumber.getSeatNumber().trim().length() != 0) {
                    totalSeat++;
                    if (totalColumn == 6) {
                        switch (i) {
                            case 0:
                                Log.d(TAG, "first row-" + seatNumber.getSeatNumber());
                                firstRowSeats.add(new SeatNumberData(seatNumber));
                                break;
                            case 1:
                                Log.d(TAG, "second row-" + seatNumber.getSeatNumber());
                                secondRowSeats.add(new SeatNumberData(seatNumber));
                                break;
                            case 2:
                                Log.d(TAG, "middle row-" + seatNumber.getSeatNumber());
                                middleRowSeats.add(new SeatNumberData(seatNumber));
                                break;
                            case 3:
                                Log.d(TAG, "middle Right row-" + seatNumber.getSeatNumber());
                                middleRowSeatsRight.add(new SeatNumberData(seatNumber));
                                break;
                            case 4:
                                Log.d(TAG, "third row-" + seatNumber.getSeatNumber());
                                thirdRowSeats.add(new SeatNumberData(seatNumber));
                                break;
                            case 5:
                                Log.d(TAG, "forth row-" + seatNumber.getSeatNumber());
                                forthRowSeats.add(new SeatNumberData(seatNumber));
                                break;
                        }
                    }
                    else if (totalColumn == 5) {
                        switch (i) {
                            case 0:
                                Log.d(TAG, "first row-" + seatNumber.getSeatNumber());
                                firstRowSeats.add(new SeatNumberData(seatNumber));
                                break;
                            case 1:
                                Log.d(TAG, "second row-" + seatNumber.getSeatNumber());
                                secondRowSeats.add(new SeatNumberData(seatNumber));
                                break;
                            case 2:
                                Log.d(TAG, "middle row-" + seatNumber.getSeatNumber());
                                middleRowSeats.add(new SeatNumberData(seatNumber));
                                break;
                            case 3:
                                Log.d(TAG, "third row-" + seatNumber.getSeatNumber());
                                thirdRowSeats.add(new SeatNumberData(seatNumber));
                                break;
                            case 4:
                                Log.d(TAG, "forth row-" + seatNumber.getSeatNumber());
                                forthRowSeats.add(new SeatNumberData(seatNumber));
                                break;
                        }
                    } else if (totalColumn == 4) {
                        switch (i) {
                            case 0:
                                Log.d(TAG, "first row-" + seatNumber.getSeatNumber());
                                firstRowSeats.add(new SeatNumberData(seatNumber));
                                break;
                            case 1:
                                Log.d(TAG, "second row-" + seatNumber.getSeatNumber());
                                secondRowSeats.add(new SeatNumberData(seatNumber));
                                break;
                            case 2:
                                Log.d(TAG, "third row-" + seatNumber.getSeatNumber());
                                thirdRowSeats.add(new SeatNumberData(seatNumber));
                                break;
                            case 3:
                                Log.d(TAG, "forth row-" + seatNumber.getSeatNumber());
                                forthRowSeats.add(new SeatNumberData(seatNumber));
                                break;
                        }
                    }
                }
            }
        }
        return totalSeat;
    }
}
