package com.android.food.foodyhut.adapter.listview;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.food.foodyhut.R;
import com.android.food.foodyhut.api.data.item.searchticket.TicketSearchItem;
import com.thin.downloadmanager.DefaultRetryPolicy;
import com.thin.downloadmanager.DownloadRequest;
import com.thin.downloadmanager.DownloadStatusListener;
import com.thin.downloadmanager.ThinDownloadManager;

import java.io.File;
import java.util.List;

/**
 * Created by sajid on 12/8/2015.
 */
public class TicketSearchAdapter extends BaseAdapter {

    private static final String TAG = TicketSearchAdapter.class.getSimpleName();
    private List<TicketSearchItem> ticketSearchItems;
    private Activity activity;
    private LayoutInflater layoutInflater;
    private ThinDownloadManager downloadManager;
    private ProgressDialog progressDialog;


    public TicketSearchAdapter(List<TicketSearchItem> ticketSearchItemList, Activity activity) {
        this.ticketSearchItems = ticketSearchItemList;
        this.activity = activity;
        layoutInflater = (LayoutInflater) this.activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        return ticketSearchItems.size();
    }

    @Override
    public Object getItem(int position) {
        return ticketSearchItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        final ViewHolder viewHolder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.list_item_ticket_search, parent, false);
        }
            viewHolder = new ViewHolder();
            viewHolder.name = (TextView) convertView.findViewById(R.id.customer_name_text_view);
            viewHolder.pnr = (TextView) convertView.findViewById(R.id.ticket_pnr_text_view);
            viewHolder.status = (TextView) convertView.findViewById(R.id.ticket_status_text_view);
            viewHolder.download = (Button) convertView.findViewById(R.id.ticket_downloading_button);

        TicketSearchItem ticketSearchItem = ticketSearchItems.get(position);
        viewHolder.name.setText(ticketSearchItem.getName());
        viewHolder.pnr.setText(ticketSearchItem.getPnr());
        viewHolder.download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                downloadPDFTicket();
            }
        });




        return convertView;
    }

    private static class ViewHolder {
       private TextView name;
        private TextView pnr;
        private TextView status;
        private Button download;

    }

    public void downloadPDFTicket(){
        downloadManager = new ThinDownloadManager();

        progressDialog =new ProgressDialog(activity);
        progressDialog.setMessage("Downloading Ticket..");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressDialog.setIndeterminate(true);
        progressDialog.setProgress(0);

        String path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Shohoz.com";

        File dir = new File(path);
        if (!dir.exists())
            dir.mkdirs();
        File file = new File(dir, "ticket.pdf");
        Uri downloadUri = Uri.parse("http://templatelab.com/download/raffle-ticket-templates-10/?wpdmdl=1576");
        Uri destinationUri = Uri.parse(path+"/ticket.pdf");
        DownloadRequest downloadRequest = new DownloadRequest(downloadUri)
//                .addCustomHeader("Auth-Token", "YourTokenApiKey")
                .setRetryPolicy(new DefaultRetryPolicy())
                .setDestinationURI(destinationUri).setPriority(DownloadRequest.Priority.HIGH)
                .setDownloadContext(this)//Optional
                .setDownloadListener(new DownloadStatusListener() {
                    @Override
                    public void onDownloadComplete(int id) {
                        Toast.makeText(activity, "Download Completed..", Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                        openTicketPDF();

                    }

                    @Override
                    public void onDownloadFailed(int id, int errorCode, String errorMessage) {

                    }

                    @Override
                    public void onProgress(int id, long totalBytes, long downlaodedBytes, int progress) {

                        progressDialog.setProgress(progress);
                    }
                });
        progressDialog.show();
        downloadManager.add(downloadRequest);
    }
    public void openTicketPDF() {
        File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/Shohoz.com/ticket.pdf");
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setDataAndType(Uri.fromFile(file), "application/pdf");
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        activity.startActivity(intent);
    }
}
