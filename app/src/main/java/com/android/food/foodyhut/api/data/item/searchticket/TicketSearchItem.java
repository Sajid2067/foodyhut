package com.android.food.foodyhut.api.data.item.searchticket;

/**
 * Created by TT on 3/11/2017.
 */

public class TicketSearchItem {

    String name;
    String pnr;
    String status;

    public TicketSearchItem() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPnr() {
        return pnr;
    }

    public void setPnr(String pnr) {
        this.pnr = pnr;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
