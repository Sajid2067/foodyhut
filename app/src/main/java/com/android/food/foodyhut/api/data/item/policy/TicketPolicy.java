package com.android.food.foodyhut.api.data.item.policy;

/**
 * Created by sajid on 11/3/2015.
 */
public class TicketPolicy {

    private int max;
    private int min;

    public TicketPolicy() {
    }

    public TicketPolicy(int max, int min) {

        this.max = max;
        this.min = min;
    }

    public int getMax() {

        return max;
    }

    public void setMax(int max) {
        this.max = max;
    }

    public int getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;
    }

    @Override
    public String toString() {
        return "TicketPolicy{" +
                "max=" + max +
                ", min=" + min +
                '}';
    }
}
