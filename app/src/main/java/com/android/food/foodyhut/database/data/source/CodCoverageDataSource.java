package com.android.food.foodyhut.database.data.source;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.android.food.foodyhut.database.DataBaseOpenHelper;
import com.android.food.foodyhut.database.data.item.CodCoverageDataItem;
import com.android.food.foodyhut.database.exception.DataSourceException;

import java.util.ArrayList;

/**
 * Created by tuman on 4/5/2015.
 */
public class CodCoverageDataSource {

    protected Context context;
    protected SQLiteDatabase database;
    protected DataBaseOpenHelper dbHelper;
    private String[] allColumns = {
            DataBaseOpenHelper.AREA_ID_COLUMN,
            DataBaseOpenHelper.AREA_NAME_COLUMN,
            DataBaseOpenHelper.CITY_ID_COLUMN,
            DataBaseOpenHelper.COD_FEES_COLUMN};

    public CodCoverageDataSource(Context context) {
        this.context = context;
        dbHelper = new DataBaseOpenHelper(this.context);
    }

    public CodCoverageDataItem insertCodCoverageDataItem(int areaId, String areaName, String cityId, float codFees)
            throws DataSourceException {
        try {
            ContentValues values = new ContentValues();
            values.put(DataBaseOpenHelper.AREA_ID_COLUMN, areaId);
            values.put(DataBaseOpenHelper.AREA_NAME_COLUMN, areaName);
            values.put(DataBaseOpenHelper.CITY_ID_COLUMN, cityId);
            values.put(DataBaseOpenHelper.COD_FEES_COLUMN, codFees);
            database.insert(DataBaseOpenHelper.COD_COVERAGE_TABLE, null, values);
            Cursor cursor = database.query(DataBaseOpenHelper.COD_COVERAGE_TABLE,
                    allColumns, DataBaseOpenHelper.AREA_ID_COLUMN + " = "
                            + areaId, null, null, null, null);
            if (cursor == null) {
                return null;
            }
            cursor.moveToFirst();
            CodCoverageDataItem codCoverageDataItem = cursorToCodCoverageDataItem(cursor);
            cursor.close();
            return codCoverageDataItem;
        } catch (Exception e) {
            return null;
        }
    }

    public CodCoverageDataItem insertCodCoverageDataItem(CodCoverageDataItem item)
            throws DataSourceException {
        return insertCodCoverageDataItem(item.getAreaId(), item.getAreaName(), item.getCityId(), item.getCodFees());
    }

    public CodCoverageDataItem getCodCoverageDataItem(CodCoverageDataItem item) {
        int areaId = item.getAreaId();
        Cursor cursor = database.query(DataBaseOpenHelper.COD_COVERAGE_TABLE,
                allColumns, DataBaseOpenHelper.AREA_ID_COLUMN + " = "
                        + areaId, null, null, null, null);
        cursor.moveToFirst();
        CodCoverageDataItem codCoverageDataItem = cursorToCodCoverageDataItem(cursor);
        cursor.close();
        return codCoverageDataItem;
    }

    public void updateCodCoverageDataItem(CodCoverageDataItem item) {
        ContentValues values = new ContentValues();
//        values.put(DataBaseOpenHelper.LATEST_COLUMN, item.getLatestOne());
        //      database.update(DataBaseOpenHelper.DATA_TABLE, values, DataBaseOpenHelper.ID_COLUMN + " = " + item.getId(), null);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public void deleteCodCoverageItem(CodCoverageDataItem item) {
        String cityId = item.getCityId();
        database.delete(DataBaseOpenHelper.COD_COVERAGE_TABLE,
                DataBaseOpenHelper.AREA_ID_COLUMN + " = "
                        + cityId, null);
    }

    public ArrayList<CodCoverageDataItem> getAllCodCoverageDataItems() {
        ArrayList<CodCoverageDataItem> items = new ArrayList<>();
        Cursor cursor = database.query(DataBaseOpenHelper.COD_COVERAGE_TABLE,
                allColumns, null, null, null, null, null);
        cursor.moveToFirst();
        do {
            CodCoverageDataItem item = cursorToCodCoverageDataItem(cursor);
            items.add(item);
        } while (cursor.moveToNext());
        cursor.close();
        return items;
    }

    public int getSize() {
        Cursor cursor = database.query(DataBaseOpenHelper.COD_COVERAGE_TABLE,
                allColumns, null, null, null, null, null);
        cursor.moveToFirst();
        int count = cursor.getCount();
        cursor.close();
        return count;
    }

    private CodCoverageDataItem cursorToCodCoverageDataItem(Cursor cursor) {
        CodCoverageDataItem item = new CodCoverageDataItem();
        if (cursor.getCount() == 0)
            return item;
        item.setCityId(cursor.getString(cursor
                .getColumnIndex(DataBaseOpenHelper.CITY_ID_COLUMN)));
        item.setAreaId(cursor.getInt(cursor
                .getColumnIndex(DataBaseOpenHelper.AREA_ID_COLUMN)));
        item.setAreaName(cursor.getString(cursor
                .getColumnIndex(DataBaseOpenHelper.AREA_NAME_COLUMN)));
        item.setCodFees(cursor.getFloat(cursor
                .getColumnIndex(DataBaseOpenHelper.COD_FEES_COLUMN)));
        return item;
    }

    public ArrayList<CodCoverageDataItem> getAllCodCoverageDataItemsByCityId(String cityId) {
        ArrayList<CodCoverageDataItem> items = new ArrayList<>();
        Cursor cursor = database.query(DataBaseOpenHelper.COD_COVERAGE_TABLE,
                allColumns, DataBaseOpenHelper.CITY_ID_COLUMN + " LIKE " + "'"
                        + cityId + "'", null, null, null, DataBaseOpenHelper.AREA_NAME_COLUMN + " ASC");
        cursor.moveToFirst();
        do {
            CodCoverageDataItem item = cursorToCodCoverageDataItem(cursor);
            items.add(item);
        } while (cursor.moveToNext());
        cursor.close();
        return items;
    }
}
