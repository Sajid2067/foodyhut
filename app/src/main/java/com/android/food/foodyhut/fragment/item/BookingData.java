package com.android.food.foodyhut.fragment.item;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by sajid on 11/3/2015.
 */
public class BookingData implements Parcelable {

    public static final Creator<BookingData> CREATOR = new Creator<BookingData>() {
        @Override
        public BookingData createFromParcel(Parcel in) {
            return new BookingData(in);
        }

        @Override
        public BookingData[] newArray(int size) {
            return new BookingData[size];
        }
    };

    private String searchPoint;
    private String searchPointLatLngAddress;
    private String cuisineId;
    private String cuisineName;
    private String searchType;  //0=given address, 1=given LatLng



    public BookingData(Parcel in) {
        setSearchPoint(in.readString());
        setSearchPointLatLngAddress(in.readString());
        setCuisineId(in.readString());
        setCuisineName(in.readString());
        setSearchType(in.readString());

    }

    public BookingData(){

    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeString(getSearchPoint());
        dest.writeString(getSearchPointLatLngAddress());
        dest.writeString(getCuisineId());
        dest.writeString(getCuisineName());
        dest.writeString(getSearchType());

    }


    public String getCuisineId() {
        return cuisineId;
    }

    public void setCuisineId(String cuisineId) {
        this.cuisineId = cuisineId;
    }

    public String getCuisineName() {
        return cuisineName;
    }

    public void setCuisineName(String cuisineName) {
        this.cuisineName = cuisineName;
    }

    public String getSearchPoint() {
        return searchPoint;
    }

    public void setSearchPoint(String searchPoint) {
        this.searchPoint = searchPoint;
    }

    public String getSearchPointLatLngAddress() {
        return searchPointLatLngAddress;
    }

    public void setSearchPointLatLngAddress(String searchPointLatLngAddress) {
        this.searchPointLatLngAddress = searchPointLatLngAddress;
    }

    public String getSearchType() {
        return searchType;
    }

    public void setSearchType(String searchType) {
        this.searchType = searchType;
    }
}
