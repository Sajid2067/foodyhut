package com.android.food.foodyhut.adapter.recyclerview.item;

/**
 * Created by sajid on 10/29/2015.
 */
public class MenuItem implements RecyclerViewBaseItem {

    private int iconResourceId;
    private String menuItemName;

    public MenuItem() {
        this(-1, null);
    }

    public MenuItem(int iconResourceId, String menuItemName) {
        this.iconResourceId = iconResourceId;
        this.menuItemName = menuItemName;
    }

    public int getIconResourceId() {
        return iconResourceId;
    }

    public void setIconResourceId(int iconResourceId) {
        this.iconResourceId = iconResourceId;
    }

    public String getMenuItemName() {
        return menuItemName;
    }

    public void setMenuItemName(String menuItemName) {
        this.menuItemName = menuItemName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MenuItem)) return false;

        MenuItem menuItem = (MenuItem) o;

        if (this.iconResourceId != menuItem.getIconResourceId()) return false;
        return this.menuItemName.equals(menuItem.getMenuItemName());

    }

    @Override
    public int hashCode() {
        int result = this.iconResourceId;
        result = 31 * result + this.menuItemName.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "MenuItem{" +
                "iconResourceId=" + iconResourceId +
                ", menuItemName='" + menuItemName + '\'' +
                '}';
    }
}
