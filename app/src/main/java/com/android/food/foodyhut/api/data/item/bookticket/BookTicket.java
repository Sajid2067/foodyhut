package com.android.food.foodyhut.api.data.item.bookticket;

import com.google.gson.annotations.SerializedName;


/**
 * Created by sajid on 11/3/2015.
 */
public class BookTicket {

    @SerializedName("data")
    private BookTicketData bookTicketData;
    @SerializedName("error")
    private BookTicketError bookTicketError;

    public BookTicket() {
    }

    public BookTicket(BookTicketData bookTicketData, BookTicketError bookTicketError) {
        this.bookTicketData = bookTicketData;
        this.bookTicketError = bookTicketError;
    }

    public BookTicketData getBookTicketData() {
        return bookTicketData;
    }

    public void setBookTicketData(BookTicketData bookTicketData) {
        this.bookTicketData = bookTicketData;
    }

    public BookTicketError getBookTicketError() {
        return bookTicketError;
    }

    public void setBookTicketError(BookTicketError bookTicketError) {
        this.bookTicketError = bookTicketError;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BookTicket)) return false;

        BookTicket that = (BookTicket) o;

        if (!getBookTicketData().equals(that.getBookTicketData())) return false;
        return getBookTicketError().equals(that.getBookTicketError());

    }

    @Override
    public int hashCode() {
        int result = getBookTicketData().hashCode();
        result = 31 * result + getBookTicketError().hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "BookTicket{" +
                "bookTicketData=" + bookTicketData +
                ", bookTicketError=" + bookTicketError +
                '}';
    }
}
