package com.android.food.foodyhut.adapter.spinner;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.food.foodyhut.R;
import com.android.food.foodyhut.database.data.item.CibDataItem;
import com.android.food.foodyhut.database.data.source.CibDataSource;
import com.android.food.foodyhut.util.Constant;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Droid on 29-Mar-16.
 */
public class CardNameAdapter extends BaseAdapter {
    private Activity activity;
    private LayoutInflater inflater;
    private List<CibDataItem> bankNames;
    ArrayList<CibDataItem> cibDataItemArrayList;
    CibDataSource cibDataSource;
    public CardNameAdapter(Activity activity, List<CibDataItem> bankNames) {
        this.activity = activity;
        this.bankNames = bankNames;
        cibDataSource = new CibDataSource(activity);
        cibDataSource.open();
         cibDataItemArrayList = cibDataSource.getAllCibDataItem(Constant.CONST_CIB_TYPE_CARDS);
    }


//    public CardNameAdapter(Activity activity, ArrayList<CibDataItem> cibDataItemArrayList) {
//        this.activity = activity;
//        this.cibDataItemArrayList = cibDataItemArrayList;
//        cibDataSource = new CibDataSource(activity);
//        cibDataSource.open();
//         cibDataItemArrayList = cibDataSource.getAllCibDataItem(Constant.CONST_CIB_TYPE_CARDS);
//    }

    @Override
    public int getCount() {
        return cibDataItemArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return cibDataItemArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.spinner_item_row, null);


//        Log.e("SPINNER ADAPTER>>", "cibDataItemArrayList: " + cibDataItemArrayList.toString());

//        CibDataItem items = bankNames.get(position);

        TextView tvSpinner = (TextView) convertView.findViewById(R.id.tvSpinner);

        tvSpinner.setText(cibDataItemArrayList.get(position).getCibName());
        cibDataSource.close();

        return convertView;
    }
}
