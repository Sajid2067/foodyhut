package com.android.food.foodyhut.adapter.tabspager;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.android.food.foodyhut.api.data.item.user.UserStatus;
import com.android.food.foodyhut.fragment.GoogleSearchResultFragmentFH;
import com.android.food.foodyhut.fragment.item.BookingData;
import com.android.food.foodyhut.util.API;


public class SearchResultTabsPagerAdapterFH extends FragmentPagerAdapter {

    FragmentManager fm;
    BookingData bookingData;
    public static String PARCEL_KEY = "bookingData";
    public SearchResultTabsPagerAdapterFH(FragmentManager fm, BookingData bookingData) {
        super(fm);
        this.fm=fm;
        this.bookingData=bookingData;
    }

    @Override
    public Fragment getItem(int position) {
        GoogleSearchResultFragmentFH searchResultFragment=new GoogleSearchResultFragmentFH();
        Bundle bundle = new Bundle();
        bundle.putParcelable(PARCEL_KEY, bookingData);

        switch (position){

            case 0:
              //  bundle.putString("api", API.TRIP_DETAILS_HISTORY_BASE_API_URL+"/"+userId+"/"+API.TRIP_PAST_API_TAG);

                searchResultFragment.setArguments(bundle);
                return searchResultFragment;

            case 1:
              //  bundle.putString("api", API.TRIP_DETAILS_HISTORY_BASE_API_URL+"/"+userId+"/"+API.TRIP_UPCOMING_API_TAG);
                searchResultFragment.setArguments(bundle);
                return searchResultFragment;

            case 2:
             //   bundle.putString("api", API.TRIP_DETAILS_HISTORY_BASE_API_URL+"/"+userId+"/"+API.TRIP_CANCELLED_API_TAG);
                searchResultFragment.setArguments(bundle);
                return searchResultFragment;


        }
        return searchResultFragment;
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {

        switch (position){
            case 0:
                return "Google";
            case 1:
                return "Facebook";
            case 2:
                return "Foodyhut";

        }
        return "Google";
    }
}