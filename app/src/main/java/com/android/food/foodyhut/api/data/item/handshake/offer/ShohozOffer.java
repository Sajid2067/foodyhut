package com.android.food.foodyhut.api.data.item.handshake.offer;

import com.google.gson.annotations.SerializedName;

/**
 * Created by sajid on 11/7/2015.
 */
public class ShohozOffer {

    @SerializedName("title")
    private String title;
    @SerializedName("body")
    private String body;
    @SerializedName("image_url")
    private String imageUrl;
    @SerializedName("valid_till")
    private String validTill;

    public ShohozOffer() {
    }

    public ShohozOffer(String title, String body, String imageUrl, String validTill) {

        this.title = title;
        this.body = body;
        this.imageUrl = imageUrl;
        this.validTill = validTill;
    }

    public String getTitle() {

        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getValidTill() {
        return validTill;
    }

    public void setValidTill(String validTill) {
        this.validTill = validTill;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ShohozOffer)) return false;

        ShohozOffer that = (ShohozOffer) o;


        return this.getTitle().equals(that.getTitle())
                && this.getBody().equals(that.getBody())
                && this.getImageUrl().equals(that.getImageUrl())
                && this.getValidTill().equals(that.getValidTill());

    }

    @Override
    public int hashCode() {
        int result = getTitle().hashCode();
        result = 31 * result + getBody().hashCode();
        result = 31 * result + getImageUrl().hashCode();
        result = 31 * result + getValidTill().hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "ShohozOffer{" +
                "title='" + title + '\'' +
                ", body='" + body + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                ", validTill='" + validTill + '\'' +
                '}';
    }
}
