package com.android.food.foodyhut.fragment.item;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Droid on 03-Apr-16.
 */
public class SSLPaymentData implements Parcelable {
    private PaymentDetailsData paymentDetailsData;
    private float mSSLFee;


    public SSLPaymentData() {
    }

    public SSLPaymentData(PaymentDetailsData paymentDetailsData, float mSSLFee) {

        this.paymentDetailsData = paymentDetailsData;
        this.mSSLFee = mSSLFee;
    }

    protected SSLPaymentData(Parcel in) {
        paymentDetailsData = in.readParcelable(PaymentDetailsData.class.getClassLoader());
        mSSLFee = in.readFloat();
    }

    public static final Creator<SSLPaymentData> CREATOR = new Creator<SSLPaymentData>() {
        @Override
        public SSLPaymentData createFromParcel(Parcel in) {
            return new SSLPaymentData(in);
        }

        @Override
        public SSLPaymentData[] newArray(int size) {
            return new SSLPaymentData[size];
        }
    };

    public PaymentDetailsData getPaymentDetailsData() {

        return paymentDetailsData;
    }

    public void setPaymentDetailsData(PaymentDetailsData paymentDetailsData) {
        this.paymentDetailsData = paymentDetailsData;
    }

    public float getmSSLFee() {
        return mSSLFee;
    }

    public void setmSSLFee(float mSSLFee) {
        this.mSSLFee = mSSLFee;
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) return true;
        if (!(o instanceof SSLPaymentData)) return false;

        SSLPaymentData that = (SSLPaymentData) o;

        if (Float.compare(that.mSSLFee, mSSLFee) != 0) return false;
        return paymentDetailsData.equals(that.paymentDetailsData);

    }

    @Override
    public int hashCode() {
        int result = paymentDetailsData.hashCode();
        result = 31 * result + (mSSLFee != +0.0f ? Float.floatToIntBits(mSSLFee) : 0);
        return result;
    }

    @Override
    public String toString() {
        return "SSLPaymentData{" +
                "paymentDetailsData=" + paymentDetailsData +
                ", mSSLFee=" + mSSLFee +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(paymentDetailsData, flags);
        dest.writeFloat(mSSLFee);
    }
}
