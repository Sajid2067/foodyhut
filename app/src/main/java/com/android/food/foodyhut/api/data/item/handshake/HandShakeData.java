package com.android.food.foodyhut.api.data.item.handshake;

import com.google.gson.annotations.SerializedName;
import com.android.food.foodyhut.api.data.item.handshake.cib.CardNames;
import com.android.food.foodyhut.api.data.item.handshake.offer.ShohozOffer;
import com.android.food.foodyhut.api.data.item.handshake.paymentmethods.PaymentMethods;

import java.util.List;

/**
 * Created by sajid on 11/7/2015.
 */
public class HandShakeData {
    @SerializedName("bkash_fee")
    private float bkashFee;
    @SerializedName("shohoz_fee")
    private int shohozFee;
    @SerializedName("offers")
    private List<ShohozOffer> shohozOfferList;
    @SerializedName("card_names")
    private CardNames cardNames;
    @SerializedName("payment_methods")
    private PaymentMethods paymentMethods;
    @SerializedName("cities")
    private List<City> cities;
    @SerializedName("cod_coverages")
    private List<CodCoverage> codCoverages;
    @SerializedName("hash")
    private String hash;
    @SerializedName("app_specific_discount")
    private boolean app_specific_discount;

    public HandShakeData() {
    }

    public HandShakeData(float bkashFee, int shohozFee, List<ShohozOffer> shohozOfferList, PaymentMethods paymentMethods, CardNames cardNames, List<City> cities, List<CodCoverage> codCoverages, String hash) {
        this.bkashFee = bkashFee;
        this.shohozFee = shohozFee;
        this.shohozOfferList = shohozOfferList;
        this.paymentMethods = paymentMethods;
        this.cardNames = cardNames;
        this.cities = cities;
        this.codCoverages = codCoverages;
        this.hash = hash;
    }

    public float getBkashFee() {
        return bkashFee;
    }

    public void setBkashFee(float bkashFee) {
        this.bkashFee = bkashFee;
    }

    public List<ShohozOffer> getShohozOfferList() {
        return shohozOfferList;
    }

    public void setShohozOfferList(List<ShohozOffer> shohozOfferList) {
        this.shohozOfferList = shohozOfferList;
    }

    public CardNames getCardNames() {
        return cardNames;
    }

    public void setCardNames(CardNames cardNames) {
        this.cardNames = cardNames;
    }

    public List<City> getCities() {
        return cities;
    }

    public void setCities(List<City> cities) {
        this.cities = cities;
    }

    public List<CodCoverage> getCodCoverages() {
        return codCoverages;
    }

    public void setCodCoverages(List<CodCoverage> codCoverages) {
        this.codCoverages = codCoverages;
    }

    public boolean isApp_specific_discount() {
        return app_specific_discount;
    }

    public void setApp_specific_discount(boolean app_specific_discount) {
        this.app_specific_discount = app_specific_discount;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public int getShohozFee() {
        return shohozFee;
    }

    public void setShohozFee(int shohozFee) {
        this.shohozFee = shohozFee;
    }

    public PaymentMethods getPaymentMethods() {
        return paymentMethods;
    }

    public void setPaymentMethods(PaymentMethods paymentMethods) {
        this.paymentMethods = paymentMethods;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof HandShakeData)) return false;

        HandShakeData that = (HandShakeData) o;

        if (Float.compare(that.getBkashFee(), getBkashFee()) != 0) return false;
        if (getShohozFee() != that.getShohozFee()) return false;
        if (!getShohozOfferList().equals(that.getShohozOfferList())) return false;
        if (!getPaymentMethods().equals(that.getPaymentMethods())) return false;
        if (!getCardNames().equals(that.getCardNames())) return false;
        if (!getCities().equals(that.getCities())) return false;
        if (!getCodCoverages().equals(that.getCodCoverages())) return false;
        return getHash().equals(that.getHash());

    }

    @Override
    public int hashCode() {
        int result = (getBkashFee() != +0.0f ? Float.floatToIntBits(getBkashFee()) : 0);
        result = 31 * result + getShohozFee();
        result = 31 * result + getShohozOfferList().hashCode();
        result = 31 * result + getPaymentMethods().hashCode();
        result = 31 * result + getCardNames().hashCode();
        result = 31 * result + getCities().hashCode();
        result = 31 * result + getCodCoverages().hashCode();
        result = 31 * result + getHash().hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "HandShakeData{" +
                "bkashFee=" + bkashFee +
                ", shohozFee=" + shohozFee +
                ", shohozOfferList=" + shohozOfferList +
                ", paymentMethods=" + paymentMethods +
                ", cardNames=" + cardNames +
                ", cities=" + cities +
                ", app_specific_discount=" + app_specific_discount +
                ", codCoverages=" + codCoverages +
                ", hash='" + hash + '\'' +
                '}';
    }
}
