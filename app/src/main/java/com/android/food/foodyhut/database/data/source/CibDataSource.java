package com.android.food.foodyhut.database.data.source;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.android.food.foodyhut.database.DataBaseOpenHelper;
import com.android.food.foodyhut.database.data.item.CibDataItem;
import com.android.food.foodyhut.database.exception.DataSourceException;

import java.util.ArrayList;

/**
 * Created by tuman on 4/5/2015.
 */
public class CibDataSource {

    protected Context context;
    protected SQLiteDatabase database;
    protected DataBaseOpenHelper dbHelper;
    private String[] allColumns = {
            DataBaseOpenHelper.CIB_SHORT_CODE_COLUMN,
            DataBaseOpenHelper.CIB_NAME_COLUMN,
            DataBaseOpenHelper.CIB_PERCENTAGE_COLUMN,
            DataBaseOpenHelper.CIB_TYPE_COLUMN};

    public CibDataSource(Context context) {
        this.context = context;
        dbHelper = new DataBaseOpenHelper(this.context);
    }

    public CibDataItem insertCibDataItem(String cibShortCode, String cibName, float cibPercentage, String cibType)
            throws DataSourceException {
        try {
            ContentValues values = new ContentValues();
            values.put(DataBaseOpenHelper.CIB_SHORT_CODE_COLUMN, cibShortCode);
            values.put(DataBaseOpenHelper.CIB_NAME_COLUMN, cibName);
            values.put(DataBaseOpenHelper.CIB_PERCENTAGE_COLUMN, cibPercentage);
            values.put(DataBaseOpenHelper.CIB_TYPE_COLUMN, cibType);
            database.insert(DataBaseOpenHelper.CIB_TABLE, null, values);
            Cursor cursor = database.query(DataBaseOpenHelper.CIB_TABLE,
                    allColumns, DataBaseOpenHelper.CIB_SHORT_CODE_COLUMN + " LIKE "
                            + "'" + cibShortCode + "'", null, null, null, null);
            if (cursor == null) {
                return null;
            }
            cursor.moveToFirst();
            CibDataItem cibDataItem = cursorToCibDataItem(cursor);
            cursor.close();
            return cibDataItem;
        } catch (Exception e) {
            return null;
        }
    }

    public CibDataItem insertCibDataItem(CibDataItem item)
            throws DataSourceException {
        return insertCibDataItem(item.getCibShortCode(), item.getCibName(), item.getCibPercentage(), item.getCibType());
    }

    public CibDataItem getCibDataItem(String cibName) {
        Cursor cursor = database.query(DataBaseOpenHelper.CIB_TABLE,
                allColumns, DataBaseOpenHelper.CIB_NAME_COLUMN + " LIKE "
                        + "'" + cibName + "'", null, null, null, null);
        cursor.moveToFirst();
        CibDataItem cibDataItem = cursorToCibDataItem(cursor);
        cursor.close();
        return cibDataItem;
    }

    public void updateCibDataItem(CibDataItem item) {
        ContentValues values = new ContentValues();
//        values.put(DataBaseOpenHelper.LATEST_COLUMN, item.getLatestOne());
        //      database.update(DataBaseOpenHelper.DATA_TABLE, values, DataBaseOpenHelper.ID_COLUMN + " = " + item.getId(), null);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public void deleteCibDataItem(CibDataItem item) {
        String cibName = item.getCibName();
        database.delete(DataBaseOpenHelper.CIB_TABLE,
                DataBaseOpenHelper.CIB_NAME_COLUMN + " LIKE "
                        + "'" + cibName + "'", null);
    }

    public ArrayList<CibDataItem> getAllCibDataItem() {
        ArrayList<CibDataItem> items = new ArrayList<>();
        Cursor cursor = database.query(DataBaseOpenHelper.CIB_TABLE,
                allColumns, null, null, null, null, null);
        cursor.moveToFirst();
        do {
            CibDataItem item = cursorToCibDataItem(cursor);
            items.add(item);
        } while (cursor.moveToNext());
        cursor.close();
        return items;
    }

    public ArrayList<CibDataItem> getAllCibDataItem(String cibType) {
        ArrayList<CibDataItem> items = new ArrayList<>();
        CibDataItem cibDataItem=new CibDataItem("default", "Select Gateway",0,"0");
       items.add(cibDataItem);

        Cursor cursor = database.query(DataBaseOpenHelper.CIB_TABLE,
                allColumns,
                DataBaseOpenHelper.CIB_TYPE_COLUMN + " LIKE "
                        + "'" + cibType + "'", null, null, null, null);
        cursor.moveToFirst();
        do {
            CibDataItem item = cursorToCibDataItem(cursor);
            items.add(item);
        } while (cursor.moveToNext());
        cursor.close();
        return items;
    }

    public ArrayList<CibDataItem> getAllCibDataItem(CibDataItem cibDataItem) {
        ArrayList<CibDataItem> items = new ArrayList<>();
        Cursor cursor = database.query(DataBaseOpenHelper.CIB_TABLE,
                allColumns,
                DataBaseOpenHelper.CIB_NAME_COLUMN + " LIKE "
                        + "'" + cibDataItem.getCibName() + "'", null, null, null, null);
        cursor.moveToFirst();
        do {
            CibDataItem item = cursorToCibDataItem(cursor);
            items.add(item);
        } while (cursor.moveToNext());
        cursor.close();
        return items;
    }

    public int getSize() {
        Cursor cursor = database.query(DataBaseOpenHelper.CIB_TABLE,
                allColumns, null, null, null, null, null);
        cursor.moveToFirst();
        cursor.close();
        return cursor.getCount();
    }

    private CibDataItem cursorToCibDataItem(Cursor cursor) {
        CibDataItem item = new CibDataItem();
        if (cursor.getCount() == 0)
            return item;
        item.setCibShortCode(cursor.getString(cursor
                .getColumnIndex(DataBaseOpenHelper.CIB_SHORT_CODE_COLUMN)));
        item.setCibName(cursor.getString(cursor
                .getColumnIndex(DataBaseOpenHelper.CIB_NAME_COLUMN)));
        item.setCibPercentage(cursor.getFloat(cursor
                .getColumnIndex(DataBaseOpenHelper.CIB_PERCENTAGE_COLUMN)));
        item.setCibType(cursor.getString(cursor
                .getColumnIndex(DataBaseOpenHelper.CIB_TYPE_COLUMN)));
        return item;
    }
}
