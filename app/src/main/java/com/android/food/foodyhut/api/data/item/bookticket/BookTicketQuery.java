package com.android.food.foodyhut.api.data.item.bookticket;

import com.google.gson.annotations.SerializedName;

/**
 * Created by sajid on 11/3/2015.
 */
public class BookTicketQuery {

    @SerializedName("from_city")
    private String fromCity;
    @SerializedName("to_city")
    private String toCity;
    @SerializedName("date_of_journey")
    private String dateOfJourney;

    public BookTicketQuery() {
    }

    public BookTicketQuery(String fromCity, String toCity, String dateOfJourney) {
        this.fromCity = fromCity;
        this.toCity = toCity;
        this.dateOfJourney = dateOfJourney;
    }

    public String getFromCity() {
        return fromCity;
    }

    public void setFromCity(String fromCity) {
        this.fromCity = fromCity;
    }

    public String getToCity() {
        return toCity;
    }

    public void setToCity(String toCity) {
        this.toCity = toCity;
    }

    public String getDateOfJourney() {
        return dateOfJourney;
    }

    public void setDateOfJourney(String dateOfJourney) {
        this.dateOfJourney = dateOfJourney;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BookTicketQuery)) return false;

        BookTicketQuery that = (BookTicketQuery) o;

        if (!getFromCity().equals(that.getFromCity())) return false;
        if (!getToCity().equals(that.getToCity())) return false;
        return getDateOfJourney().equals(that.getDateOfJourney());

    }

    @Override
    public int hashCode() {
        int result = getFromCity().hashCode();
        result = 31 * result + getToCity().hashCode();
        result = 31 * result + getDateOfJourney().hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "BookTicketQuery{" +
                "fromCity='" + fromCity + '\'' +
                ", toCity='" + toCity + '\'' +
                ", dateOfJourney='" + dateOfJourney + '\'' +
                '}';
    }
}
