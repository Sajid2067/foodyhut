package com.android.food.foodyhut.fragment;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.food.foodyhut.adapter.autocomplete.AutoCompleteAdapter;
import com.android.food.foodyhut.api.data.item.model.PlaceAutoComplete;
import com.android.food.foodyhut.api.data.item.model.PlacePredictions;
import com.android.food.foodyhut.fragment.item.BookingData;
import com.android.food.foodyhut.util.VolleyJSONRequest;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.android.food.foodyhut.R;
import com.android.food.foodyhut.adapter.gridview.CityAdapter;
import com.android.food.foodyhut.application.AppController;
import com.android.food.foodyhut.database.data.item.CityDataItem;
import com.android.food.foodyhut.activity.listener.OnFragmentInteractionListener;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import static android.support.design.widget.Snackbar.make;

public class TopSearchFragment extends BaseFragment implements AdapterView.OnItemClickListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener  {

    private static final String TAG = TopSearchFragment.class.getSimpleName();

    //view level objects
    private GridView topSearchDistrictGridView;
    private EditText topSearchEditText;
    Tracker mTracker;
    //data level objects
    private List<CityDataItem> cityDataItemList;
    private List<CityDataItem> topSearchCityDataItemList;

    private CityAdapter cityAdapter;
    private TextView noResultTextView;
    private BookingData bookingData;
    double latitude;
    double longitude;
    private ListView mAutoCompleteList;
    private EditText addressEditText;
    private String GETPLACESHIT = "places_hit";
    private PlacePredictions predictions;
    private Location mLastLocation;
    private AutoCompleteAdapter mAutoCompleteAdapter;
    PlaceAutoComplete placeAutoComplete;
    private int CUSTOM_AUTOCOMPLETE_REQUEST_CODE = 20;
    private static final int MY_PERMISSIONS_REQUEST_LOC = 30;
    //   private ImageView searchBtn;
    private FragmentManager fragmentManager;
    private String preFilledText;
    private Handler handler;
    private VolleyJSONRequest request;
    private GoogleApiClient mGoogleApiClient;
    LatLng loadingUnloadingPointLatLng;

    public static TopSearchFragment newInstance(ArrayList<Integer> previousFragmentIds, Parcelable parcelable) {
        TopSearchFragment topSearchFragment = new TopSearchFragment();
        Bundle bundle = new Bundle();
        bundle.putIntegerArrayList(PREVIOUS_FRAGMENT_ID_TAG, previousFragmentIds);
        bundle.putParcelable(PARCELABLE_FRAGMENT_ITEM_ID_TAG, parcelable);
        topSearchFragment.setArguments(bundle);
        return topSearchFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (parcelable instanceof BookingData) {
            bookingData = (BookingData) parcelable;
        }
        // Obtain the shared Tracker instance.
        AppController application = (AppController) getActivity().getApplication();
        mTracker = application.getDefaultTracker();
        mTracker.setScreenName("Top Search");
        // Send a screen view.
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());


    }


    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.content_main, container, false);
        }

        addressEditText = (EditText) findViewById(R.id.top_search_edit_text);
        mAutoCompleteList = (ListView) findViewById(R.id.searchResultLV);

         placeAutoComplete =new PlaceAutoComplete();
        placeAutoComplete.setPlaceDesc("Set Location From Map");
        placeAutoComplete.setPlaceID("100");

        ArrayList<PlaceAutoComplete> placeAutoCompleteList = new ArrayList<>() ;
        placeAutoCompleteList.add(placeAutoComplete);

        predictions= new PlacePredictions();
        predictions.setPlaces(placeAutoCompleteList);

        if (mAutoCompleteAdapter == null) {
            mAutoCompleteAdapter = new AutoCompleteAdapter(getActivity(),placeAutoCompleteList,getActivity());
          //  mAutoCompleteAdapter.add(placeAutoComplete);
            mAutoCompleteList.setAdapter(mAutoCompleteAdapter);
        }
        //  searchBtn=(ImageView) findViewById(R.id.search);
        addressEditText.requestFocus();
        InputMethodManager imgr = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imgr.showSoftInput(addressEditText, 0);
        imgr.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);


        //get permission for Android M
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            fetchLocation();
        } else {

            // Here, thisActivity is the current activity
            if (ContextCompat.checkSelfPermission(getActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOC);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            } else {
                fetchLocation();
            }
        }


        //Add a text change listener to implement autocomplete functionality
        addressEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                // optimised way is to start searching for laction after user has typed minimum 3 chars
                if (addressEditText.getText().length() > 3) {


                    //     searchBtn.setVisibility(View.GONE);

                    Runnable run = new Runnable() {


                        @Override
                        public void run() {

                            // cancel all the previous requests in the queue to optimise your network calls during autocomplete search
                            AppController.volleyQueueInstance.cancelRequestInQueue(GETPLACESHIT);

                            //build Get url of Place Autocomplete and hit the url to fetch result.
                            request = new VolleyJSONRequest(Request.Method.GET, getPlaceAutoCompleteUrl(addressEditText.getText().toString()), null, null,listenerResponse,listenerError);

                            //Give a tag to your request so that you can use this tag to cancle request later.
                            request.setTag(GETPLACESHIT);

                            AppController.volleyQueueInstance.addToRequestQueue(request);

                        }

                    };

                    // only canceling the network calls will not help, you need to remove all callbacks as well
                    // otherwise the pending callbacks and messages will again invoke the handler and will send the request
                    if (handler != null) {
                        handler.removeCallbacksAndMessages(null);
                    } else {
                        handler = new Handler();
                    }
                    handler.postDelayed(run, 1000);

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }

        });

      //  addressEditText.setText(preFilledText);
      //  addressEditText.setSelection(addressEditText.getText().length());

        mAutoCompleteList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // pass the result to the calling activity



                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(rootView.getWindowToken(), 0);

                if (bookingData != null) {
                    Log.e(TAG, bookingData.getSearchPoint());


                    bookingData.setSearchPoint(predictions.getPlaces().get(position).getPlaceDesc());
                    bookingData.setSearchPointLatLngAddress(predictions.getPlaces().get(position).getPlaceID());


                }

                if(position==mAutoCompleteAdapter.getCount()-1){
                    bookingData.setSearchType("1");
                    onFragmentChangeCallListener.fragmentChange(OnFragmentInteractionListener.MAP_FRAGMENT_ID, bookingData, previousFragmentIds);
                }else {
                   // new PlaceAMarker().execute(predictions.getPlaces().get(position).getPlaceDesc(),position+"");
//                    if (bookingData != null) {
//                        Log.e(TAG, bookingData.getSearchPoint());
//
//                        bookingData.setSearchPoint(predictions.getPlaces().get(Integer.parseInt(position)).getPlaceDesc());
//                        bookingData.setSearchPointLatLngAddress( loadingUnloadingPointLatLng.latitude+","+ loadingUnloadingPointLatLng.longitude);
//                    }
                    bookingData.setSearchType("0");
                    previousFragmentIds.remove(previousFragmentIds.size() - 1);
                    onFragmentChangeCallListener.fragmentChange(OnFragmentInteractionListener.LOCATION_FRAGMENT_FH, bookingData, previousFragmentIds);

                }


            }
        });



        return super.onCreateView(inflater, container, savedInstanceState);
    }


    class PlaceAMarker extends AsyncTask<String, String, String> {
        String startAddress;
        String position;
        @Override
        protected String doInBackground(String... params) {
            startAddress = params[0];
            position=params[1];
            //  startAddress = startAddress.replaceAll(" ", "%20");

            getLatLng(startAddress);

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            if (bookingData != null) {
                Log.e(TAG, bookingData.getSearchPoint());

                    bookingData.setSearchPoint(predictions.getPlaces().get(Integer.parseInt(position)).getPlaceDesc());
                    bookingData.setSearchPointLatLngAddress( loadingUnloadingPointLatLng.latitude+","+ loadingUnloadingPointLatLng.longitude);
            }

            previousFragmentIds.remove(previousFragmentIds.size() - 1);
            onFragmentChangeCallListener.fragmentChange(OnFragmentInteractionListener.LOCATION_FRAGMENT_FH, bookingData, previousFragmentIds);

        }
    }

    protected void getLatLng(String address) {
        Geocoder coder = new Geocoder(getActivity());
        try {
            ArrayList<android.location.Address> adresses = (ArrayList<android.location.Address>) coder.getFromLocationName(address, 1);
            for(android.location.Address add : adresses){
                //  if (statement) {//Controls to ensure it is right address such as country etc.
                double longitude = add.getLongitude();
                double latitude = add.getLatitude();
                loadingUnloadingPointLatLng =new LatLng(latitude,longitude);
                Log.d("TripDetailsInfoFragment","longitude"+longitude);
                //  }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    void initializeEditTextComponents() {
        //  topSearchEditText = findViewById(R.id.top_search_edit_text);
        //  topSearchEditText.addTextChangedListener(this);
    }

    @Override
    void initializeButtonComponents() {


    }

    @Override
    void initializeTextViewComponents() {
        //  noResultTextView = findViewById(R.id.no_result_text_view);
    }

    @Override
    void initializeOtherViewComponents() {
//        topSearchDistrictGridView = findViewById(R.id.top_search_district_grid_view);
//        cityAdapter = new CityAdapter(getActivity(), cityDataItemList, topSearchCityDataItemList, noResultTextView);
//        topSearchDistrictGridView.setAdapter(cityAdapter);
//        topSearchDistrictGridView.setOnItemClickListener(this);
    }

    @Override
    void initializeOnclickListener() {
    }

    @Override
    void removeOnclickListener() {
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
//            case R.id.back_button:
//                onBackPressed();
//                return;
        }
        super.onClick(view);
    }

    @Override
    public void onBackPressed() {
        if (bookingData != null) {
            Log.e(TAG, bookingData.getSearchPoint());

            if (bookingData.getSearchPoint().contains(":")) {
                if (bookingData.getSearchPoint().split(":").length == 2) {
                    bookingData.setSearchPoint(bookingData.getSearchPoint().split(":")[1]);
                } else {
                    bookingData.setSearchPoint("");
                }
            }
        }
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(rootView.getWindowToken(), 0);
        previousFragmentIds.remove(previousFragmentIds.size() - 1);
        onFragmentChangeCallListener.fragmentChange(OnFragmentInteractionListener.LOCATION_FRAGMENT_FH, bookingData, previousFragmentIds);

    }

    private Response.Listener<String> listenerResponse = new Response.Listener<String>() {

        @Override
        public void onResponse(String response) {
            // searchBtn.setVisibility(View.VISIBLE);
            Log.d("PLACES RESULT:", response);
            Gson gson = new Gson();
            predictions = gson.fromJson(response, PlacePredictions.class);

//            PlaceAutoComplete placeAutoComplete =new PlaceAutoComplete();
//            placeAutoComplete.setPlaceDesc("Set Location From Map");
//            placeAutoComplete.setPlaceID("100");

//            if (mAutoCompleteAdapter == null) {
//                mAutoCompleteAdapter = new AutoCompleteAdapter(getActivity(), predictions.getPlaces(),getActivity());
//                mAutoCompleteAdapter.add(placeAutoComplete);
//                mAutoCompleteList.setAdapter(mAutoCompleteAdapter);
//            } else {
//                mAutoCompleteAdapter.clear();
//                mAutoCompleteAdapter.addAll(predictions.getPlaces());
//                mAutoCompleteAdapter.add(placeAutoComplete);
//                mAutoCompleteAdapter.notifyDataSetChanged();
//                mAutoCompleteList.invalidate();
//            }


                mAutoCompleteAdapter.clear();
                mAutoCompleteAdapter.addAll(predictions.getPlaces());
                mAutoCompleteAdapter.add(placeAutoComplete);
                mAutoCompleteAdapter.notifyDataSetChanged();
                mAutoCompleteList.invalidate();

        }
    };

    private Response.ErrorListener listenerError = new Response.ErrorListener() {

        @Override
        public void onErrorResponse(VolleyError error) {
            //  searchBtn.setVisibility(View.VISIBLE);
            Log.d(TAG,"onErrorResponse :"+ error.getStackTrace()+"");
            make(addressEditText, "No internet connection", Snackbar.LENGTH_SHORT).show();

        }
    };

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//        CityDataItem cityDataItem = (CityDataItem) cityAdapter.getItem(position);
//        if (bookingData != null) {
//            Log.e(TAG, bookingData.getSearchPoint());
//            Log.e(TAG, bookingData.getunloadingPoint());
//            if (bookingData.getSearchPoint().contains(":")) {
//                bookingData.setSearchPoint(cityDataItem.getCityName());
//            } else if (bookingData.getunloadingPoint().contains(":")) {
//                bookingData.setunloadingPoint(cityDataItem.getCityName());
//            }
//        }
//        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
//        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
//        previousFragmentIds.remove(previousFragmentIds.size() - 1);
//        onFragmentChangeCallListener.fragmentChange(OnFragmentInteractionListener.TRIP_DETAILS_INFO_FRAGMENT_ID, bookingData, previousFragmentIds);
    }

    /*
      * Create a get url to fetch results from google place autocomplete api.
      * Append the input received from autocomplete edittext
      * Append your current location
      * Append radius you want to search results within
      * Choose a language you want to fetch data in
      * Append your google API Browser key
   */
    public String getPlaceAutoCompleteUrl(String input) {
        StringBuilder urlString = new StringBuilder();
        urlString.append("https://maps.googleapis.com/maps/api/place/autocomplete/json");
        urlString.append("?input=");
        try {
            urlString.append(URLEncoder.encode(input, "utf8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        urlString.append("&location=");
        urlString.append(latitude + "," + longitude); // append lat long of current location to show nearby results.
        urlString.append("&radius=500&language=en&components=country:BD");
        urlString.append("&key=" + "AIzaSyB2JCXptYRlKtBXqOa8LG6VAYylPUuNOLI");

        Log.d("FINAL URL:::   ", urlString.toString());
        return urlString.toString();
    }



    @Override
    public void onConnected(Bundle bundle) {
        try {
            mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                    mGoogleApiClient);

            if (mLastLocation != null) {
                latitude = mLastLocation.getLatitude();
                longitude = mLastLocation.getLongitude();
            }

        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    public void fetchLocation(){
        //Build google API client to use fused location
        buildGoogleApiClient();

        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOC: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission granted!
                    fetchLocation();

                } else {
                    // permission denied!

                    Toast.makeText(getActivity(), "Please grant permission for using this app!", Toast.LENGTH_LONG).show();
                }
                return;
            }


        }
    }
}

