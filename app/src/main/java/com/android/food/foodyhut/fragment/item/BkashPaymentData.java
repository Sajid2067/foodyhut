package com.android.food.foodyhut.fragment.item;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by sajid on 12/14/2015.
 */
public class BkashPaymentData implements Parcelable {
    private PaymentDetailsData paymentDetailsData;
    private float bKashFee;


    public BkashPaymentData() {
    }

    public BkashPaymentData(PaymentDetailsData paymentDetailsData, float bKashFee) {

        this.paymentDetailsData = paymentDetailsData;
        this.bKashFee = bKashFee;
    }

    protected BkashPaymentData(Parcel in) {
        paymentDetailsData = in.readParcelable(PaymentDetailsData.class.getClassLoader());
        bKashFee = in.readFloat();
    }

    public static final Creator<BkashPaymentData> CREATOR = new Creator<BkashPaymentData>() {
        @Override
        public BkashPaymentData createFromParcel(Parcel in) {
            return new BkashPaymentData(in);
        }

        @Override
        public BkashPaymentData[] newArray(int size) {
            return new BkashPaymentData[size];
        }
    };

    public PaymentDetailsData getPaymentDetailsData() {

        return paymentDetailsData;
    }

    public void setPaymentDetailsData(PaymentDetailsData paymentDetailsData) {
        this.paymentDetailsData = paymentDetailsData;
    }

    public float getbKashFee() {
        return bKashFee;
    }

    public void setbKashFee(float bKashFee) {
        this.bKashFee = bKashFee;
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) return true;
        if (!(o instanceof BkashPaymentData)) return false;

        BkashPaymentData that = (BkashPaymentData) o;

        if (Float.compare(that.bKashFee, bKashFee) != 0) return false;
        return paymentDetailsData.equals(that.paymentDetailsData);

    }

    @Override
    public int hashCode() {
        int result = paymentDetailsData.hashCode();
        result = 31 * result + (bKashFee != +0.0f ? Float.floatToIntBits(bKashFee) : 0);
        return result;
    }

    @Override
    public String toString() {
        return "BkashPaymentData{" +
                "paymentDetailsData=" + paymentDetailsData +
                ", bKashFee=" + bKashFee +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(paymentDetailsData, flags);
        dest.writeFloat(bKashFee);
    }
}
