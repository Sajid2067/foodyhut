package com.android.food.foodyhut.fragment;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.text.Html;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.android.food.foodyhut.R;
import com.android.food.foodyhut.application.AppController;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

/**
 * Created by sajid on 10/29/2015.
 */
public class AppTermsFragment extends BaseFragment {

    private static final String TAG = AppTermsFragment.class.getSimpleName();

    private TextView generalTermsTextView;
    private TextView communicationPolicyTextView;
    Tracker mTracker;
    public static AppTermsFragment newInstance(ArrayList<Integer> previousFragmentIds, Parcelable parcelable) {
        Log.d(TAG, "AppTermsFragment newInstance(@FragmentId int previousFragmentId)");
        AppTermsFragment appTermsFragment = new AppTermsFragment();
        Bundle bundle = new Bundle();
        bundle.putIntegerArrayList(PREVIOUS_FRAGMENT_ID_TAG, previousFragmentIds);
        bundle.putParcelable(PARCELABLE_FRAGMENT_ITEM_ID_TAG, parcelable);
        appTermsFragment.setArguments(bundle);
        return appTermsFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Obtain the shared Tracker instance.
        AppController application = (AppController) getActivity().getApplication();
        mTracker = application.getDefaultTracker();
        mTracker.setScreenName("Terms");
        // Send a screen view.
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)");

        if (rootView == null) {
            rootView = inflater.inflate(R.layout.fragment_terms_and_conditions, container, false);
        }



        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    void initializeEditTextComponents() {
        Log.d(TAG, "initializeEditTextComponents()");

    }

    @Override
    void initializeButtonComponents() {
        Log.d(TAG, "initializeButtonComponents()");
    }

    @Override
    void initializeTextViewComponents() {
        Log.d(TAG, "initializeTextViewComponents()");
        {
            generalTermsTextView = (TextView) rootView.findViewById(R.id.general_terms_text_view);
            generalTermsTextView.setText(getParsedHTML("general_terms.html"));
            generalTermsTextView.setMovementMethod(LinkMovementMethod.getInstance());
        }

        {
            communicationPolicyTextView = (TextView) rootView.findViewById(R.id.communication_policy_text_view);
            communicationPolicyTextView.setText(getParsedHTML("communication_policy.html"));
            communicationPolicyTextView.setMovementMethod(LinkMovementMethod.getInstance());
        }
    }

    @Override
    void initializeOtherViewComponents() {
        Log.d(TAG, "initializeOtherViewComponents()");
    }

    @Override
    void initializeOnclickListener() {
        Log.d(TAG, "initializeOnclickListener()");
    }

    @Override
    void removeOnclickListener() {
        Log.d(TAG, "removeOnclickListener()");
    }

    public Spanned getParsedHTML(String fileName) {
        Log.d(TAG, "getParsedHTML(String fileName)");
        InputStream inputStream;
        String htmlText = "";
        try {
            inputStream = getActivity().getAssets().open(fileName);
            int fileSize = inputStream.available();
            byte[] fileByteData = new byte[fileSize];
            inputStream.read(fileByteData);
            inputStream.close();
            htmlText = new String(fileByteData, "UTF-8");
        } catch (IOException e) {
            Log.e(TAG, Log.getStackTraceString(e));
        }
        return Html.fromHtml(htmlText);
    }
}
