package com.android.food.foodyhut.api.data.item.handshake.paymentmethods;

import com.google.gson.annotations.SerializedName;

/**
 * Created by sajid on 11/7/2015.
 */
public class PaymentMethodData {

    @SerializedName("hours")
    private int hours;
    @SerializedName("is_active")
    private boolean isActive;

    public PaymentMethodData() {
    }

    public PaymentMethodData(int hours, boolean isActive) {

        this.hours = hours;
        this.isActive = isActive;
    }

    public int getHours() {
        return hours;
    }

    public void setHours(int hours) {
        this.hours = hours;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PaymentMethodData)) return false;

        PaymentMethodData that = (PaymentMethodData) o;

        if (getHours() != that.getHours()) return false;
        return isActive() == that.isActive();

    }

    @Override
    public int hashCode() {
        int result = getHours();
        result = 31 * result + (isActive() ? 1 : 0);
        return result;
    }

    @Override
    public String toString() {
        return "PaymentMethodData{" +
                "hours=" + hours +
                ", isActive=" + isActive +
                '}';
    }
}
