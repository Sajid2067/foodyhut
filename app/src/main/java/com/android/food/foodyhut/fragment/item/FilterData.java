package com.android.food.foodyhut.fragment.item;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by sajid on 12/7/2015.
 */
public class FilterData implements Parcelable {

    private int busType=-1;
    private String operator;
    private String boardingPoint;
    private String time;
    private String bookTicketAsJson;
    private BookTicketData bookTicketData;

    public FilterData() {
    }

    public FilterData(int busType, String operator, String boardingPoint, String time, String bookTicketAsJson, BookTicketData bookTicketData) {

        this.busType = busType;
        this.operator = operator;
        this.boardingPoint = boardingPoint;
        this.time = time;
        this.bookTicketAsJson = bookTicketAsJson;
        this.bookTicketData = bookTicketData;
    }

    protected FilterData(Parcel in) {
        busType = in.readInt();
        operator = in.readString();
        boardingPoint = in.readString();
        time = in.readString();
        bookTicketAsJson = in.readString();
        bookTicketData = in.readParcelable(BookTicketData.class.getClassLoader());
    }

    public static final Creator<FilterData> CREATOR = new Creator<FilterData>() {
        @Override
        public FilterData createFromParcel(Parcel in) {
            return new FilterData(in);
        }

        @Override
        public FilterData[] newArray(int size) {
            return new FilterData[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    public int getBusType() {
        return busType;
    }

    public void setBusType(int busType) {
        this.busType = busType;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getBoardingPoint() {
        return boardingPoint;
    }

    public void setBoardingPoint(String boardingPoint) {
        this.boardingPoint = boardingPoint;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getBookTicketAsJson() {
        return bookTicketAsJson;
    }

    public void setBookTicketAsJson(String bookTicketAsJson) {
        this.bookTicketAsJson = bookTicketAsJson;
    }

    public BookTicketData getBookTicketData() {
        return bookTicketData;
    }

    public void setBookTicketData(BookTicketData bookTicketData) {
        this.bookTicketData = bookTicketData;
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) return true;
        if (!(o instanceof FilterData)) return false;

        FilterData that = (FilterData) o;

        if (busType != that.busType) return false;
        if (!operator.equals(that.operator)) return false;
        if (!boardingPoint.equals(that.boardingPoint)) return false;
        if (!time.equals(that.time)) return false;
        if (!bookTicketAsJson.equals(that.bookTicketAsJson)) return false;
        return bookTicketData.equals(that.bookTicketData);

    }

    @Override
    public int hashCode() {
        int result = busType;
        result = 31 * result + operator.hashCode();
        result = 31 * result + boardingPoint.hashCode();
        result = 31 * result + time.hashCode();
        result = 31 * result + bookTicketAsJson.hashCode();
        result = 31 * result + bookTicketData.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "FilterData{" +
                "busType=" + busType +
                ", operator='" + operator + '\'' +
                ", boardingPoint='" + boardingPoint + '\'' +
                ", time='" + time + '\'' +
                ", bookTicketAsJson='" + bookTicketAsJson + '\'' +
                ", bookTicketData=" + bookTicketData +
                '}';
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(busType);
        dest.writeString(operator);
        dest.writeString(boardingPoint);
        dest.writeString(time);
        dest.writeString(bookTicketAsJson);
        dest.writeParcelable(bookTicketData, flags);
    }
}
