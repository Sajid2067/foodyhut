package com.android.food.foodyhut.fragment;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import com.android.food.foodyhut.activity.LoginActivity;
import com.android.food.foodyhut.adapter.recyclerview.SearchDetailsListAdapterFH;
import com.android.food.foodyhut.api.data.item.cuisineType.CuisineTypeResponse;
import com.android.food.foodyhut.api.data.item.login.LoginVerification;
import com.android.food.foodyhut.api.data.item.user.UserStatus;
import com.android.food.foodyhut.toolbox.PostUrlBuilder;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.android.food.foodyhut.activity.listener.OnFragmentInteractionListener;
import com.android.food.foodyhut.adapter.recyclerview.RecentSearchListAdapter;
import com.android.food.foodyhut.api.data.item.cuisineType.CuisineTypeData;
import com.android.food.foodyhut.application.AppController;
import com.android.food.foodyhut.fragment.item.BookingData;
import com.android.food.foodyhut.toolbox.GetUrlBuilder;
import com.android.food.foodyhut.toolbox.ObjectRequest;
import com.android.food.foodyhut.util.API;
import com.android.food.foodyhut.util.AppManager;
import com.android.food.foodyhut.view.widget.ProgressBar;
import com.android.food.foodyhut.R;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.nipunbirla.boxloader.BoxLoaderView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.support.design.widget.Snackbar.make;
import static android.widget.Toast.LENGTH_SHORT;
import static android.widget.Toast.makeText;

/**
 * Created by sajid on 10/28/2015.
 */
public class CuisineTypeFragment extends BaseFragment implements Response.Listener<CuisineTypeData>, Response.ErrorListener, RecyclerView.OnItemTouchListener {

    private static final String TAG = CuisineTypeFragment.class.getSimpleName();
    private RecyclerView recentSearchRecyclerView;
    private BoxLoaderView recentSearchProgressBar;
    private ObjectRequest<CuisineTypeData> cuisineTypeObjectRequest;
    private RecentSearchListAdapter recentSearchListAdapter;
    private List<CuisineTypeResponse> cuisineTypes;
    private LinearLayoutManager linearLayoutManager;
    private GestureDetector mGestureDetector;
    private AppManager appManager;
    private AppController appController = AppController.getInstance();
    Tracker mTracker;
    private BookingData bookingData;

    public static CuisineTypeFragment newInstance(ArrayList<Integer> previousFragmentIds, Parcelable parcelable) {
        Log.d(TAG, "newInstance(@FragmentId int previousFragmentId, Parcelable parcelable)");
        Log.i(TAG, "previousFragmentIds - " + previousFragmentIds.toString());
        if (parcelable != null) {
            Log.i(TAG, "parcelable - " + parcelable.toString());
        }

        CuisineTypeFragment recentSearchFragment = new CuisineTypeFragment();
        Bundle bundle = new Bundle();
        bundle.putIntegerArrayList(PREVIOUS_FRAGMENT_ID_TAG, previousFragmentIds);
        bundle.putParcelable(PARCELABLE_FRAGMENT_ITEM_ID_TAG, parcelable);
        recentSearchFragment.setArguments(bundle);
        return recentSearchFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        appManager = new AppManager(getActivity());
      //  cuisineTypes = new List<>();
     //   recentSearchListAdapter = new RecentSearchListAdapter(getContext(), cuisineTypes);

        if(bookingData==null){
            bookingData=new BookingData();
        }

        if (parcelable instanceof BookingData) {

            bookingData = (BookingData) parcelable;
        }

        // Obtain the shared Tracker instance.
        AppController application = (AppController) getActivity().getApplication();
        mTracker = application.getDefaultTracker();
        mTracker.setScreenName("Terms");
        // Send a screen view.
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.fragment_cuisine_type, container, false);
        }
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (recentSearchRecyclerView.getVisibility() == View.GONE) {
            recentSearchRecyclerView.setVisibility(View.VISIBLE);
        }
        recentSearchProgressBar.setVisibility(View.VISIBLE);

        deviceRegistration();


//
//        String url = API.CUISINE_TYPE_API_URL;
//        HashMap<String, String> params = new HashMap<>();
//        params.put(API.Parameter.ANDROID_APP_VERSION, appManager.getAppVersion());
//        params.put(API.Parameter.ANDROID_DEVICE_ID, appManager.getDeviceId());
//        GetUrlBuilder getUrlBuilder = new GetUrlBuilder(url, params);
//        cuisineTypeObjectRequest = new ObjectRequest<>(API.Method.CUISINE_TYPE_API_METHOD,
//                getUrlBuilder.getQueryUrl(), this, this, CuisineTypeData.class);
//        String Url =getUrlBuilder.getQueryUrl();  //only for showing URL and params
//        Log.d(TAG, "URL:" + Url);
//        appController.addToRequestQueue(cuisineTypeObjectRequest);
    }

    private void deviceRegistration() {
      //  String url = API.CUISINE_TYPE_API_URL;
        HashMap<String, String> params = new HashMap<>();
        params.put(API.Parameter.ANDROID_APP_VERSION, appManager.getAppVersion());
        params.put(API.Parameter.ANDROID_DEVICE_ID, appManager.getDeviceId());


      //  PostUrlBuilder postUrlBuilder = new PostUrlBuilder(API.DEVICE_REGISTRATION_API_URL, null, null);
        GetUrlBuilder getUrlBuilder = new GetUrlBuilder( API.CUISINE_TYPE_API_URL, params);
        String url =getUrlBuilder.getQueryUrl();  //only for showing URL and params
        Log.d(TAG, "url:" + url);
        Log.d(TAG, "PARAMS:" + params.toString());

        cuisineTypeObjectRequest = new ObjectRequest<>(API.Method.CUISINE_TYPE_API_METHOD, url, null, new Response.Listener<CuisineTypeData>() {
            @Override
            public void onResponse(CuisineTypeData recentSearch) {

                recentSearchProgressBar.setVisibility(View.GONE);
                if(recentSearch.getResponse()!=null){
                    // Log.d(TAG, googleSearchDetailList.getResponse().getAddress());
                    recentSearchRecyclerView.setVisibility(View.VISIBLE);

                    cuisineTypes =recentSearch.getResponse();
                    recentSearchListAdapter = new RecentSearchListAdapter(getContext(), cuisineTypes);

                    recentSearchRecyclerView.setAdapter(recentSearchListAdapter);
                    recentSearchListAdapter.setItems(cuisineTypes);
                    recentSearchListAdapter.notifyDataSetChanged();


//			totalTripListTextView.setText(googleSearchDetailListResponsesList.size()+"");
                } else {
                    make(recentSearchRecyclerView, "Found some problems", Snackbar.LENGTH_SHORT).show();
                }




            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                recentSearchProgressBar.setVisibility(View.GONE);
                connectionErrorView.setVisibility(View.VISIBLE);
              //  make(shohozLoginForm, message, Snackbar.LENGTH_SHORT).show();
            }
        },CuisineTypeData.class);

//        progressBarDialog.show();
        appController.addToRequestQueue(cuisineTypeObjectRequest);

    }

    @Override
    void initializeEditTextComponents() {

    }

    @Override
    void initializeButtonComponents() {

    }

    @Override
    void initializeTextViewComponents() {

    }

    @Override
    void initializeOtherViewComponents() {
        recentSearchProgressBar = (BoxLoaderView) rootView.findViewById(R.id.progress);
        recentSearchRecyclerView = (RecyclerView) rootView.findViewById(R.id.recent_search_recycler_view);
        recentSearchRecyclerView.setAdapter(recentSearchListAdapter);
        linearLayoutManager = new LinearLayoutManager(getContext());
        recentSearchRecyclerView.setLayoutManager(linearLayoutManager);
        recentSearchRecyclerView.setHasFixedSize(true);
        recentSearchRecyclerView.addOnItemTouchListener(this);
        mGestureDetector = new GestureDetector(getActivity(), new GestureDetector.SimpleOnGestureListener() {

            @Override
            public boolean onSingleTapUp(MotionEvent e) {
                return true;
            }
        });
    }

    @Override
    void initializeOnclickListener() {

    }

    @Override
    void removeOnclickListener() {

    }

    @Override
    public void onErrorResponse(VolleyError error) {
        recentSearchProgressBar.setVisibility(View.GONE);
        connectionErrorView.setVisibility(View.VISIBLE);
    }

    @Override
    public void onResponse(CuisineTypeData recentSearch) {


        this.cuisineTypes =recentSearch.getResponse();
        recentSearchListAdapter = new RecentSearchListAdapter(getContext(), cuisineTypes);
        recentSearchListAdapter.setItems(this.cuisineTypes);

        recentSearchListAdapter.notifyDataSetChanged();
        if (recentSearchRecyclerView.getVisibility() == View.GONE) {
            recentSearchRecyclerView.setVisibility(View.VISIBLE);
        }
        recentSearchProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        switch (view.getId()) {
            case R.id.retry_button:
                connectionErrorView.setVisibility(View.GONE);
                recentSearchProgressBar.setVisibility(View.VISIBLE);
                appController.addToRequestQueue(cuisineTypeObjectRequest);
                break;
        }
    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView recyclerView, MotionEvent motionEvent) {
        View child = recyclerView.findChildViewUnder(motionEvent.getX(), motionEvent.getY());
        mGestureDetector.setIsLongpressEnabled(true);

        if (child != null && mGestureDetector.onTouchEvent(motionEvent)) {
            int position = recyclerView.getChildAdapterPosition(child);
            CuisineTypeResponse cuisineType = cuisineTypes.get(position);
           // BookingData otherBookTicketData = new BookingData();
            bookingData.setCuisineId(cuisineType.getCuisineId());
            bookingData.setCuisineName(cuisineType.getCuisineName());


            previousFragmentIds.add(OnFragmentInteractionListener.CUISINE_TYPE_FRAGMENT_ID);
            onFragmentChangeCallListener.fragmentChange(OnFragmentInteractionListener.SEARCH_RESULT_CONTAINER_FRAGMENT_ID_FH, bookingData, previousFragmentIds);
            return true;

        }
        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {

    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

    }
}
