package com.android.food.foodyhut.adapter.recyclerview;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.android.food.foodyhut.api.data.item.googlesearchdetailshistory.GoogleSearchDetailListResponse;
import com.android.food.foodyhut.util.AppManager;
import com.android.food.foodyhut.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by sajid on 10/28/2015.
 */
public class SearchDetailsListAdapterFH extends RecyclerView.Adapter<SearchDetailsListAdapterFH.ViewHolder> implements Filterable {

    private Activity activity;
    private List<GoogleSearchDetailListResponse> tripLists;
    private List<GoogleSearchDetailListResponse> backupTripLists;
    private LayoutInflater layoutInflater;
    private AppManager appManager;
    private TextView noResultTextView;


    public SearchDetailsListAdapterFH(Activity activity, List<GoogleSearchDetailListResponse> recyclerViewBaseItems, TextView noResultTextView) {
        this.activity = activity;
        backupTripLists = recyclerViewBaseItems;
        this.tripLists = new ArrayList<>(this.backupTripLists);
        this.noResultTextView = noResultTextView;
        layoutInflater = (LayoutInflater) this.activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        appManager = new AppManager(this.activity);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView;
        rootView = layoutInflater.inflate(R.layout.list_item_google_search, parent, false);
        return new ViewHolder(rootView);
    }

    public void setItems(final List<GoogleSearchDetailListResponse> recyclerViewBaseItems) {
        backupTripLists = recyclerViewBaseItems;
        this.tripLists = new ArrayList<>(this.backupTripLists);
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (recyclerViewBaseItems.size() == 0) {
                    noResultTextView.setVisibility(View.VISIBLE);
                } else {
                    noResultTextView.setVisibility(View.GONE);
                }
            }
        });
    }
    public GoogleSearchDetailListResponse getItem(int position) {
        return tripLists.get(position);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        GoogleSearchDetailListResponse tripList = tripLists.get(position);
        //holder.img_rest
        holder.resNameTV.setText(tripList.getName());
        holder.openNowTV.setTextColor(tripList.getOpeningHours().getOpen_now().compareTo("false")==0? Color.RED: Color.GREEN);
        holder.openNowTV.setText(tripList.getOpeningHours().getOpen_now().compareTo("false")==0? "Closed Now": "Open Now");
        holder.ratingRB.setRating(Float.parseFloat(tripList.getRating()));



        if (getItemCount() - 1 == position) {
            holder.listDivider.setVisibility(View.GONE);
        } else {
            holder.listDivider.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public int getItemCount() {
        return tripLists != null ? tripLists.size() : 0;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();
                List<GoogleSearchDetailListResponse> tripLists = getTripListsSorted(constraint);
                results.count = tripLists.size();
                results.values = tripLists;
                Log.e("VALUES", results.values.toString());
                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                Log.e("VALUES", results.values.toString());
                tripLists = (List<GoogleSearchDetailListResponse>) results.values;
                notifyDataSetChanged();
            }
        };
    }

    @NonNull
    private List<GoogleSearchDetailListResponse> getTripListsSorted(CharSequence constraint) {
        List<GoogleSearchDetailListResponse> tripLists = new ArrayList<>(SearchDetailsListAdapterFH.this.backupTripLists);
        Log.e("TAG", tripLists.size() + "");
        if (constraint.toString().equals("operator:1")) {
            Log.e("VALUES", "operator:1");
            Collections.sort(tripLists, new Comparator<GoogleSearchDetailListResponse>() {
                @Override
                public int compare(GoogleSearchDetailListResponse lhs, GoogleSearchDetailListResponse rhs) {
                    return lhs.getRating().compareTo(rhs.getRating());
                }
            });
        } else if (constraint.toString().equals("operator:2")) {
            Collections.sort(tripLists, new Comparator<GoogleSearchDetailListResponse>() {
                @Override
                public int compare(GoogleSearchDetailListResponse rhs, GoogleSearchDetailListResponse lhs) {
                    return rhs.getRating().compareTo(lhs.getRating());
                }
            });
        } else if (constraint.toString().equals("departure:1")) {
            Collections.sort(tripLists, new Comparator<GoogleSearchDetailListResponse>() {
                @Override
                public int compare(GoogleSearchDetailListResponse lhs, GoogleSearchDetailListResponse rhs) {
                    return rhs.getName().compareTo(lhs.getName());
                }
            });
        } else if (constraint.toString().equals("departure:2")) {
            Collections.sort(tripLists, new Comparator<GoogleSearchDetailListResponse>() {
                @Override
                public int compare(GoogleSearchDetailListResponse rhs, GoogleSearchDetailListResponse lhs) {
                    return rhs.getName().compareTo(lhs.getName());
                }
            });
        } else if (constraint.toString().equals("fare:1")) {
            Collections.sort(tripLists, new Comparator<GoogleSearchDetailListResponse>() {
                @Override
                public int compare(GoogleSearchDetailListResponse lhs, GoogleSearchDetailListResponse rhs) {
                    return sortFare(lhs, rhs);
                }
            });
        } else if (constraint.toString().equals("fare:2")) {
            Collections.sort(tripLists, new Comparator<GoogleSearchDetailListResponse>() {
                @Override
                public int compare(GoogleSearchDetailListResponse lhs, GoogleSearchDetailListResponse rhs) {
                    return sortFare(rhs, lhs);
                }
            });
        }
        return tripLists;
    }

    private int sortFare(GoogleSearchDetailListResponse lhs, GoogleSearchDetailListResponse rhs) {
        if (Integer.parseInt(lhs.getOpeningHours().getOpen_now()) < Integer.parseInt(rhs.getOpeningHours().getOpen_now()) ) {
            return -1;
        } else if (Integer.parseInt(lhs.getOpeningHours().getOpen_now())  > Integer.parseInt(rhs.getOpeningHours().getOpen_now()) ) {
            return 1;
        } else {
            return 0;
        }
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView img_rest;
        private TextView reviewsTV;
        private TextView resNameTV;
        private TextView openNowTV;
        private TextView willVisitTV;
        private TextView menuTV;
        private RatingBar ratingRB;
        private View listDivider;

        public ViewHolder(View rootView) {
            super(rootView);
            img_rest = (ImageView) rootView.findViewById(R.id.img_rest);
            reviewsTV = (TextView) rootView.findViewById(R.id.reviewsTV);
            resNameTV = (TextView) rootView.findViewById(R.id.resNameTV);
            openNowTV = (TextView) rootView.findViewById(R.id.openNowTV);
            willVisitTV = (TextView) rootView.findViewById(R.id.willVisitTV);
            menuTV = (TextView) rootView.findViewById(R.id.menuTV);
            ratingRB = (RatingBar) rootView.findViewById(R.id.ratingRB);
            listDivider = (View) itemView.findViewById(R.id.list_divider);
        }
    }
}
