package com.android.food.foodyhut.adapter.recyclerview;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.RatingBar;
import android.widget.TextView;

import com.android.food.foodyhut.R;
import com.android.food.foodyhut.api.data.item.googlesearchdetailshistory.GoogleSearchDetailList;
import com.android.food.foodyhut.util.AppManager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by sajid on 10/28/2015.
 */
public class TimelineListAdapterFH extends RecyclerView.Adapter<TimelineListAdapterFH.ViewHolder> implements Filterable {

    private Activity activity;
    private List<GoogleSearchDetailList> tripLists;
    private List<GoogleSearchDetailList> backupTripLists;
    private LayoutInflater layoutInflater;
    private AppManager appManager;
    private TextView noResultTextView;


    public TimelineListAdapterFH(Activity activity, List<GoogleSearchDetailList> recyclerViewBaseItems, TextView noResultTextView) {
        this.activity = activity;
        backupTripLists = recyclerViewBaseItems;
        this.tripLists = new ArrayList<>(this.backupTripLists);
        this.noResultTextView = noResultTextView;
        layoutInflater = (LayoutInflater) this.activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        appManager = new AppManager(this.activity);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView;
        rootView = layoutInflater.inflate(R.layout.list_item_timeline, parent, false);
        return new ViewHolder(rootView);
    }

    public void setItems(final List<GoogleSearchDetailList> recyclerViewBaseItems) {
        backupTripLists = recyclerViewBaseItems;
        this.tripLists = new ArrayList<>(this.backupTripLists);
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (recyclerViewBaseItems.size() == 0) {
                    noResultTextView.setVisibility(View.VISIBLE);
                } else {
                    noResultTextView.setVisibility(View.GONE);
                }
            }
        });
    }
    public GoogleSearchDetailList getItem(int position) {
        return tripLists.get(position);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        GoogleSearchDetailList tripList = tripLists.get(position);
      //  holder.pnrTextView.setText("PNR: "+tripList.getPnr());
     //   String[] dateTime =tripList.getJourney_date().split(" ");
     //   String time=appManager.getTime(dateTime[1]);
     //   String [] date = dateTime[0].split("-");
      //  Calendar calendar= new GregorianCalendar(Integer.parseInt(date[0]),Integer.parseInt(date[1])-1,Integer.parseInt(date[2]));
    //    CalendarData calendarData=new CalendarData(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
    //    String htmlDate = appManager.getHTMLFormatDate(calendarData, calendar);
   //     htmlDate= new StringBuffer(htmlDate).reverse().toString();
    //    htmlDate=htmlDate.replaceFirst(" "," , ");
    //    htmlDate= new StringBuffer(htmlDate).reverse().toString();
      //  holder.tripTimeTextView.setText(htmlDate+" at "+time);
     //   holder.busTypeTextView.setText(tripList.getOrigin_city() +" - "+tripList.getDestination_city());
     //   holder.busOperatorNameTextView.setText(tripList.getCompany_name());
     //   String ticketFare = String.format(activity.getResources().getString(R.string.ticket_fare),Float.parseFloat( tripList.getOrder_value()));
     //   holder.newTicketPriceTextView.setText(ticketFare);

//        if (getItemCount() - 1 == position) {
//            holder.listDivider.setVisibility(View.GONE);
//        } else {
//            holder.listDivider.setVisibility(View.VISIBLE);
//        }

    }

    @Override
    public int getItemCount() {
        return tripLists != null ? tripLists.size() : 0;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();
                List<GoogleSearchDetailList> tripLists = getTripListsSorted(constraint);
                results.count = tripLists.size();
                results.values = tripLists;
                Log.e("VALUES", results.values.toString());
                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                Log.e("VALUES", results.values.toString());
                tripLists = (List<GoogleSearchDetailList>) results.values;
                notifyDataSetChanged();
            }
        };
    }

    @NonNull
    private List<GoogleSearchDetailList> getTripListsSorted(CharSequence constraint) {
        List<GoogleSearchDetailList> tripLists = new ArrayList<>(TimelineListAdapterFH.this.backupTripLists);
        Log.e("TAG", tripLists.size() + "");
//        if (constraint.toString().equals("operator:1")) {
//            Log.e("VALUES", "operator:1");
//            Collections.sort(tripLists, new Comparator<GoogleSearchDetailList>() {
//                @Override
//                public int compare(GoogleSearchDetailList lhs, GoogleSearchDetailList rhs) {
//                    return lhs.getCompany_name().compareTo(rhs.getCompany_name());
//                }
//            });
//        } else if (constraint.toString().equals("operator:2")) {
//            Collections.sort(tripLists, new Comparator<GoogleSearchDetailList>() {
//                @Override
//                public int compare(GoogleSearchDetailList rhs, GoogleSearchDetailList lhs) {
//                    return rhs.getCompany_name().compareTo(lhs.getCompany_name());
//                }
//            });
//        } else if (constraint.toString().equals("departure:1")) {
//            Collections.sort(tripLists, new Comparator<GoogleSearchDetailList>() {
//                @Override
//                public int compare(GoogleSearchDetailList lhs, GoogleSearchDetailList rhs) {
//                    return rhs.getJourney_date().compareTo(lhs.getJourney_date());
//                }
//            });
//        } else if (constraint.toString().equals("departure:2")) {
//            Collections.sort(tripLists, new Comparator<GoogleSearchDetailList>() {
//                @Override
//                public int compare(GoogleSearchDetailList rhs, GoogleSearchDetailList lhs) {
//                    return rhs.getJourney_date().compareTo(lhs.getJourney_date());
//                }
//            });
//        } else if (constraint.toString().equals("fare:1")) {
//            Collections.sort(tripLists, new Comparator<GoogleSearchDetailList>() {
//                @Override
//                public int compare(GoogleSearchDetailList lhs, GoogleSearchDetailList rhs) {
//                    return sortFare(lhs, rhs);
//                }
//            });
//        } else if (constraint.toString().equals("fare:2")) {
//            Collections.sort(tripLists, new Comparator<GoogleSearchDetailList>() {
//                @Override
//                public int compare(GoogleSearchDetailList lhs, GoogleSearchDetailList rhs) {
//                    return sortFare(rhs, lhs);
//                }
//            });
//        }
        return tripLists;
    }

//    private int sortFare(GoogleSearchDetailList lhs, GoogleSearchDetailList rhs) {
//        if (Integer.parseInt(lhs.getOrder_value()) < Integer.parseInt(rhs.getOrder_value()) ) {
//            return -1;
//        } else if (Integer.parseInt(lhs.getOrder_value())  > Integer.parseInt(rhs.getOrder_value()) ) {
//            return 1;
//        } else {
//            return 0;
//        }
//    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private TextView reviewsTV;
        private TextView resNameTV;
        private TextView openNowTV;
        private TextView willVisitTV;
        private TextView menuTV;
        private RatingBar ratingRB;
      //  private View listDivider;

        public ViewHolder(View rootView) {
            super(rootView);
            reviewsTV = (TextView) rootView.findViewById(R.id.reviewsTV);
            resNameTV = (TextView) rootView.findViewById(R.id.resNameTV);
            openNowTV = (TextView) rootView.findViewById(R.id.openNowTV);
            willVisitTV = (TextView) rootView.findViewById(R.id.willVisitTV);
            menuTV = (TextView) rootView.findViewById(R.id.menuTV);
            ratingRB = (RatingBar) rootView.findViewById(R.id.ratingRB);
         //   listDivider = (View) itemView.findViewById(R.id.list_divider);
        }
    }
}
