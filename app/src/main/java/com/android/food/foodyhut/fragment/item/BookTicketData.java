package com.android.food.foodyhut.fragment.item;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by sajid on 11/3/2015.
 */
public class BookTicketData implements Parcelable {

    public static final Creator<BookTicketData> CREATOR = new Creator<BookTicketData>() {
        @Override
        public BookTicketData createFromParcel(Parcel in) {
            return new BookTicketData(in);
        }

        @Override
        public BookTicketData[] newArray(int size) {
            return new BookTicketData[size];
        }
    };
    private CalendarData calendarData;
    private String fromCity;
    private String toCity;

    public BookTicketData() {
        calendarData = new CalendarData();
    }

    public BookTicketData(Parcel in) {
        setCalendarData((CalendarData) in.readParcelable(CalendarData.class.getClassLoader()));
        setFromCity(in.readString());
        setToCity(in.readString());
    }

    public BookTicketData(CalendarData calendarData, String fromCity, String toCity) {
        this.calendarData = calendarData;
        this.fromCity = fromCity;
        this.toCity = toCity;
    }

    public CalendarData getCalendarData() {
        return calendarData;
    }

    public void setCalendarData(CalendarData calendarData) {
        this.calendarData = calendarData;
    }

    public String getFromCity() {
        return fromCity;
    }

    public void setFromCity(String fromCity) {
        this.fromCity = fromCity;
    }

    public String getToCity() {
        return toCity;
    }

    public void setToCity(String toCity) {
        this.toCity = toCity;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(calendarData, flags);
        dest.writeString(getFromCity());
        dest.writeString(getToCity());
    }

    @Override
    public String toString() {
        return "BookTicketData{" +
                "calendarData=" + calendarData +
                ", fromCity='" + fromCity + '\'' +
                ", toCity='" + toCity + '\'' +
                '}';
    }
}
