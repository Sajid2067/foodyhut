package com.android.food.foodyhut.adapter.listview;

import android.app.Activity;
import android.content.Context;
import android.os.Parcelable;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.android.food.foodyhut.R;
import com.android.food.foodyhut.fragment.item.SeatNumberData;
import com.android.food.foodyhut.util.Constant;

import java.util.HashMap;
import java.util.List;

/**
 * Created by sajid on 12/8/2015.
 */
public class PassengerInfoAdapter extends BaseAdapter {

    private static final String TAG = PassengerInfoAdapter.class.getSimpleName();
    private List<Parcelable> selectedSeats;
    private Activity activity;
    private LayoutInflater layoutInflater;

    private HashMap<String, String> backupFullNames;
//    private HashMap<String, String> backupAges;
    private HashMap<String, String > backupPassport;
    private HashMap<String, String> backupGenders;
    private HashMap<String, String> fullNames;
//    private HashMap<String, String> ages;
private HashMap<String, String> passport;
    private HashMap<String, String> genders;
    private boolean fromNextFragment = false;
    private UpdateContactPerson updateContactPerson;

    public PassengerInfoAdapter(List<Parcelable> selectedSeats, Activity activity, UpdateContactPerson updateContactPerson) {
        this.selectedSeats = selectedSeats;
        this.activity = activity;
        layoutInflater = (LayoutInflater) this.activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        fullNames = new HashMap<>();
        passport = new HashMap<>();
//        ages = new HashMap<>();
        genders = new HashMap<>();
        this.updateContactPerson = updateContactPerson;
    }

    @Override
    public int getCount() {
        return selectedSeats.size();
    }

    @Override
    public Object getItem(int position) {
        return selectedSeats.get(position);
    }

    @Override
    public long getItemId(int position) {
        return ((SeatNumberData) (selectedSeats.get(position))).getTicketId();
    }


    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        final ViewHolder viewHolder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.list_item_passenger_info, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.addSeatTextView = (TextView) convertView.findViewById(R.id.add_seat_text_view);
            viewHolder.fullNameEditText = (EditText) convertView.findViewById(R.id.full_name_edit_text);
            viewHolder.genderRadioGroup = (RadioGroup) convertView.findViewById(R.id.gender_radio_group);
            viewHolder.maleRadioButton = (RadioButton) convertView.findViewById(R.id.male_radio_button);
            viewHolder.femaleRadioButton = (RadioButton) convertView.findViewById(R.id.female_radio_button);
            viewHolder.passportEditText = (EditText)convertView.findViewById(R.id.passport_edit_text);

            if (Constant.isKolkataTrip==false){
                viewHolder.passportEditText.setVisibility(View.GONE);
            }
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        SeatNumberData seatNumberData = (SeatNumberData) selectedSeats.get(position);
        viewHolder.addSeatTextView.setText(seatNumberData.getSeatNumber());
        if (backupGenders != null && backupFullNames != null) {
            try {
                Log.d(TAG, position + " " + backupFullNames.get("fullNames:" + position));
//                Log.d(TAG, position + " " + backupAges.get("ages:" + position));
                Log.d(TAG, position + " " + backupPassport.get("passport:" + position));
                Log.d(TAG, position + " " + backupGenders.get("genders:" + position));

//                viewHolder.ageEditText.setText(backupAges.get("ages:" + position));
//                ages.put("ages:" + position, backupAges.get("ages:" + position));

//                if (Constant.isKolkataTrip){
                    viewHolder.passportEditText.setText(backupPassport.get("passport:" + position));
                    passport.put("passport:"+position, backupPassport.get("passport:" + position));
//                }



                viewHolder.fullNameEditText.setText(backupFullNames.get("fullNames:" + position));
                fullNames.put("fullNames:" + position, backupFullNames.get("fullNames:" + position));
                Log.d(TAG, "getView fromNextFragment-" + fromNextFragment + " position-" + position);
                if (position == 0 && !fromNextFragment) {
                    updateContactPerson.updateName(backupFullNames.get("fullNames:" + position).trim());
                }
                if (backupGenders.get("genders:" + position).equals("1")) {
                    viewHolder.maleRadioButton.setChecked(true);
                } else {
                    viewHolder.femaleRadioButton.setChecked(true);
                }
                genders.put("genders:" + position, backupGenders.get("genders:" + position));
            } catch (Exception e) {
                Log.e(TAG, e.getMessage());
            }
        }
        viewHolder.fullNameEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                Log.d(TAG, "fromNextFragment-" + fromNextFragment + " position-" + position);
                if ((position == 0 && fromNextFragment)) {
                    fromNextFragment = false;
                }
                if (s.toString().trim().length() == 0) {
                    fullNames.remove("fullNames:" + position);
                    if (position == 0 && !fromNextFragment) {
                        updateContactPerson.updateName("");
                    }
                } else {
                    if (position == 0 && !fromNextFragment) {
                        updateContactPerson.updateName(s.toString().trim());
                    }
                    fullNames.put("fullNames:" + position, s.toString().trim());
                }

            }
        });
//        viewHolder.ageEditText.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//                if (s.toString().trim().length() == 0) {
//                    ages.remove("ages:" + position);
//                } else {
//                    ages.put("ages:" + position, s.toString().trim());
//                }
//            }
//        });

        viewHolder.passportEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                Log.d(TAG, "fromNextFragment-" + fromNextFragment + " position-" + position);
                if ((position == 0 && fromNextFragment)) {
                    fromNextFragment = false;
                }
                if (s.toString().trim().length() == 0) {
                    passport.remove("passport:" + position);
//                    if (position == 0 && !fromNextFragment) {
//                        updateContactPerson.updateName("");
//                    }
                } else {
//                    if (position == 0 && !fromNextFragment) {
//                        updateContactPerson.updateName(s.toString().trim());
//                    }
                    passport.put("passport:" + position, s.toString().trim());
                }

            }
        });

        viewHolder.genderRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                genders.put("genders:" + position, checkedId == R.id.male_radio_button ? "1" : "2");
            }
        });

        if (Constant.KEY_IS_DEVELOPMENT_BUILD) {
            viewHolder.fullNameEditText.setText("Muktadir");
//            viewHolder.ageEditText.setText("12");
//            viewHolder.passportEditText.setText("123455");
            viewHolder.maleRadioButton.setChecked(true);
        }
        return convertView;
    }

    public HashMap<String, String> getFullNames() {
        return fullNames;
    }

    public void setFullNames(HashMap<String, String> fullNames) {
        this.fullNames = fullNames;
        this.backupFullNames = new HashMap<>(fullNames);
        Log.d(TAG, fullNames.toString());
    }

    public void setFullNames(HashMap<String, String> fullNames, boolean fromNextFragment) {
        this.fromNextFragment = fromNextFragment;
        Log.d(TAG, "fromNextFragment-" + fromNextFragment);
        setFullNames(fullNames);
    }

//    public HashMap<String, String> getAges() {
//        return ages;
//
//    }
//
//    public void setAges(HashMap<String, String> ages) {
//        this.ages = ages;
//        this.backupAges = new HashMap<>(ages);
//        Log.d(TAG, ages.toString());
//
//    }


    public HashMap<String, String> getPassport() {
        return passport;
    }

    public void setPassport(HashMap<String, String> passport) {
        this.passport = passport;
        this.backupPassport = new HashMap<>(passport);
        Log.d(TAG, passport.toString());
    }

    public HashMap<String, String> getGenders() {
        return genders;
    }

    public void setGenders(HashMap<String, String> genders) {
        this.genders = genders;
        this.backupGenders = new HashMap<>(genders);
        Log.d(TAG, genders.toString());
    }

    public interface UpdateContactPerson {
        void updateName(String name);
    }

    private static class ViewHolder {
        private TextView addSeatTextView;
        private EditText fullNameEditText;
//        private EditText ageEditText;
        private RadioGroup genderRadioGroup;
        private RadioButton maleRadioButton;
        private RadioButton femaleRadioButton;
        private EditText passportEditText;

    }
}
