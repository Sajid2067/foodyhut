package com.android.food.foodyhut.fragment;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.android.food.foodyhut.R;
import com.android.food.foodyhut.activity.annotation.FragmentId;
import com.android.food.foodyhut.adapter.listview.BoardingPointFilterAdapter;
import com.android.food.foodyhut.adapter.listview.OperatorFilterAdapter;
import com.android.food.foodyhut.api.data.item.bookticket.BookTicket;
import com.android.food.foodyhut.api.data.item.filter.Filter;
import com.android.food.foodyhut.application.AppController;
import com.android.food.foodyhut.fragment.item.FilterData;

import java.util.ArrayList;

/**
 * Created by sajid on 12/3/2015.
 */
public class FilterFragment extends BaseFragment {
    private static final String TAG = FilterFragment.class.getSimpleName();

    private Button acBusFilterButton;
    private Button nonAcBusFilterButton;
    private Button sleeperBusFilterButton;
    private Button nonSleeperBusFilterButton;

    private View operationFilterButton;
    private View boardingPointFilterButton;
    private ImageView operationFilterImageView;
    private ImageView boardingPointFilterImageView;

    private ListView boardingPointListView;
    private ListView operationListView;

    Tracker mTracker;

    private Button morningBusFilterButton;
    private Button noonBusFilterButton;
    private Button eveningBusFilterButton;
    private Button nightBusFilterButton;

    private BoardingPointFilterAdapter boardingPointFilterAdapter;
    private OperatorFilterAdapter operatorFilterAdapter;
    private Filter filter;

    private FilterData filterData;
    private BookTicket bookTicket;

    private Button applyButton;

    public static FilterFragment newInstance(ArrayList<Integer> previousFragmentIds, Parcelable parcelable) {
        Log.d(TAG, "FilterFragment newInstance(@FragmentId int previousFragmentId)");
        FilterFragment filterFragment = new FilterFragment();
        Bundle bundle = new Bundle();
        bundle.putIntegerArrayList(PREVIOUS_FRAGMENT_ID_TAG, previousFragmentIds);
        bundle.putParcelable(PARCELABLE_FRAGMENT_ITEM_ID_TAG, parcelable);
        filterFragment.setArguments(bundle);
        return filterFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (parcelable instanceof FilterData) {
            filterData = (FilterData) parcelable;
            Gson gson = new GsonBuilder().create();
            bookTicket = gson.fromJson(filterData.getBookTicketAsJson(), BookTicket.class);
            Log.e(TAG, bookTicket.toString());
            filter = bookTicket.getBookTicketData().getFilter();
        }
        // Obtain the shared Tracker instance.
        AppController application = (AppController) getActivity().getApplication();
        mTracker = application.getDefaultTracker();
        mTracker.setScreenName("Filter");
        // Send a screen view.
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)");

        if (rootView == null) {
            rootView = inflater.inflate(R.layout.fragment_location_travels, container, false);
        }
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    void initializeEditTextComponents() {

    }

    @Override
    void initializeButtonComponents() {
        acBusFilterButton = findViewById(R.id.ac_bus_filter_button);
        if (filter.getBusType().getAc() == -1) {
            acBusFilterButton.setEnabled(false);
        }
        nonAcBusFilterButton = findViewById(R.id.non_ac_bus_filter_button);
        if (filter.getBusType().getNonAc() == -1) {
            nonAcBusFilterButton.setEnabled(false);
        }
        sleeperBusFilterButton = findViewById(R.id.sleeper_bus_filter_button);
        if (filter.getBusType().getSleeper() == -1) {
            sleeperBusFilterButton.setEnabled(false);
        }
        nonSleeperBusFilterButton = findViewById(R.id.non_sleeper_bus_filter_button);
        if (filter.getBusType().getNonSleeper() == -1) {
            nonSleeperBusFilterButton.setEnabled(false);
        }

        operationFilterButton = findViewById(R.id.operation_filter_button);
        boardingPointFilterButton = findViewById(R.id.boarding_point_filter_button);

        morningBusFilterButton = findViewById(R.id.morning_bus_filter_button);
        noonBusFilterButton = findViewById(R.id.noon_bus_filter_button);
        eveningBusFilterButton = findViewById(R.id.evening_bus_filter_button);
        nightBusFilterButton = findViewById(R.id.night_bus_filter_button);

        applyButton = findViewById(R.id.apply_button);
    }

    @Override
    void initializeTextViewComponents() {

    }

    @Override
    public void onBackPressed() {
        @FragmentId
        int previousFragmentId = previousFragmentIds.get(previousFragmentIds.size() - 1);
        previousFragmentIds.remove(previousFragmentIds.size() - 1);
        filterData.setBusType(-100);
        onFragmentChangeCallListener.onBackPressed(parcelable, previousFragmentId, previousFragmentIds);
    }

    @Override
    void initializeOtherViewComponents() {
        operationFilterImageView = findViewById(R.id.operation_filter_image_view);
        boardingPointFilterImageView = findViewById(R.id.boarding_point_image_view);

        operationListView = findViewById(R.id.operator_list_view);
        operatorFilterAdapter = new OperatorFilterAdapter(getActivity(), filter.getOperators());
        operationListView.setAdapter(operatorFilterAdapter);
        setListViewHeightBasedOnChildren(operationListView);

        boardingPointListView = findViewById(R.id.boarding_point_list_view);
        boardingPointFilterAdapter = new BoardingPointFilterAdapter(getActivity(), filter.getBoardingPoints());
        boardingPointListView.setAdapter(boardingPointFilterAdapter);
        setListViewHeightBasedOnChildren(boardingPointListView);
    }

    @Override
    void initializeOnclickListener() {
        acBusFilterButton.setOnClickListener(this);
        nonAcBusFilterButton.setOnClickListener(this);
        sleeperBusFilterButton.setOnClickListener(this);
        nonSleeperBusFilterButton.setOnClickListener(this);

        operationFilterButton.setOnClickListener(this);
        boardingPointFilterButton.setOnClickListener(this);

        morningBusFilterButton.setOnClickListener(this);
        noonBusFilterButton.setOnClickListener(this);
        eveningBusFilterButton.setOnClickListener(this);
        nightBusFilterButton.setOnClickListener(this);
        applyButton.setOnClickListener(this);


    }

    @Override
    void removeOnclickListener() {
        acBusFilterButton.setOnClickListener(null);
        nonAcBusFilterButton.setOnClickListener(null);
        sleeperBusFilterButton.setOnClickListener(null);
        nonSleeperBusFilterButton.setOnClickListener(null);

        operationFilterButton.setOnClickListener(null);
        boardingPointFilterButton.setOnClickListener(null);

        morningBusFilterButton.setOnClickListener(null);
        noonBusFilterButton.setOnClickListener(null);
        eveningBusFilterButton.setOnClickListener(null);
        nightBusFilterButton.setOnClickListener(null);
        applyButton.setOnClickListener(null);

    }

    private boolean boardingPointShow = false;
    private boolean operatorShow = false;

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.back_button:
                onBackPressed();
                break;
            case R.id.ac_bus_filter_button:
                if (acBusFilterButton.isEnabled()) {
                    acBusFilterButton.setSelected(true);
                    nonAcBusFilterButton.setSelected(false);
                    sleeperBusFilterButton.setSelected(false);
                    nonSleeperBusFilterButton.setSelected(false);
                }
                break;
            case R.id.non_ac_bus_filter_button:
                if (nonAcBusFilterButton.isEnabled()) {
                    acBusFilterButton.setSelected(false);
                    nonAcBusFilterButton.setSelected(true);
                    sleeperBusFilterButton.setSelected(false);
                    nonSleeperBusFilterButton.setSelected(false);
                }
                break;
            case R.id.sleeper_bus_filter_button:
                if (sleeperBusFilterButton.isEnabled()) {
                    acBusFilterButton.setSelected(false);
                    nonAcBusFilterButton.setSelected(false);
                    sleeperBusFilterButton.setSelected(true);
                    nonSleeperBusFilterButton.setSelected(false);
                }
                break;
            case R.id.non_sleeper_bus_filter_button:
                if (nonSleeperBusFilterButton.isEnabled()) {
                    acBusFilterButton.setSelected(false);
                    nonAcBusFilterButton.setSelected(false);
                    sleeperBusFilterButton.setSelected(false);
                    nonSleeperBusFilterButton.setSelected(true);
                }
                break;
            case R.id.operation_filter_button:
                operatorShow = !operatorShow;
                operationListView.setVisibility(operatorShow ? View.VISIBLE : View.GONE);
                operationFilterImageView.setImageResource(operatorShow ? R.mipmap.ic_expand_more_black_24dp : R.mipmap.ic_expand_less_black_24dp);
                operatorFilterAdapter.notifyDataSetChanged();
                operatorFilterAdapter.getView(operatorFilterAdapter.getSelectedPosition(), operatorFilterAdapter.getOldSelectedView(), operatorFilterAdapter.getParent());
                break;
            case R.id.boarding_point_filter_button:
                boardingPointShow = !boardingPointShow;
                boardingPointListView.setVisibility(boardingPointShow ? View.VISIBLE : View.GONE);
                boardingPointFilterImageView.setImageResource(boardingPointShow ? R.mipmap.ic_expand_more_black_24dp : R.mipmap.ic_expand_less_black_24dp);
                boardingPointFilterAdapter.getView(boardingPointFilterAdapter.getSelectedPosition(), boardingPointFilterAdapter.getOldSelectedView(), boardingPointFilterAdapter.getParent());
                break;
            case R.id.morning_bus_filter_button:
                morningBusFilterButton.setSelected(true);
                noonBusFilterButton.setSelected(false);
                eveningBusFilterButton.setSelected(false);
                nightBusFilterButton.setSelected(false);
                break;
            case R.id.noon_bus_filter_button:
                morningBusFilterButton.setSelected(false);
                noonBusFilterButton.setSelected(true);
                eveningBusFilterButton.setSelected(false);
                nightBusFilterButton.setSelected(false);
                break;
            case R.id.evening_bus_filter_button:
                morningBusFilterButton.setSelected(false);
                noonBusFilterButton.setSelected(false);
                eveningBusFilterButton.setSelected(true);
                nightBusFilterButton.setSelected(false);
                break;
            case R.id.night_bus_filter_button:
                morningBusFilterButton.setSelected(false);
                noonBusFilterButton.setSelected(false);
                eveningBusFilterButton.setSelected(false);
                nightBusFilterButton.setSelected(true);
                break;
            case R.id.apply_button:
                Gson gson = new GsonBuilder().create();
                if (boardingPointFilterAdapter.getSelectedPosition() != -1) {
                    filterData.setBoardingPoint(gson.toJson(boardingPointFilterAdapter.getItem(boardingPointFilterAdapter.getSelectedPosition())));
                } else {
                    filterData.setBoardingPoint("");
                }
                if (operatorFilterAdapter.getSelectedPosition() != -1) {
                    filterData.setOperator(gson.toJson(operatorFilterAdapter.getItem(operatorFilterAdapter.getSelectedPosition())));
                } else {
                    filterData.setOperator("");
                }
                if (acBusFilterButton.isSelected()) {
                    filterData.setBusType(bookTicket.getBookTicketData().getFilter().getBusType().getAc());
                } else if (nonAcBusFilterButton.isSelected()) {
                    filterData.setBusType(bookTicket.getBookTicketData().getFilter().getBusType().getNonAc());
                } else if (sleeperBusFilterButton.isSelected()) {
                    filterData.setBusType(bookTicket.getBookTicketData().getFilter().getBusType().getSleeper());
                } else if (nonSleeperBusFilterButton.isSelected()) {
                    filterData.setBusType(bookTicket.getBookTicketData().getFilter().getBusType().getNonSleeper());
                } else {
                    filterData.setBusType(-1);
                }
                if (morningBusFilterButton.isSelected()) {
                    filterData.setTime(morningBusFilterButton.getText().toString());
                } else if (noonBusFilterButton.isSelected()) {
                    filterData.setTime(noonBusFilterButton.getText().toString());
                } else if (eveningBusFilterButton.isSelected()) {
                    filterData.setTime(eveningBusFilterButton.getText().toString());
                } else if (nightBusFilterButton.isSelected()) {
                    filterData.setTime(nightBusFilterButton.getText().toString());
                } else {
                    filterData.setTime("");
                }
                super.onBackPressed();
                break;
        }
    }

    public void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return;

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 0;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, ViewGroup.LayoutParams.WRAP_CONTENT));

            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }
}
