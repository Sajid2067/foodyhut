package com.android.food.foodyhut.adapter.recyclerview.item;

import com.google.gson.annotations.SerializedName;

/**
 * Created by sajid on 11/2/2015.
 */
public class CuisineType implements RecyclerViewBaseItem {

    @SerializedName("cuisineId")
    private String cuisineId;
    @SerializedName("cuisineName")
    private String cuisineName;
    @SerializedName("cuisineDetails")
    private String cuisineDetails;
    @SerializedName("cuisineStatus")
    private String cuisineStatus;
    @SerializedName("lastModified")
    private String lastModified;

    public CuisineType(String cuisineId, String cuisineName, String cuisineDetails, String cuisineStatus, String lastModified) {
        this.cuisineId = cuisineId;
        this.cuisineName = cuisineName;
        this.cuisineDetails = cuisineDetails;
        this.cuisineStatus = cuisineStatus;
        this.lastModified = lastModified;
    }

    public CuisineType() {
    }

    public String getCuisineId() {
        return cuisineId;
    }

    public void setCuisineId(String cuisineId) {
        this.cuisineId = cuisineId;
    }

    public String getCuisineName() {
        return cuisineName;
    }

    public void setCuisineName(String cuisineName) {
        this.cuisineName = cuisineName;
    }

    public String getCuisineDetails() {
        return cuisineDetails;
    }

    public void setCuisineDetails(String cuisineDetails) {
        this.cuisineDetails = cuisineDetails;
    }

    public String getCuisineStatus() {
        return cuisineStatus;
    }

    public void setCuisineStatus(String cuisineStatus) {
        this.cuisineStatus = cuisineStatus;
    }

    public String getLastModified() {
        return lastModified;
    }

    public void setLastModified(String lastModified) {
        this.lastModified = lastModified;
    }
}
