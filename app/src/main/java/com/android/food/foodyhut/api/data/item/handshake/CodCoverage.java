package com.android.food.foodyhut.api.data.item.handshake;

import com.google.gson.annotations.SerializedName;

/**
 * Created by sajid on 11/7/2015.
 */
public class CodCoverage {

    @SerializedName("area_id")
    private int areaId;
    @SerializedName("area_name")
    private String areaName;
    @SerializedName("city_id")
    private String cityId;
    @SerializedName("cod_fees")
    private float codFees;

    public CodCoverage() {
    }

    public CodCoverage(int areaId, String areaName, String cityId, float codFees) {

        this.areaId = areaId;
        this.areaName = areaName;
        this.cityId = cityId;
        this.codFees = codFees;
    }

    public int getAreaId() {

        return areaId;
    }

    public void setAreaId(int areaId) {
        this.areaId = areaId;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public float getCodFees() {
        return codFees;
    }

    public void setCodFees(float codFees) {
        this.codFees = codFees;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CodCoverage)) return false;

        CodCoverage that = (CodCoverage) o;

        return (this.getAreaId() == that.getAreaId())
                && (Float.compare(that.getCodFees(), this.getCodFees()) == 0)
                && this.getAreaName().equals(that.getAreaName())
                && this.getCityId().equals(that.getCityId());

    }

    @Override
    public int hashCode() {
        int result = getAreaId();
        result = 31 * result + getAreaName().hashCode();
        result = 31 * result + getCityId().hashCode();
        result = 31 * result + (getCodFees() != +0.0f ? Float.floatToIntBits(getCodFees()) : 0);
        return result;
    }

    @Override
    public String toString() {
        return "CodCoverage{" +
                "areaId=" + areaId +
                ", areaName='" + areaName + '\'' +
                ", cityId='" + cityId + '\'' +
                ", codFees=" + codFees +
                '}';
    }
}
