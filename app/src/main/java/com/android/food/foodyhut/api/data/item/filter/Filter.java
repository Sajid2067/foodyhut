package com.android.food.foodyhut.api.data.item.filter;

import com.google.gson.annotations.SerializedName;
import com.android.food.foodyhut.api.data.item.BoardingPoint;
import com.android.food.foodyhut.api.data.item.bus.BusType;
import com.android.food.foodyhut.api.data.item.bus.Operator;

import java.util.List;

/**
 * Created by sajid on 11/3/2015.
 */
public class Filter {

    @SerializedName("boarding_points")
    private List<BoardingPoint> boardingPoints;

    @SerializedName("bus_types")
    private BusType busType;

    private List<Operator> operators;

    public Filter() {
    }

    public Filter(List<BoardingPoint> boardingPoints, BusType busType, List<Operator> operators) {
        this.boardingPoints = boardingPoints;
        this.busType = busType;
        this.operators = operators;
    }

    public List<BoardingPoint> getBoardingPoints() {
        return boardingPoints;
    }

    public void setBoardingPoints(List<BoardingPoint> boardingPoints) {
        this.boardingPoints = boardingPoints;
    }

    public BusType getBusType() {
        return busType;
    }

    public void setBusType(BusType busType) {
        this.busType = busType;
    }

    public List<Operator> getOperators() {
        return operators;
    }

    public void setOperators(List<Operator> operators) {
        this.operators = operators;
    }

    @Override
    public String toString() {
        return "Filter{" +
                "boardingPoints=" + boardingPoints +
                ", busType=" + busType +
                ", operators=" + operators +
                '}';
    }
}
