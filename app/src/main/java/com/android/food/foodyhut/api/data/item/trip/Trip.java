package com.android.food.foodyhut.api.data.item.trip;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by sajid on 11/3/2015.
 */
public class Trip {

    @SerializedName("list")
    private List<TripList> tripLists;
    @SerializedName("total")
    private int total;

    public Trip() {
    }

    public Trip(List<TripList> tripLists, int total) {

        this.tripLists = tripLists;
        this.total = total;
    }

    public List<TripList> getTripLists() {

        return tripLists;
    }

    public void setTripLists(List<TripList> tripLists) {
        this.tripLists = tripLists;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    @Override
    public String toString() {
        return "Trip{" +
                "tripLists=" + tripLists +
                ", total=" + total +
                '}';
    }
}
