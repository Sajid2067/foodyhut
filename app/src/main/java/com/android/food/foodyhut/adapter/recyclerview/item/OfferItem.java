package com.android.food.foodyhut.adapter.recyclerview.item;

/**
 * Created by sajid on 11/2/2015.
 */
public class OfferItem implements RecyclerViewBaseItem {

    private String percentage;
    private String offerName;
    private String offerRule;

    public OfferItem() {
    }

    public OfferItem(String percentage, String offerName, String offerRule) {
        this.percentage = percentage;
        this.offerName = offerName;
        this.offerRule = offerRule;
    }

    public String getPercentage() {
        return percentage;
    }

    public void setPercentage(String percentage) {
        this.percentage = percentage;
    }

    public String getOfferName() {
        return offerName;
    }

    public void setOfferName(String offerName) {
        this.offerName = offerName;
    }

    public String getOfferRule() {
        return offerRule;
    }

    public void setOfferRule(String offerRule) {
        this.offerRule = offerRule;
    }

    @Override
    public String toString() {
        return "OfferItem{" +
                "percentage='" + percentage + '\'' +
                ", offerName='" + offerName + '\'' +
                ", offerRule='" + offerRule + '\'' +
                '}';
    }
}
