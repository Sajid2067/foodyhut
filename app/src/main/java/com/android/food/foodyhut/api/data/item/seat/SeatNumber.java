package com.android.food.foodyhut.api.data.item.seat;

import com.google.gson.annotations.SerializedName;

/**
 * Created by sajid on 11/4/2015.
 */
public class SeatNumber {

    @SerializedName("ticket_id")
    private int ticketId;
    @SerializedName("seat_number")
    private String seatNumber;
    @SerializedName("seat_availability")
    private byte seatAvailability;
    @SerializedName("seat_type")
    private int seatType;
    @SerializedName("seat_fare_type")
    private String seatFareType;
    @SerializedName("seat_fare")
    private float seatFare;

    public SeatNumber() {
    }

    public SeatNumber(int ticketId, String seatNumber, byte seatAvailability, int seatType, String seatFareType, float seatFare) {

        this.ticketId = ticketId;
        this.seatNumber = seatNumber;
        this.seatAvailability = seatAvailability;
        this.seatType = seatType;
        this.seatFareType = seatFareType;
        this.seatFare = seatFare;
    }

    public int getTicketId() {

        return ticketId;
    }

    public void setTicketId(int ticketId) {
        this.ticketId = ticketId;
    }

    public String getSeatNumber() {
        return seatNumber;
    }

    public void setSeatNumber(String seatNumber) {
        this.seatNumber = seatNumber;
    }

    public byte getSeatAvailability() {
        return seatAvailability;
    }

    public void setSeatAvailability(byte seatAvailability) {
        this.seatAvailability = seatAvailability;
    }

    public int getSeatType() {
        return seatType;
    }

    public void setSeatType(int seatType) {
        this.seatType = seatType;
    }

    public String getSeatFareType() {
        return seatFareType;
    }

    public void setSeatFareType(String seatFareType) {
        this.seatFareType = seatFareType;
    }

    public float getSeatFare() {
        return seatFare;
    }

    public void setSeatFare(float seatFare) {
        this.seatFare = seatFare;
    }

    @Override
    public String toString() {
        return "SeatNumber{" +
                "ticketId=" + ticketId +
                ", seatNumber='" + seatNumber + '\'' +
                ", seatAvailability=" + seatAvailability +
                ", seatType=" + seatType +
                ", seatFareType='" + seatFareType + '\'' +
                ", seatFare=" + seatFare +
                '}';
    }
}
