package com.android.food.foodyhut.fragment;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.food.foodyhut.activity.MainAppActivity;
import com.android.food.foodyhut.activity.annotation.FragmentId;
import com.android.food.foodyhut.activity.listener.OnFragmentInteractionListener;
import com.android.food.foodyhut.R;

import java.util.ArrayList;

/**
 * Created by sajid on 10/28/2015.
 */
public abstract class BaseFragment extends Fragment implements View.OnClickListener {

    @FragmentId
    static final String PREVIOUS_FRAGMENT_ID_TAG = "previousFragmentId";
    static final String PARCELABLE_FRAGMENT_ITEM_ID_TAG = "parcelableFragmentItem";
    private static final String TAG = BaseFragment.class.getSimpleName();
    protected View rootView;

    protected ArrayList<Integer> previousFragmentIds;
    protected Button retryButton;
    protected View connectionErrorView;
    protected TextView connectionErrorTextView;
    protected Parcelable parcelable;
    protected AlertDialog progressBarDialog;
    protected TextView progressBarMessageTextView;
    protected OnFragmentInteractionListener onFragmentChangeCallListener;
    private ImageButton backButton;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)");
        initializeViewComponents();
        return rootView;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onCreate(@Nullable Bundle savedInstanceState)");
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            previousFragmentIds = getArguments().getIntegerArrayList(PREVIOUS_FRAGMENT_ID_TAG);
            if (previousFragmentIds == null) {
                previousFragmentIds = new ArrayList<>();
            }
            parcelable = getArguments().getParcelable(PARCELABLE_FRAGMENT_ITEM_ID_TAG);
        } else {
            if (previousFragmentIds == null) {
                previousFragmentIds = new ArrayList<>();
            }
        }
    }

    protected void createProgressDialogView() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());
        View view = View.inflate(getContext(), R.layout.progressbar_view, null);
        progressBarMessageTextView = (TextView) view.findViewById(R.id.progress_bar_message_text_view);
        alertDialogBuilder.setView(view);
        alertDialogBuilder.setCancelable(false);
        progressBarDialog = alertDialogBuilder.create();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onActivityCreated(@Nullable Bundle savedInstanceState)");
        super.onActivityCreated(savedInstanceState);
        initializeOnclickListener();
        if (backButton != null) {
            backButton.setOnClickListener(this);
        }
        if (retryButton != null) {
            retryButton.setOnClickListener(this);
        }
        onFragmentChangeCallListener = (MainAppActivity) getActivity();
    }

    @Override
    public void onDetach() {
        Log.d(TAG, "onDetach()");
        super.onDetach();
        removeOnclickListener();
        if (backButton != null) {
            backButton.setOnClickListener(null);
        }
        if (retryButton != null) {
            retryButton.setOnClickListener(null);
        }
        onFragmentChangeCallListener = null;
    }


    protected void initializeViewComponents() {
        Log.d(TAG, "initializeViewComponents()");

        initializeEditTextComponents();

        initializeButtonComponents();

        initializeTextViewComponents();

        initializeOtherViewComponents();
        if (rootView != null) {
            backButton = (ImageButton) rootView.findViewById(R.id.back_button);
            retryButton = (Button) rootView.findViewById(R.id.retry_button);
            connectionErrorTextView = (TextView) rootView.findViewById(R.id.connection_error_text_view);
            connectionErrorView = rootView.findViewById(R.id.connection_error_view);
        }
    }

    abstract void initializeEditTextComponents();

    abstract void initializeButtonComponents();

    abstract void initializeTextViewComponents();

    abstract void initializeOtherViewComponents();

    abstract void initializeOnclickListener();

    abstract void removeOnclickListener();

    @Override
    public void onClick(View view) {
        Log.d(TAG, "onClick(View view)");
        switch (view.getId()) {
            case R.id.back_button:
                Log.d(TAG, "Back Button Pressed");
                onBackPressed();
                return;
            default:
                break;
        }
    }

    public ArrayList<Integer> getPreviousFragmentIds() {
        return previousFragmentIds;
    }

    public void setPreviousFragmentIds(ArrayList<Integer> previousFragmentIds) {
        this.previousFragmentIds = previousFragmentIds;
    }

    public Parcelable getParcelable() {
        return parcelable;
    }

    public void setParcelable(Parcelable parcelable) {
        this.parcelable = parcelable;
    }

    protected void makeAToast(String message, int toastTime) {
        Toast.makeText(getContext(), message, toastTime).show();
    }

    public void onBackPressed() {
        @FragmentId
        int previousFragmentId = previousFragmentIds.get(previousFragmentIds.size() - 1);
        previousFragmentIds.remove(previousFragmentIds.size() - 1);
        onFragmentChangeCallListener.onBackPressed(parcelable, previousFragmentId, previousFragmentIds);
    }

    protected String getText(TextView textView) {
        return textView.getText().toString().trim();
    }

    protected boolean isZeroLengthText(EditText editText) {
        return isZeroLengthText(editText.getText().toString());
    }

    protected boolean isZeroLengthText(TextView editText) {
        return isZeroLengthText(editText.getText().toString());
    }

    protected boolean isZeroLengthText(String string) {
        return string.trim().length() == 0;
    }

    protected <T extends View> T findViewById(int id) {
        return (T) rootView.findViewById(id);
    }
}
