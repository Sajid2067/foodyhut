package com.android.food.foodyhut.api.data.item.seatlayout;

import com.google.gson.annotations.SerializedName;

/**
 * Created by sajid on 11/4/2015.
 */
public class SeatLayout {

    @SerializedName("data")
    private SeatLayoutData seatLayoutData;

    public SeatLayout() {
    }

    public SeatLayout(SeatLayoutData seatLayoutData) {

        this.seatLayoutData = seatLayoutData;
    }

    public SeatLayoutData getSeatLayoutData() {

        return seatLayoutData;
    }

    public void setSeatLayoutData(SeatLayoutData seatLayoutData) {
        this.seatLayoutData = seatLayoutData;
    }

    @Override
    public String toString() {
        return "SeatLayout{" +
                "seatLayoutData=" + seatLayoutData +
                '}';
    }
}
