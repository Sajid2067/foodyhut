package com.android.food.foodyhut.util;

import com.android.food.foodyhut.api.data.item.handshake.offer.ShohozOffer;

import java.util.List;

/**
 * Created by sajid on 1/6/2016.
 */
public class OffersPreference {

    private List<ShohozOffer> shohozOfferList;

    public OffersPreference() {
    }

    public OffersPreference(List<ShohozOffer> shohozOfferList) {

        this.shohozOfferList = shohozOfferList;
    }

    public List<ShohozOffer> getShohozOfferList() {

        return shohozOfferList;
    }

    public void setShohozOfferList(List<ShohozOffer> shohozOfferList) {
        this.shohozOfferList = shohozOfferList;
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) return true;
        if (!(o instanceof OffersPreference)) return false;

        OffersPreference that = (OffersPreference) o;

        return shohozOfferList.equals(that.shohozOfferList);

    }

    @Override
    public int hashCode() {
        return shohozOfferList.hashCode();
    }

    @Override
    public String toString() {
        return "OffersPreference{" +
                "shohozOfferList=" + shohozOfferList +
                '}';
    }
}
