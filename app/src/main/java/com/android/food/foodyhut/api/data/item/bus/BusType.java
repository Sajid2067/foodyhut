package com.android.food.foodyhut.api.data.item.bus;

import com.google.gson.annotations.SerializedName;

/**
 * Created by sajid on 11/3/2015.
 */
public class BusType {

    @SerializedName("AC")
    private int ac = -1;
    @SerializedName("Non AC")
    private int nonAc = -1;
    @SerializedName("Sleeper")
    private int sleeper = -1;

    @SerializedName("Non Sleeper")
    private int nonSleeper = -1;

    public BusType() {
    }

    public BusType(int ac, int nonAc, int sleeper, int nonSleeper) {

        this.ac = ac;
        this.nonAc = nonAc;
        this.sleeper = sleeper;
        this.nonSleeper = nonSleeper;
    }

    public int getAc() {

        return ac;
    }

    public void setAc(int ac) {
        this.ac = ac;
    }

    public int getNonAc() {
        return nonAc;
    }

    public void setNonAc(int nonAc) {
        this.nonAc = nonAc;
    }

    public int getSleeper() {
        return sleeper;
    }

    public void setSleeper(int sleeper) {
        this.sleeper = sleeper;
    }

    public int getNonSleeper() {
        return nonSleeper;
    }

    public void setNonSleeper(int nonSleeper) {
        this.nonSleeper = nonSleeper;
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) return true;
        if (!(o instanceof BusType)) return false;

        BusType busType = (BusType) o;

        if (ac != busType.ac) return false;
        if (nonAc != busType.nonAc) return false;
        if (sleeper != busType.sleeper) return false;
        return nonSleeper == busType.nonSleeper;

    }

    @Override
    public int hashCode() {
        int result = ac;
        result = 31 * result + nonAc;
        result = 31 * result + sleeper;
        result = 31 * result + nonSleeper;
        return result;
    }

    @Override
    public String toString() {
        return "BusType{" +
                "ac=" + ac +
                ", nonAc=" + nonAc +
                ", sleeper=" + sleeper +
                ", nonSleeper=" + nonSleeper +
                '}';
    }
}
