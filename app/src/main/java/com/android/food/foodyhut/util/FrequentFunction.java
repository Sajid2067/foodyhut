package com.android.food.foodyhut.util;

import android.os.Build;
import android.text.Html;
import android.widget.TextView;

/**
 * Created by TT on 4/8/2017.
 */

public class FrequentFunction {

    public static void convertHtml(TextView textView, String htmlStr) {

        if (Build.VERSION.SDK_INT >= 24) {
            textView.setText(Html.fromHtml(htmlStr, Html.FROM_HTML_MODE_LEGACY)); // for 24 api and more
        } else {
           textView.setText(Html.fromHtml(htmlStr)); // or for older api
        }
    }
}
