package com.android.food.foodyhut.api.data.item.ticketWinning;

import com.google.gson.annotations.SerializedName;

/**
 * Created by sajid on 12/14/2015.
 */
public class Data {
    @SerializedName("pnr")
    private String pnr;
    @SerializedName("message")
    private String message;

    public Data() {
    }

    public Data(String pnr, String message) {
        this.pnr = pnr;
        this.message = message;
    }

    public String getPnr() {
        return pnr;
    }

    public void setPnr(String pnr) {
        this.pnr = pnr;
    }

    public String getMessage() {

        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Data)) return false;

        Data data = (Data) o;

        if (!getPnr().equals(data.getPnr())) return false;
        return getMessage().equals(data.getMessage());

    }

    @Override
    public int hashCode() {
        int result = getPnr().hashCode();
        result = 31 * result + getMessage().hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Data{" +
                "pnr='" + pnr + '\'' +
                ", message='" + message + '\'' +
                '}';
    }
}
