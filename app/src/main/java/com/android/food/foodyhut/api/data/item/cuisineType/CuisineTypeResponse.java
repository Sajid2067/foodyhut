package com.android.food.foodyhut.api.data.item.cuisineType;

import com.google.gson.annotations.SerializedName;

/**
 * Created by sajid on 12/14/2015.
 */
public class CuisineTypeResponse {

    @SerializedName("cuisineId")
    private String cuisineId;
    @SerializedName("cuisineName")
    private String cuisineName;
    @SerializedName("cuisineDetails")
    private String cuisineDetails;
    @SerializedName("cuisineStatus")
    private String cuisineStatus;
    @SerializedName("lastModified")
    private String lastModified;


    public CuisineTypeResponse() {
    }

    public String getCuisineId() {
        return cuisineId;
    }

    public void setCuisineId(String cuisineId) {
        this.cuisineId = cuisineId;
    }

    public String getCuisineName() {
        return cuisineName;
    }

    public void setCuisineName(String cuisineName) {
        this.cuisineName = cuisineName;
    }

    public String getCuisineDetails() {
        return cuisineDetails;
    }

    public void setCuisineDetails(String cuisineDetails) {
        this.cuisineDetails = cuisineDetails;
    }

    public String getCuisineStatus() {
        return cuisineStatus;
    }

    public void setCuisineStatus(String cuisineStatus) {
        this.cuisineStatus = cuisineStatus;
    }

    public String getLastModified() {
        return lastModified;
    }

    public void setLastModified(String lastModified) {
        this.lastModified = lastModified;
    }
}
