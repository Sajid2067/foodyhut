package com.android.food.foodyhut.location;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.widget.TextView;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

/**
 * Created by TT on 5/31/2017.
 */

public class LocationAddress {
    private static final String TAG = "LocationAddress";
    static String result = null;
    static public int count=0;
    static StringBuilder sb;

    public static void getAddressFromLocation(final TextView fromCityTextView,final double latitude, final double longitude,
                                              final Context context, final Handler handler) {

        final Thread thread = new Thread() {
            @Override
            public void run() {
                Geocoder geocoder = new Geocoder(context, Locale.getDefault());

                try {
                    List<Address> addressList = geocoder.getFromLocation(
                            latitude, longitude, 1);
                    if (addressList != null && addressList.size() > 0) {
                        Address address = addressList.get(0);
                         sb = new StringBuilder();
                        for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                           sb= sbAppend(sb,address.getAddressLine(i));
                           // sb.append(address.getAddressLine(i)).append(",");
                        }
                      //  sb= sbAppend(sb,address.getLocality());
                        sb= sbAppend(sb,address.getPostalCode());
                        sb= sbAppend(sb,address.getCountryName());
                        Log.d("Location",sb.toString());
                      //  sb.append(address.getLocality()).append(",");
                    //    sb.append(address.getPostalCode()).append(",");
                   //     sb.append(address.getCountryName());
                        result = sb.toString();
                    //    LocationFragmentFH locationFragmentFH = new LocationFragmentFH();
                    //    locationFragmentFH.moveToAnotherFragment();
                        fromCityTextView.post(new Runnable() {
                            public void run() {
                                fromCityTextView.setText("");
                                count=0;
                            }
                        });
                      //  count=0;
                      //  communicationChannel.setReload("Trigger");



                    }
                } catch (IOException e) {
                    Log.e(TAG, "Unable connect to Geocoder", e);
                } finally {
                    Message message = Message.obtain();
                    message.setTarget(handler);
                    if (result != null) {
                        message.what = 1;
                        Bundle bundle = new Bundle();
                        result = "Latitude: " + latitude + " Longitude: " + longitude +
                                "\n\nAddress:\n" + result;
                        bundle.putString("address", result);
                        message.setData(bundle);
                    } else {
                        message.what = 1;
                        Bundle bundle = new Bundle();
                        result = "Latitude: " + latitude + " Longitude: " + longitude +
                                "\n Unable to get address for this lat-long.";
                        bundle.putString("address", result);
                        message.setData(bundle);
                    }
                    message.sendToTarget();
                }
            }
        };
        thread.start();
    }

  public static  StringBuilder sbAppend(StringBuilder sb,String s){
       if(TextUtils.isEmpty(s)){
           return sb;
       }else {
           if(count==0){
               count++;
               return sb.append(s);
           }else {
               return sb.append(",").append(s);
           }
       }
    }


}
