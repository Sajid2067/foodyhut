package com.android.food.foodyhut.database.data.source;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.android.food.foodyhut.database.DataBaseOpenHelper;
import com.android.food.foodyhut.database.data.item.PaymentMethodDataItem;
import com.android.food.foodyhut.database.exception.DataSourceException;

import java.util.ArrayList;

/**
 * Created by tuman on 4/5/2015.
 */
public class PaymentMethodDataSource {

    protected Context context;
    protected SQLiteDatabase database;
    protected DataBaseOpenHelper dbHelper;
    private String[] allColumns = {
            DataBaseOpenHelper.PAYMENT_METHOD_NAME_COLUMN,
            DataBaseOpenHelper.PAYMENT_METHOD_HOURS_COLUMN,
            DataBaseOpenHelper.PAYMENT_METHOD_IS_ACTIVE_COLUMN};

    public PaymentMethodDataSource(Context context) {
        this.context = context;
        dbHelper = new DataBaseOpenHelper(this.context);
    }

    public PaymentMethodDataItem insertPaymentMethodDataItem(String paymentMethodName, int paymentMethodHours, boolean paymentMethodIsActive)
            throws DataSourceException {
        try {
            ContentValues values = new ContentValues();
            values.put(DataBaseOpenHelper.PAYMENT_METHOD_NAME_COLUMN, paymentMethodName);
            values.put(DataBaseOpenHelper.PAYMENT_METHOD_HOURS_COLUMN, paymentMethodHours);
            values.put(DataBaseOpenHelper.PAYMENT_METHOD_IS_ACTIVE_COLUMN, Boolean.toString(paymentMethodIsActive));
            database.insert(DataBaseOpenHelper.PAYMENT_METHOD_TABLE, null, values);
            Cursor cursor = database.query(DataBaseOpenHelper.PAYMENT_METHOD_TABLE,
                    allColumns, DataBaseOpenHelper.PAYMENT_METHOD_NAME_COLUMN + " LIKE "
                            + "'" + paymentMethodName + "'", null, null, null, null);
            if (cursor == null) {
                return null;
            }
            cursor.moveToFirst();
            PaymentMethodDataItem paymentMethodDataItem = cursorToPaymentMethodDataItem(cursor);
            cursor.close();
            return paymentMethodDataItem;
        } catch (Exception e) {
            return null;
        }
    }

    public PaymentMethodDataItem insertPaymentMethodDataItem(PaymentMethodDataItem item)
            throws DataSourceException {
        return insertPaymentMethodDataItem(item.getPaymentMethodName(), item.getPaymentMethodHours(), item.isPaymentMethodIsActive());
    }

    public PaymentMethodDataItem getPaymentMethodDataItem(String paymentMethodName) {
        Cursor cursor = database.query(DataBaseOpenHelper.PAYMENT_METHOD_TABLE,
                allColumns, DataBaseOpenHelper.PAYMENT_METHOD_NAME_COLUMN + " LIKE "
                        + "'" + paymentMethodName + "'", null, null, null, null);
        cursor.moveToFirst();
        PaymentMethodDataItem paymentMethodDataItem = cursorToPaymentMethodDataItem(cursor);
        cursor.close();
        return paymentMethodDataItem;
    }

    public void updatePaymentMethodDataItem(PaymentMethodDataItem item) {
        ContentValues values = new ContentValues();
//        values.put(DataBaseOpenHelper.LATEST_COLUMN, item.getLatestOne());
        //      database.update(DataBaseOpenHelper.DATA_TABLE, values, DataBaseOpenHelper.ID_COLUMN + " = " + item.getId(), null);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public void deletePaymentMethodDataItem(PaymentMethodDataItem item) {
        String paymentMethodName = item.getPaymentMethodName();
        database.delete(DataBaseOpenHelper.PAYMENT_METHOD_TABLE,
                DataBaseOpenHelper.PAYMENT_METHOD_NAME_COLUMN + " LIKE "
                        + "'" + paymentMethodName + "'", null);
    }

    public ArrayList<PaymentMethodDataItem> getAllPaymentMethodDataItem() {
        ArrayList<PaymentMethodDataItem> items = new ArrayList<>();
        Cursor cursor = database.query(DataBaseOpenHelper.PAYMENT_METHOD_TABLE,
                allColumns, null, null, null, null, null);
        cursor.moveToFirst();
        do {
            PaymentMethodDataItem item = cursorToPaymentMethodDataItem(cursor);
            items.add(item);
        } while (cursor.moveToNext());
        cursor.close();
        return items;
    }

    public ArrayList<PaymentMethodDataItem> getAllPaymentMethodDataItem(PaymentMethodDataItem paymentMethodDataItem) {
        ArrayList<PaymentMethodDataItem> items = new ArrayList<>();
        Cursor cursor = database.query(DataBaseOpenHelper.PAYMENT_METHOD_TABLE,
                allColumns, DataBaseOpenHelper.PAYMENT_METHOD_NAME_COLUMN + " LIKE "
                        + "'" + paymentMethodDataItem.getPaymentMethodName() + "'", null, null, null, null);
        cursor.moveToFirst();
        do {
            PaymentMethodDataItem item = cursorToPaymentMethodDataItem(cursor);
            items.add(item);
        } while (cursor.moveToNext());
        cursor.close();
        return items;
    }

    public int getSize() {
        Cursor cursor = database.query(DataBaseOpenHelper.PAYMENT_METHOD_TABLE,
                allColumns, null, null, null, null, null);
        cursor.moveToFirst();
        cursor.close();
        return cursor.getCount();
    }

    private PaymentMethodDataItem cursorToPaymentMethodDataItem(Cursor cursor) {
        PaymentMethodDataItem item = new PaymentMethodDataItem();
        if (cursor.getCount() == 0)
            return item;
        item.setPaymentMethodName(cursor.getString(cursor
                .getColumnIndex(DataBaseOpenHelper.PAYMENT_METHOD_NAME_COLUMN)));
        item.setPaymentMethodHours(cursor.getInt(cursor
                .getColumnIndex(DataBaseOpenHelper.PAYMENT_METHOD_HOURS_COLUMN)));
        item.setPaymentMethodIsActive(Boolean.parseBoolean(cursor.getString(cursor
                .getColumnIndex(DataBaseOpenHelper.PAYMENT_METHOD_IS_ACTIVE_COLUMN))));
        return item;
    }
}
