package com.android.food.foodyhut.api.data.item.coupon;

import com.google.gson.annotations.SerializedName;

/**
 * Created by sajid on 12/14/2015.
 */
public class CouponVerificationMessage {

    @SerializedName("data")
    private Data data;

    @SerializedName("error")
    private Error error;

    public CouponVerificationMessage() {
    }

    public CouponVerificationMessage(Data data, Error error) {

        this.data = data;
        this.error = error;
    }

    public Data getData() {

        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) return true;
        if (!(o instanceof CouponVerificationMessage)) return false;

        CouponVerificationMessage that = (CouponVerificationMessage) o;

        if (!data.equals(that.data)) return false;
        return error.equals(that.error);

    }

    @Override
    public int hashCode() {
        int result = data.hashCode();
        result = 31 * result + error.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "CouponVerificationMessage{" +
                "data=" + data +
                ", error=" + error +
                '}';
    }
}
