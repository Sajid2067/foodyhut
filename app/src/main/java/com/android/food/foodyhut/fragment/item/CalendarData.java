package com.android.food.foodyhut.fragment.item;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Calendar;

/**
 * Created by sajid on 10/29/2015.
 */
public class CalendarData implements Parcelable {

    public static final Creator<CalendarData> CREATOR = new Creator<CalendarData>() {
        @Override
        public CalendarData createFromParcel(Parcel in) {
            return new CalendarData(in);
        }

        @Override
        public CalendarData[] newArray(int size) {
            return new CalendarData[size];
        }
    };
    private int year;
    private int month;
    private int day;

    public CalendarData() {
        this(0, 0, 0);
    }

    public CalendarData(Calendar calendar) {
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
    }

    public CalendarData(Parcel in) {
        setDay(in.readInt());
        setMonth(in.readInt());
        setYear(in.readInt());
    }

    public CalendarData(int year, int month, int day) {
        this.year = year;
        this.day = day;
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }


    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CalendarData)) return false;

        CalendarData that = (CalendarData) o;

        if (this.year != that.year) return false;
        if (this.day != that.day) return false;
        return this.month == that.month;

    }

    @Override
    public int hashCode() {
        int result = this.year;
        result = 31 * result + this.day;
        result = 31 * result + this.month;
        return result;
    }

    @Override
    public String toString() {
        return "CalendarData{" +
                "year=" + year +
                ", day=" + day +
                ", month=" + month +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.getDay());
        dest.writeInt(this.getMonth());
        dest.writeInt(this.getYear());
    }
}
