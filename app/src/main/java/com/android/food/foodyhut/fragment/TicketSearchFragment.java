package com.android.food.foodyhut.fragment;

import android.Manifest;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.android.food.foodyhut.R;
import com.android.food.foodyhut.adapter.listview.TicketSearchAdapter;
import com.android.food.foodyhut.api.data.item.searchticket.TicketSearchItem;
import com.android.food.foodyhut.api.data.item.searchticket.TicketSearchMessage;
import com.android.food.foodyhut.application.AppController;
import com.android.food.foodyhut.fragment.item.BookTicketData;
import com.android.food.foodyhut.fragment.item.CalendarData;
import com.android.food.foodyhut.toolbox.ObjectRequest;
import com.android.food.foodyhut.util.API;
import com.android.food.foodyhut.util.AppManager;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Locale;

public class TicketSearchFragment extends BaseFragment implements OnClickListener, DialogInterface.OnClickListener {

    private static final String TAG = TicketSearchFragment.class.getSimpleName();

    private static final SimpleDateFormat monthFormat = new SimpleDateFormat("MMM", Locale.US);
    private static final SimpleDateFormat dayFormat = new SimpleDateFormat("EEEE", Locale.US);
    private static String[] PERMISSIONS_PHONECALL = {Manifest.permission.CALL_PHONE};
    private static final int PERMISSIONS_REQUEST_PHONE_CALL = 100;
    Tracker mTracker;
  //  private TextView fromCityEditText;
  //  private TextView toCityEditText;
    private LinearLayout mainSearchButton;

    private TextView helpLineNumberTextView;
    private TextView tvAppTitle;
    private EditText editTextPNR;
    private AppManager appManager;
    private AlertDialog noInternetAlertDialog;
    private AlertDialog errorAlertDialog;
    private Calendar calendar;
    private BookTicketData bookTicketData;
    ObjectRequest<TicketSearchMessage> tikcetSearchObjectRequest;
    private AppController appController = AppController.getInstance();
    int timeoutCounter = 0;
    private ListView ticketSearchListView;
    private TicketSearchAdapter ticketSearchAdapter;
    private LinearLayout lLItemHeadings;

    public static TicketSearchFragment newInstance(ArrayList<Integer> previousFragmentIds, Parcelable parcelable) {
        Log.d(TAG, "newInstance(@FragmentId int previousFragmentId, Parcelable parcelable)");
        Log.i(TAG, "previousFragmentIds - " + previousFragmentIds.toString());
        if (parcelable != null) {
            Log.i(TAG, "parcelable - " + parcelable.toString());
        } else {
            Log.i(TAG, "parcelable is null");
        }
        TicketSearchFragment mainSearchFragment = new TicketSearchFragment();
        Bundle bundle = new Bundle();
        bundle.putIntegerArrayList(PREVIOUS_FRAGMENT_ID_TAG, previousFragmentIds);
        bundle.putParcelable(PARCELABLE_FRAGMENT_ITEM_ID_TAG, parcelable);
        mainSearchFragment.setArguments(bundle);
        return mainSearchFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onCreate(@Nullable Bundle savedInstanceState)");
        super.onCreate(savedInstanceState);
        calendar = new GregorianCalendar();
        appManager = new AppManager(getActivity());
        if (parcelable != null) {
            if (parcelable instanceof BookTicketData) {
                bookTicketData = (BookTicketData) parcelable;
                calendar = new GregorianCalendar(bookTicketData.getCalendarData().getYear(), bookTicketData.getCalendarData().getMonth(), bookTicketData.getCalendarData().getDay());
            } else {
                bookTicketData = new BookTicketData();
                bookTicketData.setCalendarData(new CalendarData(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)));
            }
        } else {
            bookTicketData = new BookTicketData();
            bookTicketData.setCalendarData(new CalendarData(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)));
        }
        // Obtain the shared Tracker instance.
        AppController application = (AppController) getActivity().getApplication();
        mTracker = application.getDefaultTracker();
        mTracker.setScreenName("Main Search");
        // Send a screen view.
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)");
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.fragment_ticktet_search, container, false);
        }
        createAlertDialog();
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onActivityCreated(@Nullable Bundle savedInstanceState)");
        super.onActivityCreated(savedInstanceState);
        if (parcelable != null && parcelable instanceof CalendarData) {
            updateUIComponent((CalendarData) parcelable);
        } else if (parcelable != null && parcelable instanceof BookTicketData) {
            updateUIComponent(((BookTicketData) parcelable).getCalendarData());
        }
    }

    @Override
    void initializeEditTextComponents() {
        Log.d(TAG, "initializeEditTextComponents()");
        editTextPNR = (EditText) rootView.findViewById(R.id.pnr_edit_text);

    }

    @Override
    void initializeButtonComponents() {
        Log.d(TAG, "initializeButtonComponents()");

        mainSearchButton = (LinearLayout) rootView.findViewById(R.id.main_search_button);
        ticketSearchListView = findViewById(R.id.ticket_search_list_view);

    }

    @Override
    void initializeTextViewComponents() {
        Log.d(TAG, "initializeTextViewComponents()");

        int date = calendar.get(Calendar.DAY_OF_MONTH);
        String htmlDate = appManager.useDateTerminology(date);

        helpLineNumberTextView = findViewById(R.id.help_line_number_text_view);


    }

    @Override
    void initializeOtherViewComponents() {
        Log.d(TAG, "initializeOtherViewComponents()");
        lLItemHeadings=(LinearLayout)findViewById(R.id.item_headings);
        lLItemHeadings.setVisibility(View.GONE);
    }

    @Override
    void initializeOnclickListener() {
        Log.d(TAG, "initializeOnclickListener()");

        mainSearchButton.setOnClickListener(this);

        helpLineNumberTextView.setOnClickListener(this);

    }

    @Override
    void removeOnclickListener() {
        Log.d(TAG, "removeOnclickListener()");

        mainSearchButton.setOnClickListener(null);

        helpLineNumberTextView.setOnClickListener(null);

    }

    @Override
    public void onClick(View v) {
        Log.d(TAG, "removeOnclickListener()");
        if (bookTicketData != null) {
            bookTicketData.setCalendarData(bookTicketData.getCalendarData());
        } else {
            bookTicketData = new BookTicketData();
        }
        switch (v.getId()) {

            case R.id.main_search_button:
                if(appManager.isNetworkAvailable()) {
                    searchTicket();
                }else {
                    noInternetAlertDialog.show();

                }

                break;
            case R.id.help_line_number_text_view:
                call();
                break;
            default:
                break;
        }
    }

    private void searchTicket() {
        String pnr = editTextPNR.getText().toString().trim();
        if(!TextUtils.isEmpty(pnr)){
            AppManager appManager = new AppManager(getActivity());
            String url = API.SEARCH_TICKET_API_URL;

            HashMap<String, String> postParams = new HashMap<>();
            postParams.put(API.Parameter.ANDROID_DEVICE_ID, appManager.getDeviceId());
            postParams.put(API.Parameter.ANDROID_APP_VERSION, appManager.getAppVersion());
            postParams.put(API.Parameter.PNR,pnr);

            Log.e(TAG, url);
            Log.e(TAG, postParams.toString());
            tikcetSearchObjectRequest = new ObjectRequest<>(API.Method.SEARCH_TICKET_API_METHOD, url, postParams, new Response.Listener<TicketSearchMessage>() {
                @Override
                public void onResponse(TicketSearchMessage response) {
                    progressBarDialog.cancel();
                    if (response.getData() != null) {

                        lLItemHeadings.setVisibility(View.VISIBLE);

                        ArrayList<TicketSearchItem> ticketSearchItemArrayList = new ArrayList<TicketSearchItem>();
                        String name = response.getData().getName();
                        String pnr = response.getData().getPnr();
                        String status = response.getData().getStatus();

                        TicketSearchItem ticketSearchItem = new TicketSearchItem();

                        ticketSearchItem.setName(name);
                        ticketSearchItem.setPnr(pnr);
                        ticketSearchItem.setStatus(status);

                        ticketSearchItemArrayList.add(ticketSearchItem);


                        ticketSearchAdapter = new TicketSearchAdapter(ticketSearchItemArrayList, getActivity());
                       
                        ticketSearchListView.setAdapter(ticketSearchAdapter);

                    } else {

                        lLItemHeadings.setVisibility(View.GONE);

                        String message = "";
                        int i = 0;
                        for (String m : response.getError().getMessages()) {
                            if (i > 0) {
                                message += "\n";
                            }
                            message += m;
                        }

                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    try {
                        Log.e(TAG, Integer.toString(error.networkResponse.statusCode));
                        Log.e(TAG, error.networkResponse.toString());
                    } catch (Exception e) {
                        if (timeoutCounter != 2) {
                            timeoutCounter++;
                            appController.addToRequestQueue(tikcetSearchObjectRequest);
                            return;
                        } else {
                            timeoutCounter = 0;
                        }
                    }
                    progressBarDialog.cancel();
                    String message = "";
                    try {
                        Gson gson = new GsonBuilder().create();
                        TicketSearchMessage response = gson.fromJson(new String(error.networkResponse.data, "UTF-8"), TicketSearchMessage.class);
                        int i = 0;
                        for (String m : response.getError().getMessages()) {
                            if (i > 0) {
                                message += "\n";
                            }
                            message += m;
                        }
                    } catch (Exception e) {
                        message = "Something went wrong.Please Check your Internet Connection";
                        Log.e(TAG, e.getMessage());
                    }

                }
            }, TicketSearchMessage.class);
            progressBarDialog.show();
            appController.addToRequestQueue(tikcetSearchObjectRequest);
        }else {
            Toast.makeText(getActivity(),"Please enter a valid PNR",Toast.LENGTH_LONG).show();
        }

    }

    private void updateUIComponent(@Nullable CalendarData calendarData) {
        Log.d(TAG, "updateUIComponent(CalendarData calendarData)");

        if (calendarData != null) {
            Log.i(TAG, calendarData.toString());
            Calendar newJourneyDate = new GregorianCalendar(calendarData.getYear(), calendarData.getMonth(), calendarData.getDay());
            int date = newJourneyDate.get(Calendar.DAY_OF_MONTH);
            String htmlDate = appManager.useDateTerminology(date);


        } else {
            Log.i(TAG, CalendarData.class.getSimpleName() + " is null ");
        }
    }




    private void call() {
        // Check the SDK version and whether the permission is already granted or not.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && getActivity().checkSelfPermission(Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.CALL_PHONE}, PERMISSIONS_REQUEST_PHONE_CALL);
        } else {
            //Open call function
            String number = "16374";
            Intent intent = new Intent(Intent.ACTION_CALL);
            intent.setData(Uri.parse("tel:" + number));
            startActivity(intent);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        if (requestCode == PERMISSIONS_REQUEST_PHONE_CALL) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission is granted
                call();
            } else {
                Toast.makeText(getActivity(), "Sorry!!! Permission Denied", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        Log.d(TAG, "onClick(DialogInterface dialog, int which)");
        if (dialog.equals(noInternetAlertDialog)) {
            switch (which) {
                case Dialog.BUTTON_POSITIVE:
                    Log.d(TAG, "noInternetAlertDialog Positive Button Pressed");
                    if (appManager.isNetworkAvailable()) {
                        noInternetAlertDialog.cancel();
                        noInternetAlertDialog.dismiss();
                        searchTicket();
                    } else {
                        noInternetAlertDialog.cancel();
                        noInternetAlertDialog = getNoInternetAlertDialogBuilder().create();
                        noInternetAlertDialog.show();
                    }
                    break;
                case Dialog.BUTTON_NEUTRAL:
                    Log.d(TAG, "noInternetAlertDialog Neutral Button Pressed");
                    noInternetAlertDialog.cancel();
                    startActivityForResult(new Intent(Settings.ACTION_SETTINGS), 0);
                    break;
                case Dialog.BUTTON_NEGATIVE:
                    Log.d(TAG, "noInternetAlertDialog Negative Button Pressed");
                    getActivity().finish();
                    break;
            }
        } else if (dialog.equals(errorAlertDialog)) {
            switch (which) {
                case Dialog.BUTTON_POSITIVE:
                    Log.d(TAG, "errorAlertDialog Positive Button Pressed");
                    searchTicket();
                    break;
                case Dialog.BUTTON_NEUTRAL:
                    Log.d(TAG, "errorAlertDialog Neutral Button Pressed");
                    break;
                case Dialog.BUTTON_NEGATIVE:
                    Log.d(TAG, "errorAlertDialog Negative Button Pressed");
                    errorAlertDialog.cancel();
                    errorAlertDialog.dismiss();
                    getActivity().finish();
                    break;
            }
        }
    }

    public AlertDialog.Builder getNoInternetAlertDialogBuilder() {
        AlertDialog.Builder alertDialogBuilder;
        alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setNeutralButton(R.string.action_settings, this);
        alertDialogBuilder.setPositiveButton(R.string.try_again, this);
        alertDialogBuilder.setNegativeButton(R.string.close_app, this);
        alertDialogBuilder.setTitle(R.string.error);
        alertDialogBuilder.setMessage(R.string.no_internet_connection);
        return alertDialogBuilder;
    }

    private void createAlertDialog() {
        Log.d(TAG, "createAlertDialog()");
        noInternetAlertDialog = getNoInternetAlertDialogBuilder().create();
        errorAlertDialog = getErrorAlertDialog().create();
    }

    private AlertDialog.Builder getErrorAlertDialog() {
        AlertDialog.Builder alertDialogBuilder;
        alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setPositiveButton(R.string.try_again, this);
        alertDialogBuilder.setNegativeButton(R.string.close_app, this);
        alertDialogBuilder.setTitle(R.string.error);
        alertDialogBuilder.setMessage(R.string.server_connection_fail);
        return alertDialogBuilder;
    }
}
