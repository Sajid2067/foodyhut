package com.android.food.foodyhut.fragment.item;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by sajid on 11/4/2015.
 */
public class SeatLayoutData implements Parcelable {


    public static final Creator<SeatLayoutData> CREATOR = new Creator<SeatLayoutData>() {
        @Override
        public SeatLayoutData createFromParcel(Parcel in) {
            return new SeatLayoutData(in);
        }

        @Override
        public SeatLayoutData[] newArray(int size) {
            return new SeatLayoutData[size];
        }
    };
    private String tripId;
    private String tripRouteId;
    private BookTicketData bookTicketData;
    private String journeyTime;
    private int availableSeats;
    private String operatorName;
    private String busType;
    private int maxSeat;
    private int minSeat;
    private int companyId;
    private float businessClassFare;
    private float economyClassFare;
    private float specialClassFare;
    private List<Parcelable> boardingPointDataList;
    private String seatLayoutAsJson;
    private List<Parcelable> selectedSeat = new ArrayList<>();
    private BoardingPointData selectedBoardingPoint;

    public SeatLayoutData() {
    }

    protected SeatLayoutData(Parcel in) {
        setTripId(in.readString());
        setTripRouteId(in.readString());
        setBookTicketData((BookTicketData) in.readParcelable(BookTicketData.class.getClassLoader()));
        setJourneyTime(in.readString());
        setAvailableSeats(in.readInt());
        setOperatorName(in.readString());
        setBusType(in.readString());
        setMaxSeat(in.readInt());
        setMinSeat(in.readInt());
        setCompanyId(in.readInt());
        setBusinessClassFare(in.readFloat());
        setEconomyClassFare(in.readFloat());
        setSpecialClassFare(in.readFloat());
        setBoardingPointDataList(Arrays.asList(in.readParcelableArray(BoardingPointData.class.getClassLoader())));
        setSeatLayoutAsJson(in.readString());
        setSelectedSeat(Arrays.asList(in.readParcelableArray(SeatNumberData.class.getClassLoader())));
        setSelectedBoardingPoint((BoardingPointData) in.readParcelable(BoardingPointData.class.getClassLoader()));

    }

    public SeatLayoutData(String tripId, String tripRouteId, BookTicketData bookTicketData, String journeyTime, int availableSeats, String operatorName, String busType, int maxSeat, int minSeat, float businessClassFare, float economyClassFare, float specialClassFare, List<Parcelable> boardingPointDataList, String seatLayoutAsJson, List<Parcelable> selectedSeat, BoardingPointData selectedBoardingPoint) {
        this.tripId = tripId;
        this.tripRouteId = tripRouteId;
        this.bookTicketData = bookTicketData;
        this.journeyTime = journeyTime;
        this.availableSeats = availableSeats;
        this.operatorName = operatorName;
        this.busType = busType;
        this.maxSeat = maxSeat;
        this.minSeat = minSeat;
        this.businessClassFare = businessClassFare;
        this.economyClassFare = economyClassFare;
        this.specialClassFare = specialClassFare;
        this.boardingPointDataList = boardingPointDataList;
        this.seatLayoutAsJson = seatLayoutAsJson;
        this.selectedSeat = selectedSeat;
        this.selectedBoardingPoint = selectedBoardingPoint;
    }

    public String getTripRouteId() {
        return tripRouteId;
    }

    public void setTripRouteId(String tripRouteId) {
        this.tripRouteId = tripRouteId;
    }

    public String getTripId() {
        return tripId;
    }

    public void setTripId(String tripId) {
        this.tripId = tripId;
    }

    public BookTicketData getBookTicketData() {
        return bookTicketData;
    }

    public void setBookTicketData(BookTicketData bookTicketData) {
        this.bookTicketData = bookTicketData;
    }

    public String getJourneyTime() {
        return journeyTime;
    }

    public void setJourneyTime(String journeyTime) {
        this.journeyTime = journeyTime;
    }

    public int getAvailableSeats() {
        return availableSeats;
    }

    public void setAvailableSeats(int availableSeats) {
        this.availableSeats = availableSeats;
    }

    public String getOperatorName() {
        return operatorName;
    }

    public void setOperatorName(String operatorName) {
        this.operatorName = operatorName;
    }

    public String getBusType() {
        return busType;
    }

    public void setBusType(String busType) {
        this.busType = busType;
    }

    public int getMaxSeat() {
        return maxSeat;
    }

    public void setMaxSeat(int maxSeat) {
        this.maxSeat = maxSeat;
    }

    public int getMinSeat() {
        return minSeat;
    }

    public void setMinSeat(int minSeat) {
        this.minSeat = minSeat;
    }

    public int getCompanyId() {
        return companyId;
    }

    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }

    public List<Parcelable> getBoardingPointDataList() {
        return boardingPointDataList;
    }

    public void setBoardingPointDataList(List<Parcelable> boardingPointDataList) {
        this.boardingPointDataList = boardingPointDataList;
    }

    public float getBusinessClassFare() {
        return businessClassFare;
    }

    public void setBusinessClassFare(float businessClassFare) {
        this.businessClassFare = businessClassFare;
    }

    public float getEconomyClassFare() {
        return economyClassFare;
    }

    public void setEconomyClassFare(float economyClassFare) {
        this.economyClassFare = economyClassFare;
    }

    public float getSpecialClassFare() {
        return specialClassFare;
    }

    public void setSpecialClassFare(float specialClassFare) {
        this.specialClassFare = specialClassFare;
    }

    public String getSeatLayoutAsJson() {
        return seatLayoutAsJson;
    }

    public void setSeatLayoutAsJson(String seatLayoutAsJson) {
        this.seatLayoutAsJson = seatLayoutAsJson;
    }

    public List<Parcelable> getSelectedSeat() {
        return selectedSeat;
    }

    public void setSelectedSeat(List<Parcelable> selectedSeat) {
        this.selectedSeat = selectedSeat;
    }

    public BoardingPointData getSelectedBoardingPoint() {
        return selectedBoardingPoint;
    }

    public void setSelectedBoardingPoint(BoardingPointData selectedBoardingPoint) {
        this.selectedBoardingPoint = selectedBoardingPoint;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SeatLayoutData)) return false;

        SeatLayoutData that = (SeatLayoutData) o;

        if (getAvailableSeats() != that.getAvailableSeats()) return false;
        if (getMaxSeat() != that.getMaxSeat()) return false;
        if (getMinSeat() != that.getMinSeat()) return false;
        if (Float.compare(that.getBusinessClassFare(), getBusinessClassFare()) != 0) return false;
        if (Float.compare(that.getEconomyClassFare(), getEconomyClassFare()) != 0) return false;
        if (Float.compare(that.getSpecialClassFare(), getSpecialClassFare()) != 0) return false;
        if (!getTripId().equals(that.getTripId())) return false;
        if (!getTripRouteId().equals(that.getTripRouteId())) return false;
        if (!getBookTicketData().equals(that.getBookTicketData())) return false;
        if (!getJourneyTime().equals(that.getJourneyTime())) return false;
        if (getCompanyId()!=that.getCompanyId()) return false;
        if (!getOperatorName().equals(that.getOperatorName())) return false;
        if (!getBusType().equals(that.getBusType())) return false;
        if (!getBoardingPointDataList().equals(that.getBoardingPointDataList())) return false;
        if (!getSeatLayoutAsJson().equals(that.getSeatLayoutAsJson())) return false;
        if (!getSelectedSeat().equals(that.getSelectedSeat())) return false;
        return getSelectedBoardingPoint().equals(that.getSelectedBoardingPoint());

    }

    @Override
    public int hashCode() {
        int result = getTripId().hashCode();
        result = 31 * result + getTripRouteId().hashCode();
        result = 31 * result + getBookTicketData().hashCode();
        result = 31 * result + getJourneyTime().hashCode();
        result = 31 * result + getAvailableSeats();
        result = 31 * result + getOperatorName().hashCode();
        result = 31 * result + getCompanyId();
        result = 31 * result + getBusType().hashCode();
        result = 31 * result + getMaxSeat();
        result = 31 * result + getMinSeat();
        result = 31 * result + (getBusinessClassFare() != +0.0f ? Float.floatToIntBits(getBusinessClassFare()) : 0);
        result = 31 * result + (getEconomyClassFare() != +0.0f ? Float.floatToIntBits(getEconomyClassFare()) : 0);
        result = 31 * result + (getSpecialClassFare() != +0.0f ? Float.floatToIntBits(getSpecialClassFare()) : 0);
        result = 31 * result + getBoardingPointDataList().hashCode();
        result = 31 * result + getSeatLayoutAsJson().hashCode();
        result = 31 * result + getSelectedSeat().hashCode();
        result = 31 * result + getSelectedBoardingPoint().hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "SeatLayoutData{" +
                "tripId='" + tripId + '\'' +
                ", tripRouteId='" + tripRouteId + '\'' +
                ", bookTicketData=" + bookTicketData +
                ", journeyTime='" + journeyTime + '\'' +
                ", availableSeats=" + availableSeats +
                ", company_id='" + companyId + '\'' +
                ", operatorName='" + operatorName + '\'' +
                ", busType='" + busType + '\'' +
                ", maxSeat=" + maxSeat +
                ", minSeat=" + minSeat +
                ", businessClassFare=" + businessClassFare +
                ", economyClassFare=" + economyClassFare +
                ", specialClassFare=" + specialClassFare +
                ", boardingPointDataList=" + boardingPointDataList +
                ", seatLayoutAsJson='" + seatLayoutAsJson + '\'' +
                ", selectedSeat=" + selectedSeat +
                ", selectedBoardingPoint=" + selectedBoardingPoint +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(getTripId());
        dest.writeString(getTripRouteId());
        dest.writeParcelable(getBookTicketData(), flags);
        dest.writeString(getJourneyTime());
        dest.writeInt(getAvailableSeats());
        dest.writeString(getOperatorName());
        dest.writeInt(getCompanyId());
        dest.writeString(getBusType());
        dest.writeInt(getMaxSeat());
        dest.writeInt(getMinSeat());
        dest.writeFloat(getBusinessClassFare());
        dest.writeFloat(getEconomyClassFare());
        dest.writeFloat(getSpecialClassFare());
        dest.writeParcelableArray(boardingPointDataList.toArray(new Parcelable[boardingPointDataList.size()]), flags);
        dest.writeString(getSeatLayoutAsJson());
        dest.writeParcelableArray(selectedSeat.toArray(new Parcelable[selectedSeat.size()]), flags);
        dest.writeList(getSelectedSeat());
        dest.writeParcelable(getSelectedBoardingPoint(), flags);
    }


}
