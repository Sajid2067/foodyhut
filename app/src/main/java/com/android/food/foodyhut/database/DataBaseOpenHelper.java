package com.android.food.foodyhut.database;

import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by tuman on 4/5/2015.
 */
public class DataBaseOpenHelper extends SQLiteOpenHelper {

    public static final String DB_NAME = "Shohoz.db";
    public static final int DB_VERSION = 1;
    public static final String PAYMENT_METHOD_NAME_COLUMN = "PAYMENT_METHOD_NAME_COLUMN";
    public static final String PAYMENT_METHOD_HOURS_COLUMN = "PAYMENT_METHOD_HOURS_COLUMN";
    public static final String PAYMENT_METHOD_IS_ACTIVE_COLUMN = "PAYMENT_METHOD_IS_ACTIVE_COLUMN";
    public static final String CIB_NAME_COLUMN = "CIB_NAME_COLUMN";
    public static final String CIB_SHORT_CODE_COLUMN = "CIB_SHORT_CODE_COLUMN";
    public static final String CIB_PERCENTAGE_COLUMN = "CIB_PERCENTAGE_COLUMN";
    public static final String CIB_TYPE_COLUMN = "CIB_TYPE_COLUMN";
    public static final String CITY_ID_COLUMN = "city_id";
    public static final String CITY_NAME_COLUMN = "city_name";
    public static final String CITY_SEQUENCE_COLUMN = "city_sequence";
    public static final String IS_TOP_CITY_COLUMN = "is_top_city";

    public static final String AREA_ID_COLUMN = "area_id";
    public static final String AREA_NAME_COLUMN = "area_name";
    public static final String COD_FEES_COLUMN = "cod_fees";
    public static final String CITY_TABLE = "city";
    public static final String PAYMENT_METHOD_TABLE = "payment_method";
    public static final String CIB_TABLE = "cards_internet_banking";
    public static final String COD_COVERAGE_TABLE = "cod_coverage";
    private static final String CREATE = "CREATE";
    private static final String COLUMN = "COLUMN";
    private static final String TABLE = "TABLE";
    private static final String PRIMARY_KEY = "PRIMARY KEY";
    private static final String FOREIGN_KEY = "FOREIGN KEY";
    private static final String REFERENCES = "REFERENCES";
    private static final String DROP = "DROP";
    private static final String IF_NOT_EXISTS = "IF NOT EXISTS";
    private static final String BOOLEAN = "BOOLEAN";
    private static final String INTEGER = "INTEGER";
    private static final String VARCHAR = "VARCHAR";
    private static final String FLOAT = "FLOAT";
    private static final String REAL = "REAL";

    private static final String CITY_CREATE = CREATE + " " + TABLE + " "
            + IF_NOT_EXISTS + " " + CITY_TABLE + "(" + CITY_ID_COLUMN +
            " " + VARCHAR + "," + " " + CITY_NAME_COLUMN + " " + VARCHAR + "," + " " +
            CITY_SEQUENCE_COLUMN + " " + VARCHAR + "," + " " +
            IS_TOP_CITY_COLUMN + " " + VARCHAR + "," + " "
            + PRIMARY_KEY + "(" + CITY_ID_COLUMN + ")" + ");";

    private static final String COD_COVERAGE_CREATE = CREATE + " " + TABLE + " "
            + IF_NOT_EXISTS + " " + COD_COVERAGE_TABLE + "(" + CITY_ID_COLUMN +
            " " + VARCHAR + "," + " " + AREA_NAME_COLUMN + " " + VARCHAR + "," + " " +
            AREA_ID_COLUMN + " " + INTEGER + "," + " " +
            COD_FEES_COLUMN + " " + REAL + "," + " "
            + PRIMARY_KEY + "(" + AREA_ID_COLUMN + ")" + ");";
    private static final String CITY_DROP = DROP + " " + TABLE + " "
            + CITY_TABLE;
    private static final String COD_COVERAGE_DROP = DROP + " " + TABLE + " "
            + COD_COVERAGE_TABLE;

    private static final String PAYMENT_METHOD_CREATE = CREATE + " " + TABLE + " "
            + IF_NOT_EXISTS + " " + PAYMENT_METHOD_TABLE + "(" + PAYMENT_METHOD_NAME_COLUMN + " " + VARCHAR + "," + " " +
            PAYMENT_METHOD_HOURS_COLUMN + " " + INTEGER + "," + " " +
            PAYMENT_METHOD_IS_ACTIVE_COLUMN + " " + BOOLEAN + "," + " "
            + PRIMARY_KEY + "(" + PAYMENT_METHOD_NAME_COLUMN + ")" + ");";
    private static final String PAYMENT_METHOD_DROP = DROP + " " + TABLE + " "
            + PAYMENT_METHOD_TABLE;

    private static final String CIB_CREATE = CREATE + " " + TABLE + " "
            + IF_NOT_EXISTS + " " + CIB_TABLE + "(" + CIB_SHORT_CODE_COLUMN + " " + VARCHAR + "," + " " +
            CIB_NAME_COLUMN + " " + VARCHAR + "," + " " +
            CIB_PERCENTAGE_COLUMN + " " + FLOAT + "," + " " +
            CIB_TYPE_COLUMN + " " + VARCHAR + "," + " "
            + PRIMARY_KEY + "(" + CIB_SHORT_CODE_COLUMN + ")" + ");";
    private static final String CIB_DROP = DROP + " " + TABLE + " "
            + CIB_TABLE;

    private Context context;

    public DataBaseOpenHelper(Context context) {
        this(context, DB_NAME, null, DB_VERSION);
        this.context = context;
    }

    public DataBaseOpenHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        this(context, name, factory, version, null);
    }

    public DataBaseOpenHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version, DatabaseErrorHandler errorHandler) {
        super(context, name, factory, version, errorHandler);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        try {
            db.execSQL(CITY_CREATE);
            Log.i("Database Create", "Created city table");
        } catch (Exception e) {
            Log.e("Database Create", e.getMessage());
        }

        try {
            db.execSQL(PAYMENT_METHOD_CREATE);
            Log.i("Database Create", "PAYMENT_METHOD_CREATE");
        } catch (Exception e) {
            Log.e("Database Create", e.getMessage());
        }

        try {
            db.execSQL(CIB_CREATE);
            Log.i("Database Create", "CIB_CREATE");
        } catch (Exception e) {
            Log.e("Database Create", e.getMessage());
        }

        try {
            db.execSQL(COD_COVERAGE_CREATE);
            Log.i("Database Create", "Created cod coverage table");
        } catch (Exception e) {
            Log.e("Database Create", e.getMessage());
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        try {
            db.execSQL(CITY_DROP);
            Log.i("Database Create", "Dropped city table");
        } catch (Exception e) {
            Log.e("Database Drop", e.getMessage());
        }
        try {
            db.execSQL(PAYMENT_METHOD_DROP);
            Log.i("Database Create", "PAYMENT_METHOD_DROP");
        } catch (Exception e) {
            Log.e("Database Drop", e.getMessage());
        }
        try {
            db.execSQL(CIB_DROP);
            Log.i("Database Create", "CIB_DROP");
        } catch (Exception e) {
            Log.e("Database Drop", e.getMessage());
        }
        try {
            db.execSQL(COD_COVERAGE_DROP);
            Log.i("Database Create", "Dropped cod coverage table");
        } catch (Exception e) {
            Log.e("Database Drop", e.getMessage());
        }
        onCreate(db);
    }

    public void dropDatabase(SQLiteDatabase db) {
        try {
            db.execSQL(CITY_DROP);
            Log.i("Database Create", "Dropped city table");
        } catch (Exception e) {
            Log.e("Database Drop", e.getMessage());
        }
        try {
            db.execSQL(PAYMENT_METHOD_DROP);
            Log.i("Database Create", "PAYMENT_METHOD_DROP");
        } catch (Exception e) {
            Log.e("Database Drop", e.getMessage());
        }
        try {
            db.execSQL(CIB_DROP);
            Log.i("Database Create", "CIB_DROP");
        } catch (Exception e) {
            Log.e("Database Drop", e.getMessage());
        }
        try {
            db.execSQL(COD_COVERAGE_DROP);
            Log.i("Database Create", "Dropped cod coverage table");
        } catch (Exception e) {
            Log.e("Database Drop", e.getMessage());
        }
    }

    public void createDatabase(SQLiteDatabase db) {
        try {
            db.execSQL(CITY_CREATE);
            Log.i("Database Create", "Created city table");
        } catch (Exception e) {
            Log.e("Database Create", e.getMessage());
        }
        try {
            db.execSQL(PAYMENT_METHOD_CREATE);
            Log.i("Database Create", "PAYMENT_METHOD_CREATE");
        } catch (Exception e) {
            Log.e("Database Create", e.getMessage());
        }
        try {
            db.execSQL(CIB_CREATE);
            Log.i("Database Create", "CIB_CREATE");
        } catch (Exception e) {
            Log.e("Database Create", e.getMessage());
        }
        try {
            db.execSQL(COD_COVERAGE_CREATE);
            Log.i("Database Create", "Created cod coverage table");
        } catch (Exception e) {
            Log.e("Database Create", e.getMessage());
        }
    }
}
