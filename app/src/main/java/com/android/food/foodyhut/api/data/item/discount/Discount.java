package com.android.food.foodyhut.api.data.item.discount;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Droid on 13-Jul-16.
 */
public class Discount {


    /**
     * shohoz_discount : 0
     * app_discount : 0
     * nth_order : 0
     */

    @SerializedName("data")
    private Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public static class Data {
        @SerializedName("shohoz_discount")
        private int shohozDiscount;
        @SerializedName("app_discount")
        private int appDiscount;
        @SerializedName("nth_order")
        private int nthOrder;

        public int getShohozDiscount() {
            return shohozDiscount;
        }

        public void setShohozDiscount(int shohozDiscount) {
            this.shohozDiscount = shohozDiscount;
        }

        public int getAppDiscount() {
            return appDiscount;
        }

        public void setAppDiscount(int appDiscount) {
            this.appDiscount = appDiscount;
        }

        public int getNthOrder() {
            return nthOrder;
        }

        public void setNthOrder(int nthOrder) {
            this.nthOrder = nthOrder;
        }
    }

    @Override
    public String toString() {
        return "Discount{" +
                "\n shohoz_discount=" + data.getShohozDiscount() +
                ",\n app_discount=" + data.getAppDiscount() +
                ",\n nth_order=" + data.getNthOrder() +"\n"+
                '}';
    }
}
