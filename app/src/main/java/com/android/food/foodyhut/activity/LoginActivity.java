package com.android.food.foodyhut.activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.food.foodyhut.toolbox.PostUrlBuilder;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Password;
import com.android.food.foodyhut.R;
import com.android.food.foodyhut.api.data.item.login.LoginVerification;
import com.android.food.foodyhut.api.data.item.user.UserStatus;
import com.android.food.foodyhut.application.AppController;
import com.android.food.foodyhut.toolbox.GetUrlBuilder;
import com.android.food.foodyhut.toolbox.ObjectRequest;
import com.android.food.foodyhut.util.API;
import com.android.food.foodyhut.util.AppManager;
import com.android.food.foodyhut.view.widget.ProgressBar;
import com.nipunbirla.boxloader.BoxLoaderView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.support.design.widget.Snackbar.make;
import static android.widget.Toast.LENGTH_SHORT;
import static android.widget.Toast.makeText;


/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends BaseActivity implements DialogInterface.OnClickListener,OnClickListener,Validator.ValidationListener,GoogleApiClient.OnConnectionFailedListener {

    private static final String TAG = LoginActivity.class.getSimpleName();
    //validation check
    @Password(min = 6, message = "Your password must contain minimum 6 characters and Maximum 20 characters")
    private EditText passwordEditText;

    private static final String VALID_MOBILE_REG_EX = "^01(1|5|6|7|8|9)\\d{8}$";
    // UI references.
    private EditText emailEditText;


    private LinearLayout shohozSignInButton,facebookSignInButton,googleSignInButton;
    private Button signUpActivityTransferButton,buttonSkip;
    private BoxLoaderView loginProgressBar;
    private View shohozLoginForm;
    Tracker mTracker;
    private AppManager appManager;
    private String email,password;
    ObjectRequest<LoginVerification> loginVerificationObjectRequest;
    protected TextView progressBarMessageTextView;
    private AlertDialog noInternetAlertDialog;
    private AlertDialog errorAlertDialog;
    protected AlertDialog progressBarDialog;
    private AppController appController = AppController.getInstance();
    int timeoutCounter = 0;
    private Validator loginValidator;
    private CallbackManager callbackManager;
    LoginButton btnFbLogin;
    public static String token;
 //   TextView loginStatus;
    //Signin button
 //   private SignInButton signInButton;

    //Signing Options
    private GoogleSignInOptions gso;

    //google api client
    private GoogleApiClient mGoogleApiClient;

    //Signin constant to check the activity result
    private int RC_SIGN_IN = 100;
 //   private Facebook facebook;
  //  private AsyncFacebookRunner mAsyncRunner;
    String FILENAME = "AndroidSSO_data";
    private SharedPreferences mPrefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate(Bundle savedInstanceState)");
        super.onCreate(savedInstanceState);
      //  FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_login);
       // printKeyHash();
        initializeUIViews();
        initializeButtonListeners();
        loginValidator = new Validator(this);
        loginValidator.setValidationListener(this);
        // Obtain the shared Tracker instance.
        AppController application = (AppController) getApplication();
        appManager = new AppManager(this);
        mTracker = application.getDefaultTracker();
    }

//    private void printKeyHash() {
//        // Add code to print out the key hash
//        try {
//            PackageInfo info = getPackageManager().getPackageInfo("com.android.food.foodyhut", PackageManager.GET_SIGNATURES);
//            for (Signature signature : info.signatures) {
//                MessageDigest md = MessageDigest.getInstance("SHA");
//                md.update(signature.toByteArray());
//                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
//            }
//        } catch (PackageManager.NameNotFoundException e) {
//            Log.e("KeyHash:", e.toString());
//        } catch (NoSuchAlgorithmException e) {
//            Log.e("KeyHash:", e.toString());
//        }
//    }



    private void initializeButtonListeners() {
        Log.d(TAG, "initializeButtonListeners()");
        shohozSignInButton.setOnClickListener(this);
        facebookSignInButton.setOnClickListener(this);
        googleSignInButton.setOnClickListener(this);
        signUpActivityTransferButton.setOnClickListener(this);
     //   backButton.setOnClickListener(this);
        btnFbLogin.setOnClickListener(this);
        //Setting onclick listener to signing button
       // signInButton.setOnClickListener(this);
        buttonSkip.setOnClickListener(this);
    }

    private void initializeUIViews() {

        Log.d(TAG, "initializeUIViews()");
        emailEditText = (EditText) findViewById(R.id.email_number_edit_text);
        passwordEditText = (EditText) findViewById(R.id.password_edit_text);
        shohozSignInButton = (LinearLayout) findViewById(R.id.shohoz_sign_in_button);
        facebookSignInButton = (LinearLayout) findViewById(R.id.facebook_sign_in_button_LL);
        googleSignInButton = (LinearLayout) findViewById(R.id.google_sign_in_button_LL);
       // backButton=(ImageButton) findViewById(R.id.back_button);
        signUpActivityTransferButton = (Button) findViewById(R.id.sign_up_activity_transfer_button);
        btnFbLogin = (LoginButton) findViewById(R.id.login_button);
        loginProgressBar = (BoxLoaderView) findViewById(R.id.progress);
        shohozLoginForm = findViewById(R.id.shohoz_login_form);
        createAlertDialog();
      //  signUpActivityTransferButton = (Button) findViewById(R.id.textViewSignUp);
        buttonSkip=(Button) findViewById(R.id.buttonskip);
      //  loginStatus =(TextView) findViewById(R.id.loginstatus);
        createProgressDialogView();


        callbackManager= CallbackManager.Factory.create();
        btnFbLogin.setBackgroundResource(R.mipmap.facebook_login);

        btnFbLogin.setReadPermissions(Arrays.asList("public_profile","email"));
        btnFbLogin.setText("Login");
        btnFbLogin.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
        btnFbLogin.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
             //   loginStatus.setText(loginResult.getAccessToken().getUserId()+" "+loginResult.getAccessToken().getToken());
                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {

                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {
                                Log.v("Main", response.toString());
                              //  setProfileToView(object);
                                try {
                                    String email =object.getString("email");
                                    String name =object.getString("name");
                                    String id =object.getString("id");


                                    userLoginUsingFbGoogle(email!=null?email:id,name,"Facebook");

                                    // Toast.makeText(LoginActivity.this,email+name+id,Toast.LENGTH_LONG).show();
                             //       finishActivity();
              //  profilePictureView.setPresetSize(ProfilePictureView.NORMAL);
             //   profilePictureView.setProfileId(jsonObject.getString("id"));
             //   infoLayout.setVisibility(View.VISIBLE);
            } catch (JSONException e) {
                e.printStackTrace();

            }
                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,first_name,last_name,email");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                //loginStatus.setText("onCancel ");
            }

            @Override
            public void onError(FacebookException error) {
              //  loginStatus.setText(error.getMessage().toString());
            }
        });

//        private void setProfileToView(JSONObject jsonObject) {
//            try {
//                email.setText(jsonObject.getString("email"));
//                gender.setText(jsonObject.getString("gender"));
//                facebookName.setText(jsonObject.getString("name"));
//
//                profilePictureView.setPresetSize(ProfilePictureView.NORMAL);
//                profilePictureView.setProfileId(jsonObject.getString("id"));
//                infoLayout.setVisibility(View.VISIBLE);
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//        }

        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        //Initializing signinbutton
      //  signInButton = (SignInButton) findViewById(R.id.sign_in_button);
     //   signInButton.setSize(SignInButton.SIZE_WIDE);
      //  signInButton.setScopes(gso.getScopeArray());

        //Initializing google api client
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();



    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        //If signin
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            //Calling a new function to handle signin
            handleSignInResult(result);
        }else {

            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    //After the signing we are calling this function
    private void handleSignInResult(GoogleSignInResult result) {
        //If the login succeed
        if (result.isSuccess()) {
            //Getting google account
            GoogleSignInAccount acct = result.getSignInAccount();
            userLoginUsingFbGoogle(acct.getEmail(),acct.getDisplayName(),"Google+");
         //   finishActivity();
            //Displaying name and email
           // textViewName.setText(acct.getDisplayName());
          //  textViewEmail.setText(acct.getEmail());
          //  Toast.makeText(this, "Login Name: " +acct.getDisplayName(), Toast.LENGTH_LONG).show();



        } else {
            //If login fails
            Toast.makeText(this, "Login Failed", Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        Log.d(TAG, "showProgress(final boolean show)");
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_longAnimTime);

            shohozLoginForm.setVisibility(show ? View.GONE : View.VISIBLE);
            shohozLoginForm.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    shohozLoginForm.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            loginProgressBar.setVisibility(show ? View.VISIBLE : View.GONE);
            loginProgressBar.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    loginProgressBar.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            loginProgressBar.setVisibility(show ? View.VISIBLE : View.GONE);
            shohozLoginForm.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    @Override
    public void onClick(View view) {
        Log.d(TAG, "onClick(View view)");
        switch (view.getId()) {
//            case R.id.sign_in_button:
//                signIn();
//                break;
            case R.id.shohoz_sign_in_button:
                Log.d(TAG, "Log In Button Pressed.");
                if (!checkEmpty(emailEditText)) {
                    Toast.makeText(LoginActivity.this, "Enter your email.", Toast.LENGTH_SHORT).show();
                } else if (!checkEmpty(passwordEditText)) {
                    Toast.makeText(LoginActivity.this, "Enter your password.", Toast.LENGTH_SHORT).show();

                } else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(emailEditText.getText().toString()).matches()) {
                    makeText(LoginActivity.this, "Enter a valid Email.", LENGTH_SHORT).show();
                }else if (checkEmpty(emailEditText) && checkEmpty(passwordEditText)) {
                    loginValidator.validate();
                }
                break;
            case R.id.facebook_sign_in_button_LL:
                btnFbLogin.performClick();
                break;
            case R.id.google_sign_in_button_LL:
                Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
                startActivityForResult(signInIntent, RC_SIGN_IN);
                break;
            case R.id.sign_up_activity_transfer_button:
                Log.d(TAG, "Sign Up Activity Transfer Button Pressed.");
                startActivity(new Intent(LoginActivity.this, SignUpActivity.class));
                finish();
                break;
//            case R.id.back_button:
//                Log.d(TAG, "back button Pressed.");
//                finishActivity();
//                break;
            case R.id.buttonskip:
                Log.d(TAG, "buttonskip Pressed.");
                if (appManager.isNetworkAvailable()) {
                    deviceRegistration();
                }

               // finishActivity();
                break;
        }
    }

    private void deviceRegistration() {
        Map<String, String> params = new HashMap<>();
        params.put(API.Parameter.DEVICE_ID, appManager.getDeviceId());
        params.put(API.Parameter.ANDROID_DEVICE_ID, appManager.getDeviceId());
        params.put(API.Parameter.ANDROID_APP_VERSION, appManager.getAppVersion());


        PostUrlBuilder postUrlBuilder = new PostUrlBuilder(API.DEVICE_REGISTRATION_API_URL, null, null);
        GetUrlBuilder getUrlBuilder = new GetUrlBuilder(API.DEVICE_REGISTRATION_API_URL, params);
        String Url =getUrlBuilder.getQueryUrl();  //only for showing URL and params
        Log.d(TAG, "URL:" + Url);
        Log.d(TAG, "PARAMS:" + params.toString());

        loginVerificationObjectRequest = new ObjectRequest<>(API.Method.DEVICE_REGISTRATION_API_METHOD, postUrlBuilder.getUrl(), params, new Response.Listener<LoginVerification>() {
            @Override
            public void onResponse(LoginVerification loginVerification) {
                progressBarDialog.cancel();

                if (loginVerification.getResponse()!= null) {
                    UserStatus userStatus=new UserStatus(LoginActivity.this);
                    userStatus.setLogin(true);
                    token=loginVerification.getResponse();
                    userStatus.setToken(loginVerification.getResponse());
                    //   userStatus.setUser_id(userVerification.getResponse().getUserId());
                    //  userStatus.setUser_full_name(response.getData().getFirst_name()+" "+response.getData().getLast_name());
                    //   userStatus.setUser_mobile(userVerification.getResponse().getUserMobil());
                    makeText(LoginActivity.this, loginVerification.getStatus().getMessage(), LENGTH_SHORT).show();
                    finishActivity();

                } else {

                    String message =  loginVerification.getStatus().getMessage();
//                    for (String m : loginVerification.getStatus().getMessage()) {
//                        message += (m + "\n");
//                    }
                    make(shohozLoginForm, message, Snackbar.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                try {
                    Log.e(TAG, Integer.toString(error.networkResponse.statusCode));
                } catch (Exception e) {
                    if (timeoutCounter != 2) {
                        timeoutCounter++;
                        appController.addToRequestQueue(loginVerificationObjectRequest);
                        return;
                    } else {
                        timeoutCounter = 0;
                    }
                }
                progressBarDialog.cancel();
                String message = "";
                try {

                    Gson gson = new GsonBuilder().create();
                    LoginVerification loginVerification = gson.fromJson(new String(error.networkResponse.data, "UTF-8"), LoginVerification.class);
                    message= loginVerification.getStatus().getMessage();
//                    for (String m : response.getError().getMessages()) {
//                        message += (m + "\n");
//                    }
                } catch (Exception e) {
                    message = "Something went wrong.Please Check your Internet Connection";
                }
                make(shohozLoginForm, message, Snackbar.LENGTH_SHORT).show();
            }
        },LoginVerification.class);

        progressBarDialog.show();
        appController.addToRequestQueue(loginVerificationObjectRequest);

    }


    private boolean checkEmpty(EditText editText) {
        Log.d(TAG, "checkEmpty(EditText editText)");
        return editText.getText().toString().trim().length() != 0;
    }
    @Override
    public void onValidationSucceeded() {
        Log.d(TAG, "onValidationSucceeded()");
        if (appManager.isNetworkAvailable()) {
            Log.d(TAG, "handshakeRequest done");

            email = emailEditText.getText().toString().trim();
            password = passwordEditText.getText().toString().trim();
            userLogin(email,password);

        } else {

            noInternetAlertDialog.show();

        }

    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        Log.d(TAG, "onValidationFailed(List<ValidationError> errors) totalError = " + errors.size());
        for (ValidationError validationError : errors) {
            makeText(LoginActivity.this, validationError.getCollatedErrorMessage(LoginActivity.this),
                    LENGTH_SHORT).show();
            break;
        }
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        Log.d(TAG, "onClick(DialogInterface dialog, int which)");
        if (dialog.equals(noInternetAlertDialog)) {
            switch (which) {
                case Dialog.BUTTON_POSITIVE:
                    Log.d(TAG, "noInternetAlertDialog Positive Button Pressed");
                    if (appManager.isNetworkAvailable()) {
                        noInternetAlertDialog.cancel();
                        noInternetAlertDialog.dismiss();
                        userLogin(email,password);
                    } else {
                        noInternetAlertDialog.cancel();
                        noInternetAlertDialog = getNoInternetAlertDialogBuilder().create();
                        noInternetAlertDialog.show();
                    }
                    break;
                case Dialog.BUTTON_NEUTRAL:
                    Log.d(TAG, "noInternetAlertDialog Neutral Button Pressed");
                    noInternetAlertDialog.cancel();
                    startActivityForResult(new Intent(Settings.ACTION_SETTINGS), 0);
                    break;
                case Dialog.BUTTON_NEGATIVE:
                    Log.d(TAG, "noInternetAlertDialog Negative Button Pressed");
                    finish();
                    break;
            }
        } else if (dialog.equals(errorAlertDialog)) {
            switch (which) {
                case Dialog.BUTTON_POSITIVE:
                    Log.d(TAG, "errorAlertDialog Positive Button Pressed");
                    userLogin(email,password);
                    break;
                case Dialog.BUTTON_NEUTRAL:
                    Log.d(TAG, "errorAlertDialog Neutral Button Pressed");
                    break;
                case Dialog.BUTTON_NEGATIVE:
                    Log.d(TAG, "errorAlertDialog Negative Button Pressed");
                    errorAlertDialog.cancel();
                    errorAlertDialog.dismiss();
                    finish();
                    break;
            }
        }
    }

    void finishActivity(){
        startActivity(new Intent(LoginActivity.this, MainAppActivity.class));
        finish();
    }

    private void userLogin (String email,String password){
        Map<String, String> params = new HashMap<>();
        params.put(API.Parameter.EMAIL_ADDRESS,email);
        params.put(API.Parameter.PASSWORD, password);
        params.put(API.Parameter.ANDROID_DEVICE_ID, appManager.getDeviceId());
        params.put(API.Parameter.ANDROID_APP_VERSION, appManager.getAppVersion());


        PostUrlBuilder postUrlBuilder = new PostUrlBuilder(API.USER_LOGIN_API_URL, null, null);
        GetUrlBuilder getUrlBuilder = new GetUrlBuilder(API.USER_LOGIN_API_URL, params);
        String Url =getUrlBuilder.getQueryUrl();  //only for showing URL and params
        Log.d(TAG, "URL:" + Url);
        Log.d(TAG, "PARAMS:" + params.toString());

        loginVerificationObjectRequest = new ObjectRequest<>(API.Method.USER_lOGIN_API_METHOD, postUrlBuilder.getUrl(), params, new Response.Listener<LoginVerification>() {
            @Override
            public void onResponse(LoginVerification loginVerification) {
                progressBarDialog.cancel();

                if (loginVerification.getResponse()!= null) {
                    UserStatus userStatus=new UserStatus(LoginActivity.this);
                    userStatus.setLogin(true);
                    token=loginVerification.getResponse();
                    userStatus.setToken(loginVerification.getResponse());
                    //   userStatus.setUser_id(userVerification.getResponse().getUserId());
                    //  userStatus.setUser_full_name(response.getData().getFirst_name()+" "+response.getData().getLast_name());
                    //   userStatus.setUser_mobile(userVerification.getResponse().getUserMobil());
                    makeText(LoginActivity.this, loginVerification.getStatus().getMessage(), LENGTH_SHORT).show();
                    finishActivity();

                } else {

                    String message =  loginVerification.getStatus().getMessage();
//                    for (String m : loginVerification.getStatus().getMessage()) {
//                        message += (m + "\n");
//                    }
                    make(shohozLoginForm, message, Snackbar.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                try {
                    Log.e(TAG, Integer.toString(error.networkResponse.statusCode));
                } catch (Exception e) {
                    if (timeoutCounter != 2) {
                        timeoutCounter++;
                        appController.addToRequestQueue(loginVerificationObjectRequest);
                        return;
                    } else {
                        timeoutCounter = 0;
                    }
                }
                progressBarDialog.cancel();
                String message = "";
                try {

                    Gson gson = new GsonBuilder().create();
                    LoginVerification loginVerification = gson.fromJson(new String(error.networkResponse.data, "UTF-8"), LoginVerification.class);
                     message= loginVerification.getStatus().getMessage();
//                    for (String m : response.getError().getMessages()) {
//                        message += (m + "\n");
//                    }
                } catch (Exception e) {
                    message = "Something went wrong.Please Check your Internet Connection";
                }
                make(shohozLoginForm, message, Snackbar.LENGTH_SHORT).show();
            }
        },LoginVerification.class);

        progressBarDialog.show();
        appController.addToRequestQueue(loginVerificationObjectRequest);

    }

    private void userLoginUsingFbGoogle (String email,String name,String socialType){
        Map<String, String> params = new HashMap<>();
        params.put(API.Parameter.EMAIL_ADDRESS,email);
        params.put(API.Parameter.USER_NAME, name);
        params.put(API.Parameter.SOCIAL_TYPE, socialType);
        params.put(API.Parameter.ANDROID_DEVICE_ID, appManager.getDeviceId());
        params.put(API.Parameter.ANDROID_APP_VERSION, appManager.getAppVersion());


        PostUrlBuilder postUrlBuilder = new PostUrlBuilder(API.SOCIEL_USER_REGISTRATION_API_URL, null, null);
        GetUrlBuilder getUrlBuilder = new GetUrlBuilder(API.SOCIEL_USER_REGISTRATION_API_URL, params);
        String Url =getUrlBuilder.getQueryUrl();  //only for showing URL and params
        Log.d(TAG, "URL:" + Url);
        Log.d(TAG, "PARAMS:" + params.toString());

        loginVerificationObjectRequest = new ObjectRequest<>(API.Method.SOCIEL_USER_REGISTRATION_API_METHOD, postUrlBuilder.getUrl(), params, new Response.Listener<LoginVerification>() {
            @Override
            public void onResponse(LoginVerification loginVerification) {
                progressBarDialog.cancel();

                if (loginVerification.getResponse()!= null) {
                    UserStatus userStatus=new UserStatus(LoginActivity.this);
                    userStatus.setLogin(true);
                    token=loginVerification.getResponse();
                    //   userStatus.setUser_id(userVerification.getResponse().getUserId());
                    //  userStatus.setUser_full_name(response.getData().getFirst_name()+" "+response.getData().getLast_name());
                    //   userStatus.setUser_mobile(userVerification.getResponse().getUserMobil());
                    makeText(LoginActivity.this, loginVerification.getStatus().getMessage(), LENGTH_SHORT).show();
                    finishActivity();

                } else {

                    String message =  loginVerification.getStatus().getMessage();
//                    for (String m : loginVerification.getStatus().getMessage()) {
//                        message += (m + "\n");
//                    }
                    make(shohozLoginForm, message, Snackbar.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                try {
                    Log.e(TAG, Integer.toString(error.networkResponse.statusCode));
                } catch (Exception e) {
                    if (timeoutCounter != 2) {
                        timeoutCounter++;
                        appController.addToRequestQueue(loginVerificationObjectRequest);
                        return;
                    } else {
                        timeoutCounter = 0;
                    }
                }
                progressBarDialog.cancel();
                String message = "";
                try {

                    Gson gson = new GsonBuilder().create();
                    LoginVerification loginVerification = gson.fromJson(new String(error.networkResponse.data, "UTF-8"), LoginVerification.class);
                    message= loginVerification.getStatus().getMessage();
//                    for (String m : response.getError().getMessages()) {
//                        message += (m + "\n");
//                    }
                } catch (Exception e) {
                    message = "Something went wrong.Please Check your Internet Connection";
                }
                make(shohozLoginForm, message, Snackbar.LENGTH_SHORT).show();
            }
        },LoginVerification.class);

        progressBarDialog.show();
        appController.addToRequestQueue(loginVerificationObjectRequest);

    }

    protected void createProgressDialogView() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(LoginActivity.this);
        View view = View.inflate(LoginActivity.this, R.layout.progressbar_view, null);
        progressBarMessageTextView = (TextView) view.findViewById(R.id.progress_bar_message_text_view);
        alertDialogBuilder.setView(view);
        alertDialogBuilder.setCancelable(false);
        progressBarDialog = alertDialogBuilder.create();
    }

    public AlertDialog.Builder getNoInternetAlertDialogBuilder() {
        AlertDialog.Builder alertDialogBuilder;
        alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setNeutralButton(R.string.action_settings, this);
        alertDialogBuilder.setPositiveButton(R.string.try_again, this);
        alertDialogBuilder.setNegativeButton(R.string.close_app, this);
        alertDialogBuilder.setTitle(R.string.error);
        alertDialogBuilder.setMessage(R.string.no_internet_connection);
        return alertDialogBuilder;
    }

    private void createAlertDialog() {
        Log.d(TAG, "createAlertDialog()");
        noInternetAlertDialog = getNoInternetAlertDialogBuilder().create();
        errorAlertDialog = getErrorAlertDialog().create();
    }

    private AlertDialog.Builder getErrorAlertDialog() {
        AlertDialog.Builder alertDialogBuilder;
        alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setPositiveButton(R.string.try_again, this);
        alertDialogBuilder.setNegativeButton(R.string.close_app, this);
        alertDialogBuilder.setTitle(R.string.error);
        alertDialogBuilder.setMessage(R.string.server_connection_fail);
        return alertDialogBuilder;
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Toast.makeText(this, "onConnectionFailed", Toast.LENGTH_LONG).show();
    }
}


