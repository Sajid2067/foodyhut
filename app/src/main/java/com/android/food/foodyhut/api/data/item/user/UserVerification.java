package com.android.food.foodyhut.api.data.item.user;

import com.android.food.foodyhut.api.data.item.user.Response;
import com.android.food.foodyhut.api.data.item.user.UserRegistrationStatus;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sajid on 12/14/2015.
 */
public class UserVerification {

    @SerializedName("status")
    private UserRegistrationStatus status;

    @SerializedName("response")
    private String response;

    public UserVerification() {
    }

    public UserRegistrationStatus getStatus() {
        return status;
    }

    public void setStatus(UserRegistrationStatus status) {
        this.status = status;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }
}
